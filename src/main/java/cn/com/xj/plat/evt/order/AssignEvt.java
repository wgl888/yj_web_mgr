package cn.com.xj.plat.evt.order;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AssignEvt extends BaseEvt {

    @Field(value = "订单ID",length = 11,nullable = false)
    private Long orderId;
    @Field(value = "报价记录ID",length = 11,nullable = false)
    private Long quotesId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getQuotesId() {
        return quotesId;
    }

    public void setQuotesId(Long quotesId) {
        this.quotesId = quotesId;
    }
}
