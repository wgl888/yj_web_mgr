<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>角色列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
    <%--按钮权限校验--%>
    <%--<script type="text/javascript" src="${applicationScope.staticFilepath }/js/mgrRight.js"></script>--%>
    <style type="text/css">
        .tdover {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        td {
            vertical-align: middle;
        !important;
        }

        table {
            table-layout: fixed;
        }
    </style>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <!-- 查询条件 -->
    <!-- /.page-content -->
    <%--输入框要小的 就直接加jui-mini-control 这个类名style="padding: 10px"--%>
    <div class="page-content jui-mini-control" transition>
        <div class="jui-search-icon"></div>
        <!-- /.page-form start-->
        <form method="post" id="queryForm" class="form-horizontal clearfix" order="form">
            <input type="hidden" value="${queryEvt.orderStatus }" name="orderStatus" />
            <table class="queryTable">
                <tr>
                    <td>
                        <span>订单编号：</span>
                        <input type="text" class="input-width-160"
                               id="orderNo" name="orderNo"/>
                    </td>
                    <td>
                        <span>货主姓名：</span>
                        <input type="text" class="input-width-160"
                               id="userName" name="userName"/>
                    </td>
                    <td>
                        <span>车主姓名：</span>
                        <input type="text" class="input-width-160"
                               id="freighterName" name="freighterName"/>
                    </td>
                    <td>
                        <span>保单编号：</span>
                        <input type="text" class="input-width-160"
                               id="policyNo" name="policyNo"/>
                    </td>
                </tr>
                <tr>
                    <%--<td>--%>
                        <%--<span>订单状态：</span>--%>
                        <%--<select id="orderStatus" name="orderStatus" class="input-width-160">--%>
                            <%--<option value="">全部</option>--%>
                            <%--<option value="DZP">待指派</option>--%>
                            <%--<option value="DJD">待接单</option>--%>
                            <%--<option value="SJJUDAN">司机拒单</option>--%>
                            <%--<option value="SJJIEDAN">司机接单</option>--%>
                            <%--<option value="DHZFK">待货主付款</option>--%>
                            <%--<option value="DFH">待发货</option>--%>
                            <%--<option value="DSH">待收货</option>--%>
                            <%--<option value="SH">售后</option>--%>
                            <%--<option value="YSH">已收货</option>--%>
                        <%--</select>--%>
                    <%--</td>--%>
                    <td align="center" colspan="4">
                        <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询
                        </button>
                        <button onclick="javascript:resetSearch(1);" type="button" class="btn btn-orange inline-block">
                            重置
                        </button>
                    </td>
                    <%--<td>--%>
                        <%--&lt;%&ndash;<span>车主姓名：</span>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;<input type="text" class="input-width-160"&ndash;%&gt;--%>
                               <%--&lt;%&ndash;id="freighterName" name="freighterName"/>&ndash;%&gt;--%>
                    <%--</td>--%>
                    <%--<td>--%>
                        <%--&lt;%&ndash;<span>保单编号：</span>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;<input type="text" class="input-width-160"&ndash;%&gt;--%>
                               <%--&lt;%&ndash;id="policyNo" name="policyNo"/>&ndash;%&gt;--%>
                    <%--</td>--%>
                </tr>
                <%--<tr>--%>
                    <%--<td align="center" colspan="4">--%>
                        <%----%>
                    <%--</td>--%>
                <%--</tr>--%>
            </table>
        </form>
        <!-- /.page-form end-->
    </div>

    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <%--<div class="">--%>
            <%--<button onclick="javascript:add();"--%>
            <%--type="button" class="btn btn-lightgreen inline-block">新增--%>
            <%--</button>--%>
            <%--</div>--%>
            <table class="jui-theme-table table-hover margin-top" id="myTable">
                <thead>
                <tr>
                    <th width="180px">订单编号</th>
                    <th width="100px">货主姓名</th>

                    <c:if test="${queryEvt.orderStatus=='' || queryEvt.orderStatus== null }">
                        <th width="160px">启运时间</th>
                        <th width="160px">结束时间</th>
                        <th width="160px">车辆属性</th>
                        <th width="160px">货物类型</th>
                    </c:if>

                    <c:if test="${queryEvt.orderStatus=='DJD' || queryEvt.orderStatus=='DZP' }">
                        <th width="120px">货主电话</th>
                        <th width="80px">期望费用</th>
                        <th width="160px">启运时间</th>
                        <th width="160px">结束时间</th>
                    </c:if>
                    <c:if test="${queryEvt.orderStatus=='SJJUDAN'}">
                        <th width="120px">车主姓名</th>
                        <th width="160px">拒单时间</th>
                        <th width="280px">拒单理由</th>
                    </c:if>
                    <c:if test="${queryEvt.orderStatus=='DHZFK'}">
                        <th width="120px">车主姓名</th>
                        <th width="160px">接单时间</th>
                        <th width="120px">最终费用</th>
                        <th width="120px">应付款</th>
                    </c:if>
                    <c:if test="${queryEvt.orderStatus=='DFH'}">
                        <th width="100px">发货人</th>
                        <th width="200px">发货地址</th>
                        <th width="100px">收货人</th>
                        <th width="200px">收货地址</th>
                    </c:if>
                    <c:if test="${queryEvt.orderStatus=='DSH'}">
                        <th width="160px">发货时间</th>
                        <th width="120px">收货人</th>
                        <th width="300px">收货地址</th>
                    </c:if>
                    <c:if test="${queryEvt.orderStatus=='SH'}">
                        <th width="120px">联系人</th>
                        <th width="320px">问题描述</th>
                        <th width="120px">售后状态</th>
                    </c:if>
                    <c:if test="${queryEvt.orderStatus=='YSH'}">
                        <th width="120px">车主姓名</th>
                        <th width="120px">货物类型</th>
                        <th width="120px">订单费用</th>
                        <th width="120px">保险费用</th>
                        <th width="100px">车主评价</th>
                        <th width="100px">货主评价</th>
                    </c:if>
                    <c:if test="${queryEvt.orderStatus=='YQX'}">
                        <th width="120px">车主姓名</th>
                        <th width="120px">取消时间</th>
                        <th width="120px">取消原因</th>
                    </c:if>
                    <th width="120px">订单状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">

            </div>
        </div>
    </div>
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/order/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
        setPageHeight();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    function resetSearch() {
        $('#orderNo').val('');
        $('#userName').val('');
        $('#policyNo').val('');
        $('#freighterName').val('');
//        $("#orderStatus option:first").prop("selected", 'selected');
        loadPage(1);
    }
    add = function () {
        location.href = "${pageContext.request.contextPath}/order/toAdd";
    }
    edit = function (id) {
        location.href = '${pageContext.request.contextPath}/order/toEdit?id=' + id;
    }

    assign = function(id){
        <%--layer_full('${pageContext.request.contextPath}/order/toAppoint?id=' + id);--%>

        location.href = '${pageContext.request.contextPath}/order/toAssign?id=' + id;
    }

    detail = function (id) {
        layer_full('${pageContext.request.contextPath}/order/toDetail?id=' + id);
    }
    invoice = function (orderNo,userId) {
        location.href = '${pageContext.request.contextPath}/invoice/toAdd?orderNo=' + orderNo + '&userId=' + userId;
    }
    del = function (id) {
        parent.layer.confirm('删除后拥有该角色的用户将会失去对应功能，确认要删除吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/order/doEdit",
                data: {"id": id, "status": 'D'},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        reload(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    confirmOrder = function(id){
        parent.layer.confirm('请确认货物已收到，是否确认要执行收货操作？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/order/confirmOrder",
                data: {"id": id},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        reload(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }


    resetOrder = function(id){
        parent.layer.confirm('确认要将该订单扔进订单池吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                dataType : "JSON",
                url: "<%=request.getContextPath() %>/order/resetOrder",
                data: {"id": id},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        reload(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }



    cancleOrder = function(id){
        parent.layer.confirm('确认要取消订单吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                dataType : "JSON",
                url: "<%=request.getContextPath() %>/order/cancleOrder",
                data: {"id": id},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        reload(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    confirmReceipt = function(id){
        parent.layer.confirm('确认要确认收货吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                dataType : "JSON",
                url: "<%=request.getContextPath() %>/order/confirmReceipt",
                data: {"id": id},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        reload(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    layerCallBack = function () {
        reload(0);
    }
    reload = function (init) {
        loadPage(init);
    }
</script>
<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function () {
        $(this).parent().hasClass("jui-hide") ? $(this).parent().removeClass("jui-hide") : $(this).parent().addClass("jui-hide");
    })
</script>
<script type="text/javascript">
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
