package cn.com.xj.plat.evt.carLength;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditCarLengthEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(length = 32 , value = "长度值",nullable = true)
    private String lengthValue;
    @Field(value = "车型ID集合",nullable = true)
    private Long[] carTypes;
    @Field(value = "重量ID集合",nullable = true)
    private Long[] carWeights;
    @Field(value = "体积ID集合",nullable = true)
    private Long[] carVolumes;

    public Long[] getCarWeights() {
        return carWeights;
    }

    public void setCarWeights(Long[] carWeights) {
        this.carWeights = carWeights;
    }

    public Long[] getCarVolumes() {
        return carVolumes;
    }

    public void setCarVolumes(Long[] carVolumes) {
        this.carVolumes = carVolumes;
    }
    public Long[] getCarTypes() {
        return carTypes;
    }

    public void setCarTypes(Long[] carTypes) {
        this.carTypes = carTypes;
    }

    public String getLengthValue() {
        return lengthValue;
    }

    public void setLengthValue(String lengthValue) {
        this.lengthValue = lengthValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
