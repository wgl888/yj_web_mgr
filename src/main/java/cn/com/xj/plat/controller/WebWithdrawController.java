package cn.com.xj.plat.controller;

import cn.com.xj.common.param.in.Message;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.param.valid.ValidUtils;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.webUserWithdraw.*;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.WebWithdrawService;
import cn.com.xj.plat.util.LoginUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/withdraw")
public class WebWithdrawController extends BaseController {

    @Autowired
    private WebWithdrawService webWithdrawService;

    @RequestMapping("/list")
    public String list() {
        return "/withdraw/list";
    }

    @RequestMapping("/page")
    public String page(QueryWithdrawEvt evt, Model model) {
        try {
            BaseFactory.getInstance().page(webWithdrawService, evt, model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/withdraw/page";
    }

    @RequestMapping("/toAudit")
    public String toAudit(QueryWithdrawEvt evt,
                            Model model){
        if(evt.getId()==null)return "/common/error";
        BaseFactory.getInstance().toDetail(webWithdrawService,BeanUtil.transBean2Map(evt),model);
        return "/withdraw/audit";
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryWithdrawEvt evt,
                           Model model){
        if(evt.getId()==null)return "/common/error";
        BaseFactory.getInstance().toDetail(webWithdrawService,BeanUtil.transBean2Map(evt),model);
        return "/withdraw/detail";
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryWithdrawEvt evt,
                           Model model){
        if(evt.getId()==null)return "/common/error";
        BaseFactory.getInstance().toEdit(webWithdrawService,BeanUtil.transBean2Map(evt),model);
        return "/withdraw/edit";
    }

    @ResponseBody
    @RequestMapping(value = "/doEdit",method = RequestMethod.POST)
    public ServiceResp doEdit(EditWithdrawEvt evt){
        try {
            return webWithdrawService.edit(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/doDel",method = RequestMethod.POST)
    public ServiceResp doDel(DelWithdrawEvt evt){
        try {
            return webWithdrawService.del(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/doAudit",method = RequestMethod.POST)
    public ServiceResp doAudit(AuditWithdrawEvt evt, HttpSession session){
        try {
            evt.setAuditUserId(LoginUtils.getId(session));
            evt.setAuditUserName(LoginUtils.getRealName(session));
            return webWithdrawService.audit(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @ResponseBody
    @RequestMapping(value = "loan", method = RequestMethod.POST)
    public ServiceResp<Object> loan(LoanEvt evt) {
        try {
            ServiceResp resp = new ValidUtils().validate(evt);
            if(!resp.success())return resp;
            return webWithdrawService.loan(evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统异常");
        }
    }



}
