<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<dl class="row">
    <dt class="tit">
        <label>审核状态：</label>
    </dt>
    <dd class="opt">
        <strong style="font-size: 16px;">
            <jsp:include page="status.jsp">
                <jsp:param name="status" value="${item.withdrawStatus }"/>
            </jsp:include>
        </strong>
        <c:if test="${item.auditRemark!=null&&item.auditRemark!=''}">
            <span style="font-size: 12px;">(${item.auditRemark })</span>
        </c:if>
    </dd>
</dl>
<dl class="row">
    <dt class="tit">
        <label>基本信息：</label>
    </dt>
    <dd class="opt">
        用户姓名：<a href="javascript:userDetail(${item.webUserId})" style="color:blue;">
        ${item.webUserName}</a><br>
        提现金额：<strong style="color:red;">￥${item.amount}</strong><br>
        提现备注：${item.remark }
    </dd>
</dl>
<dl class="row">
    <dt class="tit">
        <label>银行信息：</label>
    </dt>
    <dd class="opt">
        收款人姓名：${item.bankUserName}<br>
        收款人电话：${item.mobile}<br>
        身份证号码：${item.idCardNo}<br>
        收款人卡号：${item.bankNo}
    </dd>
</dl>
<c:if test="${item.auditUserName!=null&&item.auditUserName!=''}">
    <dl class="row">
        <dt class="tit">
            <label>审核信息：</label>
        </dt>
        <dd class="opt">
            　审核人：${item.auditUserName}<br>
            审核时间：<jsp:useBean id="auditTime" class="java.util.Date"/>
            <c:set target="${auditTime}" property="time" value="${item.auditTime}"/>
            <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${auditTime}" type="both"/>
        </dd>
    </dl>
</c:if>

<script type="application/javascript">
    userDetail = function(id){
        layer_full('${pageContext.request.contextPath}/plat/webUser/toDetail?id='+id);
    }
</script>
