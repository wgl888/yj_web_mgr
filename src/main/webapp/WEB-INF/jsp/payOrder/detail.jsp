<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>支付订单详细信息</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
    <style type="text/css">

        .imgDiv a img{
            border:1px solid #ddd;
            margin: 5px 5px 5px 5px;
            height: 100px;
            width: 100px;
        }

    </style>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/payOrder/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>查看支付订单详细信息</h3>
                <h5>查看支付订单的订单信息，支付单信息。</h5>
            </div>
        </div>
    </div>

    <form class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>支付结果：</label>
                </dt>
                <dd class="opt">
                    <c:if test="${item.payResult=='CG' }">
                        <strong style="color:green">成功</strong>
                    </c:if>
                    <c:if test="${item.payResult=='SB' }">
                        <strong style="color:red;">失败</strong>
                    </c:if>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>基本信息：</label>
                </dt>
                <dd class="opt">
                    　订单编号：<strong>${item.orderNo }</strong> <br>
                    支付订单号：${item.payOrderNo }<br>
                    　支付类型：${item.payType=='QB'?'钱包':item.payType=='YL'?'银联':'-' }<br>
                    　支付用户：${item.userName }<br>
                    　回执编号：${item.shippingAddress } <br>
                    　支付时间：<jsp:useBean id="createTime" class="java.util.Date"/>
                                <c:set target="${createTime}" property="time" value="${item.createTime}"/>
                                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
                </dd>
            </dl>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/role/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
