<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
    //菜单点击事件
    function menuClick(num, menuName, url,menuImg) {
        var names = new Array(); //数组
        names = menuName.split(","); //字符分割
        if(menuImg==''){
            menuImg = 'fa-home';
        }
        //一级
        if (num == 0) {
            $("#title1").html('<i class="ace-icon fa '+menuImg+' home-icon"></i>' + names[0]);
            $("#title1").show();$("#title2").hide();$("#title3").hide();
        }
        //二级
        if (num == 1) {
            $("#title1").html('<i class="ace-icon fa '+menuImg+' home-icon"></i>' + names[0]);
            $("#title1").show(); $("#title2").html(names[1]);$("#title2").show();
            $("#title3").hide();
        }
        //三级
        if (num == 2) {
            $("#title1").html('<i class="ace-icon fa '+menuImg+' home-icon"></i>' + names[0]);
            $("#title1").show();$("#title2").html(names[1]);$("#title2").show();
            $("#title3").html(names[2]);$("#title3").show();
            $("#title4").hide();
        }
        //四级
        if (num == 3) {
            $("#title1").html('<i class="ace-icon fa '+menuImg+' home-icon"></i>' + names[0]);
            $("#title1").show();$("#title2").html(names[1]);$("#title2").show();
            $("#title3").html(names[2]);$("#title3").show();
            $("#title4").html(names[3]);$("#title4").show();
        }
        //如果重定向到登录页,则在顶级窗口打开
        if (url.indexOf('login') > -1) {
            window.top.location = url;
        }else {
            $(".breadcrumb").show();//展示当前位置信息
            if(url!=''){
                jumpLoading(url);
            }
//            $('#mainFrame').attr("src",url);
        }
    }
    //快捷菜单点击
    function alterPage(type){
    }
</script>
<%--快捷菜单区域--%>
<jsp:include page="shortcuts.jsp" />
<ul class="nav nav-list">
    <c:forEach items="${menus}" var="menu" >
        <%-- 一级无子菜单 ------------------------- -------------------------------%>
        <c:if test="${empty menu.children }">
            <li class="">
            <c:choose>
        <%-- 一级当前窗口--%>
            <c:when test="${menu.openType=='_self'}">
                <c:choose>
            <%-- 一级当前窗口本系统--%>
            <c:when test="${menu.isLocal==1}">
                <a href="javascript:menuClick(0,'${menu.menuName }','<%=request.getContextPath() %>${menu.menuUrl }','${menu.menuImg}')" >
                </c:when>
            <%--  一级当前窗口外系统--%>
            <c:otherwise>
            <a href="javascript:menuClick(0,'${menu.menuName }','${menu.menuUrl }','${menu.menuImg}')" >
                </c:otherwise>
            </c:choose>
            </c:when>
        <%--  一级新窗口--%>
            <c:otherwise>
            <c:choose>
            <%-- 一级新窗口本系统--%>
            <c:when test="${menu.isLocal==1}">
                <a href="<%=request.getContextPath() %>${menu.menuUrl }" target="_blank">
                </c:when>
            <%--  一级新窗口外系统--%>
            <c:otherwise>
            <a href="${menu.menuUrl }" target="_blank">
                </c:otherwise>
            </c:choose>
            </c:otherwise>
        </c:choose>
        <i class="menu-icon fa ${menu.menuImg }"></i>
            <span class="menu-text">${menu.menuName }</span>
            </a>
            <b class="arrow"></b>
            </li>
        </c:if>

        <%-- 一级有子菜单1111111111111111111111111111111--%>
        <c:if test="${!empty menu.children }">
            <li>
            <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa ${menu.menuImg }"></i> <span class="menu-text">${menu.menuName }</span>
            <b class="arrow fa fa-angle-down"></b></a>
            <b class="arrow"></b>
            <ul class="submenu">
            <c:forEach var="children" items="${menu.children }" >
        <%--children二级无子菜单22222222222222222222222222222222-------%>
        <c:if test="${empty children.children }">
                        <li class="">
                        <c:choose>
                    <%--children二级当前窗口--%>
                    <c:when test="${children.openType=='_self'}">
                        <c:choose>
                    <%--children二级当前窗口本系统--%>
                    <c:when test="${children.isLocal==1}">
                        <a href="javascript:menuClick(1,'${menu.menuName }'+','+'${children.menuName }','<%=request.getContextPath() %>${children.menuUrl}','${menu.menuImg}')" >
                        </c:when>
                    <%--children二级当前窗口外系统--%>
                    <c:otherwise>
                    <a href="javascript:menuClick(1,'${menu.menuName }'+','+'${children.menuName }','${children.menuUrl }','${menu.menuImg}')" >
                        </c:otherwise>
                    </c:choose>
                    </c:when>
                    <%-- children二级新窗口--%>
                    <c:otherwise>
                    <c:choose>
                    <%--children二级新窗口本系统--%>
                    <c:when test="${menu.isLocal==1}">
                        <a href="<%=request.getContextPath() %>${children.menuUrl }" target="_blank" >
                        </c:when>
                    <%--children二级新窗口外系统--%>
                    <c:otherwise>
                    <a href="${children.menuUrl }" target="_blank" >
                        </c:otherwise>
                    </c:choose>
                    </c:otherwise>
                    </c:choose>
                    <i class="menu-icon fa fa-caret-right"></i>
                        ${children.menuName }
                        </a>
                        <b class="arrow"></b>
                        </li>
        </c:if>
        <%--children二级有子菜单2222222222222222222222222-----------%>
        <%--children2三级--%>
        <c:if test="${! empty children.children }">
                    <li class="">
                    <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-caret-right"></i>
                    ${children.menuName }
                    <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <%--遍历children三级--%>
                    <c:forEach var="children2" items="${children.children}" >
                            <%--children2三级无子菜单--------------------------------------------------------%>
                                <%--children3 四级--%>
                                <c:if test="${empty children2.children }">
                                    <%--<p>三级</p>--%>
                                    <li class="">
                                        <c:choose>
                                            <%--children2三级当前窗口--%>
                                        <c:when test="${children2.openType=='_self'}">
                                        <c:choose>
                                            <%--children2三级本系统--%>
                                        <c:when test="${children2.isLocal==1}">
                                        <a href="javascript:menuClick(2,'${menu.menuName }'+','+'${children.menuName }'+','+'${children2.menuName }','<%=request.getContextPath() %>${children2.menuUrl }','${menu.menuImg}')">
                                            </c:when>
                                                <%--children2三级外系统--%>
                                            <c:otherwise>
                                            <a href="javascript:menuClick(2,'${menu.menuName }'+','+'${children.menuName }'+','+'${children2.menuName }','${children2.menuUrl }','${menu.menuImg}')">
                                                </c:otherwise>
                                                </c:choose>
                                                </c:when>
                                                    <%-- children2三级新窗口--%>
                                                <c:otherwise>
                                                <c:choose>
                                                    <%--children2三级本系统--%>
                                                <c:when test="${menu.isLocal==1}">
                                                <a href="<%=request.getContextPath() %>${children2.menuUrl }" target="_blank">
                                                    </c:when>
                                                        <%--children2三级外系统--%>
                                                    <c:otherwise>
                                                    <a href="${children2.menuUrl }" target="_blank">
                                                        </c:otherwise>
                                                        </c:choose>
                                                        </c:otherwise>
                                                        </c:choose>
                                                        <i class="menu-icon fa fa-caret-right"></i>
                                                            ${children2.menuName }
                                                    </a>
                                                    <b class="arrow"></b>
                                    </li>
                                </c:if>
                                <%--children2三级无子菜单--------------------------------------------------------%>
                                <%--children3 四级--%>
                                <c:if test="${! empty children2.children }">
                                    <li class="">
                                        <a href="#" class="dropdown-toggle">
                                            <i class="menu-icon fa fa-caret-right"></i>
                                                ${children2.menuName }
                                            <b class="arrow fa fa-angle-down"></b>
                                        </a>
                                        <b class="arrow"></b>
                                        <ul class="submenu">
                                                <%--遍历children四级--%>
                                            <c:forEach var="children3" items="${children2.children}" >
                                                    <li class="">
                                                        <c:choose>
                                                            <%--children3四级当前窗口--%>
                                                        <c:when test="${children3.openType=='_self'}">
                                                            <c:choose>
                                                                <%--children3四级当前窗口本系统--%>
                                                            <c:when test="${children3.isLocal==1}">
                                                            <a href="javascript:menuClick(3,'${menu.menuName }'+','+'${children.menuName }'+','+'${children2.menuName }'+','+'${children3.menuName }','<%=request.getContextPath() %>${children3.menuUrl }','${menu.menuImg}')">
                                                            </c:when>
                                                                    <%--children3四级当前窗口外系统--%>
                                                            <c:otherwise>
                                                            <a href="javascript:menuClick(3,'${menu.menuName }'+','+'${children.menuName }'+','+'${children2.menuName }'+','+'${children3.menuName }','${children3.menuUrl }','${menu.menuImg}')">
                                                            </c:otherwise>
                                                            </c:choose>
                                                        </c:when>
                                                        <%-- children3四级新窗口--%>
                                                        <c:otherwise>
                                                            <c:choose>
                                                                <%--children3四级新窗口本系统--%>
                                                            <c:when test="${menu.isLocal==1}">
                                                            <a href="<%=request.getContextPath() %>${children3.menuUrl }" target="_blank">
                                                                </c:when>
                                                                    <%--children3四级新窗口外系统--%>
                                                                <c:otherwise>
                                                                <a href="${children3.menuUrl }" target="_blank">
                                                                    </c:otherwise>
                                                            </c:choose>
                                                        </c:otherwise>
                                                        </c:choose>
                                                                    <i class="menu-icon fa fa-caret-right"></i>
                                                                    ${children3.menuName }
                                                                    </a>
                                                                    <b class="arrow"></b>
                                                    </li>
                                            </c:forEach>
                                        </ul>
                                    </li>
                                </c:if>
                    </c:forEach>
                </ul>
                </li>
        </c:if>
        </c:forEach>
        </ul>
        </li>
        </c:if>
        </c:forEach>
</ul>
<!-- /.nav-list -->

<!-- #section:basics/sidebar.layout.minimize -->
<div class="sidebar-toggle sidebar-collapse">
    <i class="ace-icon fa fa-arrow-circle-left" data-icon1="ace-icon fa fa-arrow-circle-left" data-icon2="ace-icon fa fa-arrow-circle-right"></i>
</div>
