package cn.com.xj.plat.evt.carVolume;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryCarVolumeEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;

    @Field(length = 11, value = "ID", nullable = true)
    private Long id;
    @Field(length = 32, value = "名称", nullable = true)
    private String volumeName;
    @Field(length = 11, value = "carLength", nullable = true)
    private Long carLength;

    public Long getCarLength() {
        return carLength;
    }

    public void setCarLength(Long carLength) {
        this.carLength = carLength;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVolumeName() {
        return volumeName;
    }

    public void setVolumeName(String volumeName) {
        this.volumeName = volumeName;
    }
}
