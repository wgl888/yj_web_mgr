package cn.com.xj.plat.evt.protocol;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditProtocolEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(value = "协议名称",length = 64,nullable = true)
    private String protocolName;
    @Field(value = "协议编码",length = 64,nullable = true)
    private String protocolCode;
    @Field(value = "协议内容",nullable = true)
    private String content;
    @Field(value = "协议备注",length = 1024,nullable = true)
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    public String getProtocolCode() {
        return protocolCode;
    }

    public void setProtocolCode(String protocolCode) {
        this.protocolCode = protocolCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
