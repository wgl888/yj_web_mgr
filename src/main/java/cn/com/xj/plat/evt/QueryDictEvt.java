package cn.com.xj.plat.evt;
import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryDictEvt extends QueryEvt implements Serializable{

    @Field(length = 11,value = "ID",nullable = true)
    private Long id;
    @Field(length = 64,nullable = true,value = "表名")
    private String tableName;
    @Field(length = 64,nullable = true,value = "列名")
    private String columnName;
    @Field(length = 64,nullable = true,value = "字典值")
    private String dictValue;

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
}
