<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>用户列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body class="jui-blue-theme">
<form method="post" id="queryForm" class="form-horizontal clearfix" role="form">
    <input type="hidden" value="${evt.userId }" name="userId">
    <input type="text" id="queryByUserName" name="userAlias" style="width: 180px;" placeholder="输入手机号，车牌号"/>
    <select id="carLength" name="carLength" style="width: 130px;height: 32px;">
        <option value="">请选择车辆长度...</option>
        <c:forEach items="${carLengths }" var="ct">
            <option value="${ct.id}" ${item.carLength==ct.id?'selected':'' } >${ct.lengthValue}</option>
        </c:forEach>
    </select>
    <select id="carType" name="carType" style="width: 130px;height: 32px;">
        <option value="">请选择车辆类型...</option>
    </select>
    <button type="button" class="btn btn-xs btn-info"
            onclick="loadPage(1)">搜索
    </button>
    <table class="jui-theme-table table-hover margin-top" style="width: 600px;"
           id="myTable">
        <thead>
        <tr>
            <th width="120px">司机姓名</th>
            <th width="120px">手机号</th>
            <th width="120px">车牌号</th>
            <th width="120px">车辆类型</th>
            <th width="100px">评分</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody id="paging_content">

        </tbody>
    </table>
    <div id="paging" style="margin:10px 0 0 0;">

    </div>
</form>

<script type="application/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/plat/webUser/chooseDriverPage';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
    }

    choose = function(id,alias){
        $('#userAlias',window.parent.document).val(alias);
        $('#userId',window.parent.document).val(id);
        $('#chooseUserDiv',window.parent.document).attr("src","");
        $('#chooseUserRow',window.parent.document).hide();
    }


    $(document).ready(function(){
        loadPage(1);
    });


    $('#carLength').change(function () {
        $('#carType').html("");
        if ($('#carLength').val() == null || $('#carLength').val() == '') {
            return;
        }
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath() %>/carType/queryWithCarLength",
            data: {"carLengthId": $('#carLength').val()},
            success: function (data) {
                $('#carType').append('<option value="">请选择...</option>');
                for (var i = 0; i < data.body.length; i++) {
                    if ('${item.carType}' == data.body[i].carTypeId) {
                        var opt = '<option selected value = "' + data.body[i].carTypeId + '">' + data.body[i].typeName + '</option>';
                    } else {
                        var opt = '<option value = "' + data.body[i].carTypeId + '">' + data.body[i].typeName + '</option>';
                    }
                    $('#carType').append(opt);
                }
            }
        });
    });

</script>

</body>
</html>
