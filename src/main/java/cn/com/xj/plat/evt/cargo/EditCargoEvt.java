package cn.com.xj.plat.evt.cargo;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditCargoEvt extends BaseEvt {

    @Field(value = "ID",length = 11,nullable = true)
    private Long id;
    @Field(value = "货物名称",length = 128,nullable = false)
    private String cargoName;
    @Field(value = "货物重量",length = 11,nullable = true)
    private Float weight;
    @Field(value = "重量单位",length = 12,nullable = true)
    private String weightUnit;
    @Field(value = "货物体积",length = 11,nullable = true)
    private Float volume;
    @Field(value = "体积单位",length = 12,nullable = true)
    private String volumeUnit;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCargoName() {
        return cargoName;
    }

    public void setCargoName(String cargoName) {
        this.cargoName = cargoName;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public Float getVolume() {
        return volume;
    }

    public void setVolume(Float volume) {
        this.volume = volume;
    }

    public String getVolumeUnit() {
        return volumeUnit;
    }

    public void setVolumeUnit(String volumeUnit) {
        this.volumeUnit = volumeUnit;
    }
}
