<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>提现申请管理</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
    <%--按钮权限校验--%>
    <%--<script type="text/javascript" src="${applicationScope.staticFilepath }/js/mgrRight.js"></script>--%>
    <style type="text/css">
        .tdover {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        td {
            vertical-align: middle;
        !important;
        }

        table {
            table-layout: fixed;
        }
    </style>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <div class="page-content">
        <div class="jui-search-icon"></div>
        <!-- /.page-form start-->
        <form method="post" id="queryForm" class="form-horizontal clearfix" role="form">
            <table class="queryTable">
                <tr>
                    <td>
                        <span>用户姓名：</span>
                        <input type="text" class="input-width-160"
                               placeholder="输入用户姓名"
                               name="webUserName" id="webUserName"  >
                    </td>
                    <td>
                        <span>联系电话：</span>
                        <input type="text" class="input-width-160"
                               placeholder="输入用户联系电话"
                               name="webUserPhone" id="webUserPhone"  >
                    </td>
                    <td>
                        <span>提现状态：</span>
                        <select class="input-width-160" name="withdrawStatus" id="withdrawStatus" >
                            <option value="">请选择提现状态...</option>
                            <option value="DSH">待审核</option>
                            <option value="SHTG">审核通过</option>
                            <option value="SHBTG">审核不通过</option>
                            <option value="TXZ">提现中</option>
                            <option value="TXCG">提现成功</option>
                            <option value="TXSB">提现失败</option>
                        </select>
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-success" onclick="javascript:loadPage(1);">
                            <i class="ace-icon glyphicon glyphicon-search"></i>搜索
                        </button>
                        <button type="button" onclick="javascript:resetQuery();" class="btn btn-sm btn-orange">
                            <i class="ace-icon glyphicon glyphicon-repeat"></i>重置
                        </button>
                    </td>
                </tr>
            </table>
        </form>
        <!-- /.page-form end-->
    </div>

    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <%--<div class="">--%>
                <%--<button onclick="javascript:add();"--%>
                        <%--type="button" class="btn btn-lightgreen inline-block">新增--%>
                <%--</button>--%>
            <%--</div>--%>
            <table class="jui-theme-table table-hover margin-top">
                <thead>
                    <tr>
                        <th width="60px">ID</th>
                        <th width="120px">用户姓名</th>
                        <th width="120px">联系电话</th>
                        <th width="120px">提现金额</th>
                        <th width="160px">申请时间</th>
                        <th width="100px">审核状态</th>
                        <th width="100px">审核人</th>
                        <th width="160px">审核时间</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">

            </div>
        </div>
    </div>
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/withdraw/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
    }

    $(document).ready(function () {
        loadPage(1);
    });

    function resetSearch() {
        $('#typeName').val('');
        loadPage(1);
    }

    loan = function(id){
        parent.layer.confirm('确认要放款吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/withdraw/loan",
                data: {"id": id},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        loadPage(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    audit = function (id) {
        location.href = "${pageContext.request.contextPath}/withdraw/toAudit?id=" + id;
    }

    edit = function (id) {
        location.href = "${pageContext.request.contextPath}/withdraw/toEdit?id=" + id;
    }

    detail = function (id) {
        location.href = "${pageContext.request.contextPath}/withdraw/toDetail?id=" + id;
    }

    del = function (id) {
        parent.layer.confirm('确认要删除吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/withdraw/doDel",
                data: {"id": id},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        loadPage(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }


    setSort = function (id, ipt) {
        var newOrder = $(ipt).val().trim();
        var oldOrder = $(ipt).attr("data");
        if (newOrder == oldOrder) {
            return;
        }
        if (newOrder == "") {
            return;
        }
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath() %>/withdraw/doEdit",
            data: {"id": id, "sort": newOrder},
            success: function (data) {
                if (data.head.respCode == 0) {
                    parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                    loadPage(1);
                } else {
                    parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                }
            }
        });
    }

    resetQuery = function(){
        $('#webUserName').val("");
        $('#webUserPhone').val("");
        $("#withdrawStatus option:first").prop("selected", 'selected');
        loadPage(1);
    }

</script>
</body>
</html>
