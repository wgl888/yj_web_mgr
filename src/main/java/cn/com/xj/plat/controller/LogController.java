package cn.com.xj.plat.controller;

import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.QueryLogEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/log")
public class LogController extends BaseController {

    @Autowired
    private LogService logService;

    @RequestMapping("/list")
    public String list(){
        return "/log/list";
    }

    @RequestMapping("/page")
    public String page(QueryLogEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(logService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/log/page";
    }

    /***进入菜单、按钮编辑页面*/
    @RequestMapping("/toDetail")
    public String toDetail(QueryLogEvt evt,
                         Model model){
        try{
            if(evt.getId()==null){
                return "/common/error";
            }
            BaseFactory.getInstance().toDetail(logService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/log/detail";
    }

}
