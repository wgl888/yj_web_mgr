package cn.com.xj.plat.evt.userBank;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class EditUserBankEvt extends BaseEvt implements Serializable {

    private static final long serialVersionUID = 5946594148279628900L;

    @Field(value = "ID" , length = 11 , nullable = false)
    private Long id;
    @Field(value = "银行名称",length = 64,nullable = true)
    private String bankName;
    @Field(value = "银行卡卡号",length = 64,nullable = true)
    private String bankCardNo;
    @Field(value = "银行卡有效期",length = 4,nullable = true)
    private String bankExpire;
    @Field(value = "银行卡类型",length = 12,nullable = true)
    private String bankType;
    @Field(value = "银行卡校验码",length = 3,nullable = true)
    private String cardCvn;
    @Field(value = "持卡人ID",length = 11,nullable = true)
    private Long userId;
    @Field(value = "持卡人姓名",length = 11,nullable = false)
    private String userAlias;
    @Field(value = "银行预留手机号",length = 11,nullable = true)
    private String userMobile;

    public String getUserAlias() {
        return userAlias;
    }

    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public String getBankExpire() {
        return bankExpire;
    }

    public void setBankExpire(String bankExpire) {
        this.bankExpire = bankExpire;
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public String getCardCvn() {
        return cardCvn;
    }

    public void setCardCvn(String cardCvn) {
        this.cardCvn = cardCvn;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }
}
