package cn.com.xj.plat.factory;

import cn.com.xj.common.enums.Status;
import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.param.valid.ValidUtils;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.service.BaseInterface;
import cn.com.xj.plat.util.LoginUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wanggl
 * @version V1.0
 * @Title: BaseFactory
 * @Package cn.com.xj.plat.factory
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/6/21 15:21
 */
public class BaseFactory {

    private static BaseFactory instance;

    public static synchronized BaseFactory getInstance(){
        if(instance == null)
            instance = new BaseFactory();
        return instance;
    }

    public void page(BaseInterface baseInterface, QueryEvt evt, Model model){
        if(evt.getInit()==1){
            ServiceResp<Object> resp = baseInterface.count(BeanUtil.transBean2Map(evt));
            model.addAttribute("total",resp.getBody());
        }
        ServiceResp<Object> respList = baseInterface.query(BeanUtil.transBean2Map(evt));
        model.addAttribute("list",respList.getBody());
    }

    public ServiceResp<Object> valid(BaseEvt evt){
        ServiceResp validResp = new ValidUtils().validate(evt);
        if(!validResp.success()) {
            return validResp.error(validResp.getHead().getRespMsg());
        }
        return new ServiceResp().success("");
    }

    public ServiceResp<Object> add(BaseInterface baseInterface, BaseEvt evt,HttpSession session){
        ServiceResp validResp = new ValidUtils().validate(evt);
        if(!validResp.success()) {
            return validResp.error(validResp.getHead().getRespMsg());
        }
        evt.setOperator(LoginUtils.getId(session));
        evt.setCreateTime(new Date());
        evt.setUpdateTime(new Date());
        evt.setStatus(Status.E.name());
        return baseInterface.add(BeanUtil.transBean2Map(evt));
    }

    public void toEdit(BaseInterface baseInterface,Long id,Model model){
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("id",id);
        ServiceResp<Object> resp = baseInterface.query(param);
        List<Map<String,Object>> items = (List<Map<String,Object>>)resp.getBody();
        Map<String,Object> item = items.size()>0?items.get(0):null;
        model.addAttribute("item",item);
    }

    public void toEdit(BaseInterface baseInterface,Map param,Model model){
        ServiceResp<Object> resp = baseInterface.query(param);
        List<Map<String,Object>> items = (List<Map<String,Object>>)resp.getBody();
        Map<String,Object> item = items.size()>0?items.get(0):null;
        model.addAttribute("item",item);
    }

    public void toDetail(BaseInterface baseInterface,Map param,Model model){
        toEdit(baseInterface,param,model);
    }


    public ServiceResp<Object> doEdit(BaseInterface baseInterface,BaseEvt evt){
        ServiceResp validResp = new ValidUtils().validate(evt);
        if(!validResp.success()) {
            return validResp.error(validResp.getHead().getRespMsg());
        }
        if(StringUtils.isNotBlank(evt.getStatus())){
            if(!Status.E.name().equals(evt.getStatus())&&
                    !Status.D.name().equals(evt.getStatus())){
                return new ServiceResp().error("参数越界");
            }
        }
        evt.setUpdateTime(new Date());
        return baseInterface.edit(BeanUtil.transBean2Map(evt));
    }

}
