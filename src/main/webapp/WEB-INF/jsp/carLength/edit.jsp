<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>编辑车辆长度</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/carLength/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }车辆长度</h3>
                <h5>设置系统里面使用到的车辆长度信息。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/carLength/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <input type="hidden" id="typeIds" value="${item.typeIds }" />
        <input type="hidden" id="volumeIds" value="${item.volumeIds }" />
        <input type="hidden" id="weightIds" value="${item.weightIds }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>长度值：</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.lengthValue }"  class="input-txt"
                           id="lengthValue" name="lengthValue" datatype="*1-32" maxlength="32"
                           placeholder="请输入长度值 nullmsg="长度值不能为空"/>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入长度值,默认单位为：米，例如:4.7。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_choose_role">
                <dt class="tit">
                    <label><em>*</em>包含车型：</label>
                </dt>
                <dd class="opt">
                    <c:forEach items="${types }" var="item" varStatus="status">
                        <input name="carTypes" id="type_${item.id }"
                               value="${item.id }"
                               type="checkbox" class="ace" />
                        <span class="lbl" style="width: 70px;"> ${item.typeName }</span>
                        <c:if test="${status.index!=0&&status.index%5==0}">
                            <br/>
                        </c:if>
                    </c:forEach>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        勾选该车长支持的车型信息，多选。
                    </p>
                </dd>
            </dl>
            <dl class="row" >
                <dt class="tit">
                    <label><em>*</em>包含车载重量：</label>
                </dt>
                <dd class="opt">
                    <c:forEach items="${weights }" var="item" varStatus="status">
                        <input name="carWeights" id="weights_${item.id }"
                               value="${item.id }"
                               type="checkbox" class="ace" />
                        <span class="lbl" style="width: 70px;"> ${item.weightName }</span>
                        <c:if test="${status.index!=0&&status.index%5==0}">
                            <br/>
                        </c:if>
                    </c:forEach>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        勾选该车长支持的车载重量，多选。
                    </p>
                </dd>
            </dl>
            <dl class="row" >
                <dt class="tit">
                    <label><em>*</em>包含车载体积：</label>
                </dt>
                <dd class="opt">
                    <c:forEach items="${volumes }" var="item" varStatus="status">
                        <input name="carVolumes" id="volume_${item.id }"
                               value="${item.id }"
                               type="checkbox" class="ace" />
                        <span class="lbl" style="width: 70px;"> ${item.volumeName }</span>
                        <c:if test="${status.index!=0&&status.index%5==0}">
                            <br/>
                        </c:if>
                    </c:forEach>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        勾选该车长支持的车载体积，多选。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/carLength/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

        var typeIds = $('#typeIds').val();
        if(typeIds!=null&&typeIds!=""){
            var array = typeIds.split(',');
            for(var i=0;i<array.length;i++){
                $('#type_'+array[i]).attr("checked",true);
            }
        }
        var volumeIds = $('#volumeIds').val();
        if(volumeIds!=null&&volumeIds!=""){
            var array = volumeIds.split(',');
            for(var i=0;i<array.length;i++){
                $('#volume_'+array[i]).attr("checked",true);
            }
        }
        var weightIds = $('#weightIds').val();
        if(weightIds!=null&&weightIds!=""){
            var array = weightIds.split(',');
            for(var i=0;i<array.length;i++){
                $('#weight_'+array[i]).attr("checked",true);
            }
        }

    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
<script type="text/javascript">

    var setting = {
        async: {
            enable: true,
            url: getUrl
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            expandSpeed: "",
            selectedMulti: false
        },
        callback: {
            onAsyncSuccess: onAsyncSuccess,
            onAsyncError: onAsyncError,
            onCheck: onCheck
        }
    };
    $(function () {
        $.fn.zTree.init($("#tree"), setting, zNodes);
    });
    var zNodes = [];
    function getUrl(treeId, treeNode) {
        return "<%=request.getContextPath() %>/menu/tree?scope=menu";
    }
    function onAsyncSuccess(event, treeId, treeNode, msg) {
        if (!msg || msg.length == 0) {
            return;
        }
        if (msg == "-1") {
            parent.layer.msg("对不起，你传入的参数有误，请重试！", {icon: 2, time: 2000});
            return;
        }
        var zTree = $.fn.zTree.getZTreeObj("tree");
        zTree.expandAll(true);//全部展开
        var menus = $("#limits").val();
        if(menus!=null&&menus!=""){
            var node = zTree.getNodeByParam("id",-4);
            node.checked = true;
            zTree.updateNode(node);
            var menusArray = menus.split('#');
            if(menusArray.length>0){
                for(var i=0;i<menusArray.length;i++){
                    if(menusArray[i]!=''&&menusArray[i]!=null){
                        var node = zTree.getNodeByParam("id",menusArray[i]);
                        node.checked = true;
                        zTree.updateNode(node);
                    }
                }
            }
        }

//        var nodes = zTree.getNodes();
//        for (var i = 0; i < nodes.length; i++) { //设置节点展开
//            nodes[i].checked = true;
//            zTree.expandNode(nodes[i], true);//展开一级节点
//        }
//        var pnode = nodes[0];  //初始选中最高节点
//        zTree.selectNode(pnode);
    }

    function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        parent.layer.msg("异步获取数据出现异常！", {icon: 2, time: 2000});
        treeNode.icon = "";
        zTree.updateNode(treeNode);
    }

    //点击目录时调用
    function onCheck(event, treeId, treeNode, clickFlag) {
        //获取选中的节点
        var treeObj = $.fn.zTree.getZTreeObj("tree"),
                nodes = treeObj.getCheckedNodes(true),
                v = "";
        for (var i = 0; i < nodes.length; i++) {
            v += nodes[i].id + "#";
        }
        $("#limits").val(v);
    }

</script>
</body>
</html>
