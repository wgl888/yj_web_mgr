<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>${item.id }</td>
				<td>${item.menuName!=null?item.menuName:"-" }</td>
				<td style="table-layout:fixed; word-break: break-all; overflow:auto;">
					${(item.menuUrl!=null&&item.menuUrl!='')?item.menuUrl:'-'}
				</td>
				<td>${item.openType=='_self'?'当前窗口':'新窗口'}</td>
				<td>${item.isMenu==1?'菜单':'按钮'}</td>
				<td><input onblur="javascript:setSort(${item.id},this);" type="text" data="${item.sort}" value="${item.sort}" style="width: 50px;"></td>
				<td>
					<button type="button" class="btn btn-xs btn-success hide"
							author="/plat/menu/edit"
						onclick="edit(${item.id},${item.parentId})">编辑</button>
					<button type="button" class="btn btn-xs btn-info hide"
							author="/plat/menu/delete"
							onclick="del(${item.id});">删除</button>
				</td>
		   </tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="8">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">