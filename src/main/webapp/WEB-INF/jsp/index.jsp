<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>后台首页</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <%--引入公共js、css--%>
    <jsp:include page="common/common.jsp">
        <jsp:param name="contextPath" value="${contextPath}"  />
        <jsp:param name="useJQuery" value="true" />
        <jsp:param name="bootstrap" value="true" />
        <jsp:param name="popup" value="true" />
        <jsp:param name="cookieutil" value="true" />
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css" />
    <script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace.js?t=es5i5"></script>
</head>

<body class="jui-linghtgreen-theme">
<jsp:include page="home/top.jsp" />
<!-- #section:basics/navbar.layout -->

<!-- /section:basics/navbar.layout -->
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>
    <!-- #section:basics/sidebar -->
    <div id="sidebar" class="sidebar  responsive">
        <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
        </script>
        <%--快捷菜单区域--%>
        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
            <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                <button class="btn btn-success">
                    <i class="ace-icon fa fa-signal"></i>
                </button>
                <button class="btn btn-info">
                    <i class="ace-icon fa fa-pencil"></i>
                </button>
                <!-- #section:basics/sidebar.layout.shortcuts -->
                <button class="btn btn-warning">
                    <i class="ace-icon fa fa-users"></i>
                </button>
                <button class="btn btn-danger">
                    <i class="ace-icon fa fa-cogs"></i>
                </button>
                <!-- /section:basics/sidebar.layout.shortcuts -->
            </div>
            <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                <span class="btn btn-success"></span>
                <span class="btn btn-info"></span>
                <span class="btn btn-warning"></span>
                <span class="btn btn-danger"></span>
            </div>
        </div>
        <!-- /.sidebar-shortcuts -->
        <ul class="nav nav-list">
            <c:forEach items="${menus}" var="menu" >
            <%-- 一级无子菜单 ------------------------- -------------------------------%>
            <c:if test="${empty menu.children }">
            <li class="">
                <c:choose>
                    <%-- 一级当前窗口--%>
                <c:when test="${menu.openType=='_self'}">
                <c:choose>
                    <%-- 一级当前窗口本系统--%>
                <c:when test="${menu.isLocal==1}">
                <a href="javascript:createIframe(0,'${menu.menuName }','${contextPath }${menu.menuUrl }','${menu.pageHeight}')" target="_blank">
                    </c:when>
                    <%--  一级当前窗口外系统--%>
                    <c:otherwise>
                    <a href="javascript:createIframe(0,'${menu.menuName }','${menu.menuUrl }','${menu.pageHeight}')" target="_blank">
                        </c:otherwise>
                        </c:choose>
                        </c:when>
                            <%--  一级新窗口--%>
                        <c:otherwise>
                        <c:choose>
                            <%-- 一级新窗口本系统--%>
                        <c:when test="${menu.isLocal==1}">
                        <a href="${contextPath }${menu.menuUrl }" target="_blank">
                            </c:when>
                            <%--  一级新窗口外系统--%>
                            <c:otherwise>
                            <a href="${menu.menuUrl }" target="_blank">
                            </c:otherwise>
                            </c:choose>
                            </c:otherwise>
                            </c:choose>
                                <i class="menu-icon fa ${menu.menuImg }"></i>
                                <span class="menu-text">${menu.menuName }</span>
                            </a>
                            <b class="arrow"></b>
            </li>
            </c:if>

            <%-- 一级有子菜单--------------------------------------------------------%>
            <c:if test="${!empty menu.children }">
            <li>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa ${menu.menuImg }"></i> <span
                        class="menu-text">${menu.menuName }</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <c:forEach var="children" items="${menu.children }" >
                        <%--children二级无子菜单--------------------------------------------------------%>
                        <c:if test="${empty children.children }">
                            <li class="">
                                <c:choose>
                                    <%--children二级当前窗口--%>
                                <c:when test="${children.openType=='_self'}">
                                <c:choose>
                                    <%--children二级当前窗口本系统--%>
                                <c:when test="${children.isLocal==1}">
                                <a href="javascript:createIframe(1,'${menu.menuName }'+','+'${children.menuName }','${contextPath }${children.menuUrl}','${children.pageHeight}' )" >
                                    </c:when>
                                    <%--children二级当前窗口外系统--%>
                                    <c:otherwise>
                                <a href="javascript:createIframe(1,'${menu.menuName }'+','+'${children.menuName }','${children.menuUrl }','${children.pageHeight}')" >
                                        </c:otherwise>
                                        </c:choose>
                                        </c:when>
                                            <%-- children二级新窗口--%>
                                        <c:otherwise>
                                        <c:choose>
                                            <%--children二级新窗口本系统--%>
                                        <c:when test="${menu.isLocal==1}">
                                        <a href="${contextPath }${children.menuUrl }" target="_blank" >
                                            </c:when>
                                            <%--children二级新窗口外系统--%>
                                            <c:otherwise>
                                            <a href="${children.menuUrl }" target="_blank" >
                                                </c:otherwise>
                                                </c:choose>
                                                </c:otherwise>
                                                </c:choose>
                                                <i class="menu-icon fa fa-caret-right"></i>
                                                    ${children.menuName }
                                            </a>
                                            <b class="arrow"></b>
                            </li>
                        </c:if>
                        <%--children二级有子菜单------------------------------------------------------%>
                        <c:if test="${! empty children.children }">
                            <li class="">
                                <a href="#" class="dropdown-toggle">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                        ${children.menuName }
                                    <b class="arrow fa fa-angle-down"></b>
                                </a>
                                <b class="arrow"></b>
                                <ul class="submenu">
                                    <c:forEach var="children2" items="${children.children }" >
                                        <%--children2三级无子菜单--------------------------------------------------------%>
                                        <%--<c:if test="${empty children.children.children }">

                                        </c:if>
                                        <%--children2三级有子菜单--------------------------------------------------------%>
                                        <%--<c:if test="${! empty children.children.children }">--%>

                                        <%--</c:if>&ndash;%&gt;--%>
                                        <li class="">
                                            <c:choose>
                                                <%--children2三级当前窗口--%>
                                            <c:when test="${children2.openType=='_self'}">
                                            <c:choose>
                                                <%--children2三级本系统--%>
                                            <c:when test="${children2.isLocal==1}">
                                            <a href="javascript:createIframe(2,'${menu.menuName }'+','+'${children.menuName }'+','+'${children2.menuName }','${contextPath }${children2.menuUrl }','${children2.pageHeight}')">
                                                </c:when>
                                                    <%--children2三级外系统--%>
                                                <c:otherwise>
                                                <a href="javascript:createIframe(2,'${menu.menuName }'+','+'${children.menuName }'+','+'${children2.menuName }','${children2.menuUrl }','${children2.pageHeight}')">
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </c:when>
                                                        <%-- children2三级新窗口--%>
                                                    <c:otherwise>
                                                    <c:choose>
                                                        <%--children2三级本系统--%>
                                                    <c:when test="${menu.isLocal==1}">
                                                    <a href="${contextPath }${children2.menuUrl }" target="_blank">
                                                        </c:when>
                                                        <%--children2三级外系统--%>
                                                        <c:otherwise>
                                                        <a href="${children2.menuUrl }" target="_blank">
                                                            </c:otherwise>
                                                            </c:choose>
                                                            </c:otherwise>
                                                            </c:choose>
                                                            <i class="menu-icon fa fa-caret-right"></i>
                                                                ${children2.menuName }
                                                        </a>
                                                        <b class="arrow"></b>
                                        </li>

                                    </c:forEach>
                                </ul>
                            </li>
                        </c:if>
                    </c:forEach>
                </ul>
            </li>
            </c:if>
            </c:forEach>
        </ul>
        <!-- /.nav-list -->

        <!-- #section:basics/sidebar.layout.minimize -->
        <div class="sidebar-toggle sidebar-collapse">
            <i class="ace-icon fa fa-arrow-circle-left" data-icon1="ace-icon fa fa-arrow-circle-left" data-icon2="ace-icon fa fa-arrow-circle-right"></i>
        </div>

        <!-- /section:basics/sidebar.layout.minimize -->
        <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
        </script>
    </div>
    <jsp:include page="home/content.jsp" />
    <!-- /section:basics/sidebar -->
    <%--<div class="main-content">--%>
    <%--</div><!-- /.main-content -->--%>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
<!-- basic scripts -->
<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.min.js'>"+"<"+"/script>");
</script>
<!-- <![endif]-->
<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>

</body>
</html>
