package cn.com.xj.plat.controller;

import cn.com.xj.common.enums.YesOrNo;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.plat.evt.bank.AddBankEvt;
import cn.com.xj.plat.evt.bank.EditBankEvt;
import cn.com.xj.plat.evt.bank.QueryBankEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.model.SessionLoginUser;
import cn.com.xj.plat.service.BankService;
import cn.com.xj.plat.service.MenuService;
import cn.com.xj.plat.util.LoginUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/common")
public class CommonController extends BaseController {

    @Autowired
    private MenuService menuService;

    @RequestMapping("btnForAuthor")
    @ResponseBody
    public Map<String,String> btnForAuthor(Model model,
                                         HttpSession session) throws Exception {
        Map<String,String> resp = new HashMap<String,String>();
        /** 读取当前登陆用户的权限 */
        SessionLoginUser loginUser = LoginUtils.getLoginUser(session);
        if(YesOrNo.Y.name().equals(loginUser.getIsSuper())){
            resp.put("admin","true");
            return resp;
        }else{
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("platUserId",loginUser.getId());
            param.put("isMenu",0);
            ServiceResp<Object> menuResp = menuService.queryPlatUserBtns(param);
            String urls = (String)menuResp.getBody();
            resp.put("urls",urls);
        }
        return resp;
    }

}
