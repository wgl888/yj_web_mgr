<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td class="center"><label class="pos-rel">
					<input name="cbox"type="checkbox" class="ace" id="cbx${item.id}" data="${item.id}" /> <span class="lbl"></span>
				</label></td>
				<td id="${item.id }">${item.id }</td>
				<td>${item.userAlias!=null?item.userAlias:"-" }</td>
				<td>${item.loginName}</td>
				<td>${item.userPhone}</td>
				<td>
					<c:if test="${item.authStatus=='DSQ'}">
						<span style="color:orange;">待申请</span>
					</c:if>
					<c:if test="${item.authStatus=='DSH'}">
						<span style="color:green;">待审核</span>
					</c:if>
					<c:if test="${item.authStatus=='SHTG'}">
						<span style="color:green;">审核通过</span>
					</c:if>
					<c:if test="${item.authStatus=='SHBTG'}">
						<span style="color:red;">不通过</span>
					</c:if>
				</td>
				<td>
					<c:if test="${item.carownerStatus=='DSQ'}">
						<span style="color:orange;">待申请</span>
					</c:if>
					<c:if test="${item.carownerStatus=='DSH'}">
						<span style="color:green;">待审核</span>
					</c:if>
					<c:if test="${item.carownerStatus=='SHTG'}">
						<span style="color:green;">审核通过</span>
					</c:if>
					<c:if test="${item.carownerStatus=='SHBTG'}">
						<span style="color:red;">不通过</span>
					</c:if>
				</td>
				<td>
					<c:if test="${item.status=='E'}">
						<span style="color:green;">在用</span>
					</c:if>
					<c:if test="${item.status=='T'}">
						<span style="color:red;">已禁用</span>
					</c:if>
				</td>
				<td>
					<button type="button" class="btn btn-xs btn-info"
							onclick="detail(${item.id});">详情</button>
					<c:if test="${item.authStatus!='SHTG'}">
						<button type="button" class="btn btn-xs btn-success"
								onclick="edit(${item.id})">编辑</button>
					</c:if>
					<c:if test="${item.authStatus=='DSH'}">
						<button type="button" class="btn btn-xs btn-inverse"
								onclick="toAudit(${item.id});">审核</button>
					</c:if>
					<c:if test="${item.authStatus=='SHTG'}">
						<button type="button" class="btn btn-xs btn-yellow"
								onclick="bank(${item.id});">银行卡</button>
					</c:if>
					<button type="button" class="btn btn-xs btn-danger"
							onclick="del(${item.id});">删除</button>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="9">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">