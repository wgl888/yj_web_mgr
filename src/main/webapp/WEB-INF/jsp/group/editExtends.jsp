<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <jsp:include page="../common/common.jsp" flush="true">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="chosenMultiple" value="true"/>
        <jsp:param name="jqueryUI" value="true" />
        <jsp:param name="userForm" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
    <title>${item.id==null?'新增':'编辑' }扩展用户组</title>
</head>
<body style="background-color: #fff;">
<div class="page">
	<div class="fixed-bar">
		<div class="item-title">
			<a class="back" href="javascript:history.go(-1);" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
			<div class="subject">
				<h3>${item.id==null?'新增':'编辑' }扩展用户组</h3>
			</div>
		</div>
	</div>
<form action="${pageContext.request.contextPath}/group/${item.id==null?'doAddExtends':'doEditExtends' }.htm"
      method="post" class="form form-horizontal" id="form">
      <div class="ncap-form-default">
    	<input type="hidden" id="id" name="id" value="${item.id }">
    	<input type="hidden" id="superId" name="superId" value="${item.parentId }">
    	<dl class="row">
			<dt class="tit">
				<label><em>*</em>扩展用户组名称：</label>
			</dt>
			<dd class="opt">
				<input type="text" value="${item.groupName }"
                       id="groupName" name="groupName" class="input-txt limited" datatype="*2-16" maxlength="16"
                       placeholder="扩展用户组名称" nullmsg="企业名称不能为空">
				<span class="Validform_checktip"></span>
				<p class="notic">
					1-16位字符，可由中文、英文、数字及标点符号组成。
				</p>
			</dd>
		</dl>
		<dl class="row">
			<dt class="tit">
				<label><em>*</em>扩展用户组类型：</label>
			</dt>
			<dd class="opt">
                <select id="groupType" name="groupType" style="width: 25%;">
                    <c:forEach items="${groupTypes}" var="groupType">
                        <option ${item.groupType==groupType.dictValue?'selected':''} value="${groupType.dictValue}">${groupType.valueRemark}</option>
                    </c:forEach>
                </select>
                <span class="Validform_checktip"></span>
			</dd>
		</dl>
		<dl class="row">
			<dt class="tit">
				<label><em>*</em>省市地区：</label>
			</dt>
			<dd class="opt">
				<select id="province" onchange="getCitys(this);" name="provinceCode" style="width: 25%;">
                    <c:forEach items="${provinces}" var="province">
                        <option ${item.provinceCode==province.provinceCode?'selected':''} value="${province.provinceCode}">${province.provinceName}</option>
                    </c:forEach>
                </select><span>省</span>
                <select id="city" onchange="getAreas(this);" name="cityCode" style="width: 25%;<c:if test="${fn:length(citys)<=0}">display:none;</c:if>">
                    <c:forEach items="${citys}" var="city">
                        <option ${item.cityCode==city.cityCode?'selected':''} value="${city.cityCode}">${city.cityName}</option>
                    </c:forEach>
                </select><span style="<c:if test="${fn:length(citys)<=0}">display:none;</c:if>">市</span>

               <select id="area" name="areaCode" style="width: 25%; <c:if test="${fn:length(areas)<=0}">display:none;</c:if>">
                    <c:forEach items="${areas}" var="area">
                        <option ${item.areaCode==area.areaCode?'selected':''} value="${area.areaCode}">${area.areaName}</option>
                    </c:forEach>
                </select><span style="<c:if test="${fn:length(areas)<=0}">display:none;</c:if>">区</span>
				<span class="Validform_checktip"></span>
			</dd>
		</dl>
		<dl class="row">
			<dt class="tit">
				<label>备注：</label>
			</dt>
			<dd class="opt">
				<textarea rows="3" cols="30" name="remark"
                	onchange="this.value=this.value.substring(0, 170)" onkeydown="this.value=this.value.substring(0, 170)" onkeyup="this.value=this.value.substring(0, 170)">${item.remark}</textarea>
                <span class="Validform_checktip"></span>
				<p class="notic">
					0-170位字符，可由中文、英文、数字及标点符号组成。
				</p>
			</dd>
		</dl>
		<c:forEach items="${extendPageItems}" var="extendPageItem">
			<dl class="row">
				<dt class="tit">
					<label><c:if test="${extendPageItem.isMust==1 }"><em>*</em></c:if>${extendPageItem.fieldTitle }：</label>
				</dt>
				<dd class="opt">
					<c:if test="${extendPageItem.fieldType==1 }"><!-- text -->
						<input type="text" value="${extendPageItem.value?extendPageItem.value:'' }" placeholder="" 
							<c:if test="${extendPageItem.isMust==1 }">notNull="true"</c:if>
					name="${extendPageItem.fieldCode }" maxlength="${extendPageItem.fieldValueLength }" fieldName="${extendPageItem.fieldTitle }"
					placeholder="${extendPageItem.fieldRemark }" regular="${extendPageItem.regularExpress!=null?extendPageItem.regularExpress:'' }" valueType="${extendPageItem.fieldType }"/>
					</c:if>
					<c:if test="${extendPageItem.fieldType==2 }"><!-- radio -->
						<c:forEach items="${extendPageItem.dicts}" var="configDict" varStatus="status">
							<input name="${extendPageItem.fieldCode }" <c:if test="${extendPageItem.isMust==1&&status==0 }"> checked</c:if>
							<c:if test="${extendPageItem.value!=null&&configDict.dictValue!=null&&extendPageItem.value==configDict.dictValue }"> checked</c:if>
								type="radio" value="${configDict.dictValue }" /> ${configDict.valueRemark }
						</c:forEach>
												
					</c:if>
					<c:if test="${extendPageItem.fieldType==3 }"><!-- checkbox -->
						<c:forEach items="${extendPageItem.dicts}" var="configDict" varStatus="status">
							<input name="${extendPageItem.fieldCode }" type="checkbox" value="${configDict.dictValue }" /> ${configDict.valueRemark }
						</c:forEach>
					</c:if>
					<c:if test="${extendPageItem.fieldType==4 }"><!-- select -->
						<select name="${extendPageItem.fieldCode }">
							<option value="">请选择</option>
							<c:forEach items="${extendPageItem.dicts}" var="configDict" varStatus="status">
								<option value="${configDict.dictValue }" <c:if test="${extendPageItem.value!=null&&configDict.dictValue!=null&&extendPageItem.value==configDict.dictValue }"> selected</c:if>>${configDict.valueRemark }</option>
							</c:forEach>
						</select>
					</c:if>
				</dd>
			</dl>
			<div class="bot" >
				<button type="button" onclick="add()" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交</button>
			</div>
		</c:forEach>
    </div>
</form>

</div>
<script type="text/javascript">
	/** 校验方法 */
	 function validate(){
		var result = true;
		var value,fieldName;
		var regExp; 
		$("input[type='text']").each(function(){
			value = $(this).val();
			fieldName = $(this).attr('fieldName');
			if($(this).attr('notNull') == 'true' && value == ''){
				alert(fieldName+"不能为空！");
				result = false;
				return false;
			}
			if(value != ''){
				if($(this).attr('valueType') == 1){
					if(!/^(-?\\d+)$/.test(value)){
						alert(fieldName+"必须是整型！");
						result = false;
						return false;
					}
				}else if($(this).attr('valueType') == 3){
					if(!/^(-?\\d+)(\\.\\d+)?$/.test(value)){
						alert(fieldName+"必须是浮点型！");
						result = false;
						return false;
					}
				}
				if($(this).attr('regular') != '' && $(this).attr('regular') != undefined){
					regExp = new RegExp($(this).attr('regular'));
					if(!regExp.test(value)){
						alert(fieldName+"格式不正确！");
						result = false;
						return false;
					}
				}
			}
		});
		return result;
	}
</script>
<script type="text/javascript">

    /** 弹出框自带的提交按钮回调 */
    function add(){
        /**参数验证*/
        if($("#groupName").val().trim()==''){
            alert("请输入扩展用户组名称");
            $("#groupName").focus();
            return;
        }
        if($("#groupType").val()==null){
            alert("请选择扩展用户组类型");
            $("#groupName").focus();
            return;
        }
        validate();
        $.ajax({
            type : "post",
            traditional:true,
            url : $('#form').attr("action"),
            data : $('#form').serialize(),
            success : function(data){
                var json = JSON.parse(data);
                if(json.retCode == 0){
                    parent.layer.msg(json.retMsg, {icon: 1,time:2000});
                    window.location.href="${contextPath}/group/list";
                }else{
                    parent.layer.msg(json.retMsg, {icon: 2,time:2000});
                }
            }
        });
    }
    //根据省份获取城市
    getCitys = function (dom) {
    	$("#city").hide();
    	$("#city").next().hide();
    	$("#area").hide();
    	$("#area").next().hide();
        var provinceCode = $(dom).val();
        $.ajax({
            type : "post",
            url: "${pageContext.request.contextPath}/group/queryCityByPCode",
            data:{'provinceCode':provinceCode},
            success : function (data) {
                var json = JSON.parse(data);
                var options = '';
                $.each(json.retData, function (index, city) {;
                    var cityName = city.cityName;
                    var cityCode = city.cityCode;
                    options += '<option value='+cityCode+'>'+cityName+'</option>';
                });
                $("#city").empty();
                $("#area").empty();
                $("#city").append(options);
                if(options!=null&&options!=''){
                	$("#city").show();
                	$("#city").next().show();
                	getAreas($("#city"));
                }
            },
        });
    }
    getAreas = function (dom) {
        var cityCode = $(dom).val();
        $.ajax({
            type : "post",
            url: "${pageContext.request.contextPath}/group/queryAreaByCCode",
            data:{'cityCode':cityCode},
            success : function (data) {
                var json = JSON.parse(data);
                var options = '';
                $.each(json.retData, function (index, area) {;
                    var areaName = area.areaName;
                    var areaCode = area.areaCode;
                    options += '<option value='+areaCode+'>'+areaName+'</option>';
                });
                $("#area").empty();
                $("#area").append(options);
                if(options!=null&&options!=''){
                	$("#area").show();
                	$("#area").next().show();
                }
            }
        });
    }

</script>
<script type="text/javascript">
	var type = "${item.id!=null?'edit':'add'}";
    //    公共样式切换
    $(function () {
    	if(type=="add"){
    		getCitys($("#province"));
    	}
        changeBodyClass();
    });
</script>
</body>
</html>
