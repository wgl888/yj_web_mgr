package cn.com.xj.plat.evt.carLength;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddCarLengthEvt extends BaseEvt {

    @Field(length = 32 , value = "长度值",nullable = false)
    private String lengthValue;
    @Field(value = "车型ID集合",nullable = false)
    private Long[] carTypes;
    @Field(value = "重量ID集合",nullable = false)
    private Long[] carWeights;
    @Field(value = "体积ID集合",nullable = false)
    private Long[] carVolumes;

    public Long[] getCarWeights() {
        return carWeights;
    }

    public void setCarWeights(Long[] carWeights) {
        this.carWeights = carWeights;
    }

    public Long[] getCarVolumes() {
        return carVolumes;
    }

    public void setCarVolumes(Long[] carVolumes) {
        this.carVolumes = carVolumes;
    }

    public Long[] getCarTypes() {
        return carTypes;
    }

    public void setCarTypes(Long[] carTypes) {
        this.carTypes = carTypes;
    }

    public String getLengthValue() {
        return lengthValue;
    }

    public void setLengthValue(String lengthValue) {
        this.lengthValue = lengthValue;
    }
}
