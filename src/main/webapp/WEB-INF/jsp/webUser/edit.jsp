<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="layerUi" value="true"/>
        <jsp:param name="plupload" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
    <style type="text/css">
        .delete{
            width:20px;
            height:20px;
            position:absolute;
            top:0px;
            left:78px;
            z-index:9999;
            background-repeat:no-repeat;
            cursor:pointer;
            background-image: url("<%=request.getContextPath() %>/myResources/manager/plat/images/imgdelete.png");
        }
    </style>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/plat/webUser/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }前台用户</h3>
                <h5>注册或编辑前台用户信息。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>1，登录账号新增后不可编辑，登陆账号不能重复。</li>
            <li>2，前台用户新增完后的初始密码为：<strong style="color: red;">${defaultPassword }</strong></li>
            <li>3，新增/编辑只能输入基本信息，用户余额默认为0，审核状态默认为待申请。</li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/plat/webUser/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }"/>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>登陆账号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.loginName }" placeholder="" class="input-txt"
                           id="loginName" name="loginName" datatype="*2-16" maxlength="16"
                           placeholder="登录账号" nullmsg="登录账号不能为空" ${item.loginName == null ? '': 'readonly' }>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入用户登陆账号，保存后将不可修改。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>用户姓名</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.userAlias }" placeholder="" class="input-txt"
                           id="userAlias" name="userAlias" datatype="*1-12" maxlength="12"
                           placeholder="用户名称" nullmsg="用户姓名不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入用户姓名，2~16位字符，可由中文，英文组成。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>用户组</label>
                </dt>
                <dd class="opt">
                    <div class="Jui-silder-main">
                        <ul class="Jui-silder-list ztree" id="tree">

                        </ul>
                        <input type="hidden" value="${item.userGroupId }" name="userGroupId" id="userGroupId">
                    </div>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请选择用户归属用户组，用户组数据可在用户组管理配置。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>用户性别</label>
                </dt>
                <dd class="opt">
                    <label>
                        <input name="userSex" value="NAN" ${(item.userSex=='NAN'||item.userSex==null)?'checked':''}
                               type="radio" class="ace"/>
                        <span class="lbl">男</span>
                    </label>
                    <label>
                        <input name="userSex" value="NV" ${item.userSex=='NV'?'checked':''} type="radio" class="ace"/>
                        <span class="lbl">女</span>
                    </label>
                    <span class="Validform_checktip"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>手机号码</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.userPhone }" placeholder="" class="input-txt"
                           id="userPhone" name="userPhone" datatype="*11-11" maxlength="11"
                           placeholder="手机号码" nullmsg="手机号码不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入正确格式的手机号码。
                    </p>
                </dd>
            </dl>
            <c:if test="${item.authStatus != 'SHTG'}">
                <dl class="row">
                    <dt class="tit">
                        <label><em>*</em>认证状态：</label>
                    </dt>
                    <dd class="opt">
                        <select class="input-txt" id="authStatus" name="authStatus"
                                datatype="*" nullmsg="请选择认证状态">
                            <option value="">请选择认证状态...</option>
                            <option value="DSQ" ${item.authStatus=='DSQ'?'selected':''}>待申请</option>
                            <option value="DSH" ${item.authStatus=='DSH'?'selected':''}>待审核</option>
                        </select>
                        <span class="Validform_checktip"></span>
                        <p class="notic">
                            如果是待申请状态，是需要输入用户基本信息，待审核需要输入和上传身份信息。
                        </p>
                    </dd>
                </dl>
            </c:if>

            <div id="div_userCard" style="display: none;">
                <dl class="row">
                    <dt class="tit">
                        <label><em>*</em>身份证号码</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" value="${item.userCardNo }" class="input-txt"
                               id="userCardNo" name="userCardNo" maxlength="18"
                               placeholder="身份证号码">
                        <span class="Validform_checktip"></span>
                        <p class="notic">
                            请输入18位的身份证号码。
                        </p>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label><em>*</em>身份证有效期</label>
                    </dt>
                    <dd class="opt">
                        <input readonly class="audit-input" type="text" id="userCardPeriod" name="userCardPeriod"
                               placeholder="请选择有效期截止日期" value=""
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                        <span class="Validform_checktip"></span>
                        <p class="notic">
                            请输入您身份证背面的有效期截止日期。
                        </p>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label><em>*</em>身份证地址</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" value="${item.userCardAddress }" placeholder="" class="input-txt"
                               id="userCardAddress" name="userCardAddress" maxlength="256"
                               placeholder="身份证归属地">
                        <span class="Validform_checktip"></span>
                        <p class="notic">
                            请输入身份证正面的地址信息。
                        </p>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label><em>*</em>身份证照片：</label>
                    </dt>
                    <dd class="opt">
                        <div class="pull-left file-box margin-right">
                            <div class="file-preview" filepreview>
                                <img id="cardImg1_img" src='${item!=null?item.cardImg1:''}'>
                                <div class="file-input" id="cardImg1_file"></div>
                            </div>
                            <strong style="margin: 0 auto;">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                正面
                            </strong>
                            <input type="hidden" id="cardImg1_hid"
                                   name="cardImg1" value="${item.cardImg1 }">
                            <div id="cardImg1_del" class='delete' onclick='deleteFile("cardImg1")'
                                 style="display: ${item==null?'none':''}">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="pull-left file-box margin-right">
                            <div class="file-preview" filepreview>
                                <img id="cardImg2_img" src="${item!=null?item.cardImg2:''}">
                                <div class="file-input" id="cardImg2_file"></div>
                            </div>
                            <strong style="margin: 0 auto;">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                反面
                            </strong>
                            <input type="hidden" id="cardImg2_hid"
                                   name="cardImg2" value="${item.cardImg2 }">
                            <div id="cardImg2_del" class='delete' onclick='deleteFile("cardImg2")'
                                 style="display: ${item==null?'none':''}">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        </div>
                        <div class="pull-left file-box margin-right">
                            <div class="file-preview" filepreview>
                                <img id="cardImg3_img" src="${item!=null?item.cardImg3:''}">
                                <div class="file-input" id="cardImg3_file"></div>
                            </div>
                            <strong style="margin: 0 auto;">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                手持
                            </strong>
                            <input type="hidden" id="cardImg3_hid" name="cardImg3"
                                   value="${item.cardImg3 }">
                            <div id="cardImg3_del" class='delete' onclick='deleteFile("cardImg3")'
                                 style="display: ${item==null?'none':''}">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        </div>
                        <span class="Validform_checktip"></span>
                    </dd>
                </dl>
            </div>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
</body>

<script type="text/javascript">
    /*限制文本框只能输入数字*/
    $("#userPhone").keyup(function () {
        $(this).val($(this).val().replace(/[^\d]/g, ""));
    });

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/plat/webUser/list";
                    });
                } else {
                    parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                }
            }
        });

        $('#authStatus').change(function(){
            var val = $(this).val();
            if('DSQ'==val){
                $('#div_userCard').hide();
            }
            if('DSH'==val){
                $('#div_userCard').show();
            }
        });

        $('#authStatus').trigger("change");

        layui.use('laydate', function () {
            var laydate = layui.laydate;
            var start = {
                max: '2099-06-16 23:59:59'
                , istoday: false
                , choose: function (datas) {
                    end.min = datas; //开始日选好后，重置结束日的最小日期
                    end.start = datas //将结束日的初始值设定为开始日
                }
            };
            var end = {
                max: '2099-06-16 23:59:59'
                , istoday: false
                , choose: function (datas) {
                    start.max = datas; //结束日选好后，重置开始日的最大日期
                }
            };
            document.getElementById('userCardPeriod').onclick = function () {
                start.elem = this;
                laydate(start);
            }

        });

    });

</script>



<script type="application/javascript">

    var uploadUrl = '<%=request.getContextPath() %>/upload/uploadImg';
    var ids = new Array("cardImg1_file","cardImg2_file","cardImg3_file");

    $.each(ids,function(i,n){
        var self = this.toString();
        var uploader = new plupload.Uploader({
            browse_button : self, //触发文件选择对话框的按钮，为那个元素id
            url : uploadUrl ,//服务器端的上传页面地址
            max_file_size: '5mb',//限制为2MB
            filters: [{title: "Image files",extensions: "jpg,png"}]//图片限制
        });

        //在实例对象上调用init()方法进行初始化
        uploader.init();
        //绑定各种事件，并在事件监听函数中做你想做的事
        uploader.bind('FilesAdded',function(uploader,files){
            var loading = "<%=request.getContextPath() %>/myResources/manager/plat/images/imgloading1.gif";
            $("#"+self).parent().find('img').attr('src',loading);
            uploader.start();
        });

        uploader.bind('FileUploaded', function (uploader, file, responseObject) {
            var rs = responseObject.response;
            if(rs==''||rs==null||rs*1==-1*1){
                parent.layer.open({
                    title: '失败',
                    content: '上传失败，请稍后再试。'
                });
            }
            $("#"+self).parent().find('img').attr('src',responseObject.response);
            var hid = self.split("_")[0] + "_hid";
            $("#"+hid).val(responseObject.response);
            var del = self.split("_")[0] + "_del";
            $("#"+del).show();
        });

    });

    deleteFile = function(nm){

        $("#"+nm+"_img").attr('src','');
        $("#"+nm+"_del").hide();
        $("#"+nm+"_hid").val("");//删除图片id

    }

</script>


<script type="text/javascript">
//    $(".Jui-silder-main").css('height', "800px").panel({iWheelStep: 32});//这是滚动插件的用法  需要手动定义高度
    var setting = {
        async: {
            enable: true,
            url: getUrl
        },
        check: {
            enable: true,
            chkStyle:"radio",
            radioType:"all"
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            expandSpeed: "",
            selectedMulti: false
        },
        callback: {
            onAsyncSuccess: onAsyncSuccess,
            onAsyncError: onAsyncError,
            onCheck: onCheck
        }
    };
    $(function () {
        $.fn.zTree.init($("#tree"), setting, zNodes);
    });
    var zNodes = [];
    function getUrl(treeId, treeNode) {
        return "<%=request.getContextPath() %>/userGroup/tree?scope=userGroup";
    }

    function onAsyncSuccess(event,treeId,treeNode) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        zTree.expandAll(true);//全部展开
        var userGroupId = $('#userGroupId').val();
        if(userGroupId!=null&&userGroupId!=""&&userGroupId!=undefined){
            var node = zTree.getNodeByParam("id",userGroupId);
            node.checked = true;
            zTree.updateNode(node);
        }
    }

    function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        parent.layer.msg("异步获取数据出现异常！", {icon: 2, time: 2000});
        treeNode.icon = "";
        zTree.updateNode(treeNode);
    }

    //点击目录时调用
    function onCheck(event, treeId, treeNode) {
        //获取选中的节点
        var treeObj = $.fn.zTree.getZTreeObj("tree"),
                nodes = treeObj.getCheckedNodes(true),
                v = "";
        v = nodes[0].id;
        $('#userGroupId').val(v);
    }

</script>

</html>
