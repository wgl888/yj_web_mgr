<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${param.status=='CG'}">
    <span style="color:#9d9d9d;">草稿</span>
</c:if>
<c:if test="${param.status=='DZP'}">
    待指派
</c:if>
<c:if test="${param.status=='DJD'}">
    待接单
</c:if>
<c:if test="${param.status=='SJJUDAN'}">
    司机拒单
</c:if>
<c:if test="${param.status=='SJJIEDAN'}">
    司机接单
</c:if>
<c:if test="${param.status=='DHZFK'}">
    待货主付款
</c:if>
<c:if test="${param.status=='DFH'}">
    待发货
</c:if>
<c:if test="${param.status=='DSH'}">
    待收货
</c:if>
<c:if test="${param.status=='SH'}">
    售后
</c:if>
<c:if test="${param.status=='YSH'}">
    已收货
</c:if>
<c:if test="${param.status=='SJPJ'}">
    司机评价
</c:if>
<c:if test="${param.status=='HZPJ'}">
    货主评价
</c:if>
<c:if test="${param.status=='YKP'}">
    已开票
</c:if>