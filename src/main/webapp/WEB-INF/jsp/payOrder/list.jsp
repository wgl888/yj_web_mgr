<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>支付订单列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
    <%--按钮权限校验--%>
    <%--<script type="text/javascript" src="${applicationScope.staticFilepath }/js/mgrRight.js"></script>--%>
    <style type="text/css">
        .tdover {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        td {
            vertical-align: middle;
        !important;
        }

        table {
            table-layout: fixed;
        }
    </style>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <!-- 查询条件 -->
    <!-- /.page-content -->
    <%--输入框要小的 就直接加jui-mini-control 这个类名style="padding: 10px"--%>
    <div class="page-content jui-mini-control" transition>
        <div class="jui-search-icon"></div>
        <!-- /.page-form start-->
        <form method="post" id="queryForm" class="form-horizontal clearfix" payOrder="form">
            <table class="queryTable">
                <tr>
                    <td>
                        <span>订单编号：</span>
                        <input type="text" class="input-width-160"
                               id="rechargeOrderNo" name="rechargeOrderNo"/>
                    </td>
                    <td>
                        <span>支付人：</span>
                        <input type="text" class="input-width-160"
                               id="userName" name="userName"/>
                    </td>
                    <td>
                        <span>订单状态：</span>
                        <select id="rechargeStatus" name="rechargeStatus" class="input-width-160">
                            <option value="">全部</option>
                            <option value="CLZ">处理中</option>
                            <option value="CLCG">处理成功</option>
                            <option value="CLSB">处理失败</option>
                        </select>
                    </td>
                    <td>
                        <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询
                        </button>
                        <button onclick="javascript:resetSearch(1);" type="button" class="btn btn-orange inline-block">
                            重置
                        </button>
                    </td>
                </tr>
            </table>
        </form>
        <!-- /.page-form end-->
    </div>

    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <table class="jui-theme-table table-hover margin-top" id="myTable">
                <thead>
                <tr>
                    <th width="180px">订单编号</th>
                    <th width="140px">充值人</th>
                    <th width="140px">充值人电话</th>
                    <th width="100px">充值金额</th>
                    <th width="80px">订单状态</th>
                    <th width="170px">支付渠道</th>
                    <th width="170px">充值时间</th>
                    <%--<th>操作</th>--%>
                </tr>
                </thead>
                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">

            </div>
        </div>
    </div>
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/payOrder/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
        setPageHeight();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    detail = function (id) {
        location.href = '${pageContext.request.contextPath}/payOrder/toDetail?id=' + id;
    }

    function resetSearch() {
        $('#rechargeOrderNo').val('');
        $('#userName').val('');
        $("#rechargeStatus option:first").prop("selected", 'selected');
        loadPage(1);
    }
    layerCallBack = function () {
        reload(0);
    }
    reload = function (init) {
        loadPage(init);
    }
</script>
<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function () {
        $(this).parent().hasClass("jui-hide") ? $(this).parent().removeClass("jui-hide") : $(this).parent().addClass("jui-hide");
    })
</script>
<script type="text/javascript">
    //    按钮权限校验
    function mgrPermissions() {
        var btns = new Array;
        $("[permiss]").each(function () {
            var btnCode = $(this).attr("permiss");
            btns.push(btnCode);
        });
        if (btns.length > 0) {
            $.ajax({
                type: "post",
                traditional: true,
                url: "<%=request.getContextPath() %>/auth/mgrRight",
                data: {"btns": btns},
                success: function (data) {
                    var json = JSON.parse(data);
                    for (var i = 0; i < btns.length; i++) {
                        var btnCode = btns[i];
                        var flag = json[btnCode]
                        $("[permiss]").each(function () {
                            var b = $(this).attr("permiss");
                            if (btnCode == b) {
                                if (flag) {
                                    $(this).show();
                                } else {
                                    $(this).hide();
                                }
                            }
                        });
                    }
                }
            });
        }
    }
</script>
<script type="text/javascript">
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
