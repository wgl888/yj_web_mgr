<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>售后详情</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
    <style type="text/css">

        .imgDiv a img{
            border:1px solid #ddd;
            margin: 5px 5px 5px 5px;
            height: 100px;
            width: 100px;
        }

    </style>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/service/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>货运订单售后服务详情</h3>
                <h5>查看售后问题信息，图片信息。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/order/doService"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>售后状态：</label>
                </dt>
                <dd class="opt">
                    <c:if test="${item.serviceStatus=='DCL' }" >
                        <strong style="color:orange;font-size: 16px;">待处理</strong>
                    </c:if>
                    <c:if test="${item.serviceStatus=='YCL' }" >
                        <strong style="color: green;font-size: 16px;">已处理</strong>
                    </c:if>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>申请人：</label>
                </dt>
                <dd class="opt">
                    ${item.linkMan }/ ${item.contactPhone }
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>申请时间：</label>
                </dt>
                <dd class="opt">
                    <jsp:useBean id="createTime" class="java.util.Date"/>
                    <c:set target="${createTime}" property="time" value="${item.createTime}"/>
                    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>问题描述：</label>
                </dt>
                <dd class="opt">
                    ${item.problemDesc }
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>图片信息：</label>
                </dt>
                <dd class="opt">
                    <div class="imgDiv" style="width: 380px;">
                        <c:if test="${item.img1 != null }">
                            <a href="${item.img1 }" target="_blank">
                                <img src="${item.img1 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img2 != null }">
                            <a href="${item.img2 }" target="_blank">
                                <img src="${item.img2 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img3 != null }">
                            <a href="${item.img3 }" target="_blank">
                                <img src="${item.img3 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img4 != null }">
                            <a href="${item.img4 }" target="_blank">
                                <img src="${item.img4 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img5 != null }">
                            <a href="${item.img5 }" target="_blank">
                                <img src="${item.img5 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img6 != null }">
                            <a href="${item.img6 }" target="_blank">
                                <img src="${item.img6 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img7 != null }">
                            <a href="${item.img7 }" target="_blank">
                                <img src="${item.img7 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img8 != null }">
                            <a href="${item.img8 }" target="_blank">
                                <img src="${item.img8 }" >
                            </a>
                        </c:if>
                        <c:if test="${item.img9 != null }">
                            <a href="${item.img9 }" target="_blank">
                                <img src="${item.img9 }" >
                            </a>
                        </c:if>
                    </div>
                </dd>
            </dl>
            <c:if test="${item.serviceStatus=='YCL' }" >
                <dl class="row">
                    <dt class="tit">
                        <label>处理回复：</label>
                    </dt>
                    <dd class="opt">
                        ${item.replyMsg!=null?item.replyMsg:'-' }
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label>回复时间：</label>
                    </dt>
                    <dd class="opt">
                        <jsp:useBean id="updateTime" class="java.util.Date"/>
                        <c:set target="${updateTime}" property="time" value="${item.updateTime}"/>
                        <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${updateTime}" type="both"/>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label>回复人：</label>
                    </dt>
                    <dd class="opt">
                        ${item.replyUserName!=null?item.replyUserName:'-' }
                    </dd>
                </dl>
            </c:if>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/role/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
