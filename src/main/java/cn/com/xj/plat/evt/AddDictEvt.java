package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class AddDictEvt extends BaseEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    @Field(length = 64,nullable = false,value = "表名")
    private String tableName;
    @Field(length = 64,nullable = false,value = "列名")
    private String columnName;
    @Field(length = 64,nullable = false,value = "字典值")
    private String dictValue;
    @Field(length = 128,nullable = false,value = "字典值描述")
    private String dictDesc;
    @Field(length = 11,nullable = true,value = "排序号")
    private Integer sort;
    @Field(value = "备注",length = 1024,nullable = true)
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public String getDictDesc() {
        return dictDesc;
    }

    public void setDictDesc(String dictDesc) {
        this.dictDesc = dictDesc;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
