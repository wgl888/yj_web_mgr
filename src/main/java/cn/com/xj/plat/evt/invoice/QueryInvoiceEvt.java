package cn.com.xj.plat.evt.invoice;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryInvoiceEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -785442268796807212L;

    @Field(value = "ID", length = 11, nullable = true)
    private Long id;
    @Field(value = "公司名称", length = 128, nullable = true)
    private String companyName;
    @Field(value = "发票类型", length = 12, nullable = true)
    private String invoiceType;
    @Field(value = "订单编号",length = 32,nullable = true)
    private String orderNo;
    @Field(value = "物流单号",length = 32,nullable = true)
    private String logisticeNo;
    @Field(value = "发票状态",length = 12,nullable = true)
    private String invoiceStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getLogisticeNo() {
        return logisticeNo;
    }

    public void setLogisticeNo(String logisticeNo) {
        this.logisticeNo = logisticeNo;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }
}
