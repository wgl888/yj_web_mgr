package cn.com.xj.plat.evt.webUserWithdraw;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class AuditWithdrawEvt extends BaseEvt implements Serializable {

    private static final long serialVersionUID = 5946594148279628900L;

    @Field(value = "ID", length = 11, nullable = false)
    private Long id;
    @Field(value = "提现状态", length = 12, nullable = false)
    private String withdrawStatus;
    @Field(value = "审核备注", length = 512, nullable = true)
    private String auditRemark;
    @Field(value = "审核人ID",length = 11,nullable = false)
    private Long auditUserId;
    @Field(value = "审核人姓名",length = 32,nullable = false)
    private String auditUserName;

    public Long getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(Long auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getAuditUserName() {
        return auditUserName;
    }

    public void setAuditUserName(String auditUserName) {
        this.auditUserName = auditUserName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(String withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }
}
