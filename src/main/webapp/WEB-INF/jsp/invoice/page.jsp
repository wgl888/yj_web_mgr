<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>${item.orderNo }</td>
				<td>
					<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:150px;"
						   title='${item.companyName}' >${item.companyName }</label>
				</td>
				<td>${item.invoiceType=='PP'?'普票':'专票' }</td>
				<td><strong style="color:red;">￥${item.amount }</strong></td>
				<td>${(item.logisticeNo!=null&&item.logisticeNo!='')?item.logisticeNo:'-' }</td>
				<td>
					<jsp:include page="status.jsp">
						<jsp:param name="invoiceStatus" value="${item.invoiceStatus }"/>
					</jsp:include>
				</td>
				<td>
					<c:if test="${item.auditTime!=null}">
						<jsp:useBean id="auditTime" class="java.util.Date"/>
						<c:set target="${auditTime}" property="time" value="${item.auditTime}"/>
						<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${auditTime}" type="both"/>
					</c:if>
					<c:if test="${item.auditTime==null}">
						-
					</c:if>
				</td>
				<td>${item.auditUserName}</td>
				<td >
					<div class="btn-group">
						<c:if test="${item.invoiceStatus=='DSH'}">
							<button type="button" class="btn btn-xs btn-inverse"
									onclick="audit(${item.id});">审核</button>
						</c:if>
						<c:if test="${item.invoiceStatus=='SHTG'}">
							<button type="button" class="btn btn-xs btn-purple"
									onclick="over(${item.id})">发货</button>
						</c:if>
						<c:if test="${item.invoiceStatus=='DSH' || item.invoiceStatus=='SHBTG'}">
							<button type="button" class="btn btn-xs btn-success"
									onclick="edit(${item.id})">编辑</button>
							<button type="button" class="btn btn-xs btn-info"
									onclick="del(${item.id});">删除</button>
						</c:if>
						<button type="button" class="btn btn-xs btn-warning"
								onclick="detail(${item.id})">详情</button>
					</div>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="7">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
