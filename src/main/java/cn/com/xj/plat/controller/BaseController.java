package cn.com.xj.plat.controller;

import cn.com.xj.common.enums.Status;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.param.valid.ValidUtils;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.AddPlatUserEvt;
import cn.com.xj.plat.evt.EditPlatUserEvt;
import cn.com.xj.plat.evt.QueryDictEvt;
import cn.com.xj.plat.evt.QueryPlatUserEvt;
import cn.com.xj.plat.service.DictService;
import cn.com.xj.plat.service.UserService;
import cn.com.xj.plat.util.LoginUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseController {

    @Autowired
    private DictService dictService;

    public Object queryDict(String tableName,String columnName){
        QueryDictEvt queryDictEvt = new QueryDictEvt();
        queryDictEvt.setTableName(tableName);
        queryDictEvt.setColumnName(columnName);
        ServiceResp<Object> resp = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
        if (resp.success()) {
            return resp.getBody();
        }
        return null;
    }


}
