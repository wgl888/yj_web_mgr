<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>添加角色</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/role/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }角色</h3>
                <h5>设置系统里面使用到的角色。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>请勾选角色权限树上的菜单和功能，绑定该角色的用户将能够使用这些功能。</li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/role/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>角色名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.roleName }" placeholder="" class="input-txt"
                           id="roleName" name="roleName" datatype="*2-16" maxlength="16"
                           placeholder="角色名称" nullmsg="角色名称不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        2-16位字符，可由中文、英文、数字及标点符号组成。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>角色权限</label>
                </dt>
                <dd class="opt">
                    <input type="hidden" id="limits" name="limits" value="${item.limits}">
                    <ul class="Jui-silder-list ztree" id="tree">

                    </ul>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>角色描述</label>
                </dt>
                <dd class="opt">
                    <textarea rows="3" id="roleRemark" name="roleRemark"
                              class="input-txt" maxlength="512">${item.roleRemark}</textarea>
                    <p class="notic">
                        最多512位字符。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/role/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
<script type="text/javascript">

    var setting = {
        async: {
            enable: true,
            url: getUrl
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            expandSpeed: "",
            selectedMulti: false
        },
        callback: {
            onAsyncSuccess: onAsyncSuccess,
            onAsyncError: onAsyncError,
            onCheck: onCheck
        }
    };
    $(function () {
        $.fn.zTree.init($("#tree"), setting, zNodes);
        setPageHeight();
    });
    var zNodes = [];
    function getUrl(treeId, treeNode) {
        return "<%=request.getContextPath() %>/menu/tree?scope=menu";
    }
    function onAsyncSuccess(event, treeId, treeNode, msg) {
        if (!msg || msg.length == 0) {
            return;
        }
        if (msg == "-1") {
            parent.layer.msg("对不起，你传入的参数有误，请重试！", {icon: 2, time: 2000});
            return;
        }
        var zTree = $.fn.zTree.getZTreeObj("tree");
        zTree.expandAll(true);//全部展开
        var menus = $("#limits").val();
        if(menus!=null&&menus!=""){
            var node = zTree.getNodeByParam("id",-4);
            node.checked = true;
            zTree.updateNode(node);
            var menusArray = menus.split('#');
            if(menusArray.length>0){
                for(var i=0;i<menusArray.length;i++){
                    if(menusArray[i]!=''&&menusArray[i]!=null){
                        var node = zTree.getNodeByParam("id",menusArray[i]);
                        node.checked = true;
                        zTree.updateNode(node);
                    }
                }
            }
        }

//        var nodes = zTree.getNodes();
//        for (var i = 0; i < nodes.length; i++) { //设置节点展开
//            nodes[i].checked = true;
//            zTree.expandNode(nodes[i], true);//展开一级节点
//        }
//        var pnode = nodes[0];  //初始选中最高节点
//        zTree.selectNode(pnode);
    }

    function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        parent.layer.msg("异步获取数据出现异常！", {icon: 2, time: 2000});
        treeNode.icon = "";
        zTree.updateNode(treeNode);
    }

    //点击目录时调用
    function onCheck(event, treeId, treeNode, clickFlag) {
        //获取选中的节点
        var treeObj = $.fn.zTree.getZTreeObj("tree"),
                nodes = treeObj.getCheckedNodes(true),
                v = "";
        for (var i = 0; i < nodes.length; i++) {
            v += nodes[i].id + "#";
        }
        $("#limits").val(v);
    }

</script>
</body>
</html>
