package cn.com.xj.plat.evt.userGroup;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class EditUserGroupEvt extends BaseEvt implements Serializable {

    private static final long serialVersionUID = 5946594148279628900L;

    @Field(value = "ID" , length = 11 , nullable = false)
    private Long id;
    @Field(value = "用户组名称",length = 64,nullable = true)
    private String groupName;
    @Field(value = "用户组类型",length = 12,nullable = true)
    private String groupType;
    @Field(value = "父ID",length = 11,nullable = true)
    private Long parentId;
    @Field(value = "企业营业执照编码",length = 128,nullable = true)
    private String businessLic;
    @Field(value = "是否可用",length = 1,nullable = true)
    private String isEnable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getBusinessLic() {
        return businessLic;
    }

    public void setBusinessLic(String businessLic) {
        this.businessLic = businessLic;
    }

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }
}
