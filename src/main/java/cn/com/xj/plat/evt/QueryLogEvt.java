package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;


public class QueryLogEvt extends QueryEvt {
	@Field(value = "日志ID" , length = 11, nullable = true)
	private Long id;
	@Field(value = "渠道编码" , length = 128, nullable = true)
    private String channleCode;
	@Field(value = "是否成功" , length = 12, nullable = true)
    private String isSuccess;
	@Field(value = "请求接口" , length = 512, nullable = true)
	private String operaRemark;

	public String getOperaRemark() {
		return operaRemark;
	}

	public void setOperaRemark(String operaRemark) {
		this.operaRemark = operaRemark;
	}

	public String getChannleCode() {
		return channleCode;
	}

	public void setChannleCode(String channleCode) {
		this.channleCode = channleCode;
	}

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
