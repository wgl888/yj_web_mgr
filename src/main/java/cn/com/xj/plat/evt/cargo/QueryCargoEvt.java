package cn.com.xj.plat.evt.cargo;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryCargoEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;


    @Field(value = "ID",length = 11,nullable = true)
    private Long id;
    @Field(value = "所属订单",length = 11,nullable = true)
    private Long orderId;
    @Field(value = "货物名称",length = 128,nullable = true)
    private String cargoName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getCargoName() {
        return cargoName;
    }

    public void setCargoName(String cargoName) {
        this.cargoName = cargoName;
    }
}
