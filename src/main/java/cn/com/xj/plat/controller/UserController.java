package cn.com.xj.plat.controller;

import cn.com.xj.common.enums.YesOrNo;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.common.service.aspect.md5.Md5Util;
import cn.com.xj.plat.evt.*;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.RoleService;
import cn.com.xj.plat.service.UserService;
import cn.com.xj.plat.util.LoginUtils;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @Value("${plat.comon.default-pwd:}")
    private String defaultPassword;

    @RequestMapping("/list")
    public String list(){
        return "/user/list";
    }

    @RequestMapping("/page")
    public String page(QueryPlatUserEvt evt,Model model){
        try{
            BaseFactory.getInstance().page(userService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/user/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        try {
            // 查询角色
            ServiceResp resp = roleService.query(BeanUtil.transBean2Map(new QueryPlatRoleEvt()));
            model.addAttribute("roles",resp.getBody());
            model.addAttribute("defaultPassword",defaultPassword);
            return "/user/edit";
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddPlatUserEvt evt,HttpSession session){
        try {
            evt.setLoginPassword(Md5Util.MD5(defaultPassword));
            evt.setIsEnable(YesOrNo.Y.name());
            ServiceResp resp = BaseFactory.getInstance().add(userService,evt,session);
            return resp;
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


    /**修改密码页面*/
    @RequestMapping("/toEditPwd")
    public String toEditPwd(){
        return "/user/editPwd";
    }


    @ResponseBody
    @RequestMapping(value = {"/doUpdPwd"}, method = RequestMethod.POST)
    public ServiceResp doUpdPwd(UpdPwdEvt updPwdEvt,HttpSession session) {
        try {
            if(!updPwdEvt.getNewPwd1().equals(updPwdEvt.getNewPwd2())){
                return new ServiceResp().error("两次密码输入不一致");
            }
            updPwdEvt.setUserId(LoginUtils.getId(session));
            return userService.updPwd(BeanUtil.transBean2Map(updPwdEvt));
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误,请稍后再试");
        }
    }


    /***进入菜单、按钮编辑页面*/
    @RequestMapping("/toEdit")
    public String toEdit(QueryPlatUserEvt evt,
                         Model model){
        try{
            if(evt.getId()==null){
                return "/common/error";
            }
            // 查询角色
            ServiceResp resp = roleService.query(BeanUtil.transBean2Map(new QueryPlatRoleEvt()));
            model.addAttribute("roles",resp.getBody());
            evt.setLoadRole(YesOrNo.Y.name());
            model.addAttribute("defaultPassword",defaultPassword);
            BaseFactory.getInstance().toEdit(userService,BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/user/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditPlatUserEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(userService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping(value = "doEditNormal",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEditNormal(EditPlatUserEvt evt){
        try {
            return userService.editNormal(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping(value = "resetPwd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp resetPwd(ResetPwdEvt evt){
        try {
            return userService.resetPwd(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

}
