package cn.com.xj.plat.evt.carLength;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryCarLengthEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;

    @Field(length = 11,value = "ID",nullable = true)
    private Long id;
    @Field(value = "是否加载车型信息",nullable = true)
    private boolean loadCarTypes = false;
    @Field(value = "是否加载车型信息",nullable = true)
    private boolean loadCarWeights = false;
    @Field(value = "是否加载车型信息",nullable = true)
    private boolean loadCarVolumes = false;

    public boolean isLoadCarWeights() {
        return loadCarWeights;
    }

    public void setLoadCarWeights(boolean loadCarWeights) {
        this.loadCarWeights = loadCarWeights;
    }

    public boolean isLoadCarVolumes() {
        return loadCarVolumes;
    }

    public void setLoadCarVolumes(boolean loadCarVolumes) {
        this.loadCarVolumes = loadCarVolumes;
    }

    public boolean isLoadCarTypes() {
        return loadCarTypes;
    }

    public void setLoadCarTypes(boolean loadCarTypes) {
        this.loadCarTypes = loadCarTypes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
