package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class AuditWebUserEvt extends BaseEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    @Field(length = 11,nullable = false,value = "要审批的用户ID")
    private Long webUserId;
    @Field(length = 12,nullable = false,value = "审核状态")
    private String auditStatus;
    @Field(length = 512,nullable = true,value = "审批备注")
    private String auditRemark;

    public Long getWebUserId() {
        return webUserId;
    }

    public void setWebUserId(Long webUserId) {
        this.webUserId = webUserId;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }
}
