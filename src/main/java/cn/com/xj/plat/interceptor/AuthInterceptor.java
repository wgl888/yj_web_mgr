package cn.com.xj.plat.interceptor;

import cn.com.xj.plat.util.LoginUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户管理后台登录拦截器
 */

@Component
public class AuthInterceptor implements HandlerInterceptor{

	public static final String LOGIN_PAGE="/login/index";                 //登录页面

	private LoginUtils loginUtil;

	private String loginPath;
	
	public AuthInterceptor(){
		
	}
	
	public AuthInterceptor(String loginPath){
		this.loginPath=loginPath;
	}

	//定义拦截规则
	/**
	 * 在请求处理之前进行调用（Controller方法调用之前）
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,Object obj) throws Exception {
		Boolean isLogin  =  loginUtil.isLogin(request.getSession());
		if(!isLogin){
			response.sendRedirect("/login/index");
			return false;
		}
		return true;
	}

	/**
	 * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
	 */
	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse res,Object obj, ModelAndView m) throws Exception {
		// TODO Auto-generated method stub
	}
	
	/**
	 * 在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）
	 */
	@Override
	public void afterCompletion(HttpServletRequest req,HttpServletResponse res, Object obj, Exception e)throws Exception {
		// TODO Auto-generated method stub
	}

	

}
