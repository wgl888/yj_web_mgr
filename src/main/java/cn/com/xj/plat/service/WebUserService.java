package cn.com.xj.plat.service;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.QueryWebUserCarEvt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("webUserService")
public class WebUserService extends BaseService implements BaseInterface {

    @Autowired
    private RestUtil restUtil;

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/user/count", param);
    }
    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/user/query", param);
    }
    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/user/add", param);
    }
    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/user/edit", param);
    }

    public ServiceResp<Object> batchEditStatus(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/user/batchEditStatus", param);
    }

    public ServiceResp<Object> auditUserAuth(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/user/auditUserAuth", param);
    }

    public ServiceResp queryUserWithCar(QueryWebUserCarEvt evt) {
        return restUtil.post(USER_SERVICE, "web/user/queryUserWithCar",  BeanUtil.transBean2Map(evt));
    }

    public ServiceResp queryUserWithCarCount(QueryWebUserCarEvt evt) {
        return restUtil.post(USER_SERVICE, "web/user/queryUserWithCarCount",  BeanUtil.transBean2Map(evt));
    }

}
