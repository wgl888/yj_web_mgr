package cn.com.xj.plat.evt.insurerRate;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddInsurerRateEvt extends BaseEvt {

    @Field(value = "保险公司ID",length = 11,nullable = false)
    private Long insurerId;
    @Field(value = "费率",length = 11,nullable = false)
    private Float rate;
    @Field(value = "货物类型",length = 32,nullable = true)
    private String cargoType;

    public Long getInsurerId() {
        return insurerId;
    }

    public void setInsurerId(Long insurerId) {
        this.insurerId = insurerId;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getCargoType() {
        return cargoType;
    }

    public void setCargoType(String cargoType) {
        this.cargoType = cargoType;
    }
}
