
	$(document).ready(function(){
		try{
			setPageHeight();
			author();
		}catch(e){

		}
	});

	function setPageHeight(){
		try{
			var iframe = parent.document.getElementById("mainFrame");
			var height = iframe.contentWindow.document.body.clientHeight;
			iframe.height = height + 300;
		}catch(e){

		}
	}


	getRootPath = function(){
		//获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
		var curWwwPath=window.document.location.href;
		//获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
		var pathName=window.document.location.pathname;
		var pos=curWwwPath.indexOf(pathName);
		//获取主机地址，如： http://localhost:8083
		var localhostPaht=curWwwPath.substring(0,pos);
		//获取带"/"的项目名，如：/uimcardprj
		var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
		if(projectName!='wslblhrjh-mgr'&&projectName!='/wslblhrjh-mgr'){
			return localhostPaht;
		}else{
			return(localhostPaht+projectName);
		}
	}

	author = function(){
		$.ajax({
			type : "post",
			url : getRootPath() + "/common/btnForAuthor",
			data : {},
			success : function(data){
				if(data.admin){
					showAll();
				}else if(data.urls!=''&&data.urls!=null){
					var resArray = data.urls.split(",");
					for(var i=0;i<resArray.length;i++){
						var flag = resArray[i];
						if(flag!=null&&flag!=''&&flag!=undefined){
							var obj = $("[author='" + flag + "']");
							if(obj!=null&&obj!=undefined){
								$(obj).show();
								$(obj).removeClass("hide");
								$(obj).css('display','');
							}
						}
					}
				}
			}
		});
	}

	function showAll(){
		$("[author]").each(function(){
			$(this).show();
			$(this).removeClass("hide");
			$(this).css('display','');
		});
	}

	close_layer = function(){
		parent.layer.close(parent.layer.index);
	}

	layer_full = function(url){
		var index = layer.open({
			title:'',
			closeBtn:0,
			type: 2,
			content: url
		});
		layer.full(index);
	}


	layer_default = function(url,title,w,h,dom){
		parent.layer.open({
			type: 2,
			btn:["选择","取消"],
			title: title,
			area:[w+'px',h+'px']
			,content: url,
			yes : function(index,layero){
				var iframeWin = layero.find('iframe');
				$(iframeWin)[0].contentWindow.layer_submit(dom);
			}
		});
	}

	  /**弹框1  url 默认提交和取消两个按钮*/
	  /*
	  	参数解释：
	  	title	标题
	  	url		请求的url
	  	id		需要操作的数据id
	  	w		弹出层宽度（缺省调默认值）
	  	h		弹出层高度（缺省调默认值）
	  */
	  function layer_show(title,url,w,h,dom){
	  	if (title == null || title == '') {
	  		title=false;
	  	};
	  	if (url == null || url == '') {
	  		url="404.html";
	  	};
	  	if (w == null || w == '') {
	  		w=800;
	  	};
	  	if (h == null || h == '') {
	  		h=($(window).height() - 50);
	  	};
	  	if(w.indexOf("%")>-1){//宽高使用%做单位
	  		console.log(w);
	  		var index = parent.layer.open({
		  		type: 2,
				btn:["提交","取消"],
		  		area: [w,h],
		  		shade:0.4,
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});
	  	}else{ //宽高使用px做单位
		  	var index = parent.layer.open({
		  		type: 2,
				btn:["提交","取消"],
		  		area: [w+'px',h+'px'],
		  		shade:0.4,
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});
	  	}
	  }
	  /**弹框2 - 自定义按钮*/
	  /*
	  	参数解释：
	  	title	标题
	    url		请求的url
	  	id		需要操作的数据id
	  	w		弹出层宽度（缺省调默认值）
	  	h		弹出层高度（缺省调默认值）
	  */
	  function layer_show_btns(title,url,w,h,dom,btns){
	  	if (title == null || title == '') {
	  		title=false;
	  	};
	  	if (url == null || url == '') {
	  		url="404.html";
	  	};
	  	if (w == null || w == '') {
	  		w=800;
	  	};
	  	if (h == null || h == '') {
	  		h=($(window).height() - 50);
	  	};
	  	if(w.indexOf("%")>-1){//宽高使用%做单位
		  	var index = parent.layer.open({
		  		type: 2,
				btn: btns,
		  		area: [w,h],
		  		shade:0.4,
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});	
	  	}else{
		  	var index = parent.layer.open({
		  		type: 2,
				btn: btns,
		  		area: [w+'px',h+'px'],
		  		shade:0.4,
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});	
	  	}
	  }
	  /**弹窗3，内容为html元素*/
	  function layer_show_cushtml(title,w,h,htmlContent){
		  if (title == null || title == '') {
			  title=false;
		  };
		  if (htmlContent == null || htmlContent == '') {
			  htmlContent="";
		  };
		  if (w == null || w == '') {
			  w=800;
		  };
		  if (h == null || h == '') {
			  h=($(window).height() - 50);
		  };
		  var index = parent.layer.open({
			  type: 1,
			  area: [w+'px',h+'px'],
			  shade:0.4,
			  title: title,
			  content: '<div class="setlayer" style="margin:10px">'+htmlContent+'</div>'
		  });
	  }
      /**弹窗4，内容为html，自定义按钮*/
      function layer_show_html(title,htmlContent,w,h,dom,btns){
          if (title == null || title == '') {
              title=false;
          };
          if (htmlContent == null || htmlContent == '') {
              htmlContent="";
          };
          if (w == null || w == '') {
              w=800;
          };
          if (h == null || h == '') {
              h=($(window).height() - 50);
          };
          var index = parent.layer.open({
              type: 1,
              btn: btns,
              area: [w+'px',h+'px'],
              shade:0.4,
              title: title,
              content: '<div class="setlayer" style="margin:10px">'+htmlContent+'</div>'
          });
      }
      /**弹窗5，内容为url，默认关闭按钮*/
      function layer_show_url(title,url,w,h){
          if (title == null || title == '') {
              title=false;
          };
          if (w == null || w == '') {
              w=800;
          };
          if (h == null || h == '') {
              h=($(window).height() - 50);
          };
          var index = parent.layer.open({
              type: 2,
              btn: ["关闭"],
              area: [w+'px',h+'px'],
              shade:0.4,
              title: title,
              content: url
          });
      }
      /**弹窗6，内容为函数，传递按钮信息*/
      function layer_show_func_btns(title,url,w,h,btns,Func){
          if (title == null || title == '') {
              title=false;
          };
          if (url == null || url == '') {
              url="404.html";
          };
          if (w == null || w == '') {
              w=800;
          };
          if (h == null || h == '') {
              h=($(window).height() - 50);
          };
		  var index = parent.layer.open({
			  type: 2,
			  btn: btns,
			  area: [w+'px',h+'px'],
			  shade:0.4,
			  title: title,
			  content: url,
			  yes : function(index,layero){
			  	Func();
			  }
		  });
      }
      /**弹窗7，内容为html，传递按钮信息*/
      function layer_show_html_btns(title,htmlContent,w,h,btns,Func){
          if (title == null || title == '') {
              title=false;
          };
          // if (url == null || url == '') {
          //     url="404.html";
          // };
          if (w == null || w == '') {
              w=800;
          };
          if (h == null || h == '') {
              h=($(window).height() - 50);
          };
          var index = parent.layer.open({
              type: 1,
              btn: btns,
              area: [w+'px',h+'px'],
              shade:0.4,
              title: title,
              content: '<div class="setlayer" style="margin:10px">'+htmlContent+'</div>',
              yes : function(index,layero){
                  Func(index);
              }
          });
      }
	  /**关闭弹出框口*/
	  function layer_close(){
		  var index = parent.layer.getFrameIndex(window.name);
		  parent.layer.close(index);
	  }
      /**错误信息1*/
      function layer_error_msg(msg){
          layer.msg(msg, {icon: 2,time:2000});
      }
      /**正确信息1*/
      function layer_success_msg(msg){
          layer.msg(msg, {icon: 1,time:2000});
      }
      /**错误信息2*/
      function layer_error(msg){
          parent.layer.msg(msg, {icon: 2,time:2000,offset:'5px'});
      }
      /**正确信息2*/
      function layer_success(msg){
          parent.layer.msg(msg, {icon: 1,time:2000,offset:'5px'});
      }
      /**错误信息3*/
      function layer_error_msg_offset(msg,offset){
          parent.layer.msg(msg, {icon: 2,time:2000,offset:offset+'px'});
      }
      /**正确信息3*/
      function layer_success_msg_offset(msg,offset){
          parent.layer.msg(msg, {icon: 1,time:2000,offset:offset+'px'});
      }
	  /**提示信息*/
      function show_layermsg(msg,params) {
          parent.layer.msg(msg, params);
      }
      /**关闭弹层*/
      function close_layer(index) {
          parent.layer.close(index);
      }
      /**confirm方法*/
      function layer_confirm(msg,params,Func){
          parent.layer.confirm(msg,params,
              function () {
                  Func();
              }
          );
      };
      /******* **************************************************************************************/
	  function getDvalues (dName,vName){ 
		    var values="";
		    var firstFlag=0;
		    var blackName = document.getElementsByName(dName);   
		    for(var i = 0; i < blackName.length; i++){ 
		  	 var a = blackName[i].value;
		  	   if(firstFlag==0){
		  	    values=a;
		  	    firstFlag++;
		  	   }else{
		   	    values+=","+a; }
		     } 
			     $("#"+vName).val(values);
		}
