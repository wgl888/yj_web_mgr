package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.QueryDictEvt;
import cn.com.xj.plat.evt.QueryLogEvt;
import cn.com.xj.plat.evt.car.AddCarEvt;
import cn.com.xj.plat.evt.car.EditCarEvt;
import cn.com.xj.plat.evt.car.QueryCarEvt;
import cn.com.xj.plat.evt.carLength.QueryCarLengthEvt;
import cn.com.xj.plat.evt.carType.QueryCarTypeEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.*;
import cn.com.xj.plat.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/car")
public class CarController extends BaseController {

    @Autowired
    private DictService dictService;
    @Autowired
    private CarService carService;
    @Autowired
    private CarLengthService carLengthService;

    @RequestMapping("/list")
    public String list(Model model) {
        QueryDictEvt queryDictEvt = new QueryDictEvt();
        queryDictEvt.setTableName("t_web_car");
        queryDictEvt.setColumnName("auditStatus");
        ServiceResp<Object> resp = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
        if (resp.success()) {
            model.addAttribute("auditStatus", resp.getBody());
        }
        return "/car/list";
    }

    @RequestMapping("/page")
    public String page(QueryCarEvt evt, Model model) {
        try {
            BaseFactory.getInstance().page(carService, evt, model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/car/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        formData(model);
        return "/car/edit";
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryCarEvt evt,
                         Model model) {
        try {
            if (evt.getId() == null)return "/common/error";
            formData(model);
            BaseFactory.getInstance().toEdit(carService, BeanUtil.transBean2Map(evt), model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/car/edit";
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryCarEvt evt,
                           Model model){
        return detail(evt,model,"/car/detail");
    }

    @RequestMapping("/toAudit")
    public String toAudit(QueryCarEvt evt,
                           Model model){
        QueryDictEvt queryDictEvt = new QueryDictEvt();
        queryDictEvt.setTableName("t_web_car");
        queryDictEvt.setColumnName("auditRemark");
        ServiceResp<Object> auditRemarks = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
        if (auditRemarks.success()) {
            model.addAttribute("auditRemarks", auditRemarks.getBody());
        }
        return detail(evt,model,"/car/audit");
    }

    @ResponseBody
    @RequestMapping(value = "doAudit", method = RequestMethod.POST)
    public ServiceResp doAudit(EditCarEvt evt, HttpSession session) {
        try {
            return carService.audit(BeanUtil.transBean2Map(evt));
        } catch (Exception e) {
            return new ServiceResp().error("系统错误");
        }
    }

    private String detail(QueryCarEvt evt,Model model,String page){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toDetail(carService,BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return page;
    }


    private void formData(Model model) {


        // 车长度数据
        ServiceResp<Object> resp = carLengthService.
                query(BeanUtil.transBean2Map(new QueryCarLengthEvt()));
        model.addAttribute("carLengths", resp.getBody());

        // 车牌颜色数据
        QueryDictEvt queryDictEvt = new QueryDictEvt();
        queryDictEvt.setTableName("t_web_car");
        queryDictEvt.setColumnName("plateColor");
        ServiceResp<Object> dictResp = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
        if (dictResp.success()) {
            model.addAttribute("colors", dictResp.getBody());
        }

        // 装载重量
        QueryDictEvt weightEvt = new QueryDictEvt();
        weightEvt.setTableName("t_web_car");
        weightEvt.setColumnName("loadWeight");
        ServiceResp<Object> weights = dictService.queryCache(BeanUtil.transBean2Map(weightEvt));
        if (dictResp.success()) {
            model.addAttribute("weights", weights.getBody());
        }

        // 装载体积
        QueryDictEvt volumeEvt = new QueryDictEvt();
        volumeEvt.setTableName("t_web_car");
        volumeEvt.setColumnName("loadVolume");
        ServiceResp<Object> volumes = dictService.queryCache(BeanUtil.transBean2Map(volumeEvt));
        if (dictResp.success()) {
            model.addAttribute("volumes", volumes.getBody());
        }

        // 审核状态
//        queryDictEvt = new QueryDictEvt();
//        queryDictEvt.setTableName("t_web_car");
//        queryDictEvt.setColumnName("auditStatus");
//        ServiceResp<Object> statusResp = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
//        if (statusResp.success()) {
//            model.addAttribute("auditStatus", statusResp.getBody());
//        }
    }

    @ResponseBody
    @RequestMapping(value = "doAdd", method = RequestMethod.POST)
    public ServiceResp doAdd(AddCarEvt evt, HttpSession session) {
        try {
            ServiceResp resp = BaseFactory.getInstance().add(carService, evt, session);
            return resp;
        } catch (Exception e) {
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping(value = "doEdit", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditCarEvt evt) {
        try {
            return BaseFactory.getInstance().doEdit(carService, evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


}
