package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.AddDictEvt;
import cn.com.xj.plat.evt.EditDictEvt;
import cn.com.xj.plat.evt.QueryDictEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/dict")
public class DictController extends BaseController {

    @Autowired
    private DictService dictService;

    @RequestMapping("/list")
    public String list(){
        return "/dict/list";
    }

    @RequestMapping("/page")
    public String page(QueryDictEvt evt,Model model){
        try{
            BaseFactory.getInstance().page(dictService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/dict/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/dict/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddDictEvt evt,HttpSession session){
        try {
            return BaseFactory.getInstance().add(dictService,evt,session);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryDictEvt evt,
                         Model model){
        try{
            if(evt.getId()==null){
                return "/common/error";
            }
            BaseFactory.getInstance().toEdit(dictService,
                    BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/dict/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditDictEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(dictService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


}
