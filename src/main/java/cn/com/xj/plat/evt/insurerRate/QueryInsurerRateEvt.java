package cn.com.xj.plat.evt.insurerRate;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryInsurerRateEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;
    @Field(value = "ID",length = 11,nullable = true)
    private Long id;
    @Field(value = "保险公司ID",length = 11,nullable = true)
    private Long insurerId;

    public Long getInsurerId() {
        return insurerId;
    }

    public void setInsurerId(Long insurerId) {
        this.insurerId = insurerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
