package cn.com.xj.plat.evt.payOrder;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditPayOrderEvt extends BaseEvt {

    @Field(value = "ID",length = 11,nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
