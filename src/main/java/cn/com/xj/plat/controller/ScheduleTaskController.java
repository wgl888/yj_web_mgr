package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.order.QueryOrderEvt;
import cn.com.xj.plat.evt.schedule.QueryScheduleLogEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.ScheduleTaskService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/schedule")
public class ScheduleTaskController extends BaseController {

    @Autowired
    private ScheduleTaskService scheduleTaskService;

    @RequestMapping("/list")
    public String list() {
        return "/schedule/list";
    }

    @RequestMapping("/page")
    public String page(QueryScheduleLogEvt evt, Model model) {
        try {
            BaseFactory.getInstance().page(scheduleTaskService, evt, model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/schedule/page";
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryScheduleLogEvt evt,
                           Model model) {
        try {
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toDetail(scheduleTaskService, BeanUtil.transBean2Map(evt),model);
        } catch (Exception e) {
            return "/common/error";
        }
        return "/schedule/detail";
    }

}
