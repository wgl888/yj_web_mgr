package cn.com.xj.plat.evt.carVolume;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddCarVolumeEvt extends BaseEvt {

    @Field(length = 32 , value = "车类型名称",nullable = false)
    private String volumeName;

    public String getVolumeName() {
        return volumeName;
    }

    public void setVolumeName(String volumeName) {
        this.volumeName = volumeName;
    }
}
