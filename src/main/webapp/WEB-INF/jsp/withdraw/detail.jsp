<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>提现申请详情</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="popup" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 1800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/withdraw/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>查看用户提现申请详细信息</h3>
                <h5>查看体现申请的基本信息、审核信息、银行信息等。</h5>
            </div>
        </div>
    </div>
    <form class="form form-horizontal" id="form">
        <div class="ncap-form-default">
            <jsp:include page="detailPart.jsp" flush="true" />
        </div>
    </form>
</div>
</body>
</html>
