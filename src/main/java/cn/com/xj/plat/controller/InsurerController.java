package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.carLength.QueryCarLengthEvt;
import cn.com.xj.plat.evt.insurer.AddInsurerEvt;
import cn.com.xj.plat.evt.insurer.EditInsurerEvt;
import cn.com.xj.plat.evt.insurer.QueryInsurerEvt;
import cn.com.xj.plat.evt.insurerRate.AddInsurerRateEvt;
import cn.com.xj.plat.evt.insurerRate.QueryInsurerRateEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.CarLengthService;
import cn.com.xj.plat.service.DictService;
import cn.com.xj.plat.service.InsurerRateService;
import cn.com.xj.plat.service.InsurerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/insurer")
public class InsurerController extends BaseController {

    @Autowired
    private InsurerService insurerService;
    @Autowired
    private InsurerRateService insurerRateService;
    @Autowired
    private DictService dictService;

    @RequestMapping("/list")
    public String list(){
        return "/insurer/list";
    }

    @RequestMapping("/page")
    public String page(QueryInsurerEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(insurerService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/insurer/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/insurer/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddInsurerEvt evt, HttpSession session){
        try {
            return BaseFactory.getInstance().add(insurerService,evt,session);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryInsurerEvt evt,
                         Model model){
        try{
            BaseFactory.getInstance().toDetail(insurerService,
                    BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/insurer/detail";
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryInsurerEvt evt,
                         Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toEdit(insurerService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/insurer/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditInsurerEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(insurerService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }



    @RequestMapping("toRate")
    public String toRate(QueryInsurerEvt evt,Model model) {
        BaseFactory.getInstance().toDetail(insurerService, BeanUtil.transBean2Map(evt),model);
        model.addAttribute("cargoTypes",queryDict("t_cargo_type","type"));
        return "/insurer/rate";
    }

    @RequestMapping(value = "queryRate",method = RequestMethod.POST)
    @ResponseBody
    public Object queryRate(QueryInsurerRateEvt evt){
        try {
            ServiceResp<Object> rateResp =
                    insurerRateService.query(BeanUtil.transBean2Map(evt));
            if(rateResp.success()){
                return rateResp.getBody();
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping(value = "doRate",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doRate(AddInsurerRateEvt evt){
        try {
            return insurerRateService.add(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

}
