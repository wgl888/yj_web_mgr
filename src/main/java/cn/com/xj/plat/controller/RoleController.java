package cn.com.xj.plat.controller;

import cn.com.xj.common.enums.YesOrNo;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.AddPlatRoleEvt;
import cn.com.xj.plat.evt.EditPlatRoleEvt;
import cn.com.xj.plat.evt.QueryPlatRoleEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/role")
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;

    @RequestMapping("/list")
    public String list(){
        return "/role/list";
    }

    @RequestMapping("/page")
    public String page(QueryPlatRoleEvt evt,Model model){
        try{
            BaseFactory.getInstance().page(roleService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/role/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/role/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddPlatRoleEvt evt,HttpSession session){
        try {
            ServiceResp resp = BaseFactory.getInstance().add(roleService,evt,session);
            return resp;
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryPlatRoleEvt evt,
                         Model model){
        try{
            if(evt.getId()==null){
                return "/common/error";
            }
            evt.setLoadAuthor(YesOrNo.Y.name());
            BaseFactory.getInstance().toEdit(roleService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/role/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditPlatRoleEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(roleService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


}
