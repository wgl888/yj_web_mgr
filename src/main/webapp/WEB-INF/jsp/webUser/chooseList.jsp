<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>用户列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body class="jui-blue-theme">
<form method="post" id="queryForm" class="form-horizontal clearfix" role="form">
    <input type="text" id="queryByUserName" name="userAlias" style="width: 120px;" placeholder="输入用户姓名"/>
    <input type="text" id="queryByLoginName" name="loginName" style="width: 120px;" placeholder="输入登陆账号"/>
    <button type="button" class="btn btn-xs btn-info"
            onclick="loadPage(1)">搜索
    </button>
    <table class="jui-theme-table table-hover margin-top" style="width: 600px;"
           id="myTable">
        <thead>
        <tr>
            <th width="60px">编号</th>
            <th width="100px">用户姓名</th>
            <th width="100px">登陆账号</th>
            <th width="100px">手机号码</th>
            <th width="80px">车主状态</th>
            <th width="80px">用户状态</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody id="paging_content">

        </tbody>
    </table>
    <div id="paging" style="margin:10px 0 0 0;">

    </div>
</form>

<script type="application/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/plat/webUser/choosePage';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
    }

    choose = function(id,alias){
        $('#userAlias').val(alias);
        $('#userId').val(id);
        $('#chooseUserDiv').html("");
        $('#chooseUserRow').hide();
    }

    $(document).ready(function(){
        loadPage(1);
    });

</script>

</body>
</html>
