package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.invoice.QueryInvoiceEvt;
import cn.com.xj.plat.evt.protocol.AddProtocolEvt;
import cn.com.xj.plat.evt.protocol.EditProtocolEvt;
import cn.com.xj.plat.evt.protocol.QueryProtocolEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.ProtocolService;
import cn.com.xj.plat.util.LoginUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/protocol")
public class ProtocolController extends BaseController {

    @Autowired
    private ProtocolService protocolService;

    @RequestMapping("/list")
    public String list(){
        return "/protocol/list";
    }

    @RequestMapping("/page")
    public String page(QueryProtocolEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(protocolService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/protocol/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/protocol/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddProtocolEvt evt, HttpSession session){
        try {
            evt.setCreateUserName(LoginUtils.getRealName(session));
            return BaseFactory.getInstance().add(protocolService,evt,session);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryProtocolEvt evt,
                           Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toDetail(protocolService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/protocol/detail";
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryProtocolEvt evt,
                         Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toEdit(protocolService,
                    BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/protocol/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditProtocolEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(protocolService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

}
