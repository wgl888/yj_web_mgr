package cn.com.xj.plat.evt.carWeight;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditCarWeightEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(length = 32 , value = "名称",nullable = true)
    private String weightName;
    @Field(value = "车长ID集合",nullable = true)
    private Long[] carLengths;

    public Long[] getCarLengths() {
        return carLengths;
    }

    public void setCarLengths(Long[] carLengths) {
        this.carLengths = carLengths;
    }

    public String getWeightName() {
        return weightName;
    }

    public void setWeightName(String weightName) {
        this.weightName = weightName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
