package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CustomServService extends BaseService implements BaseInterface{

    @Autowired
    private RestUtil restUtil;

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/custom/service/count", param);
    }

    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/custom/service/query", param);
    }

    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/custom/service/add", param);
    }

    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/custom/service/edit", param);
    }

    public ServiceResp<Object> handle(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/custom/service/handle", param);
    }

}
