<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:choose>
    <c:when test="${fn:length(list)>0 }">
        <c:forEach items="${list}" var="item" varStatus="status">
            <tr>
                <td value="${item.id }">${item.id }</td>
                <td>${item.tableName }</td>
                <td>${item.columnName}</td>
                <td>${item.dictValue}</td>
                <td>
                    <label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:180px;"
                           title='${item.dictDesc }' >
                            ${item.dictDesc }
                    </label>
                </td>
                <td>
                    <label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:220px;"
                           title='${item.remark }' >
                            ${item.remark!=null?item.remark:"-"}
                    </label>
                </td>
                <td><input onblur="javascript:setSort(${item.id},this);" type="text" data="${item.sort}"
                           value="${item.sort}" style="width: 50px;"></td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-xs btn-success" author="/common/dict/edit"
                                onclick="edit(${item.id})">编辑
                        </button>
                        <button type="button" class="btn btn-xs btn-danger" author="/common/dict/edit"
                                onclick="del(${item.id});">删除
                        </button>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <tr>
            <td colspan="7">
                暂无符合条件的数据记录
            </td>
        </tr>
    </c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
