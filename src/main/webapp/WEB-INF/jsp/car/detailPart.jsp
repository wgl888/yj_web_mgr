<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<dl class="row">
    <dt class="tit">
        <label>审核状态：</label>
    </dt>
    <dd class="opt">
        <strong style="font-size: 16px;">${item.auditStatusRemark }</strong>
    </dd>
</dl>
<dl class="row">
    <dt class="tit">
        <label>基本信息：</label>
    </dt>
    <dd class="opt">
        所属用户：<a href="javascript:userDetail(${item.userId})" style="color:blue;">${item.userAlias }</a> <br>
        车牌号码：${item.plateFullNo } <br>
        车辆类型：${item.carTypeName } <br>
        车辆长度：${item.carLengthValue }米 <br>
        车牌颜色：${item.plateColorRemark } <br>
        车险到期日：${item.formatAutoInsuranceDate } <br>
        年检到期日：${item.formatAnunalSurveyDate } <br>
    </dd>
</dl>
<dl class="row">
    <dt class="tit">
        <label>车辆照片：</label>
    </dt>
    <dd class="opt">
        <div class="pull-left file-box margin-right">
            <div class="file-preview" filepreview>
                <a target="_blank" href="${item.driverLicenseImg }">
                    <img id="driverLicenseImg_img" src='${item.driverLicenseImg }'>
                </a>
            </div>
        </div>
        <div class="pull-left file-box margin-right">
            <div class="file-preview" filepreview>
                <a target="_blank" href="${item.drivingLicenseImg }">
                    <img id="drivingLicenseImg_img" src="${item.drivingLicenseImg }">
                </a>
            </div>
        </div>
        <div class="pull-left file-box margin-right">
            <div class="file-preview" filepreview>
                <a target="_blank" href="${item.autoInsuranceImg }">
                    <img id="autoInsuranceImg_img" src="${item.autoInsuranceImg }">
                </a>
            </div>
        </div>
        <span class="Validform_checktip"></span>
    </dd>
</dl>
<dl class="row">
    <dt class="tit">
        <label></label>
    </dt>
    <dd class="opt">
        <div class="pull-left file-box margin-right">
            <div class="file-preview" filepreview>
                <a target="_blank" href="${item.anunalSurveyImg }">
                    <img id="anunalSurveyImg_img" src="${item.anunalSurveyImg }">
                </a>
            </div>
        </div>
        <div class="pull-left file-box margin-right">
            <div class="file-preview" filepreview>
                <a target="_blank" href="${item.plateImg }">
                    <img id="plateImg_img" src="${item.plateImg }">
                </a>
            </div>
        </div>
        <div class="pull-left file-box margin-right">
            <div class="file-preview" filepreview>
                <a target="_blank" href="${item.carImg }">
                    <img id="carImg_img" src="${item.carImg }">
                </a>
            </div>
        </div>
        <span class="Validform_checktip"></span>
        <p class="notic">
            <br>
            点击可以查看大图。
        </p>
    </dd>
</dl>

<script type="application/javascript">

    userDetail = function(id){
        layer_full('${pageContext.request.contextPath}/plat/webUser/toDetail?id='+id);
    }

</script>