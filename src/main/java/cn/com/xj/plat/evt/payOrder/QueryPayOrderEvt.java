package cn.com.xj.plat.evt.payOrder;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryPayOrderEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;


    @Field(value = "ID",length = 11,nullable = true)
    private Long id;
    @Field(value = "订单编号",length = 64,nullable = true)
    private String rechargeOrderNo;
    @Field(value = "充值人",length = 64,nullable = true)
    private String userName;
    @Field(value = "订单状态",length = 12,nullable = true)
    private String rechargeStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRechargeOrderNo() {
        return rechargeOrderNo;
    }

    public void setRechargeOrderNo(String rechargeOrderNo) {
        this.rechargeOrderNo = rechargeOrderNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRechargeStatus() {
        return rechargeStatus;
    }

    public void setRechargeStatus(String rechargeStatus) {
        this.rechargeStatus = rechargeStatus;
    }
}
