package cn.com.xj.plat.service;

import cn.com.xj.common.enums.WithdrawStatus;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.webUserWithdraw.EditWithdrawEvt;
import cn.com.xj.plat.evt.webUserWithdraw.LoanEvt;
import cn.com.xj.plat.evt.webUserWithdraw.QueryWithdrawEvt;
import cn.com.xj.plat.evt.webUserWithdraw.WithdrawEvt;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class WebWithdrawService extends BaseService implements BaseInterface {

    @Autowired
    private RestUtil restUtil;

    @Value("${withdraw.reback.url:}")
    private String invokeUrl;

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/withdraw/count", param);
    }
    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/withdraw/query", param);
    }
    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/withdraw/add", param);
    }
    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/withdraw/edit", param);
    }

    public ServiceResp<Object> audit(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/withdraw/audit", param);
    }

    public ServiceResp<Object> del(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"web/withdraw/del", param);
    }

    public synchronized ServiceResp<Object> loan(LoanEvt evt) {
        // 判断是否达到放款条件
        QueryWithdrawEvt queryWithdrawEvt = new QueryWithdrawEvt();
        queryWithdrawEvt.setId(evt.getId());
        ServiceResp<Object> resp = query(BeanUtil.transBean2Map(queryWithdrawEvt));
        if(resp==null||!resp.success()||resp.getBody()==null){
            return new ServiceResp<Object>().error("查询提现信息异常");
        }
        List<Map<String,Object>> items = (List<Map<String,Object>>)resp.getBody();
        if(items==null||items.size()==0){
            return new ServiceResp<Object>().error("无法查询到提现记录");
        }
        Map<String,Object> item = items.size()>0?items.get(0):null;
        // 只有审核通过的状态下才可以放款
        String status = MapUtils.getString(item,"withdrawStatus");
        if(!WithdrawStatus.SHTG.name().equals(status)){
            return new ServiceResp<Object>().error("只有审核通过的记录才能执行放款操作");
        }
        // 检查金额是否正常
        Float amount = MapUtils.getFloat(item,"amount"); // 提现额度
        if(amount<=0.0){
            return new ServiceResp<Object>().error("提现金额格式有误，无法提现");
        }
        String bankUserName = MapUtils.getString(item,"bankUserName"); // 收款人姓名
        String bankNo = MapUtils.getString(item,"bankNo"); // 银行账号
        String mobile = MapUtils.getString(item,"mobile"); // 收款人手机号
        String idCardNo = MapUtils.getString(item,"idCardNo"); // 收款人身份证号
        String orderNumber = UUID.randomUUID().toString().replace("-","");
        if(StringUtils.isBlank(bankUserName)||
                StringUtils.isBlank(bankNo)||
                StringUtils.isBlank(mobile)||
                StringUtils.isBlank(idCardNo)||
                amount==null||amount<=0||StringUtils.isBlank(invokeUrl)){
            return new ServiceResp<Object>().error("提现信息不完成或信息错误");
        }
        // 达到条件调用体现接口放款
        WithdrawEvt withdrawEvt = new WithdrawEvt();
        withdrawEvt.setAccountName(bankUserName);
        withdrawEvt.setAmount(amount);
        withdrawEvt.setBankCardNo(bankNo);
        withdrawEvt.setMobiles(mobile);
        withdrawEvt.setiDCardNo(idCardNo);
        withdrawEvt.setOrderNumber(orderNumber);
        withdrawEvt.setInvokeUrl(invokeUrl);
        ServiceResp<Object> withdrawResp =
                restUtil.post(OUT_INT_SERVICE,"out/int/withdraw",BeanUtil.transBean2Map(withdrawEvt));
        if(!withdrawResp.success()){
            return new ServiceResp<Object>().error("提现接口调用异常");
        }
        // 修改状态
        EditWithdrawEvt editWithdrawEvt = new EditWithdrawEvt();
        editWithdrawEvt.setId(evt.getId());
        editWithdrawEvt.setWithdrawStatus(WithdrawStatus.TXZ.name());
        editWithdrawEvt.setOrderNo(orderNumber);
        ServiceResp<Object> editForLoanResp =
                restUtil.post(USER_SERVICE,"web/withdraw/editForLoan",BeanUtil.transBean2Map(editWithdrawEvt));
        if(!editForLoanResp.success()){
            return new ServiceResp<Object>().error("警告，提现发起成功，状态修改失败。");
        }
        return new ServiceResp<Object>().success("");
    }
}
