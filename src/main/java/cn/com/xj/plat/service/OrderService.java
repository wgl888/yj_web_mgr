package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.order.DelOrderEvt;
import cn.com.xj.plat.evt.order.EditOrderEvt;
import cn.com.xj.plat.evt.order.OrderEvt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class OrderService extends BaseService implements BaseInterface{

    @Autowired
    private RestUtil restUtil;

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/order/count", param);
    }

    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/order/query", param);
    }

    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/order/add", param);
    }

    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/order/edit", param);
    }

    public ServiceResp<Object> assignOrder(Map<String, Object> param) {
        return restUtil.post(ORDER_SERVICE,"web/order/assignOrder", param);
    }

    public ServiceResp resetOrder(OrderEvt evt) {
        return restUtil.post(ORDER_SERVICE, "web/order/resetOrder", BeanUtil.transBean2Map(evt));
    }

    public ServiceResp cancleOrder(DelOrderEvt evt) {
        return restUtil.post(ORDER_SERVICE, "web/order/cancleOrder", BeanUtil.transBean2Map(evt));
    }

    public ServiceResp confirmReceipt(EditOrderEvt evt) {
        return restUtil.post(ORDER_SERVICE, "web/order/confirmReceipt", BeanUtil.transBean2Map(evt));
    }

}
