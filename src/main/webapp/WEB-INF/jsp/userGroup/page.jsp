<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>${item.id }</td>
				<td>${item.groupName!=null?item.groupName:"-" }</td>
				<td>${item.groupTypeName!=null?item.groupTypeName:'-'}</td>
				<td>${item.businessLic}</td>
				<td>${item.isEnable=='Y'?'<strong style="color:green;">可用</strong>':'<strong style="color:red;">不可用</strong>'}</td>
				<td>
					<button type="button" class="btn btn-xs btn-success hide"
							author="/web/user/group/edit"
						onclick="edit(${item.id},${item.parentId})">编辑</button>
					<button type="button" class="btn btn-xs btn-danger hide"
							author="/web/user/group/edit"
							onclick="del(${item.id});">删除</button>
				</td>
		   </tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="8">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">