<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <jsp:include page="../common/common.jsp" flush="true">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="chosenMultiple" value="true"/>
        <jsp:param name="jqueryUI" value="true" />
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="background-color: #fff;">
<form action="${pageContext.request.contextPath}/group/${item.id==null?'doAdd':'doEdit' }.htm"
      method="post" class="form form-horizontal" id="form">
    <input type="hidden" id="superId" name="superId" value="${item.parentId }">
    <input type="hidden" id="id" name="id" value="${item.id }">
    <input type="hidden" id="groupType" name="groupType" value="${groupType}">
    <table class="jui-theme-table table-hover margin-top">
        <tr>
            <td width="30%">
                <label class="control-label no-padding-right">
                    <span style="color:red;">*</span>${groupType==1?'用户组':'扩展用户组'}名称：
                </label>
            </td>
            <td style="text-align:left">
                <input type="text" value="${item.groupName }"
                       id="groupName" name="groupName" datatype="*2-16" maxlength="16"
                       placeholder="${groupType=='1'?'用户组':'扩展用户组'}名称" nullmsg="${groupType=='1'?'部门':'企业'}名称不能为空"></td>
        </tr>
        <tr style="display: none">
            <td width="30%">
                <label class="control-label no-padding-right">
                    省市地区：
                </label>
            </td>
            <td>
                <select onchange="getCitys(this);" name="provinceCode" style="width: 25%;">
                    <c:forEach items="${provinces}" var="province">
                        <option ${item.provinceCode==province.provinceCode?'selected':''} value="${province.provinceCode}">${province.provinceName}</option>
                    </c:forEach>
                </select>省
                <select id="city" onchange="getAreas(this);" name="cityCode" style="width: 25%;">
                    <c:forEach items="${citys}" var="city">
                        <option ${item.cityCode==city.cityCode?'selected':''} value="${city.cityCode}">${city.cityName}</option>
                    </c:forEach>
                </select>市

                <%--<select id="area" name="areaCode" style="width: 25%;">
                    <c:forEach items="${areas}" var="area">
                        <option ${item.areaCode==area.areaCode?'selected':''} value="${area.areaCode}">${area.areaName}</option>
                    </c:forEach>
                </select>区--%>

            </td>
        </tr>
        <tr>
            <td width="30%">
                <label class="control-label no-padding-right">
                    备注：
                </label>
            </td>
            <td style="text-align:left"><textarea rows="3" cols="30" name="groupRemark"
                          onchange="this.value=this.value.substring(0, 170)" onkeydown="this.value=this.value.substring(0, 170)" onkeyup="this.value=this.value.substring(0, 170)">${item.groupRemark}</textarea></td>
        </tr>
        ${extendPage}
    </table>
</form>


<script type="text/javascript">

    /** 弹出框自带的提交按钮回调 */
    function layer_submit(dom){
        /**参数验证*/
        var groupType = $("#groupType").val();
        if($("#groupName").val().trim()==''){
            parent.layer.msg("请输入"+(groupType==1?'用户组':'自定义用户组')+"名称", {icon: 2,time:2000});
            $("#groupName").focus();
            return;
        }
        $.ajax({
            type : "post",
            traditional:true,
            url : $('#form').attr("action"),
            data : $('#form').serialize(),
            success : function(data){
                var json = JSON.parse(data);
                if(json.retCode == 0){
                    parent.layer.msg(json.retMsg, {icon: 1,time:2000});
                    layer_close();
                    dom.layerCallBack();
                }else{
                    parent.layer.msg(json.retMsg, {icon: 2,time:2000});
                }
            }
        });
    }
    //根据省份获取城市
    getCitys = function (dom) {
        var provinceCode = $(dom).val();
        $.ajax({
            type : "post",
            url: "${pageContext.request.contextPath}/group/queryCityByPCode",
            data:{'provinceCode':provinceCode},
            success : function (data) {
                var json = JSON.parse(data);
                var options = '';
                $.each(json.retData, function (index, city) {;
                    var cityName = city.cityName;
                    var cityCode = city.cityCode;
                    options += '<option value='+cityCode+'>'+cityName+'</option>';
                });
                $("#city").empty();
                $("#city").append(options);
            },
//            complete:function () {
//                getAreas($("#city"));
//            }
        });
    }
    <%--getAreas = function (dom) {--%>
        <%--var cityCode = $(dom).val();--%>
        <%--$.ajax({--%>
            <%--type : "post",--%>
            <%--url: "${pageContext.request.contextPath}/group/queryAreaByCCode",--%>
            <%--data:{'cityCode':cityCode},--%>
            <%--success : function (data) {--%>
                <%--var json = JSON.parse(data);--%>
                <%--var options = '';--%>
                <%--$.each(json.retData, function (index, area) {;--%>
                    <%--var areaName = area.areaName;--%>
                    <%--var areaCode = area.areaCode;--%>
                    <%--options += '<option value='+areaCode+'>'+areaName+'</option>';--%>
                <%--});--%>
                <%--$("#area").empty();--%>
                <%--$("#area").append(options);--%>
            <%--}--%>
        <%--});--%>
    <%--}--%>

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
