package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class EditPlatMenuEvt extends BaseEvt implements Serializable {

    private static final long serialVersionUID = 5946594148279628900L;

    @Field(length = 11,value = "菜单ID",nullable = false)
    private Long id;
    @Field(length = 256,value = "菜单名称",nullable = true)
    private String menuName;
    @Field(length = 11,value = "排序号",nullable = true)
    private Integer sort;
    @Field(length = 256,value = "菜单图标",nullable = true)
    private String menuImg;
    @Field(length = 256,value = "菜单访问链接",nullable = true)
    private String menuUrl;
    @Field(length = 11,value = "是否本地菜单",nullable = true)
    private Integer isLocal;
    @Field(length = 20,value = "打开方式",nullable = true)
    private String openType;
    @Field(length = 12,value = "状态",nullable = true)
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getMenuImg() {
        return menuImg;
    }

    public void setMenuImg(String menuImg) {
        this.menuImg = menuImg;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Integer getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(Integer isLocal) {
        this.isLocal = isLocal;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

}
