package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("userService")
public class UserService extends BaseService implements BaseInterface{

    @Autowired
    private RestUtil restUtil;

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/user/count", param);
    }
    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/user/query", param);
    }
    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/user/add", param);
    }
    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/user/edit", param);
    }

    public ServiceResp<Object> updPwd(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/user/updPwd", param);
    }

    public ServiceResp<Object> editNormal(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/user/editNormal", param);
    }

    public ServiceResp<Object> resetPwd(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/user/resetPwd", param);
    }

}
