<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style type="text/css">
	li{
		cursor:pointer;
	}
</style>
<body class="jui-blue-theme">
<div id="navbar" class="navbar navbar-default">
	<script type="text/javascript">
		try{ace.settings.check('navbar' , 'fixed')}catch(e){}
	</script>
	<div class="navbar-container clearfix no-padding" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="#" class="navbar-brand">
				<img class="navbar-logo" src="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/logo2.png?t=es4eg">
				易路通无车承运人管理平台
			</a>
		</div>
		<%--<div class="navbar-buttons navbar-header pull-left jui-navbar" role="navigation">--%>
			<%--<div class="navbarbox">--%>
				<%--<ul class="nav ace-nav" id="dhl_">--%>
					<%--<li id="menuHome" url="${firstMenu.menuUrl}" isLocal="${firstMenu.isLocal}"--%>
						<%--menuImg="${firstMenu.menuImg}"><a>首页</a></li>--%>
					<%--<c:forEach items="${menus}" var="menu" >--%>
					<%--<li id="${menu.menuCode}" url="${menu.firstMenu.menuUrl}" isLocal="${menu.isLocal}"--%>
						<%--menuImg="${menu.menuImg}" data="&mytmenu=${menu.menuCode}">--%>
						<%--<a>${menu.menuName }</a>--%>
					<%--</li>--%>
					<%--</c:forEach>--%>
				<%--</ul>--%>
			<%--</div>--%>
		<%--</div>--%>
		<div class="theme-handover pull-right">
			<a data-toggle="dropdown" class="dropdown-toggle">
				<i class="ace-icon fa fa-dropbox"></i>
			</a>
			<div class="theme-handover-content">
				<a class="theme-handover-item" onClick="JuiTheme('jui-blue-theme')"><img src="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/theme4.png?t=es4eg"></a>
				<a class="theme-handover-item" onClick="JuiTheme('jui-linghtgreen-theme')"><img src="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/theme2.png?t=es4eg"></a>
				<a class="theme-handover-item" onClick="JuiTheme('jui-black-theme')"><img src="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/theme3.png?t=es4eg"></a>
				<a class="theme-handover-item" onClick="JuiTheme('jui-science-theme')"><img src="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/theme1.png?t=es4eg"></a>
				<a class="theme-handover-item" onClick="JuiTheme('jui-black2-theme')"><img src="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/theme5.png?t=es5i5"></a>
			</div>
		</div>
		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav user-nav">
				<li>
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<img class="nav-user-photo" src="<%=request.getContextPath()%>/myResources/manager/plat/assets/avatars/avatar3.png?t=es4eg&t=erzyu" alt="Jason's Photo" />
						<span class="user-info">
							<small>您好！</small>
							${loginName }
						</span>
						<i class="ace-icon fa fa-caret-down"></i>
					</a>
					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li><a href="javascript:updPwd();"> <i
								class="ace-icon fa fa-cog"></i> 修改密码 </a>
						</li>
						<li class="divider"></li>
						<li><a href="<%=request.getContextPath() %>/login/logout"> <i
								class="ace-icon fa fa-power-off"></i> 注销 </a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript">
    <%--修改密码、退出等--%>
    function updPwd() {
		$('#mainFrame').attr("src","<%=request.getContextPath() %>/user/toEditPwd");
    }
</script>
</body>
</html>