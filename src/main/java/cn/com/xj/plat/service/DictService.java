package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DictService extends BaseService implements BaseInterface{

    @Autowired
    private RestUtil restUtil;

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"common/dict/count", param);
    }

    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"common/dict/query", param);
    }

    public ServiceResp<Object> queryCache(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"common/dict/queryCache", param);
    }

    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"common/dict/add", param);
    }

    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"common/dict/edit", param);
    }



}
