package cn.com.xj.plat.evt.payOrder;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddPayOrderEvt extends BaseEvt {
    
    @Field(value = "订单编号",length = 32,nullable = false)
    private String orderNo;
    @Field(value = "支付订单号",length = 64,nullable = false)
    private String payOrderNo;
    @Field(value = "支付类型",length = 12,nullable = false)
    private String payType;
    @Field(value = "支付额度",nullable = false)
    private Float amount;
    @Field(value = "支付结果",length = 12,nullable = false)
    private String payResult;
    @Field(value = "支付用户",length = 11,nullable = true)
    private Long userId;
    @Field(value = "支付回执编号",length = 64,nullable = true)
    private String receiptNo;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPayOrderNo() {
        return payOrderNo;
    }

    public void setPayOrderNo(String payOrderNo) {
        this.payOrderNo = payOrderNo;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getPayResult() {
        return payResult;
    }

    public void setPayResult(String payResult) {
        this.payResult = payResult;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }
}
