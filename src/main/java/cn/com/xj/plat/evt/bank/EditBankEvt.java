package cn.com.xj.plat.evt.bank;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditBankEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(value = "银行名称",length = 64,nullable = true)
    private String bankName;
    @Field(value = "银行背景颜色",length = 12,nullable = true)
    private String bankColor;
    @Field(value = "银行图标路径",length = 1024,nullable = true)
    private String bankIcon;
    @Field(value = "是否可用",length = 1,nullable = true)
    private String isEnable;

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankColor() {
        return bankColor;
    }

    public void setBankColor(String bankColor) {
        this.bankColor = bankColor;
    }

    public String getBankIcon() {
        return bankIcon;
    }

    public void setBankIcon(String bankIcon) {
        this.bankIcon = bankIcon;
    }
}
