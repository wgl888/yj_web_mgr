package cn.com.xj.plat.evt.protocol;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddProtocolEvt extends BaseEvt {

    @Field(value = "协议名称",length = 64,nullable = false)
    private String protocolName;
    @Field(value = "协议编码",length = 64,nullable = false)
    private String protocolCode;
    @Field(value = "协议内容",nullable = false)
    private String content;
    @Field(value = "协议备注",length = 1024,nullable = true)
    private String remark;
    @Field(value = "创建人姓名",length = 32,nullable = true)
    private String createUserName;

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    public String getProtocolCode() {
        return protocolCode;
    }

    public void setProtocolCode(String protocolCode) {
        this.protocolCode = protocolCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
