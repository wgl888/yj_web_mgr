<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>编辑合作银行</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="plupload" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/bank/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }合作银行</h3>
                <h5>设置合作银行，前台用户仅能添加已配置的银行。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/bank/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <input type="hidden" id="editIds" value="${item.ids }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>银行名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankName }" placeholder="" class="input-txt"
                           id="bankName" name="bankName" datatype="*1-32" maxlength="32"
                           placeholder="请输入类型名称" nullmsg="类型名称不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入银行名称，例如：工商银行。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>银行图标</label>
                </dt>
                <dd class="opt">
                    <div class="pull-left file-box margin-right">
                        <div class="file-preview" filepreview>
                            <img id="bankIcon_img" src="${item!=null?item.bankIcon:''}">
                            <div class="file-input" id="bankIcon_file" ></div>
                        </div>
                        <input type="hidden" id="bankIcon_hid" name="bankIcon" value="${item.bankIcon }">
                        <div id="bankIcon_del" class='delete' onclick='deleteFile("bankIcon")' style="display: ${item==null?'none':''}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    </div>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请上传jpg,png格式的图片,建议尺寸:64*64。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>是否可用</label>
                </dt>
                <dd class="opt">
                    <input type="radio" ${(item.isEnable=='Y'||item.isEnable==null)?'checked':''} class="ace" name="isEnable" value="Y"/>
                    <span class="lbl">&nbsp;可用</span>
                    &nbsp;&nbsp;
                    <input type="radio" ${item.isEnable=='N'?'checked':''} class="ace" name="isEnable" value="N"/>
                    <span class="lbl">&nbsp;不可用</span>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        选择合作银行启用状态，客户端只能看到可用的合作银行。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_choose_role">
                <dt class="tit">
                    <label><em>*</em>背景颜色</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankColor }" placeholder="" class="input-txt"
                           id="bankColor" name="bankColor" datatype="*1-12" maxlength="12"
                           placeholder="请输入背景颜色" nullmsg="背景颜色不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入颜色编码，例如:#FFFFFF，参照网址：<a href="http://www.114la.com/other/rgb.htm" target="_blank">点击参考</a>
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/bank/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

        var ids = $('#editIds').val();
        if(ids!=null&&ids!=""){
            var array = ids.split(',');
            for(var i=0;i<array.length;i++){
                $('#length_'+array[i]).attr("checked",true);
            }
        }

    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>

<script type="application/javascript">

    var uploadUrl = '<%=request.getContextPath() %>/upload/uploadImg';
    var ids = new Array("bankIcon_file");

    $.each(ids,function(i,n){
        var self = this.toString();
        var uploader = new plupload.Uploader({
            browse_button : self, //触发文件选择对话框的按钮，为那个元素id
            url : uploadUrl ,//服务器端的上传页面地址
            max_file_size: '5mb',//限制为2MB
            filters: [{title: "Image files",extensions: "jpg,png"}]//图片限制
        });

        //在实例对象上调用init()方法进行初始化
        uploader.init();
        //绑定各种事件，并在事件监听函数中做你想做的事
        uploader.bind('FilesAdded',function(uploader,files){
            var loading = "<%=request.getContextPath() %>/myResources/manager/plat/images/imgloading1.gif";
            $("#"+self).parent().find('img').attr('src',loading);
            uploader.start();
        });

        uploader.bind('FileUploaded', function (uploader, file, responseObject) {
            var rs = responseObject.response;
            if(rs==''||rs==null||rs*1==-1*1){
                parent.layer.open({
                    title: '失败',
                    content: '上传失败，请稍后再试。'
                });
            }
            $("#"+self).parent().find('img').attr('src',responseObject.response);
            var hid = self.split("_")[0] + "_hid";
            $("#"+hid).val(responseObject.response);
            var del = self.split("_")[0] + "_del";
            $("#"+del).show();
        });

    });

</script>

</body>
</html>
