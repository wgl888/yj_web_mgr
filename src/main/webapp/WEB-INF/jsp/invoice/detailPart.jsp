<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<dl class="row">
    <dt class="tit">
        <label>发票状态：</label>
    </dt>
    <dd class="opt">
        <strong style="font-size: 16px;">
            <jsp:include page="status.jsp">
                <jsp:param name="invoiceStatus" value="${item.invoiceStatus }"/>
            </jsp:include>
        </strong>
        <c:if test="${item.auditRemark!=null&&item.auditRemark!=''}">
            <span style="font-size: 12px;">(${item.auditRemark })</span>
        </c:if>
    </dd>
</dl>
<dl class="row">
    <dt class="tit">
        <label>基本信息：</label>
    </dt>
    <dd class="opt">
        订单编号：${item.orderNo}<br>
        发票类型：<strong>${item.invoiceType=='PP'?'普票':'专票'}</strong><br>
        公司名称：${item.companyName }<br>
        公司税号：${item.itin }<br>
        发票金额：<strong style="color:red;">￥${item.amount }</strong><br>
        申请时间：<jsp:useBean id="createTime" class="java.util.Date"/>
        <c:set target="${createTime}" property="time" value="${item.createTime}"/>
        <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/><br>
        <c:if test="${item.invoiceType== 'ZP'}">
            开户银行：${item.bankName}<br>
            银行账号：${item.bankAccount}<br>
            公司地址：${item.address}<br>
            联系电话：${item.tel}
        </c:if>
    </dd>
</dl>
<dl class="row">
    <dt class="tit">
        <label>物流信息：</label>
    </dt>
    <dd class="opt">
        　收货人：${item.deliveryName}<br>
        联系方式：${item.deliveryPhone}<br>
        收货地址：${item.deliveryAddress }<br>
        物流费用：<strong style="color:red;">￥${item.logisticeCost }</strong><br>
        物流编码：${item.logisticeCode!=null?item.logisticeCode:'-' }<br>
        物流单号：${item.logisticeNo!=null?item.logisticeNo:'-' }
    </dd>
</dl>
<c:if test="${fn:length(logisticsLst)>0 }">
    <dl class="row">
        <dt class="tit">
            <label>物流跟踪：</label>
        </dt>
        <dd class="opt">
            <c:forEach items="${logisticsLst}" var="item" varStatus="status">
                ${item.time} &nbsp;&nbsp;&nbsp; ${item.content } </br>
            </c:forEach>
        </dd>
    </dl>
</c:if>
<c:if test="${item.auditUserName!=null&&item.auditUserName!=''}">
    <dl class="row">
        <dt class="tit">
            <label>审核信息：</label>
        </dt>
        <dd class="opt">
            　审核人：${item.auditUserName}<br>
            审核时间：<jsp:useBean id="auditTime" class="java.util.Date"/>
            <c:set target="${auditTime}" property="time" value="${item.auditTime}"/>
            <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${auditTime}" type="both"/>
        </dd>
    </dl>
</c:if>
