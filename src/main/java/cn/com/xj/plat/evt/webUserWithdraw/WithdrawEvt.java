package cn.com.xj.plat.evt.webUserWithdraw;

import cn.com.xj.common.param.valid.Field;

// 提现
public class WithdrawEvt {

    @Field(value = "收款人姓名", length = 32, nullable = false)
    private String accountName;
    @Field(value = "金额", nullable = false)
    private Float amount;
    @Field(value = "收款卡号", length = 32, nullable = false)
    private String bankCardNo;
    @Field(value = "手机号", length = 11, nullable = false)
    private String mobiles;
    @Field(value = "身份证号", length = 18, nullable = false)
    private String iDCardNo;
    @Field(value = "订单号", length = 32, nullable = false)
    private String orderNumber;
    @Field(value = "回调地址", length = 1024, nullable = false)
    private String invokeUrl;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public String getiDCardNo() {
        return iDCardNo;
    }

    public void setiDCardNo(String iDCardNo) {
        this.iDCardNo = iDCardNo;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getInvokeUrl() {
        return invokeUrl;
    }

    public void setInvokeUrl(String invokeUrl) {
        this.invokeUrl = invokeUrl;
    }
}
