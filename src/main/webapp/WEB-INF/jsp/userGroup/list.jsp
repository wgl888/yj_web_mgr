<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>菜单列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body class="jui-blue-theme">
<div class="clearfix">
    <div class="jui-view-content"><%--style="margin:0 0 0 210px;"--%>
        <div class="main-content-inner">
            <div class="jui-page">
                <!-- /.page-content -->
                <%--输入框要小的 就直接加jui-mini-control 这个类名style="padding: 10px"--%>
                <div class="page-content " transition>
                    <div class="jui-search-icon"></div>
                    <!-- /.page-form -->
                    <form method="post" id="queryForm" class="form-horizontal clearfix" role="form">
                        <input type="hidden" id="parentId" name="parentId" value="-4">
                        <table class="queryTable">
                            <tr>
                                <td>
                                    <span>用户组名：</span>
                                    <input type="text" class="input-width-160"
                                           id="groupName" name="groupName"/>
                                </td>
                                <td>
                                    <span>类型：</span>
                                    <select class="input-width-160" name="groupType" id="groupType">
                                        <option value="">选择用户组类型...</option>
                                        <c:forEach items="${groupTypes }" var="r">
                                            <option value="${r.dictValue }" ${item.groupType==r.dictValue?'selected':'' }>${r.dictDesc }</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td>
                                    <span>是否可用：</span>
                                    <select id="isEnable" name="isEnable" class="input-width-160">
                                        <option value="">全部</option>
                                        <option value="Y">可用</option>
                                        <option value="N">不可用</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询
                                    </button>
                                    <button onclick="javascript:resetSearch(1);" type="button" class="btn btn-orange inline-block">
                                        重置
                                    </button>
                                </td>
                            </tr>
                            <%--<tr>--%>
                            <%--<td align="center" colspan="4">--%>
                            <%----%>
                            <%--</td>--%>
                            <%--</tr>--%>
                        </table>
                    </form>
                </div>
                <div class="page-content margin-top">
                    <div class="">
                        <button onclick="add();" type="button" class="btn btn-lightgreen hide"
                                author="/web/user/group/add" id="btnInfo">
                            新增
                        </button>
                    </div>
                    <table class="jui-theme-table table-hover margin-top">
                        <thead>
                            <tr>
                                <th style="width: 2%">ID</th>
                                <th style="width: 14%">用户组名称</th>
                                <th style="width: 14%">用户组类型</th>
                                <th style="width: 6%">执照编码</th>
                                <th style="width: 8%">是否可用</th>
                                <th style="width: 12%">操作</th>
                            </tr>
                        </thead>
                        <tbody id="paging_content">

                        </tbody>
                    </table>
                    <div id="paging" style="margin:10px 0 0 0;"></div>
                </div>
            </div>
        </div>
    </div>

    <%--树形区域--%>
    <div class="Jui-silder s1">
        <div class="Jui-silder-view">
            <div class="Jui-silder-head">用户组树</div>
            <div class="Jui-silder-main">
                <ul class="Jui-silder-list ztree" id="tree">
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
</body>
<script type="text/javascript">
    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/userGroup/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
        setPageHeight();
        author();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    function resetSearch() {
        $('#groupName').val('');
        $("#groupType option:first").prop("selected", 'selected');
        $("#isEnable option:first").prop("selected", 'selected');
        loadPage(1);
    }

    add = function () {
        var isMenu = $("#isMenu").val();
        var he;
        if (isMenu == 1) {
            he = 390;
        } else {
            he = 267;
        }
        var pid = $('#parentId').val();
        location.href = '${pageContext.request.contextPath}/userGroup/toAdd?parentId=' + pid + "&isMenu=" + isMenu;
    }

    edit = function (id, parentId) {
        var isMenu = $("#isMenu").val();
        var he;
        if (isMenu == 1) {
            he = 395;
        } else {
            he = 267;
        }
        location.href = '${pageContext.request.contextPath}/userGroup/toEdit?id=' + id
                + "&parentId=" + parentId + "&isMenu=" + isMenu;
    }

    setSort = function (id, ipt) {
        var userGroupOrder = $(ipt).val().trim();
        var oldOrder = $(ipt).attr("data");
        if (userGroupOrder == oldOrder) {
            return;
        }
        if (userGroupOrder == "") {
            return;
        }
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath() %>/userGroup/doEdit",
            data: {"id": id, "sort": userGroupOrder},
            success: function (data) {
                if (data.head.respCode == 0) {
                    parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                    reload(0);
                } else {
                    parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                }
            }
        });
    }


    del = function (id) {
        parent.layer.confirm('确认要删除吗？', function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/userGroup/doEdit",
                data: {"id": id,"status":"D"},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        reload(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    layerCallBack = function () {
        reload(0);
    }

    reload = function (init) {
        $.fn.zTree.init($("#tree"), setting, zNodes);
        loadPage(init);
    }

</script>
<script type="text/javascript">
    $(".Jui-silder-main").css('height', "800px").panel({iWheelStep: 32});//这是滚动插件的用法  需要手动定义高度
    var setting = {
        async: {
            enable: true,
            url: getUrl
        },
        check: {
            enable: false
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            expandSpeed: "",
            selectedMulti: false
        },
        callback: {
            onAsyncSuccess: onAsyncSuccess,
            onAsyncError: onAsyncError,
            onCheck: onCheck,
            onClick: onClick
        }
    };
    $(function () {
        $.fn.zTree.init($("#tree"), setting, zNodes);
    });
    var zNodes = [];
    function getUrl(treeId, treeNode) {
        return "<%=request.getContextPath() %>/userGroup/tree?scope=userGroup";
    }
    function onAsyncSuccess(event, treeId, treeNode, msg) {
        if (!msg || msg.length == 0) {
            return;
        }
        if (msg == "-1") {
            parent.layer.msg("对不起，你传入的参数有误，请重试！", {icon: 2, time: 2000});
            return;
        }
        var zTree = $.fn.zTree.getZTreeObj("tree");
        //    zTree.expandAll(true);//全部展开
        var nodes = zTree.getNodes();
        for (var i = 0; i < nodes.length; i++) { //设置节点展开
            zTree.expandNode(nodes[i], true);//展开一级节点
        }
        var pnode = nodes[0];  //初始选中最高节点
        zTree.selectNode(pnode);
    }
    function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        parent.layer.msg("异步获取数据出现异常！", {icon: 2, time: 2000});
        treeNode.icon = "";
        zTree.updateNode(treeNode);
    }

    //点击目录时调用
    function onCheck(event, treeId, treeNode, clickFlag) {
        //获取选中的节点
        var treeObj = $.fn.zTree.getZTreeObj("tree"),
                nodes = treeObj.getCheckedNodes(true),
                v = "";
        for (var i = 0; i < nodes.length; i++) {
            v += nodes[i].id + "#";
        }
    }

    //点击目录时调用
    function onClick(event, treeId, treeNode, clickFlag) {
        var sid = treeNode.id;
        if (sid * 1 == -1) {
            sid = 0;
        }
        $('#parentId').val(sid);
        loadPage(1);
        author();
    }
</script>
<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function () {
        $(this).parent().hasClass("jui-hide") ? $(this).parent().removeClass("jui-hide") : $(this).parent().addClass("jui-hide");
    })
</script>
<script type="text/javascript">
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
