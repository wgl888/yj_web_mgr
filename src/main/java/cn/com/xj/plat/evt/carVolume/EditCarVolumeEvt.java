package cn.com.xj.plat.evt.carVolume;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditCarVolumeEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(length = 32 , value = "名称",nullable = true)
    private String volumeName;
    @Field(value = "车长ID集合",nullable = true)
    private Long[] carLengths;

    public Long[] getCarLengths() {
        return carLengths;
    }

    public void setCarLengths(Long[] carLengths) {
        this.carLengths = carLengths;
    }

    public String getVolumeName() {
        return volumeName;
    }

    public void setVolumeName(String volumeName) {
        this.volumeName = volumeName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
