<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${param.status == 'DSH'}">
	<span style="color: orange">待审核</span>
</c:if>
<c:if test="${param.status == 'SHTG'}">
	<span style="color: green">审核通过</span>
</c:if>
<c:if test="${param.status == 'SHBTG'}">
	<span style="color: red;">审核不通过</span>
</c:if>
<c:if test="${param.status == 'TXZ'}">
	<span style="color: #9d9d9d">提现中</span>
</c:if>
<c:if test="${param.status == 'TXCG'}">
	<span style="color:green">提现成功</span>
</c:if>
<c:if test="${param.status == 'TXSB'}">
	<span style="color:red">提现失败</span>
</c:if>