<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td  value="${item.id }">${item.id }</td>
				<td>${item.userAlias }</td>
				<td><strong>${item.plateFullNo }</strong></td>
				<td>${item.plateColorRemark }</td>
				<td>${item.carTypeName }</td>
				<td>${item.carLengthValue }米</td>
				<td>${item.loadWeight }吨</td>
				<td>${item.loadVolume }立方米</td>
				<td>

					<c:if test="${item.auditStatus=='DSH'}">
						<span style="color:orange;">待审核</span>
					</c:if>
					<c:if test="${item.auditStatus=='SHTG'}">
						<span style="color:green;">审核通过</span>
					</c:if>
					<c:if test="${item.auditStatus=='SHBTG'}">
						<span style="color:red;">不通过</span>
					</c:if>

				</td>
				<td >
					<div class="btn-group">
						<c:if test="${item.auditStatus == 'DSH'}">
							<button type="button" class="btn btn-xs btn-inverse hide" author="/web/car/audit"
									onclick="audit(${item.id})">审核</button>
						</c:if>
						<button type="button" class="btn btn-xs btn-warning"
								onclick="detail(${item.id})">详情</button>

						<c:if test="${item.auditStatus != 'SHTG'}">
							<button type="button" class="btn btn-xs btn-success hide" author="/web/car/edit"
									onclick="edit(${item.id})">编辑</button>
						</c:if>
						<button type="button" class="btn btn-xs btn-danger hide" author="/web/car/edit"
								onclick="del(${item.id});">删除</button>
					</div>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="8">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
