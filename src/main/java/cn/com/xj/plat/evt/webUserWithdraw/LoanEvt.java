package cn.com.xj.plat.evt.webUserWithdraw;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class LoanEvt extends BaseEvt implements Serializable {

    @Field(value = "提现记录ID", length = 11, nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}