package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.bank.AddBankEvt;
import cn.com.xj.plat.evt.bank.EditBankEvt;
import cn.com.xj.plat.evt.bank.QueryBankEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/bank")
public class BankController extends BaseController {

    @Autowired
    private BankService bankService;

    @RequestMapping("/list")
    public String list(){
        return "/bank/list";
    }

    @RequestMapping("/page")
    public String page(QueryBankEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(bankService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/bank/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/bank/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddBankEvt evt, HttpSession session){
        try {
            return BaseFactory.getInstance().add(bankService,evt,session);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryBankEvt evt,
                         Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toEdit(bankService,
                    BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/bank/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditBankEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(bankService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

}
