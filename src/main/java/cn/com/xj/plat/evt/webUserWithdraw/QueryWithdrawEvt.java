package cn.com.xj.plat.evt.webUserWithdraw;
import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryWithdrawEvt extends QueryEvt implements Serializable{

    @Field(value = "ID", length = 11, nullable = true)
    private Long id;
    @Field(value = "提现状态", length = 12, nullable = true)
    private String withdrawStatus;
    @Field(value = "用户姓名", length = 32, nullable = true)
    private String webUserName;
    @Field(value = "联系电话", length = 11, nullable = true)
    private String webUserPhone;

    public String getWebUserPhone() {
        return webUserPhone;
    }

    public void setWebUserPhone(String webUserPhone) {
        this.webUserPhone = webUserPhone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(String withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public String getWebUserName() {
        return webUserName;
    }

    public void setWebUserName(String webUserName) {
        this.webUserName = webUserName;
    }
}
