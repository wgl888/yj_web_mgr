package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class AddPlatMenuEvt extends BaseEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;
    @Field(length = 11,value = "父节点",nullable = false)
    private Long parentId;
    @Field(length = 256,value = "菜单名称",nullable = false)
    private String menuName;
    @Field(length = 256,value = "菜单图标",nullable = true)
    private String menuImg;
    @Field(length = 256,value = "菜单访问地址",nullable = true)
    private String menuUrl;
    @Field(length = 16,value = "菜单编码",nullable = true)
    private String menuCode;
    @Field(length = 11,value = "是否目录",nullable = false)
    private Integer isMenu;
    @Field(length = 11,value = "是否本地菜单",nullable = false)
    private Integer isLocal;
    @Field(length = 20,value = "打开方式",nullable = true)
    private String openType;
    @Field(length = 11,value = "排序",nullable = true)
    private Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuImg() {
        return menuImg;
    }

    public void setMenuImg(String menuImg) {
        this.menuImg = menuImg;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public Integer getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(Integer isMenu) {
        this.isMenu = isMenu;
    }

    public Integer getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(Integer isLocal) {
        this.isLocal = isLocal;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }
}
