/**
 * Created by yecz on 2017/3/28.
 */
//从cookie中获取主题样式，更换主题body 20170324
function changeBodyClass() {
    var className = getCookie('TclassName');
    if(className!=''){
        $("body").removeClass();
        $("body").addClass(className);
    }
}
window.onload=function(){
    changeBodyClass();
}

//保存cookie
function setCookie(c_name,value,expiredays)
{
    var exdate=new Date()
    exdate.setDate(exdate.getDate()+expiredays)
    document.cookie=c_name+ "=" +escape(value)+
        ((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
}
//获取cookie
function getCookie(c_name)
{
    if (document.cookie.length>0)
    {
        c_start=document.cookie.indexOf(c_name + "=")
        if (c_start!=-1)
        {
            c_start=c_start + c_name.length+1
            c_end=document.cookie.indexOf(";",c_start)
            if (c_end==-1) c_end=document.cookie.length
            return unescape(document.cookie.substring(c_start,c_end))
        }
    }
    return ""
}