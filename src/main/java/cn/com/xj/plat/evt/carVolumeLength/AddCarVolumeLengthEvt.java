package cn.com.xj.plat.evt.carVolumeLength;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddCarVolumeLengthEvt extends BaseEvt {

    @Field(length = 11 , value = "车类型ID",nullable = false)
    private Long carVolumeId;
    @Field(length = 11 , value = "车类型名称",nullable = false)
    private Long carLengthId;

    public Long getCarVolumeId() {
        return carVolumeId;
    }

    public void setCarVolumeId(Long carVolumeId) {
        this.carVolumeId = carVolumeId;
    }

    public Long getCarLengthId() {
        return carLengthId;
    }

    public void setCarLengthId(Long carLengthId) {
        this.carLengthId = carLengthId;
    }

}
