package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.carVolume.AddCarVolumeEvt;
import cn.com.xj.plat.evt.carVolume.EditCarVolumeEvt;
import cn.com.xj.plat.evt.carVolume.QueryCarVolumeEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.CarLengthService;
import cn.com.xj.plat.service.CarVolumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/carVolume")
public class CarVolumeController extends BaseController {

    @Autowired
    private CarVolumeService carVolumeService;
    @Autowired
    private CarLengthService carLengthService;

    @RequestMapping("/list")
    public String list() {
        return "/carVolume/list";
    }

    @RequestMapping("/page")
    public String page(QueryCarVolumeEvt evt, Model model) {
        try {
            BaseFactory.getInstance().page(carVolumeService, evt, model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/carVolume/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/carVolume/edit";
    }


    @RequestMapping(value = "doAdd", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddCarVolumeEvt evt, HttpSession session) {
        try {
            return BaseFactory.getInstance().add(carVolumeService, evt, session);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryCarVolumeEvt evt,
                         Model model) {
        try {
            if (evt.getId() == null) {
                return "/common/error";
            }
            BaseFactory.getInstance().toEdit(carVolumeService, BeanUtil.transBean2Map(evt), model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/carVolume/edit";
    }

    @RequestMapping(value = "doEdit", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditCarVolumeEvt evt) {
        try {
            return BaseFactory.getInstance().doEdit(carVolumeService, evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


}
