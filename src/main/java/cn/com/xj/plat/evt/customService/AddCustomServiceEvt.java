package cn.com.xj.plat.evt.customService;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddCustomServiceEvt extends BaseEvt {

    @Field(value = "订单编号",length = 32,nullable = false)
    private String orderNo;
    @Field(value = "联系人",length = 32,nullable = false)
    private String linkMan;
    @Field(value = "联系电话",length = 11,nullable = false)
    private String contactPhone;
    @Field(value = "问题描述",length = 4000,nullable = false)
    private String problemDesc;
    @Field(value = "处理回复",length = 4000,nullable = true)
    private String replyMsg;
    @Field(value = "状态",length = 12,nullable = false)
    private String serviceStatus;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img1;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img2;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img3;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img4;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img5;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img6;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img7;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img8;
    @Field(value = "售后照片",length = 512,nullable = true)
    private String img9;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getProblemDesc() {
        return problemDesc;
    }

    public void setProblemDesc(String problemDesc) {
        this.problemDesc = problemDesc;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }

    public String getImg5() {
        return img5;
    }

    public void setImg5(String img5) {
        this.img5 = img5;
    }

    public String getImg6() {
        return img6;
    }

    public void setImg6(String img6) {
        this.img6 = img6;
    }

    public String getImg7() {
        return img7;
    }

    public void setImg7(String img7) {
        this.img7 = img7;
    }

    public String getImg8() {
        return img8;
    }

    public void setImg8(String img8) {
        this.img8 = img8;
    }

    public String getImg9() {
        return img9;
    }

    public void setImg9(String img9) {
        this.img9 = img9;
    }
}
