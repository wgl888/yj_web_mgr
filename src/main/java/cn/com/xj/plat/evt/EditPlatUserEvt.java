package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.util.List;

public class EditPlatUserEvt extends BaseEvt{

    @Field(length = 11,nullable = false,value = "登陆账号ID")
    private Long id;

    @Field(length = 16,nullable = true,value = "管理员名称")
    private String userName;

    @Field(length = 128,nullable = true,value = "管理员密码")
    private String loginPassword;

    @Field(length = 11,nullable = true,value = "联系电话")
    private String contractTel;

    @Field(length = 12,nullable = true,value = "是否超级管理员")
    private String isSuper;

    @Field(length = 12,nullable = true,value = "是否启用")
    private String isEnable;

    @Field(value = "角色ID" , nullable = true)
    private List<Long> roles;

    public List<Long> getRoles() {
        return roles;
    }

    public void setRoles(List<Long> roles) {
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getContractTel() {
        return contractTel;
    }

    public void setContractTel(String contractTel) {
        this.contractTel = contractTel;
    }

    public String getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(String isSuper) {
        this.isSuper = isSuper;
    }

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    @Override
    public String toString() {
        return "EditPlatUserEvt{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", loginPassword='" + loginPassword + '\'' +
                ", contractTel='" + contractTel + '\'' +
                ", isSuper='" + isSuper + '\'' +
                ", isEnable='" + isEnable + '\'' +
                '}';
    }
}
