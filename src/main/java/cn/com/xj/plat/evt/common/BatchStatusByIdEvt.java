package cn.com.xj.plat.evt.common;

import cn.com.xj.common.param.valid.Field;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

public class BatchStatusByIdEvt implements Serializable{

    private static final long serialVersionUID = -2349676971723168417L;

    @Field(value = "ID集",nullable = false)
    private Long[] ids;
    @Field(length = 12,value = "状态值",nullable = false)
    private String status;

    public Long[] getIds() {
        return ids;
    }

    public void setIds(Long[] ids) {
        this.ids = ids;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSqlCondition(){
        String result = "";
        if(this.ids!=null&&this.ids.length>0){
            for(Long id : ids){
                result += ids + ",";
            }
            result = StringUtils.substring(result,0,result.length()-1);
        }
        return  result;
    }


}
