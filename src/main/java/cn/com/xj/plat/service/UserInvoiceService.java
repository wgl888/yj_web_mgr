package cn.com.xj.plat.service;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class UserInvoiceService extends BaseService implements BaseInterface {

    @Autowired
    private RestUtil restUtil;

    @Override
    public ServiceResp count(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE, "web/user/invoice/count", param);
    }
    @Override
    public ServiceResp query(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE, "web/user/invoice/query", param);
    }
    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE, "web/user/invoice/add", param);
    }
    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE, "web/user/invoice/edit", param);
    }
    public ServiceResp<Object> del(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE, "web/user/invoice/del", param);
    }
}
