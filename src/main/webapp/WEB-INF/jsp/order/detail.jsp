<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>订单详细信息</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
    <style type="text/css">

        .imgDiv a img{
            border:1px solid #ddd;
            margin: 5px 5px 5px 5px;
            height: 100px;
            width: 100px;
        }

    </style>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="javascript:close_layer();" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>查看货运订单详细信息</h3>
                <h5>查看货运订单的基本信息，货物信息，货主，车主信息，图片信息等。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/role/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>订单状态：</label>
                </dt>
                <dd class="opt">
                    <strong style="font-size: 16px;">
                        <jsp:include page="status.jsp">
                            <jsp:param name="status" value="${item.orderStatus }"/>
                        </jsp:include>
                    </strong>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>基本信息：</label>
                </dt>
                <dd class="opt">
                    订单编号：<strong>${item.orderNo }</strong> <br>
                    货主信息：<a href="javascript:userDetail(${item.userId})" style="color:blue;">${item.userName }</a>/${item.userMobile } <br>
                    货物类型：${item.cargoTypeCh } <br>
                    启运时间：${item.startTime}<strong>至</strong>${item.endTime}
                      <br>
                    发货地址：${item.shippingRegion }&nbsp;${item.shippingAddress } <br>
                    收货地址：${item.deliveryRegion }&nbsp;${item.deliveryAddress } <br>
                　  距离(≈)：${item.distance }米 <br>
                    期望费用：<strong style="color: red;">￥${item.expectCost }</strong><br>
                    发布日期：<jsp:useBean id="createTime" class="java.util.Date"/>
                                <c:set target="${createTime}" property="time" value="${item.createTime}"/>
                                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label>货物清单：</label>
                </dt>
                <dd class="opt">
                    <c:choose>
                        <c:when test="${fn:length(item.cargos)>0 }">
                            <c:forEach items="${item.cargos }" var="item" varStatus="status">
                                [货物名称：<strong>${item.cargoName }</strong>]/[重量：<strong>${item.weight }${item.weightUnit }</strong>]/[体积：<strong>${item.volume}${item.volumeUnit}</strong>] <br>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            没有查询到该订单下的货物信息。
                        </c:otherwise>
                    </c:choose>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label>保险信息：</label>
                </dt>
                <dd class="opt">
                    保险公司：${item.insurer.insurerName } <br>
                    保险费用：<strong style="color:red;">￥${item.insurerCost }</strong> <br>
                    保单编号：${item.policyNo!=null?item.policyNo:'-' }<br>
                </dd>
            </dl>

            <c:if test="${item.orderStatus!='DZP'&&item.orderStatus!='SJJUDAN'}">
                <dl class="row">
                    <dt class="tit">
                        <label>承运人信息：</label>
                    </dt>
                    <dd class="opt">
                        订单承运人：${item.freighterName }/${item.freighterMobile }<br>
                        承运费用：<strong style="color: red;">￥${item.finalCost }</strong><br>
                    </dd>
                </dl>
            </c:if>

            <c:if test="${item.orderStatus=='DZP'||item.orderStatus=='SJJUDAN'}">
                <dl class="row">
                    <dt class="tit">
                        <label>接单信息：</label>
                    </dt>
                    <dd class="opt">

                    </dd>
                </dl>
            </c:if>

            <c:if test="${item.orderStatus!='DZP'}">
                <dl class="row">
                    <dt class="tit">
                        <label>发货照片</label>
                    </dt>
                    <dd class="opt">
                        <div class="imgDiv" style="width: 380px;">
                            <c:if test="${item.deliverImg1 != null }">
                                <a href="${item.deliverImg1 }" target="_blank">
                                    <img src="${item.deliverImg1 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg2 != null }">
                                <a href="${item.deliverImg2 }" target="_blank">
                                    <img src="${item.deliverImg2 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg3 != null }">
                                <a href="${item.deliverImg3 }" target="_blank">
                                    <img src="${item.deliverImg3 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg4 != null }">
                                <a href="${item.deliverImg4 }" target="_blank">
                                    <img src="${item.deliverImg4 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg5 != null }">
                                <a href="${item.deliverImg5 }" target="_blank">
                                    <img src="${item.deliverImg5 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg6 != null }">
                                <a href="${item.deliverImg6 }" target="_blank">
                                    <img src="${item.deliverImg6 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg7 != null }">
                                <a href="${item.deliverImg7 }" target="_blank">
                                    <img src="${item.deliverImg7 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg8 != null }">
                                <a href="${item.deliverImg8 }" target="_blank">
                                    <img src="${item.deliverImg8 }" >
                                </a>
                            </c:if>
                            <c:if test="${item.deliverImg9 != null }">
                                <a href="${item.deliverImg9 }" target="_blank">
                                    <img src="${item.deliverImg9 }" >
                                </a>
                            </c:if>
                        </div>

                    </dd>
                </dl>
            </c:if>
        </div>
    </form>
</div>
<script type="text/javascript">

    userDetail = function(id){
        layer_full('${pageContext.request.contextPath}/plat/webUser/toDetail?id='+id);
    }

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
