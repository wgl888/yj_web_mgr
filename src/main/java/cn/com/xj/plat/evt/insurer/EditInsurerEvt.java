package cn.com.xj.plat.evt.insurer;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditInsurerEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(value = "保险公司名称",length = 16,nullable = true)
    private String insurerName;
    @Field(value = "保险协议*",length = 99999999,nullable = true)
    private String insurerAgreen;
    @Field(value = "排序",length = 12,nullable = true)
    private Integer sort;
    @Field(value = "是否可用",length = 1,nullable = true)
    private String isEnable;

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInsurerName() {
        return insurerName;
    }

    public void setInsurerName(String insurerName) {
        this.insurerName = insurerName;
    }

    public String getInsurerAgreen() {
        return insurerAgreen;
    }

    public void setInsurerAgreen(String insurerAgreen) {
        this.insurerAgreen = insurerAgreen;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
