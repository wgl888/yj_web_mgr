package cn.com.xj.plat.evt.orderQuotes;

import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QuotesOrderEvt implements Serializable {
    private static final long serialVersionUID = 5647057998014312628L;
    @Field(value = "所属订单", length = 11, nullable = false)
    private Long orderId;
    @Field(value = "期望运费", length = 11, nullable = false)
    private Float expectCost;
    @Field(value = "接单司机ID", length = 11, nullable = false)
    private Long freighter;
    @Field(value = "接单状态", length = 11, nullable = true)
    private String quotesStatus;

    public String getQuotesStatus() {
        return quotesStatus;
    }

    public void setQuotesStatus(String quotesStatus) {
        this.quotesStatus = quotesStatus;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Float getExpectCost() {
        return expectCost;
    }

    public void setExpectCost(Float expectCost) {
        this.expectCost = expectCost;
    }

    public Long getFreighter() {
        return freighter;
    }

    public void setFreighter(Long freighter) {
        this.freighter = freighter;
    }
}
