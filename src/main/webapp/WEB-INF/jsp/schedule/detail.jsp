<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/schedule/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>定时任务日志详细信息</h3>
                <h5>查看定时任务详细信息，执行过程。</h5>
            </div>
        </div>
    </div>

    <div class="ncap-form-default">
        <dl class="row">
            <dt class="tit">
                <label>任务名称：</label>
            </dt>
            <dd class="opt">
                ${item.taskName}
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label>执行开始时间：</label>
            </dt>
            <dd class="opt">
                <jsp:useBean id="startTime" class="java.util.Date"/>
                <c:set target="${startTime}" property="time" value="${item.startTime}"/>
                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${startTime}" type="both"/>
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label>执行结束时间：</label>
            </dt>
            <dd class="opt">
                <jsp:useBean id="endTime" class="java.util.Date"/>
                <c:set target="${endTime}" property="time" value="${item.endTime}"/>
                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${endTime}" type="both"/>
            </dd>
        </dl>

        <dl class="row">
            <dt class="tit">
                <label>总共耗时：</label>
            </dt>
            <dd class="opt">
                ${item.timeLong/1000}秒
            </dd>
        </dl>

        <dl class="row" id="dl_choose_role">
            <dt class="tit">
                <label>是否异常：</label>
            </dt>
            <dd class="opt">
                ${item.hasException=='Y'?'<strong style="color:red">是</strong>':'<strong style="color:green">否</strong>'}
            </dd>
        </dl>

        <dl class="row">
            <dt class="tit">
                <label>执行过程：</label>
            </dt>
            <dd class="opt">
                <div style="overflow:auto;height: 450px;width: 800px;">
                    ${item.remark }
                </div>
            </dd>
        </dl>

    </div>
</div>
</body>
</html>
