package cn.com.xj.plat.evt.carType;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddCarTypeEvt extends BaseEvt {

    @Field(length = 32 , value = "车类型名称",nullable = false)
    private String typeName;


    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}
