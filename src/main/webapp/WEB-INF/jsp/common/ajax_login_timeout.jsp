<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript">
    $.ajaxSetup({
        contentType : "application/x-www-form-urlencoded;charset=utf-8",
        complete : function(xhr, textStatus) {
            //$.ts.hideLoading();
            //session timeout
            if (xhr.status == 911) {
                window.location = "<%=request.getContextPath()%>/login/index";//返回应用登录
                return;
            }
        }
    });
</script>
