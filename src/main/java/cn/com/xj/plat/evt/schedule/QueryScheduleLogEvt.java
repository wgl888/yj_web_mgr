package cn.com.xj.plat.evt.schedule;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryScheduleLogEvt extends QueryEvt implements Serializable{

    @Field(value = "ID" , length = 11 , nullable = true)
    private Long id;
    /**任务名称*/
    @Field(value = "taskName",length = 12,nullable = true)
    private String taskName;
    /**是否有异常  Y/N*/
    @Field(value = "hasException",length = 1,nullable = true)
    private String hasException;

    public String getHasException() {
        return hasException;
    }

    public void setHasException(String hasException) {
        this.hasException = hasException;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
