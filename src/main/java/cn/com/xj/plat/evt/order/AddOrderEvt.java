package cn.com.xj.plat.evt.order;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddOrderEvt extends BaseEvt {

    @Field(value = "订单发起人ID",length = 11,nullable = false)
    private Long userId;
    @Field(value = "启运时间",length = 128,nullable = false)
    private String startTime;
    @Field(value = "结束时间",length = 128,nullable = false)
    private String endTime;
    @Field(value = "发货地址",length = 512,nullable = false)
    private String shippingAddress;
    @Field(value = "收货地址",length = 512,nullable = false)
    private String deliveryAddress;
    @Field(value = "距离",nullable = true)
    private Float distance;
    @Field(value = "承保的保险公司ID",length = 11,nullable = false)
    private Long insurerId;
    @Field(value = "承运人ID",length = 11,nullable = false)
    private Long freighter;
    @Field(value = "车辆类型ID",length = 11,nullable = false)
    private Long carTypeId;
    @Field(value = "车辆长度ID",length = 11,nullable = false)
    private Long carLengthId;
    @Field(value = "货主留言",length = 1024,nullable = true)
    private String shipperMessage;
    @Field(value = "订单状态",length = 12,nullable = false)
    private String orderStatus;
    @Field(value = "发货照片1",length = 512,nullable = true)
    private String deliverImg1;
    @Field(value = "发货照片2",length = 512,nullable = true)
    private String deliverImg2;
    @Field(value = "发货照片3",length = 512,nullable = true)
    private String deliverImg3;
    @Field(value = "发货照片4",length = 512,nullable = true)
    private String deliverImg4;
    @Field(value = "发货照片5",length = 512,nullable = true)
    private String deliverImg5;
    @Field(value = "发货照片6",length = 512,nullable = true)
    private String deliverImg6;
    @Field(value = "发货照片7",length = 512,nullable = true)
    private String deliverImg7;
    @Field(value = "发货照片8",length = 512,nullable = true)
    private String deliverImg8;
    @Field(value = "发货照片9",length = 512,nullable = true)
    private String deliverImg9;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Long getInsurerId() {
        return insurerId;
    }

    public void setInsurerId(Long insurerId) {
        this.insurerId = insurerId;
    }

    public Long getFreighter() {
        return freighter;
    }

    public void setFreighter(Long freighter) {
        this.freighter = freighter;
    }

    public Long getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(Long carTypeId) {
        this.carTypeId = carTypeId;
    }

    public Long getCarLengthId() {
        return carLengthId;
    }

    public void setCarLengthId(Long carLengthId) {
        this.carLengthId = carLengthId;
    }

    public String getShipperMessage() {
        return shipperMessage;
    }

    public void setShipperMessage(String shipperMessage) {
        this.shipperMessage = shipperMessage;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getDeliverImg1() {
        return deliverImg1;
    }

    public void setDeliverImg1(String deliverImg1) {
        this.deliverImg1 = deliverImg1;
    }

    public String getDeliverImg2() {
        return deliverImg2;
    }

    public void setDeliverImg2(String deliverImg2) {
        this.deliverImg2 = deliverImg2;
    }

    public String getDeliverImg3() {
        return deliverImg3;
    }

    public void setDeliverImg3(String deliverImg3) {
        this.deliverImg3 = deliverImg3;
    }

    public String getDeliverImg4() {
        return deliverImg4;
    }

    public void setDeliverImg4(String deliverImg4) {
        this.deliverImg4 = deliverImg4;
    }

    public String getDeliverImg5() {
        return deliverImg5;
    }

    public void setDeliverImg5(String deliverImg5) {
        this.deliverImg5 = deliverImg5;
    }

    public String getDeliverImg6() {
        return deliverImg6;
    }

    public void setDeliverImg6(String deliverImg6) {
        this.deliverImg6 = deliverImg6;
    }

    public String getDeliverImg7() {
        return deliverImg7;
    }

    public void setDeliverImg7(String deliverImg7) {
        this.deliverImg7 = deliverImg7;
    }

    public String getDeliverImg8() {
        return deliverImg8;
    }

    public void setDeliverImg8(String deliverImg8) {
        this.deliverImg8 = deliverImg8;
    }

    public String getDeliverImg9() {
        return deliverImg9;
    }

    public void setDeliverImg9(String deliverImg9) {
        this.deliverImg9 = deliverImg9;
    }
}
