<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>定时任务日志管理</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
    <%--按钮权限校验--%>
    <%--<script type="text/javascript" src="${applicationScope.staticFilepath }/js/mgrRight.js"></script>--%>
    <style type="text/css">
        .tdover {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        td {
            vertical-align: middle;
        !important;
        }

        table {
            table-layout: fixed;
        }
    </style>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <div class="page-content">
        <div class="jui-search-icon"></div>
        <!-- /.page-form start-->
        <form method="post" id="queryForm" class="form-horizontal clearfix" role="form">
            <table class="queryTable">
                <tr>
                    <td>
                        <span>任务名称：</span>
                        <input type="text" class="input-width-160"
                               placeholder="输入任务名称"
                               name="taskName" id="taskName"  >
                    </td>
                    <td>
                        <span>是否异常：</span>
                        <select style="width:127px;border-color:#37b19c;margin-left:10px"
                                id="hasException" name="hasException">
                            <option value="">全部</option>
                            <option value="Y">是</option>
                            <option value="N">否</option>
                        </select>
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-success" onclick="javascript:loadPage(1);">
                            <i class="ace-icon glyphicon glyphicon-search"></i>搜索
                        </button>
                        <button type="button" onclick="javascript:resetQuery();" class="btn btn-sm btn-orange">
                            <i class="ace-icon glyphicon glyphicon-repeat"></i>重置
                        </button>
                    </td>
                </tr>
            </table>
        </form>
        <!-- /.page-form end-->
    </div>

    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <%--<div class="">--%>
                <%--<button onclick="javascript:add();"--%>
                        <%--type="button" class="btn btn-lightgreen inline-block">新增--%>
                <%--</button>--%>
            <%--</div>--%>
            <table class="jui-theme-table table-hover margin-top">
                <thead>
                    <tr>
                        <th width="60px">ID</th>
                        <th width="260px">任务名称</th>
                        <th width="160px">开始时间</th>
                        <th width="160px">结束时间</th>
                        <th width="120px">耗时(秒)</th>
                        <th width="120px">是否异常</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">

            </div>
        </div>
    </div>
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/schedule/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
    }

    $(document).ready(function () {
        loadPage(1);
    });

    detail = function (id) {
        location.href = '${pageContext.request.contextPath}/schedule/toDetail?id=' + id;
    }

    resetQuery = function(){
        $('#taskName').val("");
        $("#hasException option:first").prop("selected", 'selected');
        loadPage(1);
    }

</script>
</body>
</html>
