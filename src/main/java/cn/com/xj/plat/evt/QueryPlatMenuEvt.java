package cn.com.xj.plat.evt;
import cn.com.xj.common.param.in.QueryEvt;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

public class QueryPlatMenuEvt extends QueryEvt implements Serializable{

    private Long id;

    private Long parentId;

    @Length( max = 256 , message = "菜单名称长度不能超过256")
    private String menuName;

    @Length( max = 16 , message = "菜单模块编号长度不能超过16")
    private String menuCode;

    private Integer isMenu;

    private Long staffId;

    private String scope;

    private Long creator;

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public Integer getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(Integer isMenu) {
        this.isMenu = isMenu;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

}
