package cn.com.xj.plat.evt.orderQuotes;

import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class CancelOrderQuotesEvt implements Serializable {

    private static final long serialVersionUID = -5312518153031059664L;
    @Field(value = "所属订单", length = 11, nullable = false)
    private Long orderId;
    @Field(value = "接单司机ID", length = 11, nullable = false)
    private Long freighter;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getFreighter() {
        return freighter;
    }

    public void setFreighter(Long freighter) {
        this.freighter = freighter;
    }
}
