package cn.com.xj.plat.evt.car;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryCarEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;

    @Field(length = 11,value = "ID",nullable = true)
    private Long id;
    @Field(length = 11,value = "所属用户",nullable = true)
    private Long userId;
    @Field(length = 32,value = "用户姓名",nullable = true)
    private String userAlias;
    @Field(value = "车牌号全称",length = 16,nullable = true)
    private String plateFullNo;
    @Field(length = 12,nullable = true,value = "车牌号颜色")
    private String plateColor;
    @Field(length = 12 ,nullable = true,value = "审核状态")
    private String auditStatus;

    public String getUserAlias() {
        return userAlias;
    }

    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPlateFullNo() {
        return plateFullNo;
    }

    public void setPlateFullNo(String plateFullNo) {
        this.plateFullNo = plateFullNo;
    }

    public String getPlateColor() {
        return plateColor;
    }

    public void setPlateColor(String plateColor) {
        this.plateColor = plateColor;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }
}
