<%@ page language="java" contentType="text/html; charset=GBK" pageEncoding="GBK" %>
<%@ include file="../common/tags.jsp" %>
<div class="main-content">
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param name="contextPath" value="${contextPath}"  />
        <jsp:param name="iframeHeight" value="true" />
    </jsp:include>
    <%--当前位置信息--%>
    <jsp:include page="breadcrumbs.jsp" />
    <div class="main-content-inner" id="iframeContent">
        <iframe id="mainFrame" onload="javascript:iframeAdaptive(this);"
                width="100%" marginwidth="0"marginheight="0"
                frameborder="0" src=""scrolling="no" >
        </iframe>
    </div>
</div>
<script type="text/javascript">
    //更换主题
    var TclassName = 'jui-linghtgreen-theme';//初始背景
    function JuiTheme(className) {
        TclassName = className;
        //保存主题样式至cookie
        setCookie('TclassName', className, 1);
        changeTheme($("body"), function (obj) {
            obj.removeClass().addClass(className);
        });
    }
    function changeTheme(obj, fun) {
        fun ? fun(obj) : null;
        if (obj.find("iframe").length > 0) {
            for (var i = 0; i < obj.find("iframe").length; i++) {
                changeTheme(obj.find("iframe").eq(i).contents().find('body'), fun)
            }
        }
    }
    //初始化加载样式
    $(function () {
        var className = getCookie('TclassName');
        console.log('className' + className);
        if (className != '' && className != null) {
            changeTheme($("body"), function (obj) {
                obj.removeClass().addClass(className);
            });
        }
    });

</script>
