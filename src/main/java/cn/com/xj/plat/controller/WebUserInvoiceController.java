package cn.com.xj.plat.controller;

import cn.com.xj.plat.evt.userInvoice.QueryUserInvoiceEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.UserInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/plat/webUser/invoice")
public class WebUserInvoiceController extends BaseController {

    @Autowired
    private UserInvoiceService userInvoiceService;


    @RequestMapping(value = "/chooseList",method = RequestMethod.GET)
    public String chooseList(QueryUserInvoiceEvt evt,Model model){
        model.addAttribute("queryEvt",evt);
        return "/webUserInvoice/chooselist";
    }

    @RequestMapping(value = "/choosePage" , method = RequestMethod.POST)
    public String choosePage(QueryUserInvoiceEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(userInvoiceService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/webUserInvoice/choosePage";
    }
}
