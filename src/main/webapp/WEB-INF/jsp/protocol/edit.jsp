<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>编辑协议信息</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="ueditor" value="true"/>
        <jsp:param name="plupload" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/protocol/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }协议信息</h3>
                <h5>设置APP里显示给客户的运输协议、注册协议等协议信息。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/protocol/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>协议名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.protocolName }" placeholder="" class="input-txt"
                           id="protocolName" name="protocolName" datatype="*1-64" maxlength="64"
                           placeholder="请输入协议名称" nullmsg="协议名称不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入协议名称，例如：运输协议。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>协议编码</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.protocolCode }" placeholder="" class="input-txt"
                           id="protocolCode" name="protocolCode" datatype="*1-64" maxlength="64"
                           placeholder="请输入协议编码" nullmsg="协议编码不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入协议编码，协议编码必须唯一，用来查询协议信息。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>协议内容</label>
                </dt>
                <dd class="opt">
                    <textarea id="editor" name="content" style="width: 500px;">
                        ${item.content }
                    </textarea>
                    <script type="text/javascript">
                        var ue=UE.getEditor('editor',{initialFrameWidth: 500,initialFrameHeight: 500,scaleEnabled:true});
                        UE.Editor.prototype._bkGetActionUrl=UE.Editor.prototype.getActionUrl;
                        UE.Editor.prototype.getActionUrl=function(action){
                            if (action == 'uploadimage' ||action== 'uploadscrawl' || action == 'catchimage') {
                                return "${contextPath}richText/upload";
                            }else if (action == 'uploadvideo') {
                                return "${contextPath}richText/upload?action="+action;
                            }else{
                                return this._bkGetActionUrl.call(this, action);
                            }
                        }
                        setPageHeight();
                    </script>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        以富文本的形式设置协议内容。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>备注</label>
                </dt>
                <dd class="opt">
                    <textarea id="remark" name="remark"
                              rows="3" class="input-txt">${item.remark }</textarea>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入协议备注。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/protocol/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

        var ids = $('#editIds').val();
        if(ids!=null&&ids!=""){
            var array = ids.split(',');
            for(var i=0;i<array.length;i++){
                $('#length_'+array[i]).attr("checked",true);
            }
        }

    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>

<script type="application/javascript">

    var uploadUrl = '<%=request.getContextPath() %>/upload/uploadImg';
    var ids = new Array("bankIcon_file");

    $.each(ids,function(i,n){
        var self = this.toString();
        var uploader = new plupload.Uploader({
            browse_button : self, //触发文件选择对话框的按钮，为那个元素id
            url : uploadUrl ,//服务器端的上传页面地址
            max_file_size: '5mb',//限制为2MB
            filters: [{title: "Image files",extensions: "jpg,png"}]//图片限制
        });

        //在实例对象上调用init()方法进行初始化
        uploader.init();
        //绑定各种事件，并在事件监听函数中做你想做的事
        uploader.bind('FilesAdded',function(uploader,files){
            var loading = "<%=request.getContextPath() %>/myResources/manager/plat/images/imgloading1.gif";
            $("#"+self).parent().find('img').attr('src',loading);
            uploader.start();
        });

        uploader.bind('FileUploaded', function (uploader, file, responseObject) {
            var rs = responseObject.response;
            if(rs==''||rs==null||rs*1==-1*1){
                parent.layer.open({
                    title: '失败',
                    content: '上传失败，请稍后再试。'
                });
            }
            $("#"+self).parent().find('img').attr('src',responseObject.response);
            var hid = self.split("_")[0] + "_hid";
            $("#"+hid).val(responseObject.response);
            var del = self.split("_")[0] + "_del";
            $("#"+del).show();
        });

    });

</script>

</body>
</html>
