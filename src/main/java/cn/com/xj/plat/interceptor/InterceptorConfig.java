package cn.com.xj.plat.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter{

    /**
     * 指定静态资源位置，不被拦截
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/myResources/**")
                .addResourceLocations("classpath:/myResources/");
        super.addResourceHandlers(registry);
    }

    /**
     *  重写WebMvcConfigurerAdapter添加自定义拦截器
     * */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/verycode/**")
                .excludePathPatterns("/login/**")
                .excludePathPatterns("/agent**")
                .excludePathPatterns("/error");
        super.addInterceptors(registry);
    }
}
