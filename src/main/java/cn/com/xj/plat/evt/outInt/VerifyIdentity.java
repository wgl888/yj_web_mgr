package cn.com.xj.plat.evt.outInt;

import cn.com.xj.common.param.valid.Field;

/**
 * @author wanggl
 * @version V1.0
 * @Title: PostEvt
 * @Package cn.com.xj.web.app.evt
 * @Description: (通过客户ID 时间段获取未开票列表)
 * @date 2017/8/12 11:18
 */
public class VerifyIdentity {

    @Field(value = "姓名",length = 32,nullable = false)
    private String personName;
    @Field(value = "身份证号",length = 18,nullable = false)
    private String IDCardNo;
    @Field(value = "手机号",length = 11,nullable = false)
    private String mobilePhone;
    @Field(value = "验证码",length = 20,nullable = false)
    private String validCode;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getiDCardNo() {
        return IDCardNo;
    }

    public void setiDCardNo(String IDCardNo) {
        this.IDCardNo = IDCardNo;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getValidCode() {
        return validCode;
    }

    public void setValidCode(String validCode) {
        this.validCode = validCode;
    }
}
