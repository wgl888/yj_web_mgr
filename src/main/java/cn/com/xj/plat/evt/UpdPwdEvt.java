package cn.com.xj.plat.evt;

import cn.com.xj.common.param.valid.Field;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author wanggl
 * @version V1.0
 * @Title: AddBrandEvt
 * @Package com.wgl.springboot.evt.plat
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/4/27 19:48
 */
public class UpdPwdEvt {

    @Field(length = 12,value = "旧密码",nullable = false)
    private String oldPwd;
    @Field(length = 12,value = "新密码",nullable = false)
    private String newPwd1;
    @Field(length = 12,value = "确认新密码",nullable = false)
    private String newPwd2;
    @Field(length = 11,value = "用户ID",nullable = false)
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOldPwd() {
        return oldPwd;
    }

    public void setOldPwd(String oldPwd) {
        this.oldPwd = oldPwd;
    }

    public String getNewPwd1() {
        return newPwd1;
    }

    public void setNewPwd1(String newPwd1) {
        this.newPwd1 = newPwd1;
    }

    public String getNewPwd2() {
        return newPwd2;
    }

    public void setNewPwd2(String newPwd2) {
        this.newPwd2 = newPwd2;
    }

}
