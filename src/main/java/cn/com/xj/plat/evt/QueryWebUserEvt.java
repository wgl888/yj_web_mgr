package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryWebUserEvt extends QueryEvt implements Serializable {

    @Field(value = "ID", length = 11, nullable = true)
    private Long id;
    @Field(value = "登陆账号", length = 16, nullable = true)
    private String loginName;
    @Field(value = "用户姓名", length = 12, nullable = true)
    private String userAlias;
    @Field(value = "认证状态", length = 12, nullable = true)
    private String authStatus;
    @Field(value = "车主状态", length = 12, nullable = true)
    private String carownerStatus;
    @Field(value = "货主状态", length = 12, nullable = true)
    private String shipperStatus;
    @Field(value = "用户手机", length = 11, nullable = true)
    private String userPhone;
    @Field(value = "是否加载车辆信息", length = 1, nullable = true)
    private String loadCar;
    @Field(value = "是否加载银行卡信息", length = 1, nullable = true)
    private String loadBankCard;
    @Field(value = "是否加载常用发票信息", length = 1, nullable = true)
    private String loadUserInvoice;

    public String getLoadUserInvoice() {
        return loadUserInvoice;
    }

    public void setLoadUserInvoice(String loadUserInvoice) {
        this.loadUserInvoice = loadUserInvoice;
    }

    public String getLoadCar() {
        return loadCar;
    }

    public void setLoadCar(String loadCar) {
        this.loadCar = loadCar;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAlias() {
        return userAlias;
    }

    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getCarownerStatus() {
        return carownerStatus;
    }

    public void setCarownerStatus(String carownerStatus) {
        this.carownerStatus = carownerStatus;
    }

    public String getShipperStatus() {
        return shipperStatus;
    }

    public void setShipperStatus(String shipperStatus) {
        this.shipperStatus = shipperStatus;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getLoadBankCard() {
        return loadBankCard;
    }

    public void setLoadBankCard(String loadBankCard) {
        this.loadBankCard = loadBankCard;
    }
}
