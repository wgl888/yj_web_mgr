package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddPlatRoleEvt extends BaseEvt {


    @Field(length = 256 , value = "角色名称",nullable = false)
    private String roleName;
    @Field(length = 512 , value = "角色描述",nullable = true)
    private String roleRemark;
    @Field(length = 1024 , value = "权限ID集合",nullable = false)
    private String limits;

    public String getLimits() {
        return limits;
    }

    public void setLimits(String limits) {
        this.limits = limits;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleRemark() {
        return roleRemark;
    }

    public void setRoleRemark(String roleRemark) {
        this.roleRemark = roleRemark;
    }

    @Override
    public String toString() {
        return "AddPlatRoleEvt{" +
                ", roleName='" + roleName + '\'' +
                ", roleRemark='" + roleRemark + '\'' +
                '}';
    }
}
