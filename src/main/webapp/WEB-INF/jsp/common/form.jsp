<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../common/tags.jsp"%>

<!-- 字体图标样式表 -->
<link href="<%=request.getContextPath()%>/myResources/manager/assets/css/font-awesome.css"
	  media="screen" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/myResources/manager/assets/css/ace-fonts.css"
	  media="screen" rel="stylesheet" type="text/css" />


<script type="text/javascript"src="<%=request.getContextPath()%>/myResources/manager/plat/form/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/layer/1.9.3/layer.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/form/js/jquery-form.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/form/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/form/js/form-common.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/form/js/jquery.validation.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/form/css/index.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/form/css/font-awesome.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/form/css/jquery-ui.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/form/css/perfect-scrollbar.min.css" />