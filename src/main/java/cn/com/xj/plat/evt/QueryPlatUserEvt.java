package cn.com.xj.plat.evt;

import cn.com.xj.common.enums.YesOrNo;
import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryPlatUserEvt extends QueryEvt implements Serializable{

    private static final long serialVersionUID = -2635264464541912275L;

    @Field(length = 11,value = "用户ID",nullable = true)
    private Long id;
    @Field(length = 12,value = "用户名称",nullable = true)
    private String userName;
    @Field(length = 12,value = "登陆账号",nullable = true)
    private String loginName;
    @Field(length = 12,value = "登陆密码",nullable = true)
    private String loginPassword;
    @Field(length = 1,value = "是否加载角色",nullable = true)
    private String loadRole = YesOrNo.N.name();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoadRole() {
        return loadRole;
    }

    public void setLoadRole(String loadRole) {
        this.loadRole = loadRole;
    }
}
