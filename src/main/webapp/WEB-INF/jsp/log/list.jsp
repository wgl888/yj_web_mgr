<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>操作日志列表</title>
    <%--引入公共js、css--%>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true" />
        <jsp:param name="ZUIplugin" value="true" />
        <jsp:param name="cookieutil" value="true" />
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <div class="page-content jui-mini-control" transition>
        <div class="jui-search-icon"></div>
        <form  method="post" id="queryForm"class="form-horizontal jui-form-justify" role="form">
            <table class="queryTable">
                <tr>
                    <td>
                        <span>请求渠道：</span>
                        <input type="text" class="input-width-160"
                               id="channleCode" name="channleCode"/>
                    </td>
                    <td>
                        <span>请求接口：</span>
                        <input type="text" class="input-width-160"
                               id="operaRemark" name="operaRemark"/>
                    </td>
                    <td>
                        <span>是否成功：</span>
                        <select id="isSuccess" name="isSuccess" class="input-width-160">
                            <option value="">全部</option>
                            <option value="Y">成功</option>
                            <option value="N">失败</option>
                        </select>
                    </td>
                    <td>
                        <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询</button>
                        <button onclick="javascript:resetSearch(1);"type="button" class="btn btn-orange inline-block" >重置</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <table class="jui-theme-table table-hover" id="myTable">
                <thead>
                <tr>
                    <th>请求渠道</th>
                    <th>请求描述</th>
                    <th>请求结果</th>
                    <th>耗时(秒)</th>
                    <th>请求时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="paging_content">
                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">
            </div>
        </div>
        <!-- /.page-table end-->
    </div>
    <!-- /.page-content -->
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">
    /** 分页 */
    function loadPage() {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/log/page';
        paging1.paging(1, target);
        initial('#data_total', paging1, 'paging', target);
        setPageHeight();
    }
    $(document).ready(function () {
        loadPage();
    });

    function resetSearch() {
        $('#channleCode').val('');
        $('#operaRemark').val('');
        $("#isSuccess option:first").prop("selected", 'selected');
        loadPage(1);
    }

    toDetail = function(id){
        location.href = '<%=request.getContextPath() %>/log/toDetail?id='+id;
    }

</script>
<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function () {
        $(this).parent().hasClass("jui-hide") ? $(this).parent().removeClass("jui-hide") : $(this).parent().addClass("jui-hide");
    })
</script>
</body>
</html>
