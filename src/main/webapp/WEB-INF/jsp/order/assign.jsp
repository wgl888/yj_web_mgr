<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>指派司机</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
    <style type="text/css">

        .imgDiv a img{
            border:1px solid #ddd;
            margin: 5px 5px 5px 5px;
            height: 100px;
            width: 100px;
        }

    </style>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="javascript:history.go(-1);" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>指派司机</h3>
                <h5>后台运营人员可以协助用户指定司机。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/role/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <input type="hidden" value="${item.userId }" id="orderUserId">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>订单信息：</label>
                </dt>
                <dd class="opt">
                    订单编号：<strong>${item.orderNo }</strong> <br>
                    货主信息：${item.userName }/${item.userMobile } <br>
                    起始地点：${item.shippingRegion }&nbsp;${item.shippingAddress }
                                <strong>至</strong>
                              ${item.deliveryRegion }&nbsp;${item.deliveryAddress }<br>
                    货物类型：${item.cogoTypeCh }<br>
                    期望费用：<strong style="color: red;">￥${item.expectCost }</strong><br>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>货物清单：</label>
                </dt>
                <dd class="opt">
                    <c:choose>
                        <c:when test="${fn:length(item.cargos)>0 }">
                            <c:forEach items="${item.cargos }" var="item" varStatus="status">
                                [货物名称：<strong>${item.cargoName }</strong>]/[重量：<strong>${item.weight }${item.weightUnit }</strong>]/[体积：<strong>${item.volume}${item.volumeUnit}</strong>] <br>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            没有查询到该订单下的货物信息。
                        </c:otherwise>
                    </c:choose>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>已报价司机：</label>
                </dt>
                <dd class="opt">
                    <c:choose>
                        <c:when test="${fn:length(quotesItem)>0 }">
                            <table class="jui-theme-table table-hover margin-top" style="width: 600px;"
                                   id="myTable">
                                <thead>
                                <tr>
                                    <th width="120px">司机姓名</th>
                                    <th width="120px">接单状态</th>
                                    <th width="120px">期望价格</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody id="paging_content">
                                    <c:forEach items="${quotesItem }" var="quotes" varStatus="status">
                                        <tr>
                                            <td>${quotes.freighterName }</td>
                                            <td>
                                                <c:if test="${quotes.quotesStatus=='DSJJIEDAN'}">
                                                    待司机接单
                                                </c:if>
                                                <c:if test="${quotes.quotesStatus=='SJJIEDAN'}">
                                                    司机接单
                                                </c:if>
                                                <c:if test="${quotes.quotesStatus=='SJJUDAN'}">
                                                    司机拒单
                                                </c:if>
                                            </td>
                                            <td style="color:red;">￥${quotes.expectCost}</td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-info"
                                                        onclick="assign(${item.id},'${quotes.id}','${quotes.freighterName }');">指派</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <strong style="color:red;">没有查询到该订单下的接单信息，请耐心等待车主接单。</strong>
                        </c:otherwise>
                    </c:choose>
                    <p class="notic">
                        上面的列表数据为货主指派或从订单池接单的司机列表，只有司机接单状态的记录可以指派。
                    </p>
                </dd>
            </dl>
            <dl class="row" style="display: none;">
                <dt class="tit">
                    <label><em>*</em>选择司机：</label>
                </dt>
                <dd class="opt">
                    <input readonly type="text" value="${item.userAlias }" class="input-txt"
                           id="userAlias" name="userAlias" />
                    <input type="hidden" id="userId" name="userId" value="${item.userId}"
                           datatype="*1-11" maxlength="11" placeholder="请选择用户" nullmsg="用户不能为空" >
                    <button type="button" class="btn btn-xs btn-success"
                            onclick="loadChooseUser();">选择
                    </button>
                </dd>
            </dl>
            <dl class="row" id="chooseUserRow" style="display: none;">
                <dt class="tit">
                    <label><em>*</em>查询司机：</label>
                </dt>
                <dd class="opt">
                    <iframe id="chooseUserDiv" style="border:none;width: 800px;height: 350px;">

                    </iframe>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入查询条件，搜索并选择用户。
                    </p>
                </dd>
            </dl>
            <div class="bot" style="display: none;">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认指派
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/role/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });

    assign = function(orderId,quotesId,name){
        parent.layer.confirm('确认要将订单指派给司机['+ name +']吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                dataType : "JSON",
                url: "<%=request.getContextPath() %>/order/doAssign",
                data: {"orderId":orderId,"quotesId":quotesId},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                            location.href = "<%=request.getContextPath() %>/order/list";
                        });
                    } else {
                        layer_error(data.head.respMsg);
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    loadChooseUser = function () {
        var url = '<%=request.getContextPath() %>/plat/webUser/chooseDriverList?userId=' + $('#orderUserId').val();
        $('#chooseUserDiv').attr('src',url);
        $('#chooseUserRow').show();
    }

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
