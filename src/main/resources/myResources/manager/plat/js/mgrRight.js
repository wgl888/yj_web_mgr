/**
 * Created by yecongzhi on 2017/3/23.
 */
function mgrPermissions() {
    var btns = new Array;
    $("[permiss]").each(function () {
        var btnCode = $(this).attr("permiss");
        btns.push(btnCode);
    });
    if(btns.length>0){
        $.ajax({
            type : "post",
            traditional:true,
            url : "<%=request.getContextPath() %>/auth/mgrRight",
            data : {"btns":btns},
            success : function(data){
                var json = JSON.parse(data);
                for(var i=0;i<btns.length;i++){
                    var btnCode = btns[i];
                    var flag = json[btnCode]
                    $("[permiss]").each(function () {
                        var b = $(this).attr("permiss");
                        if(btnCode==b){
                            if(flag){
                                $(this).show();
                            }else {
                                $(this).hide();
                            }
                        }
                    });
                }
            }
        });
    }
}