<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>车辆管理</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
    <%--按钮权限校验--%>
    <%--<script type="text/javascript" src="${applicationScope.staticFilepath }/js/mgrRight.js"></script>--%>
    <style type="text/css">
        .tdover {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        td {
            vertical-align: middle;
            !important;
        }

        table {
            table-layout: fixed;
        }
    </style>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <div class="page-content">
        <div class="jui-search-icon"></div>
        <!-- /.page-form -->
        <form method="post" id="queryForm" action="${pageContext.request.contextPath}/plat/code/list">
            <table class="queryTable">
                <tr>
                    <td>
                        <span>车牌号：</span>
                        <input type="text" class="input-width-160" placeholder="输入车牌号全称" name="plateFullNo" id="plateFullNo"  >
                    </td>
                    <td>
                        <span>用户姓名：</span>
                        <input type="text" class="input-width-160" placeholder="输入车辆所属用户" name="userAlias" id="userAlias" >
                    </td>
                    <td>
                        <span>审核状态：</span>
                        <select class="input-width-160" name="auditStatus" id="auditStatus" >
                            <option value="">请选择审核状态...</option>
                            <c:forEach items="${auditStatus }" var="item">
                                <option value="${item.dictValue}">${item.dictDesc }</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-success" onclick="javascript:loadPage(1);">
                            <i class="ace-icon glyphicon glyphicon-search"></i>搜索
                        </button>
                        <button type="button" onclick="javascript:reload();" class="btn btn-sm btn-orange">
                            <i class="ace-icon glyphicon glyphicon-repeat"></i>重置
                        </button>
                    </td>
                </tr>
            </table>
        </form>
        <!-- /.page-form -->
    </div>
    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <div class="">
                <button onclick="javascript:add();" author="/web/car/add"
                        type="button" class="btn btn-lightgreen inline-block hide">申请车主
                </button>
            </div>
            <table class="jui-theme-table table-hover margin-top">
                <thead>
                    <tr>
                        <th width="60px">ID</th>
                        <th width="120px">所属用户</th>
                        <th width="100px">车牌号</th>
                        <th width="80px">车牌颜色</th>
                        <th width="80px">车型</th>
                        <th width="80px">车长</th>
                        <th width="100px">装载重量</th>
                        <th width="100px">装载体积</th>
                        <th width="100px">审核状态</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">

            </div>
        </div>
    </div>
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/car/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
        setPageHeight();
        author();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    function resetSearch() {
        $('#tableName').val('');
        $('#columnName').val('');
        loadPage(1);
    }
    add = function () {
        location.href = "${pageContext.request.contextPath}/car/toAdd";
    }
    edit = function (id) {
        location.href = '${pageContext.request.contextPath}/car/toEdit?id=' + id;
    }

    detail = function (id) {
        location.href = '${pageContext.request.contextPath}/car/toDetail?id=' + id;
    }

    audit = function (id) {
        location.href = '${pageContext.request.contextPath}/car/toAudit?id=' + id;
    }

    del = function (id) {
        parent.layer.confirm('确认要删除吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/car/doEdit",
                data: {"id": id, "status": 'D'},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        loadPage(0);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    reload = function(){
        $('#plateFullNo').val("");
        $('#userAlias').val("");
        $("#auditStatus option:first").prop("selected", 'selected');
        loadPage(1);
    }

</script>


<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function(){
        $(this).parent().hasClass("jui-hide")?$(this).parent().removeClass("jui-hide"):$(this).parent().addClass("jui-hide");
    });
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
