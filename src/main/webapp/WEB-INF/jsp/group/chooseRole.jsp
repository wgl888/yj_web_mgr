<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp" flush="true">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="chosenMultiple" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="background-color: #fff;">
<form method="post" class="form form-horizontal" id="form">
    <table id="simple-table" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th width="6%;" style="text-align: center">
                选择
            </th>
            <th width="6%;" style="text-align: center">ID</th>
            <th width="8%;" style="text-align: center">角色名称</th>
            <th width="10%;" style="text-align: center">描述</th>
        </tr>
        </thead>
        <tbody id="paging_content">
        <input type="hidden" value="${roles }" id="roles" name="rolses">
        </tbody>
    </table>
    <div id="paging" style="margin:10px 0 0 0;"></div>
</form>
<input type="hidden" value="${groupId}" id="groupId" name="groupId" >
<script type="text/javascript">

    /** 弹出框自带的提交按钮回调 */
    layer_submit = function(dom){
        var arrayObj = new Array();
        /*
         for (var i = 0; i < len.length; i++) {
         if (len[i].checked && len[i].value != "") {
         console.log(len[i].value);
         arrayObj.push(len[i].value);
         }
         }
         */
        var values=$("input:checkbox[name='choose']");
        for (var i=0;i<values.length;i++){
            if(values[i].checked==true){
                var e = $(values[i]);
                var id = e.parents('tr').children('td').eq(1).text().trim();
                arrayObj.push(id);
            }
        }
        if(arrayObj.length==0){
            layer.msg('请选择角色', {icon:2, time: 1000});
            return;
        }
        var groupId = $('#groupId').val();
        $.ajax({
            type : "post",
            traditional:true,
            url : "${pageContext.request.contextPath}/group/setGroupRole.htm",
            data : {"ids":arrayObj,"groupId":groupId},
            success : function(data){
               var json = JSON.parse(data);
                if(json.retCode == 1){
                    parent.layer.msg(json.retMsg, {icon: 1,time:2000});
                    layer_close();
                    dom.layerCallBack();
                }else{
                    parent.layer.msg(json.retMsg, {icon: 2,time:2000});
                }
            }
        });
    }


    function loadPage(){
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/user/rolePage?type=GROUP';
        paging1.paging(1,target);
        paging1.pageCallBack = function(){
            initrole();
        };
        initial('#data_total',paging1,'paging',target);
        initrole();
    }


    $(document).ready(function () {
        loadPage();
        /*
         $("#cbx").click(function(){
         var values=$(":checkbox");
         if(this.checked==true){
         for (var i=0;i<values.length;i++){
         values[i].checked=true;
         }
         }else{
         for (var i=0;i<values.length;i++){
         values[i].checked=false;
         }
         }
         });*/
    });
    function initrole(){
        var roleList = ${roleList};
        var values=$("input:checkbox[name='choose']");
        $.each(roleList,function(n,value){
            for (var i=0;i<values.length;i++){
                var e = $(values[i]);
                var id = e.parents('tr').children('td').eq(1).text().trim();
                if(value==id){
                    values[i].checked=true;
                }
            }
        });
    }

</script>
</body>
</html>
