package cn.com.xj.plat.evt.carWeightLength;

import cn.com.xj.common.param.valid.Field;

public class DelCarWeightLengthEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long carLengthId;

    public Long getCarLengthId() {
        return carLengthId;
    }

    public void setCarLengthId(Long carLengthId) {
        this.carLengthId = carLengthId;
    }
}
