package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.common.service.aspect.bean.LogisticsBean;
import cn.com.xj.plat.evt.QueryDictEvt;
import cn.com.xj.plat.evt.invoice.AddInvoiceEvt;
import cn.com.xj.plat.evt.invoice.EditInvoiceEvt;
import cn.com.xj.plat.evt.invoice.QueryInvoiceEvt;
import cn.com.xj.plat.evt.order.QueryOrderEvt;
import cn.com.xj.plat.evt.outInt.GetLogisticsEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.DictService;
import cn.com.xj.plat.service.InvoiceService;
import cn.com.xj.plat.service.OrderService;
import cn.com.xj.plat.service.OutIntService;
import cn.com.xj.plat.util.LoginUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/invoice")
public class InvoiceController extends BaseController {

    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private DictService dictService;
    @Autowired
    private OutIntService  outIntService;

    @RequestMapping("/list")
    public String list(){
        return "/invoice/list";
    }

    @RequestMapping("/page")
    public String page(QueryInvoiceEvt evt,Model model){
        try{
            BaseFactory.getInstance().page(invoiceService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/invoice/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(QueryOrderEvt evt,Model model) {
        if(StringUtils.isBlank(evt.getOrderNo())){
            model.addAttribute("msg","订单编码不能为空");
            return "/common/error";
        }
        ServiceResp<Object> resp = orderService.query(BeanUtil.transBean2Map(evt));
        List<Map<String,Object>> items = (List<Map<String,Object>>)resp.getBody();
        Map<String,Object> item = items.size()>0?items.get(0):null;
        model.addAttribute("order",item);
        model.addAttribute("queryEvt",evt);
        return "/invoice/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddInvoiceEvt evt, HttpSession session){
        try {
            return BaseFactory.getInstance().add(invoiceService,evt,session);
        }catch (Exception e){
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryInvoiceEvt evt,
                         Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toEdit(invoiceService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            return "/common/error";
        }
        return "/invoice/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditInvoiceEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(invoiceService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryInvoiceEvt evt,
                           Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            ServiceResp<Object> resp = invoiceService.query(BeanUtil.transBean2Map(evt));
            List<Map<String,Object>> items = (List<Map<String,Object>>)resp.getBody();
            Map<String,Object> item = items.size()>0?items.get(0):null;
            model.addAttribute("item",item);
            // 如果物流编码和单号不为空，调用接口查询快递信息。
            if(item.get("logisticeCode")!=null&&item.get("logisticeNo")!=null){
                String logisticeCode = MapUtils.getString(item,"logisticeCode");
                String logisticeNo = MapUtils.getString(item,"logisticeNo");
                GetLogisticsEvt getLogisticsEvt = new GetLogisticsEvt();
                getLogisticsEvt.setCompanyCode(logisticeCode);
                getLogisticsEvt.setLogisNumber(logisticeNo);
                ServiceResp<Object> logResp =
                        outIntService.getLogistics(getLogisticsEvt);
                if(logResp.success()){
                    List<LogisticsBean> lst = (List<LogisticsBean>)logResp.getBody();
                    model.addAttribute("logisticsLst",lst);
                }
            }
        }catch(Exception e){
            return "/common/error";
        }
        return "/invoice/detail";
    }

    @RequestMapping("/toAudit")
    public String toAudit(QueryInvoiceEvt evt,
                           Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            QueryDictEvt queryDictEvt = new QueryDictEvt();
            queryDictEvt.setTableName("t_web_invoice");
            queryDictEvt.setColumnName("auditRemark");
            ServiceResp<Object> auditRemarks = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
            if (auditRemarks.success()) {
                model.addAttribute("auditRemarks", auditRemarks.getBody());
            }
            BaseFactory.getInstance().toDetail(invoiceService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            return "/common/error";
        }
        return "/invoice/audit";
    }

    @ResponseBody
    @RequestMapping(value = "doAudit", method = RequestMethod.POST)
    public ServiceResp doAudit(EditInvoiceEvt evt, HttpSession session) {
        try {
            evt.setAuditUserId(LoginUtils.getId(session));
            evt.setAuditUserName(LoginUtils.getRealName(session));
            return invoiceService.audit(BeanUtil.transBean2Map(evt));
        } catch (Exception e) {
            return new ServiceResp().error("系统错误");
        }
    }


    @RequestMapping("/toOver")
    public String toOver(QueryInvoiceEvt evt,
                          Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toDetail(invoiceService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            return "/common/error";
        }
        return "/invoice/over";
    }

    @ResponseBody
    @RequestMapping(value = "doOver", method = RequestMethod.POST)
    public ServiceResp doOver(EditInvoiceEvt evt, HttpSession session) {
        try {
            return invoiceService.over(BeanUtil.transBean2Map(evt));
        } catch (Exception e) {
            return new ServiceResp().error("系统错误");
        }
    }


}
