package cn.com.xj.plat.evt.orderQuotes;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryOrderQuotesEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = 2221456743628029302L;
    @Field(value = "ID", length = 11, nullable = true)
    private Long id;
    @Field(value = "所属订单", length = 11, nullable = false)
    private Long orderId;
    @Field(value = "接单司机ID", length = 11, nullable = true)
    private Long freighter;
    @Field(value = "接单状态", length = 11, nullable = true)
    private String quotesStatus;
    @Field(value = "用户ID", length = 11, nullable = false)
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getFreighter() {
        return freighter;
    }

    public void setFreighter(Long freighter) {
        this.freighter = freighter;
    }

    public String getQuotesStatus() {
        return quotesStatus;
    }

    public void setQuotesStatus(String quotesStatus) {
        this.quotesStatus = quotesStatus;
    }
}
