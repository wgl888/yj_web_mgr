<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${param.invoiceStatus == 'DSH'}">
	<span style="color: orange">待审核</span>
</c:if>
<c:if test="${param.invoiceStatus == 'SHTG'}">
	<span style="color: green">审核通过</span>
</c:if>
<c:if test="${param.invoiceStatus == 'DSP'}">
	<span style="color:orange">待收票</span>
</c:if>
<c:if test="${param.invoiceStatus == 'YWC'}">
	<span style="color: #9d9d9d">已完成</span>
</c:if>
<c:if test="${param.invoiceStatus == 'SHBTG'}">
	<span style="color:red">不通过</span>
</c:if>