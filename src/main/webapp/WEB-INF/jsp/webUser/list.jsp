<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>用户列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true" />
        <jsp:param name="ZUIplugin" value="true" />
        <jsp:param name="cookieutil" value="true" />
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body class="jui-blue-theme">
<div class="clearfix">
    <div class="jui-page">
        <!-- /.page-content -->
        <div class="page-content">
            <div class="jui-search-icon"></div>
            <!-- /.page-form -->
            <form  method="post" id="queryForm"class="form-horizontal jui-form-justify" role="form">
                <input type="hidden" id="superId" name="superId" value="${groupId}" >
                <table class="queryTable">
                    <tr>
                        <td>
                            <span>用户姓名：</span>
                            <input type="text" id="userAlias" name="userAlias"/>
                        </td>
                        <td>
                            <span>登陆账号：</span>
                            <input type="text" placeholder="精确匹配"
                                   id="loginName" name="loginName"/>
                        </td>
                        <td>
                            <span>认证状态：</span>
                            <select class="input-width-160" name="authStatus" id="authStatus" >
                                <option value="">请选择认证状态...</option>
                                <option value="DSQ">待申请</option>
                                <option value="DSH">待审核</option>
                                <option value="SHTG">审核通过</option>
                                <option value="SHBTG">审核不通过</option>
                            </select>
                        </td>
                        <td>
                            <span>车主状态：</span>
                            <select class="input-width-160" name="carownerStatus" id="carownerStatus" >
                                <option value="">请选择认证状态...</option>
                                <option value="DSQ">待申请</option>
                                <option value="DSH">待审核</option>
                                <option value="SHTG">审核通过</option>
                                <option value="SHBTG">审核不通过</option>
                            </select>
                        </td>
                        <td>
                            <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询</button>
                            <button onclick="javascript:resetSearch(1);"type="button" class="btn btn-orange inline-block" >重置</button>
                        </td>
                    </tr>
                </table>
            </form>
            <!-- /.page-form -->
        </div>
        <div class="page-content margin-top">
            <div class="btn-group">
                <button onclick="add();" type="button" class="btn btn-info">新增账户</button>
                <button  onclick="editState('E');"type="button" class="btn btn-lightgreen">启用账号</button>
                <button onclick="editState('T');"type="button" class="btn btn-warning">禁用账号</button>
                <%--<button onclick="resetting();"type="button" class="btn btn-violet">重置密码</button>--%>
            </div>

            <table class="jui-theme-table table-hover margin-top" id="myTable">
                <thead>
                <tr>
                    <th width="60px" class="center"><label class="pos-rel"> <input
                            type="checkbox" class="ace" id="cbx" /> <span class="lbl"></span>
                    </label></th>
                    <th width="60px" >编号</th>
                    <th width="100px">用户姓名</th>
                    <th width="100px">登陆账号</th>
                    <th width="100px">手机号码</th>
                    <th width="80px">认证状态</th>
                    <th width="80px">车主状态</th>
                    <th width="80px">用户状态</th>
                    <th >操作</th>
                </tr>
                </thead>

                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /** 分页 */
    function loadPage(init){
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '<%=request.getContextPath() %>/plat/webUser/page';
        paging1.paging(init,target);
        if(init*1==1){
            initial('#data_total',paging1,'paging',target);
        }
        setPageHeight();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    //ID全选事件
    $("#cbx").click(function(){
        var values=$(":checkbox");
        if(this.checked==true){
            for (var i=0;i<values.length;i++){
                values[i].checked=true;
            }
        }else{
            for (var i=0;i<values.length;i++){
                values[i].checked=false;
            }
        }
    });

    function resetSearch() {
        $('#userAlias').val('');
        $('#loginName').val('');
        $("#authStatus option:first").prop("selected", 'selected');
        $("#carownerStatus option:first").prop("selected", 'selected');
        loadPage(1);
    }

    add = function(){
        location.href='${pageContext.request.contextPath}/plat/webUser/toAdd';
    }

    edit = function(id){
        location.href='${pageContext.request.contextPath}/plat/webUser/toEdit?id='+id;
    }

    detail = function(id){
        layer_full('${pageContext.request.contextPath}/plat/webUser/toDetail?id='+id);
    }

    toAudit = function(id){
        location.href='${pageContext.request.contextPath}/plat/webUser/toAudit?id='+id;
    }

    bank = function(id){
        location.href='${pageContext.request.contextPath}/plat/webUser/toBank?id='+id;
    }

    del = function(id){
        parent.layer.confirm('删除用户属于危险操作，确认要删除吗？',{icon: 3, title:'提示'},function(index){
            $.ajax({
                type : "post",
                url : "<%=request.getContextPath() %>/plat/webUser/doEdit",
                data : {"id":id,"status":"D"},
                success : function(data){
                    if(data.head.respCode == 0){
                        parent.layer.msg(data.head.respMsg, {icon: 1,time:2000});
                        reload(1);
                    }else{
                        parent.layer.msg(data.head.respMsg, {icon: 2,time:2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    layerCallBack = function(){
        reload(0);
    }

    reload = function(init){
        loadPage(init);
    }

    editState = function(status){
        var checkedBox = $("input:checkbox[name='cbox']:checked");
        if(checkedBox.length==0){
            parent.layer.msg(status=='E'?'请选择要启用的用户':"请选择要禁用的用户", {icon:2, time: 1000});
            return;
        }
        var ids = new Array();
        for(var i=0;i<checkedBox.length;i++){
            ids[i] = $(checkedBox[i]).attr("data");
        }
        parent.layer.confirm(status=='E'?'确定启用所选的用户吗':"确定禁用所选的用户吗，禁用后用户将无法登陆和进行其他操作。",{icon: 3, title:'提示'},function(index){
            $.ajax({
                type : "post",
                traditional:true,
                url : "<%=request.getContextPath() %>/plat/webUser/doChangeStatus",
                data : {"ids":ids,"status":status},
                success : function(data){
                    if(data.head.respCode==0){
                        reload(0);
                    }else{
                        parent.layer.msg(data.head.respMsg, {icon: 2,time:2000});
                    }
                }
            });
            parent.layer.close(index);
        });

    }
</script>

<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function(){
        $(this).parent().hasClass("jui-hide")?$(this).parent().removeClass("jui-hide"):$(this).parent().addClass("jui-hide");
    });
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
