package cn.com.xj.plat.evt.userInvoice;

import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class DelUserInvoiceEvt implements Serializable {

    private static final long serialVersionUID = -6887970379338058783L;
    @Field(length = 11,value = "ID",nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
