<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>管理平台</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="icon" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/favicon.png?t=es4eg">
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param name="contextPath" value="${contextPath}"  />
        <jsp:param name="useJQuery" value="true" />
        <jsp:param name="bootstrap" value="true" />
        <jsp:param name="popup" value="true" />
        <jsp:param name="cookieutil" value="true" />
    </jsp:include>
    <!--2017云项目样式-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css" />
    <script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace.js?t=es5i5"></script>
</head>

<body class="jui-blue-theme">
<jsp:include page="top.jsp" />
<div class="main-container" id="main-container">
    <div id="sidebar" class="sidebar  responsive">
        <jsp:include page="leftMenuTree.jsp" />
    </div>
    <jsp:include page="content.jsp" />

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>

<script type="text/javascript">
    try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    window.jQuery || document.write("<script src='<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.min.js'>"+"<"+"/script>");
    window.jQuery || document.write("<script src='<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery1x.min.js'>"+"<"+"/script>");
    if('ontouchstart' in document.documentElement) document.write("<script src='<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    //菜单点击加载事件
    function jumpLoading(url) {
        layer.closeAll();
        layer.load(0,{shade:0,time:5000});
        $('#mainFrame').attr("src",url);
        var iframe = document.getElementById("mainFrame");
            if (iframe.attachEvent) {
            iframe.attachEvent("onload", function() {
                layer.closeAll();
            });
        } else {
            iframe.onload = function() {
                layer.closeAll();
            };
        }
    }
    //首页加载事件
    function waitLoading() {
        layer.load(0,{shade:0});
        var iframe = document.getElementById("mainFrame");
        if (iframe.attachEvent) {
            iframe.attachEvent("onload", function() {
                layer.closeAll();
            });
        } else {
            iframe.onload = function() {
                layer.closeAll();
            };
        }
    }
</script>

</body>
</html>
