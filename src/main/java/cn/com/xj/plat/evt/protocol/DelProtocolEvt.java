package cn.com.xj.plat.evt.protocol;

import cn.com.xj.common.param.valid.Field;

public class DelProtocolEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
