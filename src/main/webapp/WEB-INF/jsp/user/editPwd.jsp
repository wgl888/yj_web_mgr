<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>修改密码</title>

    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp" flush="true">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="plupload" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body >


<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="javascript:history.go(-1);" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>修改登陆密码</h3>
                <h5>修改系统管理员的登陆密码。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/user/doUpdPwd"
          method="post" class="form form-horizontal" id="form">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>旧密码</label>
                </dt>
                <dd class="opt">
                    <input type="password" class="input-txt"
                           id="oldPwd" name="oldPwd" datatype="*1-12" maxlength="12"
                           placeholder="请输入旧密码" nullmsg="旧密码不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入您的账号当前的密码，验证通过才能设置新密码。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>新密码</label>
                </dt>
                <dd class="opt">
                    <input type="password" class="input-txt"
                           id="newPwd1" name="newPwd1" datatype="*6-12" maxlength="12"
                           placeholder="请输入新密码" nullmsg="新密码不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入6-12位数字与字母的组合的新密码。
                    </p>
                </dd>
            </dl>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>确认密码</label>
                </dt>
                <dd class="opt">
                    <input type="password" class="input-txt"
                           id="newPwd2" name="newPwd2" datatype="*6-12" maxlength="12"
                           placeholder="再次确认新密码" nullmsg="新密码不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入6-12位数字与字母的组合的新密码。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    location.href = "<%=request.getContextPath() %>/login/logout";
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>