package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.outInt.GetLogisticsEvt;
import cn.com.xj.plat.evt.outInt.VerifyIdentity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OutIntService extends BaseService{

    @Autowired
    private RestUtil restUtil;

    public ServiceResp<Object> verifyIdentity(VerifyIdentity evt) {
        return restUtil.post(OUT_INT_SERVICE,"out/int/verifyIdentity", BeanUtil.transBean2Map(evt));
    }

    public ServiceResp<Object> getLogistics(GetLogisticsEvt evt) {
        return restUtil.post(OUT_INT_SERVICE,"out/int/getLogistics", BeanUtil.transBean2Map(evt));
    }

}
