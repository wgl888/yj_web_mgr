package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.common.service.aspect.bean.TreeBean;
import cn.com.xj.plat.evt.QueryDictEvt;
import cn.com.xj.plat.evt.userGroup.AddUserGroupEvt;
import cn.com.xj.plat.evt.userGroup.EditUserGroupEvt;
import cn.com.xj.plat.evt.userGroup.QueryUserGroupEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.DictService;
import cn.com.xj.plat.service.UserGroupService;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/userGroup")
public class UserGroupController {

    @Autowired
    private UserGroupService userGroupService;
    @Autowired
    private DictService dictService;

    @RequestMapping("/list")
    public String list(Model model,HttpServletRequest request){
        QueryDictEvt queryDictEvt = new QueryDictEvt();
        queryDictEvt.setTableName("t_web_user_group");
        queryDictEvt.setColumnName("groupType");
        ServiceResp<Object> auditRemarks = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
        if (auditRemarks.success()) {
            model.addAttribute("groupTypes", auditRemarks.getBody());
        }
        return "/userGroup/list";
    }

    @RequestMapping("/page")
    public String page(QueryUserGroupEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(userGroupService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/userGroup/page";
    }

    /**
     * 转到新增页面
     * @param model Model
     */
    @RequestMapping("toAdd")
    public String toAdd(@RequestParam("parentId")Long parentId,HttpServletRequest request, Model model) {
        Map<String, Object> item = new HashMap<String, Object>();
        item.put("parentId",parentId);
        model.addAttribute("item",item);
        QueryDictEvt queryDictEvt = new QueryDictEvt();
        queryDictEvt.setTableName("t_web_user_group");
        queryDictEvt.setColumnName("groupType");
        ServiceResp<Object> auditRemarks = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
        if (auditRemarks.success()) {
            model.addAttribute("groupTypes", auditRemarks.getBody());
        }
        return "/userGroup/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddUserGroupEvt evt, HttpSession session){
        try {
            return BaseFactory.getInstance().add(userGroupService,evt,session);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    /***进入菜单、按钮编辑页面*/
    @RequestMapping("/toEdit")
    public String toEdit(@RequestParam(name = "id" ,required = true) Long id,
                         Model model){
        try{
            QueryDictEvt queryDictEvt = new QueryDictEvt();
            queryDictEvt.setTableName("t_web_user_group");
            queryDictEvt.setColumnName("groupType");
            ServiceResp<Object> auditRemarks = dictService.queryCache(BeanUtil.transBean2Map(queryDictEvt));
            if (auditRemarks.success()) {
                model.addAttribute("groupTypes", auditRemarks.getBody());
            }
            BaseFactory.getInstance().toEdit(userGroupService,id,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/userGroup/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditUserGroupEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(userGroupService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @ResponseBody
    @RequestMapping("/tree")
    public String tree(HttpServletRequest request,HttpSession session,
                     HttpServletResponse response){
        try{
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html;charset=UTF-8");
            List<TreeBean> beans = new ArrayList<TreeBean>();
            TreeBean bean = new TreeBean();
            bean.setId(-4);//跟菜单
            bean.setpId(0);
            bean.setCount(0);
            bean.setName("用户组");
            beans.add(bean);
            String scope = request.getParameter("scope");
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("scope",scope);
            //1、查询所有菜单、按钮
            ServiceResp<Object> resp  = userGroupService.tree(param);
            List<Map<String,Object>> userGroups=(List<Map<String,Object>>)resp.getBody();
            if(userGroups!=null && userGroups.size()>0){
                for(Map<String,Object> item : userGroups){
                    bean = new TreeBean();
                    bean.setCount(0);
                    bean.setId(Integer.parseInt(item.get("id").toString()));
                    if(item.get("parentId")!=null&& StringUtils.isNotBlank(item.get("parentId").toString())&&!"0".equals(item.get("parentId").toString())){
                        bean.setpId(Integer.parseInt(String.valueOf(item.get("parentId"))));
                    }else{
                        bean.setpId(-1);
                    }
                    if(item.get("groupName")!=null){
                        bean.setName(item.get("groupName").toString());
                    }
                    beans.add(bean);
                }
            }
            return JSONArray.toJSONString(beans);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
