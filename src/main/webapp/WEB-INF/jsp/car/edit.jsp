<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>编辑车辆</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="layerUi" value="true"/>
        <jsp:param name="plupload" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>

    <style type="text/css">
        .delete{
            width:20px;
            height:20px;
            position:absolute;
            top:0px;
            left:78px;
            z-index:9999;
            background-repeat:no-repeat;
            cursor:pointer;
            background-image: url("<%=request.getContextPath() %>/myResources/manager/plat/images/imgdelete.png");
        }
    </style>

</head>
<body style="height: 1800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/car/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }车辆</h3>
                <h5>维护，审核，查看系统里面的车辆信息。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/car/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }"/>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>所属用户：</label>
                </dt>
                <dd class="opt">
                    <input readonly type="text" value="${item.userAlias }" class="input-txt"
                           id="userAlias" name="userAlias" />
                    <input type="hidden" id="userId" name="userId" value="${item.userId}"
                           datatype="*1-11" maxlength="11" placeholder="请选择用户" nullmsg="用户不能为空" >
                    <button type="button" class="btn btn-xs btn-success"
                            onclick="loadChooseUser();">选择
                    </button>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入长度值,默认单位为：米，例如:4.7。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="chooseUserRow" style="display: none;">
                <dt class="tit">
                    <label><em>*</em>选择用户：</label>
                </dt>
                <dd class="opt">
                    <div id="chooseUserDiv">

                    </div>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入查询条件，搜索并选择用户。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>车长：</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" id="carLength" name="carLength"
                            datatype="*" nullmsg="请选择车辆长度">
                        <option value="">请选择车辆长度...</option>
                        <c:forEach items="${carLengths }" var="ct">
                            <option value="${ct.id}" ${item.carLength==ct.id?'selected':'' } >${ct.lengthValue}</option>
                        </c:forEach>
                    </select>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请选择车辆类型，可以在【系统管理->车辆规格->车长管理】里配置数据。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>车型：</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" id="carType" name="carType" datatype="*" nullmsg="请选择商品类型">
                        <option value="">请选择车辆类型...</option>
                    </select>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请选择车辆类型，可以在【系统管理->车辆规格->车型管理】里配置数据。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>车牌颜色：</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" id="plateColor" name="plateColor"
                            datatype="*" nullmsg="请选择车牌颜色">
                        <option value="">请选择车牌颜色...</option>
                        <c:forEach items="${colors}" var="color">
                            <option value="${color.dictValue}" ${item.plateColor==color.dictValue?'selected':'' }  >${color.dictDesc }</option>
                        </c:forEach>
                    </select>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        车牌颜色通过字典配置，表名为：t_web_car，列名为：plateColor。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>车辆载重(吨)：</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" id="loadWeight" name="loadWeight"
                            datatype="*" nullmsg="请选择车辆载重(吨)">
                        <option value="">请选择车辆载重...</option>
                        <c:forEach items="${weights}" var="weight">
                            <option value="${weight.dictValue}" ${item.loadWeight==weight.dictValue?'selected':'' }  >${weight.dictDesc }</option>
                        </c:forEach>
                    </select>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        车牌颜色通过字典配置，表名为：t_web_car，列名为：loadWeight。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>装载体积(立方米)：</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" id="loadVolume" name="loadVolume"
                            datatype="*" nullmsg="请选择车辆装载体积(立方米)">
                        <option value="">请选择车辆装载体积...</option>
                        <c:forEach items="${volumes}" var="volume">
                            <option value="${volume.dictValue}" ${item.loadVolume==volume.dictValue?'selected':'' }  >${volume.dictDesc }</option>
                        </c:forEach>
                    </select>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        车牌颜色通过字典配置，表名为：t_web_car，列名为：loadVolume。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>车牌号：</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.plateProvince }"
                           id="plateProvince" name="plateProvince" datatype="*1-1" maxlength="1"
                           placeholder="闽" nullmsg="车牌号省简称不能为空" style="width: 60px;"/>
                    <input type="text" value="${item.plateLetter }"
                           id="plateLetter" name="plateLetter" datatype="*1-1" maxlength="1"
                           placeholder="A" nullmsg="车牌号首字母不能为空" style="width: 60px;"/>
                    <input type="text" value="${item.plateNo }"
                           id="plateNo" name="plateNo" datatype="*5-5" maxlength="5"
                           placeholder="88888" nullmsg="车牌号不能为空" style="width: 60px;"/>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入车牌号，例如：闽A88888
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>车辆照片：</label>
                </dt>
                <dd class="opt">
                    <div class="pull-left file-box margin-right">
                        <div class="file-preview" filepreview>
                            <img id="driverLicenseImg_img" src='${item!=null?item.driverLicenseImg:''}'>
                            <div class="file-input" id="driverLicenseImg_file" ></div>
                        </div>
                        <strong style="margin: 0 auto;">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;
                            驾驶证
                        </strong>
                        <input type="hidden" id="driverLicenseImg_hid"
                               name="driverLicenseImg" value="${item.driverLicenseImg }">
                        <div id="driverLicenseImg_del" class='delete' onclick='deleteFile("driverLicenseImg")' style="display: ${item==null?'none':''}">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="pull-left file-box margin-right">
                        <div class="file-preview" filepreview>
                            <img id="drivingLicenseImg_img" src="${item!=null?item.drivingLicenseImg:''}">
                            <div class="file-input" id="drivingLicenseImg_file" ></div>
                        </div>
                        <strong style="margin: 0 auto;">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;
                            行驶证
                        </strong>
                        <input type="hidden" id="drivingLicenseImg_hid"
                               name="drivingLicenseImg" value="${item.drivingLicenseImg }">
                        <div id="drivingLicenseImg_del" class='delete' onclick='deleteFile("drivingLicenseImg")' style="display: ${item==null?'none':''}">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    </div>
                    <div class="pull-left file-box margin-right">
                        <div class="file-preview" filepreview>
                            <img id="autoInsuranceImg_img" src="${item!=null?item.autoInsuranceImg:''}">
                            <div class="file-input" id="autoInsuranceImg_file" ></div>
                        </div>
                        <strong style="margin: 0 auto;">
                            &nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;
                            车险照片
                        </strong>
                        <input type="hidden" id="autoInsuranceImg_hid" name="autoInsuranceImg"
                               value="${item.autoInsuranceImg }">
                        <div id="autoInsuranceImg_del" class='delete' onclick='deleteFile("autoInsuranceImg")' style="display: ${item==null?'none':''}">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    </div>
                    <span class="Validform_checktip"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label></label>
                </dt>
                <dd class="opt">
                    <div class="pull-left file-box margin-right">
                        <div class="file-preview" filepreview>
                            <img id="anunalSurveyImg_img" src="${item!=null?item.anunalSurveyImg:''}">
                            <div class="file-input" id="anunalSurveyImg_file"></div>
                        </div>
                        <strong style="margin: 0 auto;">
                            &nbsp;&nbsp;
                            年检标志照片
                        </strong>
                        <input type="hidden" id="anunalSurveyImg_hid"
                               name="anunalSurveyImg" value="${item.anunalSurveyImg }">
                        <div id="anunalSurveyImg_del" class='delete' onclick='deleteFile("anunalSurveyImg")' style="display: ${item==null?'none':''}">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                     </div>
                    <div class="pull-left file-box margin-right">
                        <div class="file-preview" filepreview>
                            <img id="plateImg_img" src="${item!=null?item.plateImg:''}">
                            <div class="file-input" id="plateImg_file" ></div>
                        </div>
                        <strong style="margin: 0 auto;">
                            &nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;
                            车牌照片
                        </strong>
                        <input type="hidden" id="plateImg_hid" name="plateImg" value="${item.plateImg }">
                        <div id="plateImg_del" class='delete' onclick='deleteFile("plateImg")' style="display: ${item==null?'none':''}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    </div>
                    <div class="pull-left file-box margin-right">
                        <div class="file-preview" filepreview>
                            <img id="carImg_img" src="${item!=null?item.carImg:''}">
                            <div class="file-input" id="carImg_file" ></div>
                        </div>
                        <strong style="margin: 0 auto;">
                            &nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;
                            车身照片
                        </strong>
                        <input type="hidden" id="carImg_hid" name="carImg" value="${item.carImg }">
                        <div id="carImg_del" class='delete' onclick='deleteFile("carImg")' style="display: ${item==null?'none':''}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    </div>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        <br>
                        请上传jpg或png格式的图片，最大不超过5M。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>车险到期日：</label>
                </dt>
                <dd class="opt">
                    <input readonly class="audit-input" type="text" id="autoInsuranceDate" name="autoInsuranceDate"
                           placeholder="请选择车险到期日" datatype="*" nullmsg="请选择车险到期日" value="${item.formatAutoInsuranceDate }"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请仔细核对并输入保险合同上的到期日。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>年检到期日：</label>
                </dt>
                <dd class="opt">
                    <input readonly class="audit-input" type="text" id="anunalSurveyDate" name="anunalSurveyDate"
                           placeholder="请选择年检到期日" datatype="*" nullmsg="请选择年检到期日" value="${item.formatAnunalSurveyDate }"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请仔细核对并输入年检标志上的年检到期日。
                    </p>
                </dd>
            </dl>
            <%--<dl class="row">--%>
                <%--<dt class="tit">--%>
                    <%--<label><em>*</em>审核状态：</label>--%>
                <%--</dt>--%>
                <%--<dd class="opt">--%>
                    <%--<select class="input-txt" id="auditStatus" name="auditStatus"--%>
                            <%--datatype="*" nullmsg="请选择审核状态">--%>
                        <%--<option value="">请选择审核状态...</option>--%>
                        <%--<c:forEach items="${auditStatus}" var="as">--%>
                            <%--<option value="${as.dictValue}" ${item.auditStatus==as.dictValue?'selected':'' }>${as.dictDesc }</option>--%>
                        <%--</c:forEach>--%>
                    <%--</select>--%>
                    <%--<span class="Validform_checktip"></span>--%>
                    <%--<p class="notic">--%>
                        <%--审核状态通过字典配置，表名为：t_web_car，列名为：auditStatus。--%>
                    <%--</p>--%>
                <%--</dd>--%>
            <%--</dl>--%>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/car/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

        $('#carLength').change(function () {
            $('#carType').html("");
            if ($('#carLength').val() == null || $('#carLength').val() == '') {
                return;
            }
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/carType/queryWithCarLength",
                data: {"carLengthId": $('#carLength').val()},
                success: function (data) {
                    $('#carType').append('<option value="">请选择...</option>');
                    for (var i = 0; i < data.body.length; i++) {
                        if ('${item.carType}' == data.body[i].carTypeId) {
                            var opt = '<option selected value = "' + data.body[i].carTypeId + '">' + data.body[i].typeName + '</option>';
                        } else {
                            var opt = '<option value = "' + data.body[i].carTypeId + '">' + data.body[i].typeName + '</option>';
                        }
                        $('#carType').append(opt);
                    }
                }
            });
        });


        layui.use('laydate', function () {
            var laydate = layui.laydate;
            var start = {
                max: '2099-06-16 23:59:59'
                , istoday: false
                , choose: function (datas) {
                    end.min = datas; //开始日选好后，重置结束日的最小日期
                    end.start = datas //将结束日的初始值设定为开始日
                }
            };
            var end = {
                max: '2099-06-16 23:59:59'
                , istoday: false
                , choose: function (datas) {
                    start.max = datas; //结束日选好后，重置开始日的最大日期
                }
            };
            document.getElementById('autoInsuranceDate').onclick = function () {
                start.elem = this;
                laydate(start);
            }
            document.getElementById('anunalSurveyDate').onclick = function () {
                end.elem = this
                laydate(end);
            }

        });


        $('#carLength').trigger("change");

    });

    loadChooseUser = function () {
        $('#chooseUserDiv').load('<%=request.getContextPath() %>/plat/webUser/chooseList');
        $('#chooseUserRow').show();
    }

    deleteFile = function(nm){

        $("#"+nm+"_img").attr('src','');
        $("#"+nm+"_del").hide();
        $("#"+nm+"_hid").val("");//删除图片id

    }

</script>


<script type="application/javascript">

    var uploadUrl = '<%=request.getContextPath() %>/upload/uploadImg';
    var ids = new Array("driverLicenseImg_file","drivingLicenseImg_file","autoInsuranceImg_file",
            "anunalSurveyImg_file","plateImg_file","carImg_file");

    $.each(ids,function(i,n){
        var self = this.toString();
        var uploader = new plupload.Uploader({
            browse_button : self, //触发文件选择对话框的按钮，为那个元素id
            url : uploadUrl ,//服务器端的上传页面地址
            max_file_size: '5mb',//限制为2MB
            filters: [{title: "Image files",extensions: "jpg,png"}]//图片限制
        });

        //在实例对象上调用init()方法进行初始化
        uploader.init();
        //绑定各种事件，并在事件监听函数中做你想做的事
        uploader.bind('FilesAdded',function(uploader,files){
            var loading = "<%=request.getContextPath() %>/myResources/manager/plat/images/imgloading1.gif";
            $("#"+self).parent().find('img').attr('src',loading);
            uploader.start();
        });

        uploader.bind('FileUploaded', function (uploader, file, responseObject) {
            var rs = responseObject.response;
            if(rs==''||rs==null||rs*1==-1*1){
                parent.layer.open({
                    title: '失败',
                    content: '上传失败，请稍后再试。'
                });
            }
            $("#"+self).parent().find('img').attr('src',responseObject.response);
            var hid = self.split("_")[0] + "_hid";
            $("#"+hid).val(responseObject.response);
            var del = self.split("_")[0] + "_del";
            $("#"+del).show();
        });

    });

</script>

</body>
</html>
