package cn.com.xj.plat.evt;

import cn.com.xj.common.param.valid.Field;

/**
 * @author wanggl
 * @version V1.0
 * @Title: AddBrandEvt
 * @Package com.wgl.springboot.evt.plat
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/4/27 19:48
 */
public class ResetPwdEvt {

    @Field(length = 11,value = "用户ID",nullable = false)
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
