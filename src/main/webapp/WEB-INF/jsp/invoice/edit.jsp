<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>开票</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/invoice/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'开票':'编辑发票' }</h3>
                <h5>订单确认收货后可以给用户开具发票。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>1，普票需要提供发票抬头和发票内容。</li>
            <li>2，开票时请确认清楚用户的发票信息，减少不必要的麻烦。</li>
            <li>3，只要对发票信息进行编辑操作，都需要重新进入审核流程。</li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/invoice/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <input type="hidden" name="orderId" value="${order.id }" />
        <input type="hidden" id="userId" value="${queryEvt.userId }" />
        <input type="hidden" id="orderNo" name="orderNo" value="${order.orderNo }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>选择常用发票</label>
                </dt>
                <dd class="opt">
                    <button type="button" class="btn btn-xs btn-success"
                            onclick="loadChoose();">选择
                    </button>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        可以直接选择当前开票用户下的常用发票信息。
                    </p>
                </dd>
            </dl>

            <dl class="row" id="chooseRow" style="display: none;">
                <dt class="tit">
                    <label></label>
                </dt>
                <dd class="opt">
                    <iframe id="chooseFrame" style="border:none;width: 800px;height: 350px;">

                    </iframe>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入查询条件，搜索并选择用户。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>发票类型</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" id="invoiceType" name="invoiceType"
                            datatype="*" nullmsg="请选择发票类型">
                        <option value="PP" ${item.invoiceType=='PP'?'selected':''} >普票</option>
                        <option value="ZP" ${item.invoiceType=='ZP'?'selected':''} >专票</option>
                    </select>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        普票需要输入发票抬头和内容，专票不需要输入。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_companyName">
                <dt class="tit">
                    <label><em>*</em>公司名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.companyName }" class="input-txt"
                           id="companyName" name="companyName" datatype="*1-128" maxlength="128"
                           nullmsg="公司名称不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入要开票的企业单位全称。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_itin">
                <dt class="tit">
                    <label><em>*</em>纳税人识别号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.itin }" class="input-txt"
                           id="itin" name="itin" datatype="*1-18" maxlength="18"
                           nullmsg="纳税人识别号不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入18位的纳税人识别号并仔细核对。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>发票金额</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item!=null?item.amount:order.finalCost }" class="input-txt"
                           id="amount" name="amount" datatype="/^\d{0,8}.\d{0,2}$/g" maxlength="12"
                           errormsg="请输入正确的金额值，可包括：8位整数和2位小数。">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入开票金额，默认显示订单最终成交金额。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_address" style="display: none;">
                <dt class="tit">
                    <label><em>*</em>地址</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.address }" class="input-txt"
                           id="address" name="address" maxlength="512" >
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入公司地址。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_tel" style="display: none;">
                <dt class="tit">
                    <label><em>*</em>电话</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.tel }" class="input-txt"
                           id="tel" name="tel" maxlength="16">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入公司联系电话。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_bankName" style="display: none;">
                <dt class="tit">
                    <label><em>*</em>开户行</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankName }" class="input-txt"
                           id="bankName" name="bankName" datatype="*0-128" maxlength="128"
                           nullmsg="开户行不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入企业开户行全称，例如：中国银行福建省分行
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_bankAccount" style="display: none;">
                <dt class="tit">
                    <label><em>*</em>银行账号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankAccount }" class="input-txt"
                           id="bankAccount" name="bankAccount" datatype="*0-128" maxlength="128"
                           nullmsg="银行账号不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入开业开户行账号，并仔细核对。
                    </p>
                </dd>
            </dl>


            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>收货人姓名</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.deliveryName }" class="input-txt"
                           id="deliveryName" name="deliveryName" datatype="*1-11" maxlength="11"
                           nullmsg="收货人姓名不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入发票收货人姓名
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>收货人电话</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.deliveryPhone }" class="input-txt"
                           id="deliveryPhone" name="deliveryPhone" datatype="*1-11" maxlength="11"
                           nullmsg="收货人电话不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入收货人联系电话，输入完后请核对。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>收货人地址</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.deliveryPhone }" class="input-txt"
                           id="deliveryAddress" name="deliveryAddress" datatype="*1-512" maxlength="512"
                           nullmsg="收货人地址不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入收货人详细地址信息。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>快递费</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.deliveryPhone }" class="input-txt"
                           id="logisticeCost" name="logisticeCost" datatype="*1-11" maxlength="11"
                           nullmsg="快递费不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入快递费用。
                    </p>
                </dd>
            </dl>

            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $('#invoiceType').change(function(){
        var type = $(this).val();
        dochange(type);
    });

    dochange = function(type){
        if("PP"==type){
            $('#dl_address').hide();
            $('#dl_tel').hide();
            $('#dl_bankName').hide();
            $('#dl_bankAccount').hide();
        }
        if("ZP"==type){
            $('#dl_address').show();
            $('#dl_tel').show();
            $('#dl_bankName').show();
            $('#dl_bankAccount').show();
        }
        setPageHeight();
    }


    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/invoice/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
        $('#invoiceType').trigger("change");
    });


    loadChoose = function () {
        if($('#userId').val()==null||$('#userId').val()==''){
            layer_error("参数丢失");
            return;
        }
        var url = '<%=request.getContextPath() %>/plat/webUser/invoice/chooseList?userId='+$('#userId').val();
        $('#chooseFrame').attr('src',url);
        $('#chooseRow').show();
    }


</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
