package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.carLength.QueryCarLengthEvt;
import cn.com.xj.plat.evt.carType.AddCarTypeEvt;
import cn.com.xj.plat.evt.carType.EditCarTypeEvt;
import cn.com.xj.plat.evt.carType.QueryCarTypeEvt;
import cn.com.xj.plat.evt.carTypeLength.QueryCarTypeLengthEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.CarLengthService;
import cn.com.xj.plat.service.CarTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/carType")
public class CarTypeController extends BaseController {

    @Autowired
    private CarTypeService carTypeService;
    @Autowired
    private CarLengthService carLengthService;

    @RequestMapping("/list")
    public String list(){
        return "/carType/list";
    }

    @RequestMapping("/page")
    public String page(QueryCarTypeEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(carTypeService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/carType/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/carType/edit";
    }


    @ResponseBody
    @RequestMapping(value = "queryWithCarLength",method = RequestMethod.POST)
    public ServiceResp queryWithCarLength(QueryCarTypeLengthEvt evt,
                                        HttpSession session){
        try {
            return carLengthService.queryWithCarType(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddCarTypeEvt evt, HttpSession session){
        try {
            ServiceResp resp = BaseFactory.getInstance().add(carTypeService,evt,session);
            return resp;
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryCarTypeEvt evt,
                         Model model){
        try{
            if(evt.getId()==null){
                return "/common/error";
            }
            BaseFactory.getInstance().toEdit(carTypeService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/carType/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditCarTypeEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(carTypeService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


}
