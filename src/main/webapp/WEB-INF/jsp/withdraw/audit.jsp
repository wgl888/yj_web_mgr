<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>提现申请审核</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/withdraw/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>审核用户提现申请</h3>
                <h5>审核用户提现信息，包括银行卡信息，额度信息等。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/withdraw/doAudit"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <jsp:include page="detailPart.jsp" flush="true" />
            <dl class="row" style="height: 50px;">
                <dt class="tit">
                    <label><em>*</em>是否通过</label>
                </dt>
                <dd class="opt">
                    <input type="radio" checked class="ace" name="withdrawStatus" value="SHTG" />
                    <span class="lbl">&nbsp;通过</span>
                    &nbsp;&nbsp;
                    <input type="radio" class="ace" name="withdrawStatus" value="SHBTG" />
                    <span class="lbl">&nbsp;不通过</span>
                </dd>
            </dl>
            <dl class="row" id="dl_auditDesc" style="display: none;">
                <dt class="tit">
                    <label>审核备注</label>
                </dt>
                <dd class="opt">
                    <select class="audit-input" name="auditRemark" id="auditRemark">
                        <option value="">请选择审核不通过原因...</option>
                        <c:forEach items="${auditRemarks }" var="r">
                            <option value="${r.dictDesc}">${r.dictDesc }</option>
                        </c:forEach>
                    </select>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/withdraw/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

        $('input[name="withdrawStatus"]').click(function(){
            var val = $(this).val();
            if('SHTG' == val){
                $('#dl_auditDesc').hide();
            }else{
                $('#dl_auditDesc').show();
            }
        });

    });

    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
