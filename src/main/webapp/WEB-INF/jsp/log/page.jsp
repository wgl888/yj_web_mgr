<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>${item.channelCode}</td>
				<td>${item.operaRemark}</td>
				<td>
					<c:if test="${item.isSuccess == 'Y'}">
						<strong style="color:green;">成功</strong>
					</c:if>
					<c:if test="${item.isSuccess == 'N'}">
						<strong style="color:red;">失败</strong>
					</c:if>
				</td>
				<td>${item.timeLong/1000}</td>
				<td>
					<jsp:useBean id="createTime" class="java.util.Date"/>
					<c:set target="${createTime}" property="time" value="${item.createTime}"/>
					<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
				</td>
				<td>
					<button type="button" class="btn btn-xs btn-success"
							onclick="javascript:toDetail(${item.id})">详情</button>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="5">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
