package cn.com.xj.plat.evt;

import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class DelPlatUserEvt implements Serializable{

    private static final long serialVersionUID = 8661069256691942436L;

    @Field(length = 11,nullable = false,value = "登陆账号ID")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
