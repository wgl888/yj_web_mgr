<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <jsp:include page="../common/common.jsp" flush="true">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="chosenMultiple" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="background-color: #fff;">
<form method="post" class="form form-horizontal" id="form">
    <table id="simple-table" class="jui-theme-table table-hover margin-top">
        <thead>
        <tr>
            <th width="4%;">选择</th>
            <th width="4%;">是否授权</th>
            <th width="4%;">ID</th>
            <th width="8%;">角色名称</th>
            <th width="12%;">角色描述</th>
        </tr>
        </thead>
        <tbody id="paging_content">
        <input type="hidden" value="${roles }" id="roles" name="rolses">
        </tbody>
    </table>
    <div id="paging" style="margin:10px 0 0 0;"></div>
</form>
<input type="hidden" value="${staffId}" id="staffId" name="staffId" >
<script type="text/javascript">

    /** 弹出框自带的提交按钮回调 */
    layer_submit = function(dom){
        var arrayObj = new Array();
        var arrayObj2 = new Array();

        /*
         for (var i = 0; i < len.length; i++) {
         if (len[i].checked && len[i].value != "") {
         console.log(len[i].value);
         arrayObj.push(len[i].value);
         }
         }
         */
        var values=$("input:checkbox[name='choose']");
        var gValues = $("input:checkbox[name='grant']");
        //角色Id
        for (var i=0;i<values.length;i++){
            if(values[i].checked==true){
                var e = $(values[i]);
                var id = e.parents('tr').children('td').eq(2).text().trim();
                arrayObj.push(id);
            }
        }
        //是否授权
        for (var i=0;i<gValues.length;i++){
            if(gValues[i].checked==true){
                var e = $(gValues[i]);
                var id = e.parents('tr').children('td').eq(2).text().trim();
                arrayObj2.push(id);
            }
        }
        if(arrayObj.length==0){
            parent.layer.msg('请选择角色', {icon: 1,time:2000});
            return;
        }
        var staffId = $('#staffId').val();
        $.ajax({
            type : "post",
            traditional:true,
            url : "${pageContext.request.contextPath}/user/setStaffRole.htm",
            data : {"ids":arrayObj,'grantIds':arrayObj2,"staffId":staffId},
            success : function(data){
               var json = JSON.parse(data);
                if(json.retCode == 0){
                    parent.layer.msg(json.retMsg, {icon: 1,time:2000});
                    layer_close();
                    dom.layerCallBack();
                }else{
                    parent.layer.msg(json.retMsg, {icon: 2,time:2000});
                }
            }
        });
    }


    function loadPage(){
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/user/rolePage';
        paging1.paging(1,target);
        paging1.pageCallBack = function(){
            initrole();
        };
        initial('#data_total',paging1,'paging',target);
        initrole();
    }


    $(document).ready(function () {
        loadPage();
        /*
         $("#cbx").click(function(){
         var values=$(":checkbox");
         if(this.checked==true){
         for (var i=0;i<values.length;i++){
         values[i].checked=true;
         }
         }else{
         for (var i=0;i<values.length;i++){
         values[i].checked=false;
         }
         }
         });*/
        var values=$("input:checkbox[name='choose']");
        var gValues = $("input:checkbox[name='grant']");
        values.click(function(){
            if(this.checked==false){
                $(this).parents('tr').children('td').eq(1).find("input:checkbox[name='grant']")[0].checked = false;
            }
        });
        gValues.click(function(){
            if(this.checked==true){
                $(this).parents('tr').children('td').eq(0).find("input:checkbox[name='choose']")[0].checked = true;
            }
        });
    });
    function initrole(){
        var roleList = ${roleList};
        var grantList = ${grantList};
        var values=$("input:checkbox[name='choose']");

        var gValues = $("input:checkbox[name='grant']");
        $.each(roleList,function(n,value){
            for (var i=0;i<values.length;i++){
                var e = $(values[i]);
                var id = e.parents('tr').children('td').eq(2).text().trim();
                if(value==id){
                    values[i].checked=true;
                }
            }
        });
        $.each(grantList,function(n,value){
            for (var i=0;i<gValues.length;i++){
                var e = $(gValues[i]);
                var id = e.parents('tr').children('td').eq(2).text().trim();
                if(value==id){
                    gValues[i].checked=true;
                }
            }
        });
    }

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
