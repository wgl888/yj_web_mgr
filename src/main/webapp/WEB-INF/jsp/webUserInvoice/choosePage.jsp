<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>${item.invoiceType=='PP'?'普票':'专票' }</td>
				<td>${item.itin }</td>
				<td align="left">
					<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:100px;"
						   title='${item.companyName}' >
							${item.companyName}
					</label>
				</td>
				<td>${item.tel}</td>
				<td>
					<button type="button" class="btn btn-xs btn-info"
							onclick="choose('${item.invoiceType}',
											'${item.companyName}',
											'${item.itin}',
											'${item.tel}',
											'${item.bankName}',
											'${item.address}',
											'${item.bankAccount}');">选择</button>
				</td>
		   </tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="8">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">