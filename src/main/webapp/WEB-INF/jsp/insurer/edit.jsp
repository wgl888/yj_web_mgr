<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>编辑保险公司</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="ueditor" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/insurer/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }保险公司</h3>
                <h5>设置保险公司基本信息，各个货物类型的费率等。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>保险公司的费率涉及到客户端金钱操作，请谨慎设置。</li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/insurer/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <input type="hidden" id="editIds" value="${item.ids }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>公司名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.insurerName }" placeholder="" class="input-txt"
                           id="insurerName" name="insurerName" datatype="*1-32" maxlength="32"
                           placeholder="请输入类型名称" nullmsg="类型名称不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入1~32位的汉字或者英文。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>是否可用</label>
                </dt>
                <dd class="opt">
                    <input type="radio" ${(item.isEnable=='Y'||item.isEnable==null)?'checked':''} class="ace" name="isEnable" value="Y"/>
                    <span class="lbl">&nbsp;可用</span>
                    &nbsp;&nbsp;
                    <input type="radio" ${item.isEnable=='N'?'checked':''} class="ace" name="isEnable" value="N"/>
                    <span class="lbl">&nbsp;不可用</span>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        选择保险公司启用状态，客户端只能看到可用的保险公司。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="dl_choose_role">
                <dt class="tit">
                    <label><em>*</em>保险协议</label>
                </dt>
                <dd class="opt">
                    <textarea id="editor" name="insurerAgreen" style="width: 500px;">
                        ${item.insurerAgreen }
                    </textarea>
                    <script type="text/javascript">
                        var ue=UE.getEditor('editor',{initialFrameWidth: 500,initialFrameHeight: 500,scaleEnabled:true});
                        UE.Editor.prototype._bkGetActionUrl=UE.Editor.prototype.getActionUrl;
                        UE.Editor.prototype.getActionUrl=function(action){
                            if (action == 'uploadimage' ||action== 'uploadscrawl' || action == 'catchimage') {
                                return "${contextPath}richText/upload";
                            }else if (action == 'uploadvideo') {
                                return "${contextPath}richText/upload?action="+action;
                            }else{
                                return this._bkGetActionUrl.call(this, action);
                            }
                        }
                        setPageHeight();
                    </script>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        勾选该车型支持的车长信息，多选。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/insurer/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

        var ids = $('#editIds').val();
        if(ids!=null&&ids!=""){
            var array = ids.split(',');
            for(var i=0;i<array.length;i++){
                $('#length_'+array[i]).attr("checked",true);
            }
        }

    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>

</body>
</html>
