package cn.com.xj.plat.evt;

import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class DelWebUserEvt implements Serializable{

    private static final long serialVersionUID = 5178392567179223292L;
    @Field(value = "ID",nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
