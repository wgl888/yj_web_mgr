package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("menuService")
public class MenuService extends BaseService implements BaseInterface{

    @Autowired
    private RestUtil restUtil;

    public ServiceResp<Object> queryPlatUserMenu(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/menu/queryPlatUserMenu", param);
    }

    public ServiceResp<Object> queryPlatUserBtns(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/menu/queryPlatUserBtns", param);
    }

    public ServiceResp<Object> tree(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/menu/tree", param);
    }

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/menu/count", param);
    }

    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/menu/query", param);
    }

    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/menu/add", param);
    }

    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/menu/edit", param);
    }



}
