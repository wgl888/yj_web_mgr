<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>查看保险公司详细信息</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="false"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="ueditor" value="false"/>
        <jsp:param name="useFormCheck" value="false"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/insurer/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>查看保险公司详细信息</h3>
                <h5>查看保险公司基本信息，协议信息，费率信息。</h5>
            </div>
        </div>
    </div>
    <form method="post" class="form form-horizontal" id="form">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>协议内容</label>
                </dt>
                <dd class="opt">
                    <div style="overflow:auto;height: 450px;width: 600px;">
                        ${item.insurerAgreen }
                    </div>
                </dd>
            </dl>
        </div>
    </form>
</div>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });

</script>

</body>
</html>
