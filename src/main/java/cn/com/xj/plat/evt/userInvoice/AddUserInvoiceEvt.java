package cn.com.xj.plat.evt.userInvoice;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class AddUserInvoiceEvt extends BaseEvt implements Serializable {

    private static final long serialVersionUID = -3385032907894614427L;
    @Field(value = "用户ID", length = 11, nullable = false)
    private Long userId;
    @Field(value = "发票类型 PP - 普票 ZP - 专票", length = 12, nullable = false)
    private String invoiceType;
    @Field(value = "公司名称", length = 128, nullable = false)
    private String companyName;
    @Field(value = "纳税人识别号", length = 18, nullable = false)
    private String itin;
    @Field(value = "开户行", length = 128, nullable = true)
    private String bankName;
    @Field(value = "银行账号", length = 128, nullable = true)
    private String bankAccount;
    @Field(value = "地址", length = 512, nullable = true)
    private String address;
    @Field(value = "联系电话", length = 16, nullable = true)
    private String tel;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getItin() {
        return itin;
    }

    public void setItin(String itin) {
        this.itin = itin;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "AddUserInvoiceEvt{" +
                "userId=" + userId +
                ", invoiceType='" + invoiceType + '\'' +
                ", companyName='" + companyName + '\'' +
                ", itin='" + itin + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankAccount='" + bankAccount + '\'' +
                ", address='" + address + '\'' +
                ", tel='" + tel + '\'' +
                '}';
    }
}
