package cn.com.xj.plat.evt.car;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.util.Date;

public class EditCarEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(length = 11 ,nullable = true,value = "车辆所属用户")
    private Long userId;
    @Field(length = 12,nullable = true,value = "车牌号颜色")
    private String plateColor;
    @Field(length = 6,nullable = true,value = "车牌号-省简称")
    private String plateProvince;
    @Field(length = 6,nullable = true,value = "车牌号-首字母")
    private String plateLetter;
    @Field(length = 6,nullable = true,value = "车牌号-牌号")
    private String plateNo;
    @Field(length = 1024,nullable = true,value = "驾驶证照片")
    private String driverLicenseImg;
    @Field(length = 1024,nullable = true,value = "行驶证照片")
    private String drivingLicenseImg;
    @Field(length = 1024,nullable = true,value = "车险照片")
    private String autoInsuranceImg;
    @Field(length = 1024,nullable = true,value = "年检标志照片")
    private String anunalSurveyImg;
    @Field(length = 1024,nullable = true,value = "车牌照片")
    private String plateImg;
    @Field(length = 1024,nullable = true,value = "车车照片")
    private String carImg;
    @Field(length = 11,nullable = true,value = "车类型ID ")
    private Long carType;
    @Field(length = 11,nullable = true,value = "车长ID")
    private Long carLength;
    @Field(nullable = true,value = "车险到期日")
    private String autoInsuranceDate;
    @Field(nullable = true,value = "年检到期日")
    private String anunalSurveyDate;
    @Field(length = 12 ,nullable = true,value = "审核状态")
    private String auditStatus;
    @Field(length = 512 ,nullable = true,value = "审核不通过原因")
    private String auditRemark;
    @Field(value = "装载重重", length = 11, nullable = false)
    private String loadWeight;
    @Field(value = "装载体积", length = 11, nullable = false)
    private String loadVolume;

    public String getLoadWeight() {
        return loadWeight;
    }

    public void setLoadWeight(String loadWeight) {
        this.loadWeight = loadWeight;
    }

    public String getLoadVolume() {
        return loadVolume;
    }

    public void setLoadVolume(String loadVolume) {
        this.loadVolume = loadVolume;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPlateColor() {
        return plateColor;
    }

    public void setPlateColor(String plateColor) {
        this.plateColor = plateColor;
    }

    public String getPlateProvince() {
        return plateProvince;
    }

    public void setPlateProvince(String plateProvince) {
        this.plateProvince = plateProvince;
    }

    public String getPlateLetter() {
        return plateLetter;
    }

    public void setPlateLetter(String plateLetter) {
        this.plateLetter = plateLetter;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getDriverLicenseImg() {
        return driverLicenseImg;
    }

    public void setDriverLicenseImg(String driverLicenseImg) {
        this.driverLicenseImg = driverLicenseImg;
    }

    public String getDrivingLicenseImg() {
        return drivingLicenseImg;
    }

    public void setDrivingLicenseImg(String drivingLicenseImg) {
        this.drivingLicenseImg = drivingLicenseImg;
    }

    public String getAutoInsuranceImg() {
        return autoInsuranceImg;
    }

    public void setAutoInsuranceImg(String autoInsuranceImg) {
        this.autoInsuranceImg = autoInsuranceImg;
    }

    public String getAnunalSurveyImg() {
        return anunalSurveyImg;
    }

    public void setAnunalSurveyImg(String anunalSurveyImg) {
        this.anunalSurveyImg = anunalSurveyImg;
    }

    public String getPlateImg() {
        return plateImg;
    }

    public void setPlateImg(String plateImg) {
        this.plateImg = plateImg;
    }

    public String getCarImg() {
        return carImg;
    }

    public void setCarImg(String carImg) {
        this.carImg = carImg;
    }

    public Long getCarType() {
        return carType;
    }

    public void setCarType(Long carType) {
        this.carType = carType;
    }

    public Long getCarLength() {
        return carLength;
    }

    public void setCarLength(Long carLength) {
        this.carLength = carLength;
    }

    public String getAutoInsuranceDate() {
        return autoInsuranceDate;
    }

    public void setAutoInsuranceDate(String autoInsuranceDate) {
        this.autoInsuranceDate = autoInsuranceDate;
    }

    public String getAnunalSurveyDate() {
        return anunalSurveyDate;
    }

    public void setAnunalSurveyDate(String anunalSurveyDate) {
        this.anunalSurveyDate = anunalSurveyDate;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }
}
