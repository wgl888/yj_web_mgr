package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class EditWebUserEvt extends BaseEvt implements Serializable {

    private static final long serialVersionUID = 5946594148279628900L;

    @Field(value = "用户ID" , length = 11 , nullable = false)
    private Long id;
    @Field(value = "用户组",length = 11,nullable = true)
    private Long userGroupId;
    @Field(value = "用户认证状态" , length = 12 , nullable = true)
    private String authStatus;
    @Field(value = "车主认证状态" , length = 12 , nullable = true)
    private String carownerStatus;
    @Field(value = "用户认证审批备注" , length = 512 , nullable = true)
    private String authAuditRemark;
    @Field(value = "车主认证审批备注" , length = 512 , nullable = true)
    private String carownerAuditRemark;
    @Field(value = "登陆密码" , length = 12 , nullable = true)
    private String loginPwd;
    @Field(value = "用户姓名" , length = 12 , nullable = true)
    private String userAlias;
    @Field(value = "用户性别" , length = 6 , nullable = true)
    private String userSex;
    @Field(value = "用户手机号码" , length = 11 , nullable = true)
    private String userPhone;
    @Field(value = "用户身份证号码" , length = 18 , nullable = true)
    private String userCardNo;
    @Field(value = "身份证有效期",length = 18,nullable = true)
    private String userCardPeriod;
    @Field(value = "身份证归属地",length = 256,nullable = true)
    private String userCardAddress;
    @Field(value = "身份证照片1",length = 512,nullable = true)
    private String cardImg1;
    @Field(value = "身份证照片2",length = 512,nullable = true)
    private String cardImg2;
    @Field(value = "身份证照片3",length = 512,nullable = true)
    private String cardImg3;

    public Long getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(Long userGroupId) {
        this.userGroupId = userGroupId;
    }

    public String getUserCardPeriod() {
        return userCardPeriod;
    }

    public void setUserCardPeriod(String userCardPeriod) {
        this.userCardPeriod = userCardPeriod;
    }

    public String getUserCardAddress() {
        return userCardAddress;
    }

    public void setUserCardAddress(String userCardAddress) {
        this.userCardAddress = userCardAddress;
    }

    public String getCardImg1() {
        return cardImg1;
    }

    public void setCardImg1(String cardImg1) {
        this.cardImg1 = cardImg1;
    }

    public String getCardImg2() {
        return cardImg2;
    }

    public void setCardImg2(String cardImg2) {
        this.cardImg2 = cardImg2;
    }

    public String getCardImg3() {
        return cardImg3;
    }

    public void setCardImg3(String cardImg3) {
        this.cardImg3 = cardImg3;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getUserAlias() {
        return userAlias;
    }

    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserCardNo() {
        return userCardNo;
    }

    public void setUserCardNo(String userCardNo) {
        this.userCardNo = userCardNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getCarownerStatus() {
        return carownerStatus;
    }

    public void setCarownerStatus(String carownerStatus) {
        this.carownerStatus = carownerStatus;
    }

    public String getAuthAuditRemark() {
        return authAuditRemark;
    }

    public void setAuthAuditRemark(String authAuditRemark) {
        this.authAuditRemark = authAuditRemark;
    }

    public String getCarownerAuditRemark() {
        return carownerAuditRemark;
    }

    public void setCarownerAuditRemark(String carownerAuditRemark) {
        this.carownerAuditRemark = carownerAuditRemark;
    }
}
