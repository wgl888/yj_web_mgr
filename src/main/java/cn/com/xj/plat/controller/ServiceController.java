package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.customService.EditCustomServiceEvt;
import cn.com.xj.plat.evt.customService.QueryCustomServiceEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.CustomServService;
import cn.com.xj.plat.util.LoginUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Date;

// 售后
@Controller
@RequestMapping("/service")
public class ServiceController extends BaseController {

    @Autowired
    private CustomServService customServService;

    @RequestMapping("/list")
    public String list(QueryCustomServiceEvt evt,Model model){
        return "/service/list";
    }

    @RequestMapping("/page")
    public String page(QueryCustomServiceEvt evt, Model model){
        try{
            BaseFactory.getInstance().page(customServService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/service/page";
    }


    @RequestMapping("/toService")
    public String toService(QueryCustomServiceEvt evt,
                           Model model){
        if(evt.getId()==null)return "/common/error";
        BaseFactory.getInstance().toDetail(customServService, BeanUtil.transBean2Map(evt),model);
        return "/service/service";
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryCustomServiceEvt evt,
                            Model model){
        if(evt.getId()==null)return "/common/error";
        BaseFactory.getInstance().toDetail(customServService,BeanUtil.transBean2Map(evt),model);
        return "/service/detail";
    }


    @ResponseBody
    @RequestMapping(value = "/doService",method = RequestMethod.POST)
    public ServiceResp toService(EditCustomServiceEvt evt,HttpSession session){
        try {
            evt.setReplyUserId(LoginUtils.getId(session));
            evt.setReplyUserName(LoginUtils.getRealName(session));
            return customServService.handle(BeanUtil.transBean2Map(evt));
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

}
