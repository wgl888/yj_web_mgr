package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.carWeight.AddCarWeightEvt;
import cn.com.xj.plat.evt.carWeight.EditCarWeightEvt;
import cn.com.xj.plat.evt.carWeight.QueryCarWeightEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.CarLengthService;
import cn.com.xj.plat.service.CarWeightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/carWeight")
public class CarWeightController extends BaseController {

    @Autowired
    private CarWeightService carWeightService;
    @Autowired
    private CarLengthService carLengthService;

    @RequestMapping("/list")
    public String list() {
        return "/carWeight/list";
    }

    @RequestMapping("/page")
    public String page(QueryCarWeightEvt evt, Model model) {
        try {
            BaseFactory.getInstance().page(carWeightService, evt, model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/carWeight/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/carWeight/edit";
    }


    @RequestMapping(value = "doAdd", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddCarWeightEvt evt, HttpSession session) {
        try {
            return BaseFactory.getInstance().add(carWeightService, evt, session);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryCarWeightEvt evt,
                         Model model) {
        try {
            if (evt.getId() == null) {
                return "/common/error";
            }
            BaseFactory.getInstance().toEdit(carWeightService, BeanUtil.transBean2Map(evt), model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/carWeight/edit";
    }

    @RequestMapping(value = "doEdit", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditCarWeightEvt evt) {
        try {
            return BaseFactory.getInstance().doEdit(carWeightService, evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


}
