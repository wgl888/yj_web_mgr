package cn.com.xj.plat.evt.order;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;
import cn.com.xj.plat.evt.cargo.AddCargoEvt;
import java.util.Date;
import java.util.List;

public class EditOrderEvt extends BaseEvt {


    @Field(value = "订单ID", length = 11, nullable = true)
    private Long id;
    @Field(value = "订单发起人ID", length = 11, nullable = true)
    private Long userId;
    @Field(value = "订单发起人姓名", length = 12, nullable = true)
    private String userName;
    @Field(value = "订单发起人电话", length = 11, nullable = true)
    private String userMobile;
    @Field(value = "期望费用", nullable = true)
    private Float expectCost;
    @Field(value = "最终费用", nullable = true)
    private Float finalCost;
    @Field(value = "启运时间", length = 128, nullable = true)
    private String startTime;
    @Field(value = "结束时间", length = 128, nullable = true)
    private String endTime;
    @Field(value = "发货地址", length = 512, nullable = true)
    private String shippingAddress;
    @Field(value = "发货地址", length = 512, nullable = true)
    private String shippingRegion;
    @Field(value = "发货人姓名", length = 11, nullable = true)
    private String shippingName;
    @Field(value = "发货人电话", length = 11, nullable = true)
    private String shippingPhone;
    @Field(value = "收货地址", length = 512, nullable = true)
    private String deliveryAddress;
    @Field(value = "收货地址", length = 512, nullable = true)
    private String deliveryRegion;
    @Field(value = "收货人姓名", length = 11, nullable = true)
    private String deliveryName;
    @Field(value = "收货人电话", length = 11, nullable = true)
    private String deliveryPhone;
    @Field(value = "距离", nullable = true)
    private Float distance;
    @Field(value = "承保的保险公司ID", length = 11, nullable = true)
    private Long insurerId;
    @Field(value = "保费", nullable = true)
    private Float insurerCost;
    @Field(value = "承运人ID", length = 11, nullable = true)
    private Long freighter;
    @Field(value = "承运人姓名", length = 12, nullable = true)
    private String freighterName;
    @Field(value = "承运人电话", length = 11, nullable = true)
    private String freighterMobile;
    @Field(value = "车辆类型ID", length = 11, nullable = true)
    private Long carTypeId;
    @Field(value = "车辆类型名称", length = 32, nullable = true)
    private String carTypeName;
    @Field(value = "车辆长度ID", length = 11, nullable = true)
    private Long carLengthId;
    @Field(value = "车辆长度值", length = 32, nullable = true)
    private String carLengthValue;
    @Field(value = "货主留言", length = 1024, nullable = true)
    private String shipperMessage;
    @Field(value = "订单状态", length = 12, nullable = true)
    private String orderStatus;
    @Field(value = "货物类型", length = 12, nullable = true)
    private String cargoType;
    @Field(value = "货物价值", length = 12, nullable = true)
    private Float cargoCost;
    @Field(value = "货物", nullable = true)
    private List<AddCargoEvt> goods;
    @Field(value = "货物类型名称", length = 12, nullable = true)
    private String cargoTypeName;
    @Field(value = "取消原因", length = 512, nullable = true)
    private String cancelReasion;
    @Field(value = "货主是否评价", length = 12, nullable = true)
    private String userPj;
    @Field(value = "司机是否评价", length = 12, nullable = true)
    private String driverPj;
    @Field(value = "保证金", nullable = true)
    private Float guaranty;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg1;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg2;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg3;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg4;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg5;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg6;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg7;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg8;
    @Field(value = "发货照片", length = 512, nullable = true)
    private String deliverImg9;
    @Field(value = "发货定位地址经纬度", length = 100, nullable = true)
    private String sendGps;
    @Field(value = "发货定位地址", length = 512, nullable = true)
    private String sendGpsAddr;
    @Field(value = "发货时间", nullable = true)
    private Date sendTime;
    @Field(value = "一键开票支付密码", nullable = true)
    private String paySecret;
    @Field(value = "服务费", nullable = true)
    private Float platFee;

    public Float getPlatFee() {
        return platFee;
    }

    public void setPlatFee(Float platFee) {
        this.platFee = platFee;
    }

    public String getPaySecret() {
        return paySecret;
    }

    public void setPaySecret(String paySecret) {
        this.paySecret = paySecret;
    }

    public Float getFinalCost() {
        return finalCost;
    }

    public void setFinalCost(Float finalCost) {
        this.finalCost = finalCost;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public String getSendGps() {
        return sendGps;
    }

    public void setSendGps(String sendGps) {
        this.sendGps = sendGps;
    }

    public String getSendGpsAddr() {
        return sendGpsAddr;
    }

    public void setSendGpsAddr(String sendGpsAddr) {
        this.sendGpsAddr = sendGpsAddr;
    }

    public String getDeliverImg1() {
        return deliverImg1;
    }

    public void setDeliverImg1(String deliverImg1) {
        this.deliverImg1 = deliverImg1;
    }

    public String getDeliverImg2() {
        return deliverImg2;
    }

    public void setDeliverImg2(String deliverImg2) {
        this.deliverImg2 = deliverImg2;
    }

    public String getDeliverImg3() {
        return deliverImg3;
    }

    public void setDeliverImg3(String deliverImg3) {
        this.deliverImg3 = deliverImg3;
    }

    public String getDeliverImg4() {
        return deliverImg4;
    }

    public void setDeliverImg4(String deliverImg4) {
        this.deliverImg4 = deliverImg4;
    }

    public String getDeliverImg5() {
        return deliverImg5;
    }

    public void setDeliverImg5(String deliverImg5) {
        this.deliverImg5 = deliverImg5;
    }

    public String getDeliverImg6() {
        return deliverImg6;
    }

    public void setDeliverImg6(String deliverImg6) {
        this.deliverImg6 = deliverImg6;
    }

    public String getDeliverImg7() {
        return deliverImg7;
    }

    public void setDeliverImg7(String deliverImg7) {
        this.deliverImg7 = deliverImg7;
    }

    public String getDeliverImg8() {
        return deliverImg8;
    }

    public void setDeliverImg8(String deliverImg8) {
        this.deliverImg8 = deliverImg8;
    }

    public String getDeliverImg9() {
        return deliverImg9;
    }

    public void setDeliverImg9(String deliverImg9) {
        this.deliverImg9 = deliverImg9;
    }

    public Float getGuaranty() {
        return guaranty;
    }

    public void setGuaranty(Float guaranty) {
        this.guaranty = guaranty;
    }

    public String getUserPj() {
        return userPj;
    }

    public void setUserPj(String userPj) {
        this.userPj = userPj;
    }

    public String getDriverPj() {
        return driverPj;
    }

    public void setDriverPj(String driverPj) {
        this.driverPj = driverPj;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCancelReasion() {
        return cancelReasion;
    }

    public void setCancelReasion(String cancelReasion) {
        this.cancelReasion = cancelReasion;
    }

    public String getCargoTypeName() {
        return cargoTypeName;
    }

    public void setCargoTypeName(String cargoTypeName) {
        this.cargoTypeName = cargoTypeName;
    }

    public List<AddCargoEvt> getGoods() {
        return goods;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public String getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(String deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }

    public void setGoods(List<AddCargoEvt> goods) {
        this.goods = goods;
    }

    public Float getInsurerCost() {
        return insurerCost;
    }

    public void setInsurerCost(Float insurerCost) {
        this.insurerCost = insurerCost;
    }

    public String getCargoType() {
        return cargoType;
    }

    public void setCargoType(String cargoType) {
        this.cargoType = cargoType;
    }

    public Float getCargoCost() {
        return cargoCost;
    }

    public void setCargoCost(Float cargoCost) {
        this.cargoCost = cargoCost;
    }


    public Float getExpectCost() {
        return expectCost;
    }

    public void setExpectCost(Float expectCost) {
        this.expectCost = expectCost;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getShippingRegion() {
        return shippingRegion;
    }

    public void setShippingRegion(String shippingRegion) {
        this.shippingRegion = shippingRegion;
    }

    public String getDeliveryRegion() {
        return deliveryRegion;
    }

    public void setDeliveryRegion(String deliveryRegion) {
        this.deliveryRegion = deliveryRegion;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Long getInsurerId() {
        return insurerId;
    }

    public void setInsurerId(Long insurerId) {
        this.insurerId = insurerId;
    }

    public Long getFreighter() {
        return freighter;
    }

    public void setFreighter(Long freighter) {
        this.freighter = freighter;
    }

    public Long getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(Long carTypeId) {
        this.carTypeId = carTypeId;
    }

    public Long getCarLengthId() {
        return carLengthId;
    }

    public void setCarLengthId(Long carLengthId) {
        this.carLengthId = carLengthId;
    }

    public String getShipperMessage() {
        return shipperMessage;
    }

    public void setShipperMessage(String shipperMessage) {
        this.shipperMessage = shipperMessage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getFreighterName() {
        return freighterName;
    }

    public void setFreighterName(String freighterName) {
        this.freighterName = freighterName;
    }

    public String getFreighterMobile() {
        return freighterMobile;
    }

    public void setFreighterMobile(String freighterMobile) {
        this.freighterMobile = freighterMobile;
    }

    public String getCarTypeName() {
        return carTypeName;
    }

    public void setCarTypeName(String carTypeName) {
        this.carTypeName = carTypeName;
    }

    public String getCarLengthValue() {
        return carLengthValue;
    }

    public void setCarLengthValue(String carLengthValue) {
        this.carLengthValue = carLengthValue;
    }
}
