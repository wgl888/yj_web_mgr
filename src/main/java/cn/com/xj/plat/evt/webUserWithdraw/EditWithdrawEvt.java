package cn.com.xj.plat.evt.webUserWithdraw;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class EditWithdrawEvt extends BaseEvt implements Serializable {

    private static final long serialVersionUID = 5946594148279628900L;

    @Field(value = "ID", length = 11, nullable = false)
    private Long id;
    @Field(value = "提现金额", length = 11, nullable = true)
    private Double amount;
    @Field(value = "收款人姓名", length = 32, nullable = true)
    private String bankUserName;
    @Field(value = "银行卡号", length = 32, nullable = true)
    private String bankNo;
    @Field(value = "收款人手机号", length = 11, nullable = true)
    private String mobile;
    @Field(value = "收款人身份证号", length = 18, nullable = true)
    private String idCardNo;
    @Field(value = "支付订单号", length = 32, nullable = true)
    private String orderNo;
    @Field(value = "提现状态", length = 12, nullable = true)
    private String withdrawStatus;


    public String getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(String withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getBankUserName() {
        return bankUserName;
    }

    public void setBankUserName(String bankUserName) {
        this.bankUserName = bankUserName;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
