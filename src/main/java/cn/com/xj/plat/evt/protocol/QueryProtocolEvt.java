package cn.com.xj.plat.evt.protocol;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryProtocolEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;

    @Field(length = 11,value = "ID",nullable = true)
    private Long id;
    @Field(value = "协议名称",length = 64,nullable = true)
    private String protocolName;
    @Field(value = "协议编码",length = 64,nullable = true)
    private String protocolCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    public String getProtocolCode() {
        return protocolCode;
    }

    public void setProtocolCode(String protocolCode) {
        this.protocolCode = protocolCode;
    }

}
