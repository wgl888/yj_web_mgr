package cn.com.xj.plat.evt.carWeightLength;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddCarWeightLengthEvt extends BaseEvt {

    @Field(length = 11 , value = "车类型ID",nullable = false)
    private Long carWeightId;
    @Field(length = 11 , value = "车类型名称",nullable = false)
    private Long carLengthId;

    public Long getCarWeightId() {
        return carWeightId;
    }

    public void setCarWeightId(Long carWeightId) {
        this.carWeightId = carWeightId;
    }

    public Long getCarLengthId() {
        return carLengthId;
    }

    public void setCarLengthId(Long carLengthId) {
        this.carLengthId = carLengthId;
    }

}
