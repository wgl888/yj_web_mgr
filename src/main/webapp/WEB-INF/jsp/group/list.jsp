<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>用户组列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true" />
        <jsp:param name="ZUIplugin" value="true" />
        <jsp:param name="jqueryUI" value="true" />
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="${applicationScope.staticFilepath }/assets/css/jui.css" />
</head>
<body class="jui-linghtgreen-theme">
<div class="clearfix">
    <div class="jui-view-content">
        <div class="main-content-inner">
            <div class="jui-page">
                <!-- /.page-content -->
                <div class="page-content" >
                    <div class="jui-search-icon"></div>
                    <!-- /.page-form -->
                    <form  method="post" id="queryForm"class="form-horizontal jui-form-justify" role="form">
                        <input type="hidden" id="superId" name="superId" value="${groupId}" >
                        <div class="clearfix jui-form-group">
                            <div class="col-sm-4">
                                <label class="pull-left control-label">用户组名称</label>
                                <div class="pull-left jui-form-group-content">
                                    <div class="pos-rel">
                                        <input type="text" class="min-input"id="groupName" name="groupName"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix jui-form-group ">
                                <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询</button>
                                <button onclick="javascript:resetSearch(1);"type="button" class="btn btn-orange inline-block" >重置</button>
                            </div>
                        </div>
                    </form>
                    <!-- /.page-form -->
                </div>
                <%--style="margin-top:-1px"--%>
                <div class="page-content margin-top">
                    <div class="btn-group">
                        <c:if test="${MODaddGroup==true}">
                            <button onclick="add(1);" type="button" class="btn btn-info">新增用户组</button>
                            <button  onclick="add(2);"type="button" class="btn btn-lightgreen">新增扩展用户组</button>
                        </c:if>
                    </div>

                    <table class="jui-theme-table table-hover margin-top" id="myTable">
                        <thead>
                        <tr>
                            <th >ID</th>
                            <th >名称</th>
                            <th >类型</th>
                            <%--<th >地区</th>--%>
                            <th>排序</th>
                            <th>创建者</th>
                            <th >操作</th>
                        </tr>
                        </thead>

                        <tbody id="paging_content">

                        </tbody>
                    </table>
                    <div id="paging" style="margin:10px 0 0 0;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="Jui-silder s1">
        <div class="Jui-silder-view">
            <div class="Jui-silder-head">用户组列表</div>
            <div class="Jui-silder-main">
                <ul class="Jui-silder-list ztree" id="tree">
                </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /** 分页 */
    function loadPage(init){
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/group/page';
        paging1.paging(init,target);
        if(init*1==1){
            initial('#data_total',paging1,'paging',target);
        }
    }
    $(document).ready(function () {
        loadPage(1);
        //iframe 高度
        dyncHeight(null);
    });
    function resetSearch() {
        $('#groupName').val('');
        loadPage(1);
    }
    add = function(type){
        var pid = $('#superId').val();
        if(type==2){
            window.location.href='${pageContext.request.contextPath}/group/toAddExtends?superId='+pid+"&groupType="+type;
        }else{
            var he = 310;//高度调整
            //        if (type==1){
            //            he = 260;
            //        }
            layer_show(type==1?'新增用户组':'新增扩展用户组',
                '${pageContext.request.contextPath}/group/toAdd?superId='+pid+"&groupType="+type,'500',he,this);
        }
    }
    edit = function(id,superId,type){
        if(type==2){
            window.location.href='${pageContext.request.contextPath}/group/toEditExtends?id='+id+'&superId='+superId;
        }else{
            var he = 310;//高度调整
            //        if (type==1){
            //            he = 260;
            //        }
            layer_show(type==1?'编辑用户组':'编辑扩展用户组',
                '${pageContext.request.contextPath}/group/toEdit?id='+id+'&groupType='+type+'&superId='+superId,'500',he,this);
        }
    }
    <%--add = function(type){--%>
        <%--var pid = $('#superId').val();--%>
        <%--var he = 310;//高度调整--%>
<%--//        if (type==1){--%>
<%--//            he = 260;--%>
<%--//        }--%>
        <%--layer_show(type==1?'新增用户组':'新增扩展用户组',--%>
            <%--'${pageContext.request.contextPath}/group/toAdd?superId='+pid+"&groupType="+type,'500',he,this);--%>
    <%--}--%>
    <%--edit = function(id,superId,type){--%>
        <%--var he = 310;//高度调整--%>
<%--//        if (type==1){--%>
<%--//            he = 260;--%>
<%--//        }--%>
        <%--layer_show(type==1?'编辑用户组':'编辑扩展用户组',--%>
            <%--'${pageContext.request.contextPath}/group/toEdit?id='+id+'&groupType='+type+'&superId='+superId,'500',he,this);--%>
    <%--}--%>
    chooseRole = function(id){
        layer_show('角色分配','${pageContext.request.contextPath}/group/chooseRole?id='+id,'700','480',this);
    }
    /**保存用户组排序*/
    setSort = function(id,ipt){
        var newSort = $(ipt).val().trim();
        var oldSort =  $(ipt).attr("data");
        if(newSort == oldSort ){
            return;
        }
        if(newSort==""){
            return;
        }
        $.ajax({
            type : "post",
            url : "<%=request.getContextPath() %>/group/doEdit",
            data : {"id":id,"sort":newSort},
            success : function(data){
                var json = JSON.parse(data);
                if(json.retCode==0){
                    parent.layer.msg(json.retMsg, {icon: 1,time:2000});
                    reload(0);
                }else{
                    parent.layer.msg(json.retMsg, {icon: 2,time:2000});
                }
            }
        });
    }
    changeState = function(id,state,groupType){
        var title = '';
        if(state == 1){
            title = '确认要禁用此'+(groupType==1?'用户组':'自定义用户组')+'吗？';
            state = 'F';
        }else{
            title = '确认要启用此'+(groupType==1?'用户组':'自定义用户组')+'吗?';
            state = 'E';
        }
        parent.layer.confirm(title,function(index){
            $.ajax({
                type : "post",
                url : "<%=request.getContextPath() %>/group/changeStatus",
                data : {"id":id,"status":state},
                success : function(data){
                    var json = JSON.parse(data);
                    if(json.retCode==0){
                        parent.layer.msg(json.retMsg, {icon: 1,time:2000});
                        reload(1);
                    }else{
                        parent.layer.msg(json.retMsg, {icon: 2,time:2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    };
    del = function(id){
        parent.layer.confirm('确认要删除吗？',function(index){
            $.ajax({
                type : "post",
                url : "<%=request.getContextPath() %>/group/doDelete.htm",
                data : {"id":id},
                success : function(data){
                    var json = JSON.parse(data);
                    if(json.retCode==1){
                        parent.layer.msg(json.retMsg, {icon: 1,time:2000});
                        reload(1);
                    }else{
                        parent.layer.msg(json.retMsg, {icon: 2,time:2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }
    layerCallBack = function(){
        reload(0);
    }
    reload = function(init){
        $.fn.zTree.init($("#tree"), setting, zNodes);
        loadPage(init);
    }
</script>
<script type="text/javascript">
    $(".Jui-silder-main").css('height',"600px").panel({iWheelStep:32});//这是滚动插件的用法  需要手动定义高度
    var setting = {
        async: {
            enable: true,
            url:getUrl
        },
        check: {
            enable: false
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            expandSpeed: "",
            selectedMulti: false
        },
        callback: {
            onAsyncSuccess: onAsyncSuccess,
            onAsyncError: onAsyncError,
            onCheck:onCheck,
            onClick:onClick
        }
    };
    $(function(){
        $.fn.zTree.init($("#tree"), setting, zNodes);
//        $("#add1").hide();
//        $("#add2").hide();
    });
    var zNodes =[];
    function getUrl(treeId, treeNode) {
        return "<%=request.getContextPath() %>/group/userGroupTree";
    }
    function onAsyncSuccess(event, treeId, treeNode, msg) {
        if (!msg || msg.length == 0) {
            return;
        }
        if(msg=="-1"){
            parent.layer.msg("对不起，你传入的参数有误，请重试！", {icon: 2,time:2000});
            return;
        }
        var zTree = $.fn.zTree.getZTreeObj("tree");
//        zTree.expandAll(true);
        var nodes = zTree.getNodes();
        for (var i = 0; i < nodes.length; i++) { //设置节点展开
            zTree.expandNode(nodes[i],true);//展开一级节点
        }
        var pnode = nodes[0];
        zTree.selectNode(pnode);
    }
    function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        parent.layer.msg("异步获取数据出现异常！", {icon: 2,time:2000});
        treeNode.icon = "";
        zTree.updateNode(treeNode);
    }
    //点击目录时调用
    function onCheck(event,treeId,treeNode, clickFlag) {
        //获取选中的节点
        var treeObj=$.fn.zTree.getZTreeObj("tree"),
            nodes=treeObj.getCheckedNodes(true),
            v="";
        for(var i=0;i<nodes.length;i++){
            v+=nodes[i].id + "#";
        }
    }
    //点击目录时调用
    function onClick(event,treeId,treeNode, clickFlag) {
        var sid = treeNode.id;
        if(sid*1==-1){
            sid = 0;
        }
        $('#superId').val(sid);//设为当前选中节点
        $.ajax({
            url: "<%=request.getContextPath() %>/group/queryByParam",
            type: "post",
            data: {id: sid},
            dataType:"json",
            success:function (data) {
                if(data.retData!=1){
                    $("#add1").hide();
                    $("#add2").hide();
                }else{
                    $("#add1").show();
                    $("#add2").show();
                }
            }
        });
        loadPage(1);
    }
</script>
<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function(){
        $(this).parent().hasClass("jui-hide")?$(this).parent().removeClass("jui-hide"):$(this).parent().addClass("jui-hide");
    });
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
