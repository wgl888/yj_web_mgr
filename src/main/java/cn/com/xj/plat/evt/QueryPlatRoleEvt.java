package cn.com.xj.plat.evt;

import cn.com.xj.common.enums.YesOrNo;
import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryPlatRoleEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;

    @Field(length = 11,value = "角色ID",nullable = true)
    private Long id;
    @Field(length = 256 , value = "角色名称",nullable = true)
    private String roleName;
    @Field(length = 1,value = "是否加载全权限",nullable = true)
    private String loadAuthor = YesOrNo.N.name();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getLoadAuthor() {
        return loadAuthor;
    }

    public void setLoadAuthor(String loadAuthor) {
        this.loadAuthor = loadAuthor;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
