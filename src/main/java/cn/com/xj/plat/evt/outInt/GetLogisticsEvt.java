package cn.com.xj.plat.evt.outInt;

import cn.com.xj.common.param.valid.Field;

/**
 * @author wanggl
 * @version V1.0
 * @Title: PostEvt
 * @Package cn.com.xj.web.app.evt
 * @Description: (通过客户ID 时间段获取未开票列表)
 * @date 2017/8/12 11:18
 */
public class GetLogisticsEvt {

    @Field(value = "公司编码",length = 32,nullable = false)
    private String companyCode;
    @Field(value = "快递订单号",length = 32,nullable = false)
    private String logisNumber;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getLogisNumber() {
        return logisNumber;
    }

    public void setLogisNumber(String logisNumber) {
        this.logisNumber = logisNumber;
    }
}
