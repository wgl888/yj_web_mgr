<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td  value="${item.id }">${item.id }</td>
				<td>${item.taskName }</td>
				<td>
					<jsp:useBean id="startTime" class="java.util.Date"/>
					<c:set target="${startTime}" property="time" value="${item.startTime}"/>
					<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${startTime}" type="both"/>
				</td>
				<td>
					<jsp:useBean id="endTime" class="java.util.Date"/>
					<c:set target="${endTime}" property="time" value="${item.endTime}"/>
					<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${endTime}" type="both"/>
				</td>
				<td>
					${item.timeLong/1000}秒
				</td>
				<td>
					${item.hasException=='Y'?'<strong style="color:red">是</strong>':'<strong style="color:green">否</strong>'}
				</td>
				<td >
					<button type="button" class="btn btn-xs btn-warning"
							onclick="detail(${item.id})">详情</button>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="7">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
