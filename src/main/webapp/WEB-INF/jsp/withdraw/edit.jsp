<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>编辑提前申请信息</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="plupload" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/withdraw/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }合作银行</h3>
                <h5>设置合作银行，前台用户仅能添加已配置的银行。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/withdraw/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>基本信息：</label>
                </dt>
                <dd class="opt">
                    用户姓名：<a href="javascript:userDetail(${item.webUserId})" style="color:blue;">
                    ${item.webUserName}</a><br>
                    提现备注：${item.remark!=null?item.remark:"-" }
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>提现金额</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.amount }" placeholder="" class="input-txt"
                           id="amount" name="amount" datatype="*1-11" maxlength="11"
                           placeholder="请输入提现金额" nullmsg="提现金额不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入最终要提现的金额。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>收款人姓名</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankUserName }" class="input-txt"
                           id="bankUserName" name="bankUserName" datatype="*1-32" maxlength="32"
                           placeholder="请输入收款人姓名" nullmsg="收款人姓名不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入提现银行卡卡号在银行登记的户主姓名。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>收款人电话</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.mobile }" placeholder="" class="input-txt"
                           id="mobile" name="mobile" datatype="*1-11" maxlength="11"
                           placeholder="请输入收款人电话" nullmsg="收款人电话不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入提现银行卡卡号在银行登记的手机号码。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>身份证号码</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.idCardNo }" placeholder="" class="input-txt"
                           id="idCardNo" name="idCardNo" datatype="*1-18" maxlength="18"
                           placeholder="请输入身份证号码" nullmsg="身份证号码不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入银行卡开户人的18位身份证号码。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>收款人卡号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankNo }" placeholder="" class="input-txt"
                           id="bankNo" name="bankNo" datatype="*1-32" maxlength="32"
                           placeholder="请输入银行卡号" nullmsg="银行卡号不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入并仔细核对要提现的银行卡号。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/withdraw/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });

        var ids = $('#editIds').val();
        if(ids!=null&&ids!=""){
            var array = ids.split(',');
            for(var i=0;i<array.length;i++){
                $('#length_'+array[i]).attr("checked",true);
            }
        }

    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>

<script type="application/javascript">

    var uploadUrl = '<%=request.getContextPath() %>/upload/uploadImg';
    var ids = new Array("bankIcon_file");

    $.each(ids,function(i,n){
        var self = this.toString();
        var uploader = new plupload.Uploader({
            browse_button : self, //触发文件选择对话框的按钮，为那个元素id
            url : uploadUrl ,//服务器端的上传页面地址
            max_file_size: '5mb',//限制为2MB
            filters: [{title: "Image files",extensions: "jpg,png"}]//图片限制
        });

        //在实例对象上调用init()方法进行初始化
        uploader.init();
        //绑定各种事件，并在事件监听函数中做你想做的事
        uploader.bind('FilesAdded',function(uploader,files){
            var loading = "<%=request.getContextPath() %>/myResources/manager/plat/images/imgloading1.gif";
            $("#"+self).parent().find('img').attr('src',loading);
            uploader.start();
        });

        uploader.bind('FileUploaded', function (uploader, file, responseObject) {
            var rs = responseObject.response;
            if(rs==''||rs==null||rs*1==-1*1){
                parent.layer.open({
                    title: '失败',
                    content: '上传失败，请稍后再试。'
                });
            }
            $("#"+self).parent().find('img').attr('src',responseObject.response);
            var hid = self.split("_")[0] + "_hid";
            $("#"+hid).val(responseObject.response);
            var del = self.split("_")[0] + "_del";
            $("#"+del).show();
        });

    });

</script>

</body>
</html>
