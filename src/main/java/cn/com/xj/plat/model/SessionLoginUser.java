package cn.com.xj.plat.model;

import cn.com.xj.common.enums.YesOrNo;

/**
 * @author wanggl
 * @version V1.0
 * @Title: SessionLoginUser
 * @Package cn.com.xj.plat.model
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/6/19 16:47
 */
public class SessionLoginUser {

    /**用户ID*/
    private Long id;
    /**登录账号*/
    private String loginName;
    /**管理员名称*/
    private String userName;
    /**联系电话*/
    private String contractTel;
    /** 是否超级管理员 */
    private String isSuper;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContractTel() {
        return contractTel;
    }

    public void setContractTel(String contractTel) {
        this.contractTel = contractTel;
    }

    public String getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(String isSuper) {
        this.isSuper = isSuper;
    }
}
