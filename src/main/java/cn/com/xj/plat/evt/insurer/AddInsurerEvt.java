package cn.com.xj.plat.evt.insurer;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddInsurerEvt extends BaseEvt {

    @Field(value = "保险公司名称",length = 16,nullable = false)
    private String insurerName;
    @Field(value = "保险协议*",length = 99999999,nullable = true)
    private String insurerAgreen;
    @Field(value = "是否可用",length = 1,nullable = true)
    private String isEnable;
    @Field(value = "排序",length = 12,nullable = true)
    private Integer sort;

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    public String getInsurerName() {
        return insurerName;
    }

    public void setInsurerName(String insurerName) {
        this.insurerName = insurerName;
    }

    public String getInsurerAgreen() {
        return insurerAgreen;
    }

    public void setInsurerAgreen(String insurerAgreen) {
        this.insurerAgreen = insurerAgreen;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
