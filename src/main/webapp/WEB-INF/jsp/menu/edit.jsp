<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/menu/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }菜单</h3>
                <h5>设置系统里面使用到的菜单。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>菜单编码：按钮鉴权的时候使用,必须使用固定的值,否则无法进行权限控制。</li>
            <li>是否为本系统菜单：用来区分是否要替换服务编码。</li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/menu/${item.id==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="parentId" name="parentId" value="${item.parentId }">
        <input type="hidden" id="id" name="id" value="${item.id }">

        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>菜单类型</label>
                </dt>
                <dd class="opt">
                    <label>
                        <input type="radio" class="ace"
                               name="isMenu" ${item.isMenu=='1'||item.isMenu==null?'checked':''} value="1"/>
                        <span class="lbl"> 菜单</span>
                    </label>
                    <label>
                        <input type="radio" class="ace"
                               name="isMenu" ${item.isMenu=='0'?'checked':''} value="0"/>
                        <span class="lbl"> 功能</span>
                    </label>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>菜单名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.menuName }" class="input-txt"
                           datatype="*" maxlength="15" nullmsg="菜单名称不能为空"
                           id="menuName" name="menuName" placeholder="请输入菜单名称...">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        2-15位字符，可由中文、英文、数字及标点符号组成。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="menuUrl">菜单URL</label>
                </dt>
                <dd class="opt">
                    <input type="text" class="input-txt" value="${item.menuUrl }"
                           id="menuUrl" name="menuUrl" placeholder="请输入菜单URL..." maxlength="160">
                    <p class="notic">
                        点击菜单跳转的地址,如果是外部链接需要一http://开头,内部链接由开发团队配置.
                    </p>
                </dd>
            </dl>
            <dl class="row" id="row_isLocal">
                <dt class="tit">
                    <label><em>*</em>是否本系统菜单</label>
                </dt>
                <dd class="opt">
                    <label>
                        <input type="radio" class="ace"
                               name="isLocal" ${item.isLocal=='1'||item.isLocal==null?'checked':''} value="1"/>
                        <span class="lbl"> 是</span>
                    </label>
                    <label>
                        <input type="radio" class="ace" name="isLocal" ${item.isLocal=='0'?'checked':''} value="0"/>
                        <span class="lbl"> 否</span>
                    </label>
                </dd>
            </dl>
            <dl class="row" id="row_openType">
                <dt class="tit">
                    <label><em>*</em>打开方式</label>
                </dt>
                <dd class="opt">
                    <label>
                        <input type="radio" class="ace"
                               name="openType" ${item.openType=='_self'||item.openType==null?'checked':''}
                               value="_self"/>
                        <span class="lbl"> 当前窗口</span>
                    </label>
                    <label>
                        <input type="radio" class="ace" name="openType" ${item.openType=='_blank'?'checked':''}
                               value="_blank"/>
                        <span class="lbl"> 新窗口</span>
                    </label>

                    <span class="err"></span>
                </dd>
            </dl>
            <dl class="row" id="row_menuImg">
                <dt class="tit">
                    <label>菜单图标</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.menuImg }" class="input-txt"
                           id="menuImg" name="menuImg">
                    <p class="notic">
                        2-15位字符，可由中文、英文、数字及标点符号组成。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">

    $(".cb-enable").click(function () {
        $("#isLocal").val("1");
    });
    $(".cb-disable").click(function () {
        $("#isLocal").val("0");
    });

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/menu/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });


    $('input[name="isMenu"]').click(function(){
        var val = $(this).val();
        if(1*1 == val){
            $('#row_isLocal').show();
            $('#row_openType').show();
            $('#row_menuImg').show();
        }else{
            $('#row_isLocal').hide();
            $('#row_openType').hide();
            $('#row_menuImg').hide();
        }
    });

</script>
</body>
</html>
