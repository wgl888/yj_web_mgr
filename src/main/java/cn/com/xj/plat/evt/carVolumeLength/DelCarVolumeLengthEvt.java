package cn.com.xj.plat.evt.carVolumeLength;

import cn.com.xj.common.param.valid.Field;

public class DelCarVolumeLengthEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long carLengthId;

    public Long getCarLengthId() {
        return carLengthId;
    }

    public void setCarLengthId(Long carLengthId) {
        this.carLengthId = carLengthId;
    }
}
