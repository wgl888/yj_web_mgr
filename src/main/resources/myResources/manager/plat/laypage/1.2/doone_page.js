﻿var PagingMng = function (){
	var pagingManage = new Object();
	pagingManage.curr = 1;
	pagingManage.total = 0;
	pagingManage.pageUri = '';
	pagingManage.pageSize = 10;
	pagingManage.queryForm = 'queryForm';
	pagingManage.total = '';
	pagingManage.pageCallBack = function(){
		
	}
	pagingManage.paging = function(_init,target){
		if(this.pageUri==''){
			alert("参数缺失,无法查询");
			return;
		}
		$(target).empty();
		$.ajax({
			type : "post",
			url : this.pageUri,
			data : $('#'+this.queryForm).serialize() + 
					"&queryPage=" + this.curr + "&init="
					+ _init,
			async : false,
			success : function(data) {
				$(target).append(data);
			}
		});
		this.pageCallBack();
	};

	return pagingManage;
}

PagingManage = {
	'curr' : 1, 
	'total' : 0,
	'pageUri' :'',
	'pageSize' : 10,
	'queryForm' : 'queryForm',
	'total' : '',
	'create' : function(){
		return this;
    },
    'pageCallBack':function(){
    	
    },
    'paging' : function(_init,target){
    	if(this.pageUri==''){
			alert("参数缺失,无法查询");
			return;
		}
		$(target).empty();
			$.ajax({
				type : "post",
				url : this.pageUri,
				data : $('#'+this.queryForm).serialize() + 
						"&queryPage=" + this.curr + "&init="
						+ _init,
				async : false,
				success : function(data) {
					$(target).append(data);
				}
			});
		this.pageCallBack();
		setPageHeight();
    }
};

initial = function(comp,obj,paging,target){
	var total = $(comp).val();
	if(total<=10){
		$('#'+paging).hide();
	}else{
		$('#'+paging).show();
	}
	laypage({
		cont : paging,
		pages : Math.ceil(total / 10),
		curr : 1,
		jump : function(e) {
			if (obj.curr * 1 != e.curr * 1) {
				obj.curr = e.curr;
				obj.paging(0,target);
			}
		}
	});
}
