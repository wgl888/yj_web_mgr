<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/user/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }用户</h3>
                <h5>配置后台管理员账号。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>1，登陆账号新增后不可编辑，登陆账号不能重复。</li>
            <li>2，管理员新增完后的初始密码为：<strong style="color: red;">${defaultPassword }</strong></li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/user/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }"/>
        <input type="hidden" id="editRoleIds" value="${item.roles }"/>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>登陆账号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.loginName }" placeholder="" class="input-txt"
                           id="loginName" name="loginName" datatype="*2-16" maxlength="16"
                           placeholder="登录账号" nullmsg="登录账号不能为空" ${item.loginName == null ? '': 'readonly' }>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入用户登陆账号，保存后将不可修改。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>用户名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.userName }" placeholder="" class="input-txt"
                           id="userName" name="userName" datatype="*2-16" maxlength="16"
                           placeholder="管理员名称" nullmsg="管理员名称不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入用户姓名，2~16位字符，可由中文，英文组成。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>手机号码</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.contractTel }" placeholder="" class="input-txt"
                           id="contractTel" name="contractTel" datatype="*2-16" maxlength="11"
                           placeholder="手机号码" nullmsg="手机号码不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入正确格式的手机号码。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>超级管理员</label>
                </dt>
                <dd class="opt">
                    <label>
                        <input name="isSuper" ${item.isSuper=='Y'?'checked':''} value="Y" type="radio" class="ace" />
                        <span class="lbl">是</span>
                    </label>
                    <label>
                        <input name="isSuper" value="N" ${(item.isSuper=='N'||item==null)?'checked':''} type="radio" class="ace" />
                        <span class="lbl">否</span>
                    </label>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        超级管理员可以操作所有的功能，非超级管理员需要绑定角色。
                    </p>
                </dd>
            </dl>

            <dl class="row" id="dl_choose_role">
                <dt class="tit">
                    <label>选择角色</label>
                </dt>
                <dd class="opt">
                    <c:forEach items="${roles }" var="item" varStatus="status">
                        <input name="roles" id="role_${item.id }"
                               value="${item.id }"
                               type="checkbox" class="ace" />
                        <span class="lbl" style="width: 120px;"> ${item.roleName }</span>
                       <c:if test="${status.index!=0&&status.index%3==0}">
                           <br/>
                        </c:if>
                    </c:forEach>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        点击角色可以看到角色对应的权限信息。
                    </p>
                </dd>
            </dl>

            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交</button>
            </div>
        </div>
    </form>
</div>
</body>

<script type="text/javascript">
    /*限制文本框只能输入数字*/
    $("#contractTel").keyup(function(){
        $(this).val($(this).val().replace(/[^\d]/g, ""));
    });

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype:4,
            showAllError:true,
            postonce:false,
            ajaxPost:true,
            callback:function(data) {//提交后回调函数
                if(data.head.respCode==0){
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title:'提示'}, function(index){
                        location.href = "<%=request.getContextPath() %>/user/list";
                    });
                }else{
                    parent.layer.msg(data.head.respMsg, {icon: 2,time:2000});
                }
            }
        });
        if('${item.isSuper}' == 'Y'){
            $('#dl_choose_role').hide();
        }
    });

    $("[name='isSuper']").click(function(){
        var v = $(this).val();
        if('Y'==v){
            $('#dl_choose_role').hide();
        }else{
            $('#dl_choose_role').show();
        }
    });



    $(document).ready(function(){
        if('${item.id}'!=null){
            var roles = $('#editRoleIds').val();
            var array = roles.split('#');
            if(array.length>0){
                for(var i=0;i<array.length;i++){
                    $('#role_'+array[i]).attr("checked","checked");
                }
            }
        }
    });

</script>

</html>
