package cn.com.xj.plat.service;

import cn.com.xj.common.param.out.ServiceResp;

import java.util.Map;

/**
 * @author wanggl
 * @version V1.0
 * @Title: BaseInterface
 * @Package cn.com.xj.plat.service
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/6/21 15:19
 */
public interface BaseInterface {

    ServiceResp<Object> query(Map<String, Object> param);
    ServiceResp<Object> count(Map<String, Object> param);
    ServiceResp<Object> add(Map<String, Object> param);
    ServiceResp<Object> edit(Map<String, Object> param);

}
