/**
 * 富文本接口
 */
var RichTextManager = {
	'server': '',
	'url': 'richText/select',
   	'richTextId': '',
   	'richTextTitle': '',
   	'create' : function(){
   		return this;
    },
    'select': function(){
    	var server = this.server;
    	if($("#richText-modal").length<=0){
    		$("body").append('<div id="richText-modal" class="modal fade" tabindex="-1"></div>');
    	}
    	$.ajax({
			type : "post",
			url : server + this.url,
			async : false,
			success : function(data) {
				$("#richText-modal").empty();
				$("#richText-modal").append(data);
				$("#contextPath").val(server);
				$("#richText-modal").modal("show");
			}
		});
    },   
   	'callBack': function(){
   		
   	},
   	'show': function(){
   		var obj = this;
   		this.select();
   		$("#selectRichTextButton").on("click", function(){
   			var richTextId=$('input:radio[name="richTextId"]:checked'); 
   			var richTextTitle = $(richTextId).parents("tr").find("td[class=richTextTitle]");
   			obj.richTextId = $(richTextId).val();
   			obj.richTextTitle = $(richTextTitle).text().replace(/(^\s*)|(\s*$)/g, "");
   			$("#richText-modal").empty();
			$("#richText-modal").modal("hide");
   			obj.callBack();
   		});
   	}
};