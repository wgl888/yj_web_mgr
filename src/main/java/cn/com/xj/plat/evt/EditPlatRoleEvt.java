package cn.com.xj.plat.evt;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditPlatRoleEvt extends BaseEvt {

    @Field(length = 11,value = "角色ID",nullable = false)
    private Long id;
    @Field(length = 256 , value = "角色名称",nullable = true)
    private String roleName;
    @Field(length = 512 , value = "角色描述",nullable = true)
    private String roleRemark;
    @Field(length = 1024 , value = "权限ID集合",nullable = true)
    private String limits;

    public String getLimits() {
        return limits;
    }

    public void setLimits(String limits) {
        this.limits = limits;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleRemark() {
        return roleRemark;
    }

    public void setRoleRemark(String roleRemark) {
        this.roleRemark = roleRemark;
    }

}
