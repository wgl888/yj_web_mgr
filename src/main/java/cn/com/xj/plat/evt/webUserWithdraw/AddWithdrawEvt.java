package cn.com.xj.plat.evt.webUserWithdraw;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class AddWithdrawEvt extends BaseEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    @Field(value = "用户ID", length = 11, nullable = false)
    private Long webUserId;
    @Field(value = "提现金额", length = 11, nullable = false)
    private Double amount;
    @Field(value = "提现状态", length = 12, nullable = true)
    private String withdrawStatus;
    @Field(value = "提现备注说明", length = 512, nullable = true)
    private String remark;

    public Long getWebUserId() {
        return webUserId;
    }

    public void setWebUserId(Long webUserId) {
        this.webUserId = webUserId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(String withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
