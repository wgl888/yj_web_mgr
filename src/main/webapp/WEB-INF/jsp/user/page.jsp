<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td id="${item.id }">${item.id }</td>
				<td>${item.loginName }</td>
				<td>${item.userName}</td>
				<td>${item.contractTel}</td>
				<td>
					<c:if test="${item.isEnable=='Y'}">
						<span style="color:green;">启用</span>
					</c:if>
					<c:if test="${item.isEnable=='N'}">
						<span style="color:red;">禁用</span>
					</c:if>
				</td>
				<td>
					<c:if test="${item.isSuper=='Y'}">
						<span style="color:red;">是</span>
					</c:if>
					<c:if test="${item.isSuper=='N'}">
						<span>否</span>
					</c:if>
				</td>
				<td>
					<c:if test="${item.isEnable == 'Y'}">
						<button type="button" class="btn btn-xs btn-danger hide" author="/plat/user/edit"
								onclick="editState(${item.id},'N');" >禁用</button>
					</c:if>
					<c:if test="${item.isEnable == 'N'}">
						<button type="button" class="btn btn-xs btn-info hide" author="/plat/user/edit"
								onclick="editState(${item.id},'Y')">启用</button>
					</c:if>
					<button type="button" class="btn btn-xs btn-purple hide" author="/plat/user/edit"
							onclick="resetPwd(${item.id})">重置密码</button>
					<button type="button" class="btn btn-xs btn-success hide" author="/plat/user/edit"
							onclick="edit(${item.id})">编辑</button>
					<button type="button" class="btn btn-xs btn-danger hide" author="/plat/user/edit"
							onclick="del(${item.id});">删除</button>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="9">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">