package cn.com.xj.plat.evt.userBank;

import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class DelUserBankEvt implements Serializable{

    private static final long serialVersionUID = 5178392567179223292L;
    @Field(value = "ID",nullable = true)
    private Long id;
    @Field(value = "持卡人ID",nullable = true)
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
