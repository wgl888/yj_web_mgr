<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td value="${item.id }">${item.id }</td>
				<td>${item.webUserName }</td>
				<td>${item.webUserPhone }</td>
				<td><strong style="color:red;">￥${item.amount }</strong></td>
				<td>
					<jsp:useBean id="createTime" class="java.util.Date"/>
					<c:set target="${createTime}" property="time" value="${item.createTime}"/>
					<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
				</td>
				<td>
					<jsp:include page="status.jsp">
						<jsp:param name="status" value="${item.withdrawStatus }"/>
					</jsp:include>
				</td>
				<td>
					${item.auditUserName!=null?item.auditUserName:"-"}
				</td>
				<td>
					<c:if test="${item.auditTime!=null }">
						<jsp:useBean id="auditTime" class="java.util.Date"/>
						<c:set target="${auditTime}" property="time" value="${item.auditTime}"/>
						<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${auditTime}" type="both"/>
					</c:if>
					<c:if test="${item.auditTime==null }">
						-
					</c:if>
				</td>
				<td >
					<div class="btn-group">
						<c:if test="${item.withdrawStatus=='DSH'}">
							<button type="button" class="btn btn-xs btn-inverse"
									onclick="audit(${item.id});">审核</button>
						</c:if>
						<c:if test="${item.withdrawStatus=='SHBTG' || item.withdrawStatus == 'DSH'}">
							<%--<button type="button" class="btn btn-xs btn-success"--%>
									<%--onclick="edit(${item.id})">编辑</button>--%>
							<button type="button" class="btn btn-xs btn-danger"
									onclick="del(${item.id});">删除</button>
						</c:if>
						<c:if test="${item.withdrawStatus=='SHTG'}">
							<button type="button" class="btn btn-xs btn-pink"
									onclick="loan(${item.id});">放款</button>
						</c:if>
						<button type="button" class="btn btn-xs btn-warning"
								onclick="detail(${item.id})">详情</button>
					</div>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="9">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
