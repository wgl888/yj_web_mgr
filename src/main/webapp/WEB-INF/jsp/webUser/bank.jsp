<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/plat/webUser/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>添加银行卡</h3>
                <h5>运营人员为用户添加银行卡信息。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/plat/webUser/doAddBank"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="userId" name="userId" value="${userId }"/>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>银行卡类型</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" name="bankType" id="bankType" datatype="*" nullmsg="请选择银行卡类型">
                        <option value="">请选择银行卡类型..</option>
                        <option value="CXK">储蓄卡</option>
                        <option value="XYK">信用卡</option>
                    </select>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        银行卡类型分为储蓄卡和信用卡，信用卡需要输入有效期和校验码。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>所属银行</label>
                </dt>
                <dd class="opt">
                    <select class="input-txt" name="bankId" id="bankId" datatype="*" nullmsg="请选择所属银行">
                        <option value="">请选择银行..</option>
                        <c:forEach items="${banks }" var="b">
                            <option value="${b.id }">${b.bankName }</option>
                        </c:forEach>
                    </select>
                    <input type="hidden" id="bankName" name="bankName" >
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        合作银行在【合作银行管理】功能下配置。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>银行卡卡号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankCardNo }" class="input-txt"
                           id="bankCardNo" name="bankCardNo" datatype="*1-64" maxlength="64"
                           placeholder="请输入银行卡卡号" nullmsg="请输入银行卡卡号">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请核对清楚并输入正确的银行卡号。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>持卡人姓名</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${user.userAlias }" class="input-txt"
                           id="userAlias" name="userAlias" datatype="*1-12" maxlength="12"
                           placeholder="请输入持卡人姓名" nullmsg="请输入持卡人姓名">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入银行卡在银行登记的持卡人姓名。
                    </p>
                </dd>
            </dl>

            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>预留手机号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${user.loginName }" class="input-txt"
                           id="userMobile" name="userMobile" datatype="*11-11" maxlength="11"
                           placeholder="请输入持卡人姓名" nullmsg="请输入持卡人姓名">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入银行卡在银行登记的手机号码。
                    </p>
                </dd>
            </dl>

            <dl class="row" id="row_bankExpire">
                <dt class="tit">
                    <label><em>*</em>银行卡有效期</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.bankExpire }" class="input-txt"
                           id="bankExpire" name="bankExpire" maxlength="4"
                           placeholder="请输入银行卡有效期">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入信用卡正面的有效期，格式为：“月年”，例如：1221
                    </p>
                </dd>
            </dl>

            <dl class="row" id="row_cardCvn">
                <dt class="tit">
                    <label><em>*</em>银行卡校验码</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.cardCvn }" class="input-txt"
                           id="cardCvn" name="cardCvn" maxlength="3"
                           placeholder="请输入银行卡校验码">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入信用卡背面签名处的cvn码最后三位。
                    </p>
                </dd>
            </dl>

            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
</body>

<script type="text/javascript">
    /*限制文本框只能输入数字*/
    $("#contractTel").keyup(function () {
        $(this).val($(this).val().replace(/[^\d]/g, ""));
    });

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/plat/webUser/list";
                    });
                } else {
                    parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                }
            }
        });

        $('#bankId').change(function(){
            var checkText=$(this).find("option:selected").text();
            $('#bankName').val(checkText);
        });

        $('#bankType').change(function(){
            var val=$(this).find("option:selected").val();
            if('XYK'==val){
                $('#row_cardCvn').show();
                $('#row_bankExpire').show();
            }
            if('CXK'==val){
                $('#row_cardCvn').hide();
                $('#row_bankExpire').hide();
            }
        });

    });


</script>

</html>
