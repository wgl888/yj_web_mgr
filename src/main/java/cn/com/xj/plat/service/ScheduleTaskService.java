package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ScheduleTaskService extends BaseService implements BaseInterface{

    @Autowired
    private RestUtil restUtil;

    @Override
    public ServiceResp<Object> count(Map<String, Object> param) {
        return restUtil.post(SCHEDULE_TASK,"schedule/log/count", param);
    }

    @Override
    public ServiceResp<Object> query(Map<String, Object> param) {
        return restUtil.post(SCHEDULE_TASK,"schedule/log/query", param);
    }

    @Override
    public ServiceResp<Object> add(Map<String, Object> param) {
        return null;
    }

    @Override
    public ServiceResp<Object> edit(Map<String, Object> param) {
        return null;
    }

}
