<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>错误</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
</head>
<body class="jui-linghtgreen-theme">
    <div class="jui-page">
        <div class="jui-404 align-center">
            <div class="jui-404-hint">${msg==null?'哎呀,您的页面飞走啦':msg}</div>
            <a class="jui-control" onclick="backHome();" >返回大厅</a>
        </div>
    </div>
</body>
<script type="text/javascript">
    function backHome() {
       window.top.location="<%=request.getContextPath()%>/home";
    }
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
</html>