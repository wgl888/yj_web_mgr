package cn.com.xj.plat.util;


import cn.com.xj.plat.model.SessionLoginUser;

import javax.servlet.http.HttpSession;

/**
 * @author wanggl
 * @version V1.0
 * @Title: LoginUtils
 * @Package com.wgl.springboot.util
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/4/26 22:27
 */
public class LoginUtils {

    public static final String USER_SESSION_KEY = "SESSION_LOGIN_USER";

    public static String getRealName(HttpSession session){
        SessionLoginUser userBean = (SessionLoginUser)session.getAttribute(USER_SESSION_KEY);
        return userBean!=null?userBean.getUserName():"-";
    }

    public static SessionLoginUser getLoginUser(HttpSession session){
        SessionLoginUser userBean = (SessionLoginUser)session.getAttribute(USER_SESSION_KEY);
        return userBean;
    }

    public static String getLoginName(HttpSession session){
        SessionLoginUser userBean = (SessionLoginUser)session.getAttribute(USER_SESSION_KEY);
        return userBean!=null?userBean.getLoginName():"";
    }


    public static void removeLogUser(HttpSession session){
        session.removeAttribute(USER_SESSION_KEY);
    }

    public static Long getId(HttpSession session){
        SessionLoginUser userBean = (SessionLoginUser)session.getAttribute(USER_SESSION_KEY);
        return userBean!=null?userBean.getId():null;
    }

    public static boolean isLogin(HttpSession session){
        return session.getAttribute(USER_SESSION_KEY)!=null?true:false;
    }

}
