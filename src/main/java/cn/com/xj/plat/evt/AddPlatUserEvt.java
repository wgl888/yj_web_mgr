package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;
import java.util.List;

public class AddPlatUserEvt extends BaseEvt implements Serializable{

    private static final long serialVersionUID = -5538053306677507989L;

    @Field(length = 16,nullable = false,value = "登陆账号")
    private String loginName;

    @Field(length = 16,nullable = false,value = "管理员名称")
    private String userName;

    @Field(length = 128,nullable = false,value = "管理员密码")
    private String loginPassword;

    @Field(length = 11,nullable = false,value = "联系电话")
    private String contractTel;

    @Field(length = 12,nullable = false,value = "是否超级管理员")
    private String isSuper;

    @Field(length = 12,nullable = false,value = "是否启用")
    private String isEnable;

    @Field(value = "角色ID" , nullable = true)
    private List<Long> roles;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getContractTel() {
        return contractTel;
    }

    public void setContractTel(String contractTel) {
        this.contractTel = contractTel;
    }

    public String getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(String isSuper) {
        this.isSuper = isSuper;
    }

    public String getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    public List<Long> getRoles() {
        return roles;
    }

    public void setRoles(List<Long> roles) {
        this.roles = roles;
    }
}
