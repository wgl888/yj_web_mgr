package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.TreeBean;
import cn.com.xj.plat.evt.AddPlatMenuEvt;
import cn.com.xj.plat.evt.EditPlatMenuEvt;
import cn.com.xj.plat.evt.QueryPlatMenuEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.MenuService;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/menu")
public class MenuController{

    @Autowired
    private MenuService menuService;

    @RequestMapping("/list")
    public String list(Model model,HttpServletRequest request){
        return "/menu/list";
    }

    @RequestMapping("/page")
    public String page(QueryPlatMenuEvt evt,Model model){
        try{
            BaseFactory.getInstance().page(menuService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/menu/page";
    }

    /**
     * 转到新增页面
     * @param model Model
     */
    @RequestMapping("toAdd")
    public String toAdd(@RequestParam("parentId")Long parentId,HttpServletRequest request, Model model) {
        Map<String, Object> item = new HashMap<String, Object>();
        item.put("parentId",parentId);
        model.addAttribute("item",item);
        return "/menu/edit";
    }

    @RequestMapping(value = "doAdd",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddPlatMenuEvt evt,HttpSession session){
        try {
            return BaseFactory.getInstance().add(menuService,evt,session);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    /***进入菜单、按钮编辑页面*/
    @RequestMapping("/toEdit")
    public String toEdit(@RequestParam(name = "id" ,required = true) Long id,
                         Model model){
        try{
            BaseFactory.getInstance().toEdit(menuService,id,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/menu/edit";
    }

    @RequestMapping(value = "doEdit",method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditPlatMenuEvt evt){
        try {
            return BaseFactory.getInstance().doEdit(menuService,evt);
        }catch (Exception e){
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @ResponseBody
    @RequestMapping("/tree")
    public String tree(HttpServletRequest request,HttpSession session,
                     HttpServletResponse response){
        try{
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html;charset=UTF-8");
            List<TreeBean> beans = new ArrayList<TreeBean>();
            TreeBean bean = new TreeBean();
            bean.setId(-4);//跟菜单
            bean.setpId(0);
            bean.setCount(0);
            bean.setName("系统菜单");
            beans.add(bean);
            String scope = request.getParameter("scope");
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("scope",scope);
            //1、查询所有菜单、按钮
            ServiceResp<Object> resp  = menuService.tree(param);
            List<Map<String,Object>> menus=(List<Map<String,Object>>)resp.getBody();
            if(menus!=null && menus.size()>0){
                for(Map<String,Object> item : menus){
                    bean = new TreeBean();
                    bean.setCount(0);
                    bean.setId(Integer.parseInt(item.get("id").toString()));
                    if(item.get("parentId")!=null&& StringUtils.isNotBlank(item.get("parentId").toString())&&!"0".equals(item.get("parentId").toString())){
                        bean.setpId(Integer.parseInt(String.valueOf(item.get("parentId"))));
                    }else{
                        bean.setpId(-1);
                    }
                    if(item.get("menuName")!=null){
                        bean.setName(item.get("menuName").toString());
                    }
                    //角色管理中分配菜单、功能添加[功能]标识
                    if("role".endsWith(scope)){
                        if (item.get("isMenu")!=null){
                            Integer isMenu = Integer.valueOf(item.get("isMenu").toString());
                            if(isMenu==2){
                                bean.setName(item.get("menuName")+"[功能]");
                            }
                        }
                    }
                    beans.add(bean);
                }
            }
            return JSONArray.toJSONString(beans);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
