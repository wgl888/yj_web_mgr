package cn.com.xj.plat.evt.customService;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryCustomServiceEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;


    @Field(value = "ID",length = 11,nullable = true)
    private Long id;
    @Field(value = "订单编号",length = 32,nullable = true)
    private String orderNo;
    @Field(value = "联系人",length = 32,nullable = true)
    private String linkMan;
    @Field(value = "联系电话",length = 11,nullable = true)
    private String contactPhone;

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
