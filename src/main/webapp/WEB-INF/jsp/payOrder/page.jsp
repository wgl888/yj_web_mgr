<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>${item.rechargeOrderNo }</td>
				<td>${item.userName }</td>
				<td>${item.userMobile }</td>
				<td><strong style="color:red;">￥${item.amount}</strong></td>
				<td>
					<c:if test="${item.rechargeStatus == 'CLZ'}">
						<strong style="color:orange;">处理中</strong>
					</c:if>
					<c:if test="${item.rechargeStatus == 'CLCG'}">
						<strong style="color:green;">处理成功</strong>
					</c:if>
					<c:if test="${item.rechargeStatus == 'CLSB'}">
						<strong style="color:red;">处理失败</strong>
					</c:if>
				</td>
				<td>
					<c:if test="${item.payChannel == 'YLZX'}">
						银联在线
					</c:if>
					<c:if test="${item.payChannel == 'WXSM'}">
						微信扫描
					</c:if>
					<c:if test="${item.payChannel == 'ZFBSM'}">
						支付宝扫描
					</c:if>
					<c:if test="${item.payChannel == 'WXZF'}">
						微信支付
					</c:if>
					<c:if test="${item.payChannel == 'ZFBZF'}">
						支付宝
					</c:if>
					<c:if test="${item.payChannel == 'YLKJZF'}">
						银联快捷支付
					</c:if>
				</td>
				<td>
					<jsp:useBean id="createTime" class="java.util.Date"/>
					<c:set target="${createTime}" property="time" value="${item.createTime}"/>
					<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
				</td>
				<%--<td>--%>
					<%--<button type="button" class="btn btn-xs btn-warning"--%>
							<%--onclick="detail(${item.id})">详情</button>--%>
				<%--</td>--%>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="8">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
