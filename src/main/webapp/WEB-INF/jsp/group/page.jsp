<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(groups)>0 }">
		<c:forEach items="${groups}" var="item" varStatus="status">
			<tr>
				<%--<td class="center"><label class="pos-rel"> <input--%>
						<%--type="checkbox" class="ace" id="cbx${item.id}" /> <span class="lbl"></span>--%>
				<%--</label></td>--%>
				<td>${item.id }</td>
				<td>${item.groupName }</td>
				<td>${item.groupType=='1'?'用户组':'扩展用户组'}</td>
				<td style="display: none">${item.provinceName}${item.cityName}${item.areaName}</td>
				<td>
					<input onblur="javascript:setSort(${item.id},this);" type="text" data="${item.sort}" value="${item.sort}" style="width: 50px;">
				</td>
				<td>${item.creatorName }</td>
				<td>
					<c:if test="${MODeditGroup==true}">
					<button type="button" class="btn btn-xs btn-success"
						onclick="edit(${item.id},${item.parentId},${item.groupType})">编辑</button>
					</c:if>
					<c:if test="${MODdelGroup==true}">
					<button type="button" class="btn btn-xs btn-danger"
							onclick="del(${item.id});">删除</button>
					</c:if>
					<c:if test="${MODeditGroup==true}">
					<button type="button" class="btn btn-xs btn-warning"
							onclick="changeState(${item.id},${item.status =='E'?1:2},${item.groupType});">${item.status == 'E'?'禁用':'启用'}</button>
					</c:if>
					<c:if test="${MODrolemgrGroup==true}">
					<button type="button" class="btn btn-xs btn-info"
							onclick="chooseRole(${item.id});">角色分配</button>
					</c:if>
				</td>
		   </tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="6">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">