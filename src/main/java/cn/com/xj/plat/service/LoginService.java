package cn.com.xj.plat.service;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("loginService")
public class LoginService extends BaseService{

    @Autowired
    private RestUtil restUtil;
    
    public ServiceResp<Object> login(Map<String, Object> param) {
        return restUtil.post(USER_SERVICE,"plat/login/dologin", param);
    }

}
