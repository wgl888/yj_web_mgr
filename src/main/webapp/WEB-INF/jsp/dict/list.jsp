<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>角色列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
    <%--按钮权限校验--%>
    <%--<script type="text/javascript" src="${applicationScope.staticFilepath }/js/mgrRight.js"></script>--%>
    <style type="text/css">
        .tdover {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        td {
            vertical-align: middle;
        !important;
        }

        table {
            table-layout: fixed;
        }
    </style>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <!-- 查询条件 -->
    <!-- /.page-content -->
    <%--输入框要小的 就直接加jui-mini-control 这个类名style="padding: 10px"--%>
    <div class="page-content jui-mini-control" transition>
        <div class="jui-search-icon"></div>
        <!-- /.page-form start-->
        <form method="post" id="queryForm" class="form-horizontal clearfix" role="form">
            <table class="queryTable">
                <tr>
                    <td>
                        <span>表名：</span>
                        <input placeholder="输入表名" id="tableName" name="tableName"
                               class="input-width-160" type="text"/>
                    </td>
                    <td>
                        <span>列名：</span>
                        <input placeholder="输入列名" id="columnName" name="columnName"
                               class="input-width-160" type="text"/>
                    </td>
                    <td>
                        <button onclick="javascript:loadPage(1);"
                                type="button" class="btn btn-info inline-block">搜索
                        </button>
                        <button onclick="javascript:resetSearch(1);"
                                type="button" class="btn btn-orange inline-block">重置
                        </button>
                    </td>
                </tr>
            </table>
        </form>
        <!-- /.page-form end-->
    </div>

    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <div class="">
                <button onclick="javascript:add();" author="/common/dict/add"
                        type="button" class="btn btn-lightgreen inline-block">新增
                </button>
            </div>
            <table class="jui-theme-table table-hover margin-top" id="myTable">
                <thead>
                <tr>
                    <th width="60px">ID</th>
                    <th width="160px">表名</th>
                    <th width="160px">列名</th>
                    <th width="160px">字典值</th>
                    <th width="200px">值描述</th>
                    <th width="240px">备注</th>
                    <th width="80px">排序</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">

            </div>
        </div>
    </div>
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/dict/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
        setPageHeight();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    function resetSearch() {
        $('#tableName').val('');
        $('#columnName').val('');
        loadPage(1);
    }
    add = function () {
        location.href = "${pageContext.request.contextPath}/dict/toAdd";
    }
    edit = function (id) {
        location.href = '${pageContext.request.contextPath}/dict/toEdit?id=' + id;
    }
    del = function (id) {
        parent.layer.confirm('确认要删除吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/dict/doEdit",
                data: {"id": id, "status": 'D'},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        loadPage(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    setSort = function (id, ipt) {
        var newOrder = $(ipt).val().trim();
        var oldOrder = $(ipt).attr("data");
        if (newOrder==oldOrder) {
            return;
        }
        if (newOrder=="") {
            return;
        }
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath() %>/dict/doEdit",
            data: {"id": id, "sort": newOrder},
            success: function (data) {
                if (data.head.respCode == 0) {
                    parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                    reload(0);
                } else {
                    parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                }
            }
        });
    }
</script>
</body>
</html>
