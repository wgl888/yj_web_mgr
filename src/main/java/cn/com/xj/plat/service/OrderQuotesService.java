package cn.com.xj.plat.service;

import cn.com.xj.common.enums.QuotesStatus;
import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.rest.util.RestUtil;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.orderQuotes.CancelOrderQuotesEvt;
import cn.com.xj.plat.evt.orderQuotes.QueryOrderQuotesEvt;
import cn.com.xj.plat.evt.orderQuotes.QuotesOrderEvt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class OrderQuotesService extends BaseService  {

    @Autowired
    private RestUtil restUtil;


    public ServiceResp quotes(QuotesOrderEvt evt) {
        evt.setQuotesStatus(QuotesStatus.SJJIEDAN.name());
        return restUtil.post(ORDER_SERVICE, "web/order/quotes/quotes", BeanUtil.transBean2Map(evt));
    }

    public ServiceResp queryQuotes(QueryOrderQuotesEvt evt) {
        return restUtil.post(ORDER_SERVICE, "web/order/quotes/query", BeanUtil.transBean2Map(evt));
    }

    public ServiceResp<Object> cancelQuotes(CancelOrderQuotesEvt evt) {
        return null;
    }
}
