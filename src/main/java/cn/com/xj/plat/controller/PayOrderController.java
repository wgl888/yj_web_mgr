package cn.com.xj.plat.controller;

import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.payOrder.QueryPayOrderEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.PayOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/payOrder")
public class PayOrderController extends BaseController {

    @Autowired
    private PayOrderService payOrderService;

    @RequestMapping("/list")
    public String list(){
        return "/payOrder/list";
    }

    @RequestMapping("/page")
    public String page(QueryPayOrderEvt evt,Model model){
        try{
            BaseFactory.getInstance().page(payOrderService,evt,model);
        }catch(Exception e){
            e.printStackTrace();
            return "/common/error";
        }
        return "/payOrder/page";
    }

    @RequestMapping("/toDetail")
    public String toDetail(QueryPayOrderEvt evt,
                           Model model){
        try{
            if(evt.getId()==null)return "/common/error";
            BaseFactory.getInstance().toDetail(payOrderService, BeanUtil.transBean2Map(evt),model);
        }catch(Exception e){
            return "/common/error";
        }
        return "/payOrder/detail";
    }

}
