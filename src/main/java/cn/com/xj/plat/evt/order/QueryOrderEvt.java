package cn.com.xj.plat.evt.order;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryOrderEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;


    @Field(value = "ID",length = 11,nullable = true)
    private Long id;
    @Field(value = "订单发起人ID",length = 11,nullable = true)
    private String userId;
    @Field(value = "订单编号",length = 32,nullable = true)
    private String orderNo;
    @Field(value = "订单发起人姓名",length = 12,nullable = true)
    private String userName;
    @Field(value = "保单编号",length = 32,nullable = true)
    private String policyNo;
    @Field(value = "承运人姓名",length = 12,nullable = true)
    private String freighterName;
    @Field(value = "订单状态",length = 12,nullable = true)
    private String orderStatus;
    @Field(value = "订单发起人电话",length = 11,nullable = true)
    private String userMobile;
    @Field(value = "是否加载货物信息", length = 1, nullable = true)
    private String loadGoods;
    @Field(value = "是否加载抢单司机列表", length = 1, nullable = true)
    private String loadQuotes;
    @Field(value = "抢单状态", length = 12, nullable = true)
    private String quotesStatus;
    @Field(value = "是否加载售后信息", length = 1, nullable = true)
    private String loadService;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoadService() {
        return loadService;
    }

    public void setLoadService(String loadService) {
        this.loadService = loadService;
    }

    public String getLoadGoods() {
        return loadGoods;
    }

    public void setLoadGoods(String loadGoods) {
        this.loadGoods = loadGoods;
    }

    public String getLoadQuotes() {
        return loadQuotes;
    }

    public void setLoadQuotes(String loadQuotes) {
        this.loadQuotes = loadQuotes;
    }

    public String getQuotesStatus() {
        return quotesStatus;
    }

    public void setQuotesStatus(String quotesStatus) {
        this.quotesStatus = quotesStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getFreighterName() {
        return freighterName;
    }

    public void setFreighterName(String freighterName) {
        this.freighterName = freighterName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }
}
