<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/log/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>日志详细信息</h3>
                <h5>查看服务日志的详细信息，访问IP，出入参数等。</h5>
            </div>
        </div>
    </div>

    <div class="ncap-form-default">
        <dl class="row">
            <dt class="tit">
                <label>请求渠道：</label>
            </dt>
            <dd class="opt">
                ${item.channelCode}
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label>请求接口：</label>
            </dt>
            <dd class="opt">
                ${item.operaRemark}
            </dd>
        </dl>
        <dl class="row">
            <dt class="tit">
                <label>请求结果：</label>
            </dt>
            <dd class="opt">
                <c:if test="${item.isSuccess == 'Y'}">
                    <strong style="color:green;">成功</strong>
                </c:if>
                <c:if test="${item.isSuccess == 'N'}">
                    <strong style="color:red;">失败</strong>
                </c:if>
            </dd>
        </dl>

        <dl class="row">
            <dt class="tit">
                <label>请求耗时：</label>
            </dt>
            <dd class="opt">
                ${item.timeLong/1000}m
            </dd>
        </dl>

        <dl class="row" id="dl_choose_role">
            <dt class="tit">
                <label>请求时间：</label>
            </dt>
            <dd class="opt">
                <jsp:useBean id="createTime" class="java.util.Date"/>
                <c:set target="${createTime}" property="time" value="${item.createTime}"/>
                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
            </dd>
        </dl>

        <dl class="row">
            <dt class="tit">
                <label>请求资源：</label>
            </dt>
            <dd class="opt">
                ${item.className }.${item.methodName}()
            </dd>
        </dl>

        <dl class="row">
            <dt class="tit">
                <label>请求入参：</label>
            </dt>
            <dd class="opt">
                ${item.inParam }
            </dd>
        </dl>

        <dl class="row">
            <dt class="tit">
                <label>请求出参：</label>
            </dt>
            <dd class="opt">
                ${item.outParam }
            </dd>
        </dl>

    </div>
</div>
</body>
</html>
