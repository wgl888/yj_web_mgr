<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td  value="${item.id }">${item.id }</td>
				<td >${item.roleName }</td>
				<td class="tdover" title="${item.roleRemark}">${item.roleRemark}</td>
					<td >
						<div class="btn-group">
							<button type="button" class="btn btn-xs btn-success hide"
									author="/plat/role/edit"
									onclick="edit(${item.id})">编辑</button>
							<button type="button" class="btn btn-xs btn-info hide"
									author="/plat/role/delete"
							    	onclick="del(${item.id});">删除</button>
						</div>
					</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="4">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
