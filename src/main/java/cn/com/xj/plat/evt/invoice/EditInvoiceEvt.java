package cn.com.xj.plat.evt.invoice;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditInvoiceEvt extends BaseEvt {

    @Field(value = "ID",length = 11,nullable = false)
    private Long id;
    @Field(value = "公司名称",length = 128,nullable = true)
    private String companyName;
    @Field(value = "发票类型",length = 12,nullable = true)
    private String invoiceType;
    @Field(value = "地址",length = 512,nullable = true)
    private String address;
    @Field(value = "联系电话",length = 16,nullable = true)
    private String tel;
    @Field(value = "纳税人识别号",length = 18,nullable = true)
    private String itin;
    @Field(value = "开户行",length = 128,nullable = true)
    private String bankName;
    @Field(value = "银行账号",length = 128,nullable = true)
    private String bankAccount;
    @Field(value = "发票金额",nullable = true)
    private Float amount;
    @Field(value = "物流公司编码",length = 64,nullable = true)
    private String logisticeCode;
    @Field(value = "物流单号",length = 32,nullable = true)
    private String logisticeNo;
    @Field(value = "发票状态",length = 12,nullable = true)
    private String invoiceStatus;
    @Field(value = "审核备注",length = 512,nullable = true)
    private String auditRemark;
    @Field(value = "收货人姓名", length = 11, nullable = false)
    private String deliveryName;
    @Field(value = "收货人电话", length = 11, nullable = false)
    private String deliveryPhone;
    @Field(value = "收货地址", length = 512, nullable = false)
    private String deliveryAddress;
    @Field(value = "快递费",nullable = false)
    private Float logisticeCost;
    @Field(value = "审核人ID",length = 11,nullable = true)
    private Long auditUserId;
    @Field(value = "审核人姓名",length = 32,nullable = true)
    private String auditUserName;

    public Long getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(Long auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getAuditUserName() {
        return auditUserName;
    }

    public void setAuditUserName(String auditUserName) {
        this.auditUserName = auditUserName;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public String getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(String deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Float getLogisticeCost() {
        return logisticeCost;
    }

    public void setLogisticeCost(Float logisticeCost) {
        this.logisticeCost = logisticeCost;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getItin() {
        return itin;
    }

    public void setItin(String itin) {
        this.itin = itin;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getLogisticeNo() {
        return logisticeNo;
    }

    public void setLogisticeNo(String logisticeNo) {
        this.logisticeNo = logisticeNo;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getLogisticeCode() {
        return logisticeCode;
    }

    public void setLogisticeCode(String logisticeCode) {
        this.logisticeCode = logisticeCode;
    }
}
