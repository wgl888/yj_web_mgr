<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>您好，欢迎登陆</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="icon" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/favicon.png?t=es4eg">
		<%--RSA--%>
		<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/rsa/js/RSA.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/rsa/js/BigInt.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/rsa/js/Barrett.js"></script>
		<%--引入公共js、css--%>
		<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.min.js?t=es496" ></script>
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/bootstrap.css?t=es496" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/font-awesome.css?t=es496" />
		<!-- text fonts -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/ace-fonts.css?t=es496" />
		<!-- ace styles -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/ace.css?t=es496" />
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/ace-part2.css?t=es496" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/ace-rtl.css?t=es496" />
		<!--添加修改文件-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css" />
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/ace-ie.css?t=es496" />
		<![endif]-->
		<%--弹窗--%>
		<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/layer/1.9.3/layer.js"></script>

		<script type="text/javascript">
            //判断当前窗口是否有顶级窗口，如果有就让当前的窗口的地址栏发生变化
            function loadTopWindow() {
                try{
                    if (window.top != null && window.top.document.URL != document.URL) {
                        window.top.location = document.URL;
                    }
                }catch(e){}
            }
		</script>
	</head>
	<body class="jui-login" onload="loadTopWindow()">
		<div class="jui-fixed-with">
		<div class="jui-table">
			<div class="jui-table-middle">
				<div class="jui-login-container">
					<div class="center">
						<img src="<%=request.getContextPath()%>/myResources/manager/plat/assets/images/logo2.png?t=es1kw">
					</div>
					<div class="center jui-logi-pb20" style="padding-bottom: 20px; margin-top: -30px;">
						<h1 class="white jui-letter">无车承运人管理平台</h1>
						<h5 class="white">Cloud computing  are changing the enterprise</h5>
					</div>
					<%--登录表单--%>
					<form id="loginForm" method="post">
						<fieldset>
							<label class="block clearfix form-group">
								<span class="block input-icon input-icon-right">
									<input type="text" class="form-control" placeholder="请输入您的账号"
										   id="loginName" name="loginName" maxlength="16"/>
									<i class="ace-icon fa fa-user"></i>
								</span>
							</label>
							<label class="block clearfix form-group">
								<span class="block input-icon input-icon-right">
									<input type="password" class="form-control" placeholder="请输入您的密码"
										   name="loginPassword"
										   id="loginPassword" maxlength="18"/>
									<i class="ace-icon fa fa-lock"></i>
								</span>
							</label>
							<label class="block clearfix form-group">
								<div class="row">
								  <div class="col-md-8 col-sm-8 col-xs-8">
								  	<span class="block input-icon input-icon-right">
										<input id="veryCode" name="veryCode"
												type="text" class="form-control" placeholder="请输入您的验证码" />
									</span>
								  </div>
								  <div class="col-md-4 col-sm-4 col-xs-4 login-code-box" style="cursor:pointer">
								  	<img id="imgObj" mo src="${pageContext.request.contextPath}/verycode/getImgCode"
										 onClick="changeImg()">
								  </div>
								</div>
							</label>
							<input type="button" class="jui-btn" value="登录" onclick="doLogin()"/>
						</fieldset>
					</form>
				</div>
			</div>
			</div>
		</div>
		<script type="text/javascript">
			<%-- Enter 触发提交 --%>
			document.onkeydown = keyDownEnter;
			function keyDownEnter(e) {
				var currEvent = e || event;
				if (currEvent.keyCode == 13) {
					doLogin();
				}
			}
			function doLogin() {
				var accountId = $("#loginName").val();
				var password = $("#loginPassword").val();
				var veryCode = $("#veryCode").val();
				if(!accountId){
					parent.layer.msg("请输入账号", {icon: 2,time:2000});
					$('#loginName').focus();
					return;
				}
				if(password==''||!password){
					parent.layer.msg("请输入密码", {icon: 2,time:2000});
					$('#loginPassword').focus();
					return;
				}
				if(veryCode==''||!veryCode){
					parent.layer.msg("请输入验证码", {icon: 2,time:2000});
					$('#veryCode').focus();
					return;
				}
				layer.msg('登录中，请稍等', {
					icon: 16,//信息框和加载层的私有参数
					shade: 0.01,
					time:0,//自动关闭的时间 也可以通过layer.closeAll();来关闭所有弹层
				});

				$.ajax({
					url:"${pageContext.request.contextPath}/login/doLogin",
					type: "post",
					dataType: "json",
					data: $("#loginForm").serialize(),
					success: function (data) {
						layer.closeAll();
						if(data.head.respCode==0){
							window.top.location.href='${pageContext.request.contextPath}'+'/home';
						}else {
							parent.layer.msg(data.head.respMsg, {icon: 2,time:2000});
							 changeImg();//更新验证码
							$('#veryCode').val('');
						}
					},
					error: function (xhr, data, setting) {
						layer.closeAll();
						$("#loginName").val(a);//回填
						$("#loginPassword").val("");//回填
						parent.layer.msg("请求出现异常", {icon: 2,time:2000});
					}
				});
			}

			//验证码
			function changeImg(){
				var imgSrc = $("#imgObj");
				var src = imgSrc.attr("src");
				imgSrc.attr("src",chgUrl(src));
			}

			//时间戳
			//为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
			function chgUrl(url){
				var timestamp = (new Date()).valueOf();
				urlurl = url.substring(0,17);
				if((url.indexOf("&")>=0)){
					urlurl = url + "×tamp=" + timestamp;
				}else{
					urlurl = url + "?timestamp=" + timestamp;
				}
				return urlurl;
			}

		</script>
	</body>
</html>
