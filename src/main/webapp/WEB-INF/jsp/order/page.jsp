<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>
					<a style="color:blue;" href="javascript:detail(${item.id})">
							${item.orderNo }
					</a>
				</td>
				<td><span title="${item.userMobile }">${item.userName }</span></td>

				<c:if test="${queryEvt.orderStatus==''||queryEvt.orderStatus==null}">
					<td>
						${item.startTime }
					</td>
					<td>
						${item.endTime }
					</td>
					<td>
						${item.carLengthValue}/${item.carTypeName}
					</td>
					<td>
						${item.cargoTypeName}
					</td>
				</c:if>

				<c:if test="${queryEvt.orderStatus=='DZP'}">
					<td>${item.userMobile }</td>
					<td style="color: red;">￥${item.expectCost }</td>
					<td>
						${item.startTime}
					</td>
					<td>
						${item.endTime}
					</td>
				</c:if>
				<c:if test="${queryEvt.orderStatus=='SJJUDAN'}">
					<td>${item.orderQuotes[0].freighterName }</td>
					<td>
						<jsp:useBean id="updateTime" class="java.util.Date"/>
						<c:set target="${updateTime}" property="time" value="${item.orderQuotes[0].updateTime}"/>
						<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${updateTime}" type="both"/>
					</td>
					<td>${item.orderQuotes[0].rejectReason }</td>
				</c:if>
				<c:if test="${queryEvt.orderStatus=='DHZFK'}">
					<td>${item.orderQuotes[0].freighterName }</td>
					<td>
						<c:if test="${item.orderQuotes[0]!=null}">
							<jsp:useBean id="updateTime1" class="java.util.Date"/>
							<c:set target="${updateTime1}" property="time" value="${item.orderQuotes[0].updateTime}"/>
							<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${updateTime1}" type="both"/>
						</c:if>
					</td>
					<td style="color:red;">￥${item.finalCost }</td>
					<td style="color:red;">￥${item.finalCost }</td>
				</c:if>
				<c:if test="${queryEvt.orderStatus=='DFH'}">
					<td>
						<span title="${item.shippingPhone }">${item.shippingName }</span>
					</td>
					<td align="left">
						<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:180px;"
							   title='${item.shippingRegion }&nbsp;&nbsp;${item.shippingAddress}' >
							   ${item.shippingRegion }&nbsp;&nbsp;${item.shippingAddress }
						</label>
					</td>
					<td>
						<span title="${item.deliveryPhone }">${item.deliveryName } </span>
					</td>
					<td>
						<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:180px;"
							   title='${item.deliveryRegion }&nbsp;&nbsp;${item.deliveryAddress}' >
								${item.deliveryRegion }&nbsp;&nbsp;${item.deliveryAddress }
						</label>
					</td>
				</c:if>


				<c:if test="${queryEvt.orderStatus=='DSH'}">
					<td>
						<c:if test="${item.sendTime!=null}">
							<jsp:useBean id="sendTime" class="java.util.Date"/>
							<c:set target="${sendTime}" property="time" value="${item.sendTime}"/>
							<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${sendTime}" type="both"/>
						</c:if>
					</td>
					<td>
						<span title="${item.deliveryPhone }">${item.deliveryName } </span>
					</td>
					<td>
						<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:180px;"
							   title='${item.deliveryRegion }&nbsp;&nbsp;${item.deliveryAddress}' >
								${item.deliveryRegion }&nbsp;&nbsp;${item.deliveryAddress }
						</label>
					</td>
				</c:if>

				<c:if test="${queryEvt.orderStatus=='YSH'}">
					<td>${item.freighterName }</td>
					<td>${item.cogoTypeCh }</td>
					<td style="color:red;">￥${item.finalCost}</td>
					<td style="color:red;">￥${item.insurerCost}</td>
					<td>${item.driverPj=='Y'?'已评价':'未评价'}</td>
					<td>${item.userPj=='Y'?'已评价':'未评价'}</td>
				</c:if>

				<c:if test="${queryEvt.orderStatus=='SH'}">
					<td>
						<span title="${item.shippingPhone }">${item.shippingName }</span>
					</td>
					<td align="left">
						<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:180px;"
							   title='${item.shippingRegion }&nbsp;&nbsp;${item.shippingAddress}' >
								${item.shippingRegion }&nbsp;&nbsp;${item.shippingAddress }
						</label>
					</td>
					<td>
						<span title="${item.deliveryPhone }">${item.deliveryName } </span>
					</td>
					<td>
						<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:180px;"
							   title='${item.deliveryRegion }&nbsp;&nbsp;${item.deliveryAddress}' >
								${item.deliveryRegion }&nbsp;&nbsp;${item.deliveryAddress }
						</label>
					</td>
				</c:if>
				<td>
					<strong>
						<jsp:include page="status.jsp">
							<jsp:param name="status" value="${item.orderStatus }"/>
						</jsp:include>
					</strong>
				</td>
				<td >
					<div class="btn-group">
						<c:if test="${item.orderStatus == 'DJD'}">
							<button type="button" class="btn btn-xs btn-danger"
									onclick="cancleOrder(${item.id});">取消</button>
						</c:if>
						<c:if test="${item.orderStatus == 'DZP'}">
							<button type="button" class="btn btn-xs btn-info"
									onclick="assign('${item.id }');">指派</button>
							<button type="button" class="btn btn-xs btn-danger"
									onclick="cancleOrder(${item.id});">取消</button>
						</c:if>
						<c:if test="${item.orderStatus == 'SJJUDAN'}">
							<button type="button" class="btn btn-xs btn-orange"
									onclick="service('${item.orderNo}');">重新指派</button>
							<button type="button" class="btn btn-xs btn-info"
									onclick="resetOrder('${item.id}');">扔进订单池</button>
							<button type="button" class="btn btn-xs btn-danger"
									onclick="cancleOrder(${item.id});">取消</button>
						</c:if>
						<c:if test="${item.orderStatus == 'DHZFK'}">
							<button type="button" class="btn btn-xs btn-danger"
									onclick="cancleOrder(${item.id});">取消</button>
						</c:if>
						<c:if test="${item.orderStatus == 'DFH'}">
							<%--<button type="button" class="btn btn-xs btn-danger"--%>
									<%--onclick="cancleOrder(${item.id});">取消</button>--%>
						</c:if>
						<c:if test="${item.orderStatus == 'DSH'}">
							<button type="button" class="btn btn-xs btn-info"
									onclick="confirmOrder('${item.id}');">确认收货</button>
							<%--<button type="button" class="btn btn-xs btn-danger"--%>
									<%--onclick="cancleOrder(${item.id});">取消</button>--%>
						</c:if>
						<c:if test="${item.orderStatus == 'YSH'}">
							<button type="button" class="btn btn-xs btn-yellow"
									onclick="invoice('${item.orderNo}',${item.userId});">开票</button>
						</c:if>
					</div>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="12">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
