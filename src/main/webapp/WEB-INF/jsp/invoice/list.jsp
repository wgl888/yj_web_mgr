<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>发票订单列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
        <jsp:param name="dyncHeight" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
    <%--按钮权限校验--%>
    <%--<script type="text/javascript" src="${applicationScope.staticFilepath }/js/mgrRight.js"></script>--%>
    <style type="text/css">
        .tdover {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        td {
            vertical-align: middle;
        !important;
        }

        table {
            table-layout: fixed;
        }
    </style>

</head>
<body class="jui-blue-theme">
<div class="jui-page">
    <!-- 查询条件 -->
    <!-- /.page-content -->
    <%--输入框要小的 就直接加jui-mini-control 这个类名style="padding: 10px"--%>
    <div class="page-content jui-mini-control" transition>
        <div class="jui-search-icon"></div>
        <!-- /.page-form start-->
        <form method="post" id="queryForm" class="form-horizontal clearfix" invoice="form">
            <table class="queryTable">
                <tr>
                    <td>
                        <span>公司名称：</span>
                        <input type="text" class="input-width-160"
                               id="companyName" name="companyName"/>
                    </td>
                    <td>
                        <span>订单编号：</span>
                        <input type="text" class="input-width-160"
                               id="orderNo" name="orderNo"/>
                    </td>
                    <td>
                        <span>物流单号：</span>
                        <input type="text" class="input-width-160"
                               id="logisticeNo" name="logisticeNo"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>发票类型：</span>
                        <select id="invoiceType" name="invoiceType" class="input-width-160">
                            <option value="">全部</option>
                            <option value="PP">普票</option>
                            <option value="ZP">专票</option>
                        </select>
                    </td>
                    <td>
                        <span>发票状态：</span>
                        <select id="invoiceStatus" name="invoiceStatus" class="input-width-160">
                            <option value="">全部</option>
                            <option value="DSH">待审核</option>
                            <option value="SHTG">审核通过</option>
                            <option value="SHBTG">审核不通过</option>
                            <option value="DSP">待收票</option>
                            <option value="YWC">已完成</option>
                        </select>
                    </td>
                    <td>
                        <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询
                        </button>
                        <button onclick="javascript:resetSearch(1);" type="button" class="btn btn-orange inline-block">
                            重置
                        </button>
                    </td>
                </tr>
            </table>
        </form>
        <!-- /.page-form end-->
    </div>

    <div class="page-content margin-top">
        <!-- /.page-table start-->
        <div class="jui-padding-top">
            <%--<div class="">--%>
            <%--<button onclick="javascript:add();"--%>
            <%--type="button" class="btn btn-lightgreen inline-block">新增--%>
            <%--</button>--%>
            <%--</div>--%>
            <table class="jui-theme-table table-hover margin-top" id="myTable">
                <thead>
                <tr>
                    <th width="180px">订单编号</th>
                    <th width="160px">公司名称</th>
                    <th width="90px">发票类型</th>
                    <th width="90px">发票金额</th>
                    <th width="140px">物流单号</th>
                    <th width="100px">发票状态</th>
                    <th width="160px">审核时间</th>
                    <th width="120px">审核人</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;">

            </div>
        </div>
    </div>
</div>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="text/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/invoice/page';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
        setPageHeight();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    function resetSearch() {
        $('#companyName').val('');
        $('#orderNo').val('');
        $('#logisticeNo').val('');
        $("#invoiceType option:first").prop("selected", 'selected');
        $("#invoiceStatus option:first").prop("selected", 'selected');
        loadPage(1);
        
    }
    add = function () {
        location.href = "${pageContext.request.contextPath}/invoice/toAdd";
    }
    edit = function (id) {
        location.href = '${pageContext.request.contextPath}/invoice/toEdit?id=' + id;
    }
    detail = function (id) {
        location.href = '${pageContext.request.contextPath}/invoice/toDetail?id=' + id;
    }
    audit = function (id) {
        location.href = '${pageContext.request.contextPath}/invoice/toAudit?id=' + id;
    }
    over = function (id) {
        location.href = '${pageContext.request.contextPath}/invoice/toOver?id=' + id;
    }
    del = function (id) {
        parent.layer.confirm('确认要删除吗？', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "post",
                url: "<%=request.getContextPath() %>/invoice/doEdit",
                data: {"id": id, "status": 'D'},
                success: function (data) {
                    if (data.head.respCode == 0) {
                        parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                        reload(1);
                    } else {
                        parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }
    layerCallBack = function () {
        reload(0);
    }
    reload = function (init) {
        loadPage(init);
    }
</script>
<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function () {
        $(this).parent().hasClass("jui-hide") ? $(this).parent().removeClass("jui-hide") : $(this).parent().addClass("jui-hide");
    })
</script>
<script type="text/javascript">
    //    按钮权限校验
    function mgrPermissions() {
        var btns = new Array;
        $("[permiss]").each(function () {
            var btnCode = $(this).attr("permiss");
            btns.push(btnCode);
        });
        if (btns.length > 0) {
            $.ajax({
                type: "post",
                traditional: true,
                url: "<%=request.getContextPath() %>/auth/mgrRight",
                data: {"btns": btns},
                success: function (data) {
                    var json = JSON.parse(data);
                    for (var i = 0; i < btns.length; i++) {
                        var btnCode = btns[i];
                        var flag = json[btnCode]
                        $("[permiss]").each(function () {
                            var b = $(this).attr("permiss");
                            if (btnCode == b) {
                                if (flag) {
                                    $(this).show();
                                } else {
                                    $(this).hide();
                                }
                            }
                        });
                    }
                }
            });
        }
    }
</script>
<script type="text/javascript">
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
