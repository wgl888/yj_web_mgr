package cn.com.xj.plat.evt;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryWebUserCarEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = 8448265277077676941L;
    @Field(value = "用户ID", nullable = false)
    private Long userId;
    @Field(value = "手机号、车牌号", nullable = true)
    private String cond;
    @Field(value = "车源类型", nullable = true)
    private String fromType;
    @Field(value = "车辆类型-车长", nullable = true)
    private Long carLength;
    @Field(value = "车辆类型-车型", nullable = true)
    private Long carType;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCond() {
        return cond;
    }

    public void setCond(String cond) {
        this.cond = cond;
    }

    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }

    public Long getCarLength() {
        return carLength;
    }

    public void setCarLength(Long carLength) {
        this.carLength = carLength;
    }

    public Long getCarType() {
        return carType;
    }

    public void setCarType(Long carType) {
        this.carType = carType;
    }

    @Override
    public String toString() {
        return "QueryCarEvt{" +
                "cond='" + cond + '\'' +
                ", fromType=" + fromType +
                ", carLength=" + carLength +
                ", carType=" + carType +
                '}';
    }
}
