<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>用户列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true" />
        <jsp:param name="ZUIplugin" value="true" />
        <jsp:param name="cookieutil" value="true" />
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body class="jui-blue-theme">
<div class="clearfix">
    <div class="jui-page">
        <!-- /.page-content -->
        <div class="page-content">
            <div class="jui-search-icon"></div>
            <!-- /.page-form -->
            <form  method="post" id="queryForm"class="form-horizontal jui-form-justify" role="form">
                <input type="hidden" id="superId" name="superId" value="${groupId}" >
                <table class="queryTable">
                    <tr>
                        <td>
                            <span>用户名称：</span>
                            <input type="text" class="input-width-160"
                                   id="userName" name="userName"/>
                        </td>
                        <td>
                            <button onclick="javascript:loadPage(1);" type="button" class="btn btn-info inline-block">查询</button>
                            <button onclick="javascript:resetSearch(1);"type="button" class="btn btn-orange inline-block" >重置</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="page-content margin-top">
            <div class="btn-group">
                <button onclick="add();" type="button" class="btn btn-info hide" author="/plat/user/add" >新增账户</button>
            </div>
            <table class="jui-theme-table table-hover margin-top" id="myTable">
                <thead>
                <tr>
                    <th >ID</th>
                    <th >登录账号</th>
                    <th >账号名称</th>
                    <th >手机号码</th>
                    <th >状态</th>
                    <th >超级管理员</th>
                    <th >操作</th>
                </tr>
                </thead>

                <tbody id="paging_content">

                </tbody>
            </table>
            <div id="paging" style="margin:10px 0 0 0;"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /** 分页 */
    function loadPage(init){
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '<%=request.getContextPath() %>/user/page';
        paging1.paging(init,target);
        if(init*1==1){
            initial('#data_total',paging1,'paging',target);
        }
        setPageHeight();
        author();
    }

    $(document).ready(function () {
        loadPage(1);
    });

    //ID全选事件
    $("#cbx").click(function(){
        var values=$(":checkbox");
        if(this.checked==true){
            for (var i=0;i<values.length;i++){
                values[i].checked=true;
            }
        }else{
            for (var i=0;i<values.length;i++){
                values[i].checked=false;
            }
        }
    });

    function resetSearch() {
        $('#userName').val('');
        $('#loginName').val('');
        loadPage(1);
    }

    add = function(){
        var groupId = $("#superId").val();
        location.href='${pageContext.request.contextPath}/user/toAdd?groupId='+groupId;
    }
    edit = function(id){
        location.href='${pageContext.request.contextPath}/user/toEdit.htm?id='+id
    }
    chooseRole = function(id){
        layer_show('角色分配','${pageContext.request.contextPath}/user/chooseRole.htm?id='+id,'600','480',this);
    }


    resetPwd = function(id){
        parent.layer.confirm('重置后的密码默认为1qaz2wsx，确认要重置吗？',{icon: 3, title:'提示'},function(index){
            $.ajax({
                type : "post",
                url : "<%=request.getContextPath() %>/user/resetPwd",
                data : {"userId":id},
                success : function(data){
                    if(data.head.respCode == 0){
                        parent.layer.msg(data.head.respMsg, {icon: 1,time:2000});
                        reload(1);
                    }else{
                        parent.layer.msg(data.head.respMsg, {icon: 2,time:2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    del = function(id){
        parent.layer.confirm('删除后用户将无法登陆系统，确认要删除吗？',{icon: 3, title:'提示'},function(index){
            $.ajax({
                type : "post",
                url : "<%=request.getContextPath() %>/user/doEdit",
                data : {"id":id,"status":"D"},
                success : function(data){
                    if(data.head.respCode == 0){
                        parent.layer.msg(data.head.respMsg, {icon: 1,time:2000});
                        reload(1);
                    }else{
                        parent.layer.msg(data.head.respMsg, {icon: 2,time:2000});
                    }
                }
            });
            parent.layer.close(index);
        });
    }

    layerCallBack = function(){
        reload(0);
    }

    reload = function(init){
        loadPage(init);
    }

    editState = function(id,flag){
        parent.layer.confirm(flag=='Y'?'确定启用所选的用户吗':"确定禁用所选的用户吗",{icon: 3, title:'提示'},function(index){
            $.ajax({
                type : "post",
                url : "<%=request.getContextPath() %>/user/doEditNormal?id="+id+"&isEnable="+flag,
                success : function(data){
                    if(data.head.respCode == 0){
                        parent.layer.msg(data.head.respMsg, {icon: 1,time:2000});
                        reload(1);
                    }else{
                        parent.layer.msg(data.head.respMsg, {icon: 2,time:2000});
                    }
                }
            });
            parent.layer.close(index);
        });

    }
</script>

<script type="text/javascript">
    //搜索框隐藏显示
    $(".jui-search-icon").click(function(){
        $(this).parent().hasClass("jui-hide")?$(this).parent().removeClass("jui-hide"):$(this).parent().addClass("jui-hide");
    });
    $(function () {
        changeBodyClass();
    });
</script>
</body>
</html>
