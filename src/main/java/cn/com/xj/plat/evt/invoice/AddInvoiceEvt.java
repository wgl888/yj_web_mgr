package cn.com.xj.plat.evt.invoice;

import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class AddInvoiceEvt extends BaseEvt {

    @Field(value = "订单编号",length = 32,nullable = false)
    private String orderNo;
    @Field(value = "订单Id",length = 32,nullable = false)
    private Long orderId;
    @Field(value = "发票类型",length = 12,nullable = false)
    private String invoiceType;
    @Field(value = "公司名称",length = 128,nullable = false)
    private String companyName;
    @Field(value = "纳税人识别号",length = 18,nullable = false)
    private String itin;
    @Field(value = "地址",length = 512,nullable = true)
    private String address;
    @Field(value = "联系电话",length = 16,nullable = true)
    private String tel;
    @Field(value = "开户行",length = 128,nullable = true)
    private String bankName;
    @Field(value = "银行账号",length = 128,nullable = true)
    private String bankAccount;
    @Field(value = "发票金额",nullable = false)
    private Float amount;
    @Field(value = "物流单号",length = 32,nullable = true)
    private String logisticeNo;
    @Field(value = "收货人姓名", length = 11, nullable = false)
    private String deliveryName;
    @Field(value = "收货人电话", length = 11, nullable = false)
    private String deliveryPhone;
    @Field(value = "收货地址", length = 512, nullable = false)
    private String deliveryAddress;
    @Field(value = "快递费",nullable = false)
    private Float logisticeCost;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public String getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(String deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Float getLogisticeCost() {
        return logisticeCost;
    }

    public void setLogisticeCost(Float logisticeCost) {
        this.logisticeCost = logisticeCost;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getItin() {
        return itin;
    }

    public void setItin(String itin) {
        this.itin = itin;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getLogisticeNo() {
        return logisticeNo;
    }

    public void setLogisticeNo(String logisticeNo) {
        this.logisticeNo = logisticeNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
