<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>设置保险公司费率</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="ueditor" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/insurer/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>设置保险公司费率</h3>
                <h5>前台货主申请发货的时候根据保险公司对应货物类型的费率计算保险费用。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>保险公司的费率涉及到客户端金钱操作，请谨慎设置。</li>
        </ul>
    </div>

    <form class="form form-horizontal" id="form">
        <input type="hidden" value="${item.id }" id="insurerId">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>保险公司</label>
                </dt>
                <dd class="opt">
                    <strong style="font-size: 16px;color:red;">
                        ${item.insurerName }
                    </strong>
                    <span class="Validform_checktip"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>设置费率</label>
                </dt>
                <dd class="opt">
                    <c:forEach items="${cargoTypes}" var="as" varStatus="status">
                        <div style="margin-bottom: 5px;">
                            <label style="width: 80px;font-size: 12px;">
                                ${as.dictDesc }：
                            </label>
                            <input type="text" style="width:100px;" id="${as.dictValue}"
                                   onblur="javascript:setRate('${as.dictValue}',this);" />
                        </div>
                    </c:forEach>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        设置完立即生效，无需保存操作。货物类型在字段管理配置，表名为：t_cargo_type，列名为:type
                    </p>
                </dd>
            </dl>
        </div>
    </form>
</div>
<script type="text/javascript">

    setRate = function (id, ipt) {
        var newVal = $(ipt).val().trim();
        var oldVal = $(ipt).attr("data");
        if (newVal == oldVal) {
            return;
        }
        if (newVal == "") {
            return;
        }
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath() %>/insurer/doRate",
            data: {"insurerId": $('#insurerId').val(),
                    "rate": $(ipt).val().trim(),
                    "cargoType" : id },
            success: function (data) {
                if (data.head.respCode == 0) {
                    parent.layer.msg(data.head.respMsg, {icon: 1, time: 2000});
                } else {
                    parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                }
            }
        });
    }

    $(document).ready(function(){
        var insurerId = $('#insurerId').val();
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath() %>/insurer/queryRate",
            data: {"insurerId": insurerId },
            success: function (data) {
                if(data.length>0){
                    for(var i=0;i<data.length;i++){
                        var cargoType = data[i].cargoType;
                        var rate = data[i].rate;
                        $('#' + cargoType).val(rate);
                        $('#' + cargoType).attr("data",rate);
                    }
                }
            }
        });

    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>

</body>
</html>
