package cn.com.xj.plat.service;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author wanggl
 * @version V1.0
 * @Title: BaseService
 * @Package cn.com.doone.tx.cloud.user.plat.service
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/4/14 16:34
 */
public class BaseService {

    @Value("${eureka.resources.userService:}")
    public String USER_SERVICE;

    @Value("${eureka.resources.orderService:}")
    public String ORDER_SERVICE;

    @Value("${eureka.resources.outIntService:outIntService}")
    public String OUT_INT_SERVICE;

    @Value("${eureka.resources.scheduleTask}")
    public String SCHEDULE_TASK;

}
