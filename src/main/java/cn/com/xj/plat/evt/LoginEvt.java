package cn.com.xj.plat.evt;


import cn.com.xj.common.param.valid.Field;

/**
 * @author wanggl
 * @version V1.0
 * @Title: LoginEvt
 * @Package cn.com.doone.tx.cloud.user.plat.evt
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/3/16 11:06
 */
public class LoginEvt {

    /** 登陆账号 */
    @Field(nullable = false ,length = 16,value = "登陆账号")
    private String loginName;
    /** 登陆密码 */
    @Field(nullable = false , length = 18 ,value = "登陆密码")
    private String loginPassword;
    /** 验证码 */
    @Field(nullable = false , length = 8 ,value = "验证码不能为空")
    private String veryCode;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getVeryCode() {
        return veryCode;
    }

    public void setVeryCode(String veryCode) {
        this.veryCode = veryCode;
    }
}
