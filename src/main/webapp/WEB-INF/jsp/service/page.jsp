<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td>
					<a style="color:blue;" href="javascript:orderDetail('${item.orderNo }')">
						${item.orderNo }
					</a>
				</td>
				<td>${item.linkMan }</td>
				<td>${item.contactPhone}</td>
				<td align="left">
					<label style="overflow: hidden;text-overflow:ellipsis;white-space: nowrap;width:140px;"
						   title='${item.problemDesc}' >
							${item.problemDesc}
					</label>
				</td>
				<td>
					<jsp:useBean id="createTime" class="java.util.Date"/>
					<c:set target="${createTime}" property="time" value="${item.createTime}"/>
					<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${createTime}" type="both"/>
				</td>
				<td>
					<c:if test="${item.serviceStatus=='DCL' }" >
						<strong style="color:orange;">待处理</strong>
					</c:if>
					<c:if test="${item.serviceStatus=='YCL' }" >
						<strong style="color: green;">已处理</strong>
					</c:if>
				</td>
				<td>
					<c:if test="${item.replyTime!=null}">
						<jsp:useBean id="replyTime" class="java.util.Date"/>
						<c:set target="${replyTime}" property="time" value="${item.replyTime}"/>
						<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${replyTime}" type="both"/>
					</c:if>
					<c:if test="${item.replyTime==null}">
						-
					</c:if>
				</td>
				<td>${item.replyUserName!=null?item.replyUserName:'-' }</td>
				<td >
					<div class="btn-group">
						<c:if test="${item.serviceStatus=='DCL' }" >
							<button type="button" class="btn btn-xs btn-pink"
									onclick="service('${item.id}');">处理</button>
						</c:if>
						<button type="button" class="btn btn-xs btn-warning"
								onclick="detail('${item.id}')">详情</button>
					</div>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="7">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">
