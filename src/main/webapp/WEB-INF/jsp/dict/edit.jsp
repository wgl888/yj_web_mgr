<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>添加字典值</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/dict/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }字典值</h3>
                <h5>设置系统里面使用到的字典值。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>字典值用来区分各种不通的类型，状态。例如：订单状态，车辆类型等。</li>
            <li>为了保证数据的一致性，请谨慎配置，该功能一般由系统维护人员使用。</li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/dict/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>表名</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.tableName }" placeholder="" class="input-txt"
                           id="tableName" name="tableName" datatype="*1-64" maxlength="64"
                           placeholder="请输入表名" nullmsg="表名不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        表名+列表确定一组唯一的字典值，表名一般填写字典所属的真实表名。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>列名</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.columnName }" placeholder="" class="input-txt"
                           id="columnName" name="columnName" datatype="*1-64" maxlength="64"
                           placeholder="请输入列名" nullmsg="列名不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        表名+列表确定一组唯一的字典值，列名一般填写字典所属的真实表中的真实列名。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>字典描述</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="dictDesc" name="dictDesc" datatype="*1-128" maxlength="128"
                              placeholder="请输入字典描述" nullmsg="字典描述不能为空" value="${item.dictDesc}"
                              class="input-txt" />
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入值对应的描述信息，例如：字典值“Y”的中文描述是“是”
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>字典值</label>
                </dt>
                <dd class="opt">
                    <input type="text" id="dictValue" name="dictValue" datatype="*1-64" maxlength="64"
                              placeholder="请输入字典值" nullmsg="字典值不能为空" value="${item.dictValue}"
                              class="input-txt" />
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        请输入值对应的字典值，例如：中文“是”的字典值为“Y”。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>备注</label>
                </dt>
                <dd class="opt">
                    <textarea class="input-txt" rows="5" id="remark" name="remark">${item.remark}</textarea>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        如有需要，请输入字典值备注信息。
                    </p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/dict/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });

</script>
<script type="text/javascript">
    //    公共样式切换
    $(function () {
        changeBodyClass();
    });
</script>
<script type="text/javascript">

    var setting = {
        async: {
            enable: true,
            url: getUrl
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        view: {
            expandSpeed: "",
            selectedMulti: false
        },
        callback: {
            onAsyncSuccess: onAsyncSuccess,
            onAsyncError: onAsyncError,
            onCheck: onCheck
        }
    };
    $(function () {
        $.fn.zTree.init($("#tree"), setting, zNodes);
    });
    var zNodes = [];
    function getUrl(treeId, treeNode) {
        return "<%=request.getContextPath() %>/menu/tree?scope=menu";
    }
    function onAsyncSuccess(event, treeId, treeNode, msg) {
        if (!msg || msg.length == 0) {
            return;
        }
        if (msg == "-1") {
            parent.layer.msg("对不起，你传入的参数有误，请重试！", {icon: 2, time: 2000});
            return;
        }
        var zTree = $.fn.zTree.getZTreeObj("tree");
        zTree.expandAll(true);//全部展开
        var menus = $("#limits").val();
        if(menus!=null&&menus!=""){
            var node = zTree.getNodeByParam("id",-4);
            node.checked = true;
            zTree.updateNode(node);
            var menusArray = menus.split('#');
            if(menusArray.length>0){
                for(var i=0;i<menusArray.length;i++){
                    if(menusArray[i]!=''&&menusArray[i]!=null){
                        var node = zTree.getNodeByParam("id",menusArray[i]);
                        node.checked = true;
                        zTree.updateNode(node);
                    }
                }
            }
        }

//        var nodes = zTree.getNodes();
//        for (var i = 0; i < nodes.length; i++) { //设置节点展开
//            nodes[i].checked = true;
//            zTree.expandNode(nodes[i], true);//展开一级节点
//        }
//        var pnode = nodes[0];  //初始选中最高节点
//        zTree.selectNode(pnode);
    }

    function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        parent.layer.msg("异步获取数据出现异常！", {icon: 2, time: 2000});
        treeNode.icon = "";
        zTree.updateNode(treeNode);
    }

    //点击目录时调用
    function onCheck(event, treeId, treeNode, clickFlag) {
        //获取选中的节点
        var treeObj = $.fn.zTree.getZTreeObj("tree"),
                nodes = treeObj.getCheckedNodes(true),
                v = "";
        for (var i = 0; i < nodes.length; i++) {
            v += nodes[i].id + "#";
        }
        $("#limits").val(v);
    }

</script>
</body>
</html>
