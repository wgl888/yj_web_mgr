package cn.com.xj.plat.evt.customService;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

import java.util.Date;

public class EditCustomServiceEvt extends BaseEvt {

    @Field(value = "ID",length = 11,nullable = false)
    private Long id;
    @Field(value = "处理回复",length = 4000,nullable = true)
    private String replyMsg;
    @Field(value = "状态",length = 12,nullable = true)
    private String serviceStatus;
    @Field(value = "处理人ID",length = 11,nullable = true)
    private Long replyUserId;
    @Field(value = "处理人姓名",length = 32,nullable = true)
    private String replyUserName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public Long getReplyUserId() {
        return replyUserId;
    }

    public void setReplyUserId(Long replyUserId) {
        this.replyUserId = replyUserId;
    }

    public String getReplyUserName() {
        return replyUserName;
    }

    public void setReplyUserName(String replyUserName) {
        this.replyUserName = replyUserName;
    }
}
