package cn.com.xj.plat.evt.userBank;
import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryUserBankEvt extends QueryEvt implements Serializable{

    @Field(value = "ID" , length = 11,nullable = true)
    private Long id;
    @Field(value = "银行卡卡号",length = 64,nullable = true)
    private String bankCardNo;
    @Field(value = "银行卡类型",length = 12,nullable = true)
    private String bankType;
    @Field(value = "持卡人ID",length = 11,nullable = true)
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
