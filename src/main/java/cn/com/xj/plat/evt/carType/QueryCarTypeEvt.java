package cn.com.xj.plat.evt.carType;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryCarTypeEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;

    @Field(length = 11,value = "ID",nullable = true)
    private Long id;
    @Field(length = 32 , value = "名称",nullable = true)
    private String typeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
