package cn.com.xj.plat.controller;

import cn.com.xj.common.param.out.ServiceResp;
import cn.com.xj.common.service.aspect.bean.BeanUtil;
import cn.com.xj.plat.evt.carLength.AddCarLengthEvt;
import cn.com.xj.plat.evt.carLength.EditCarLengthEvt;
import cn.com.xj.plat.evt.carLength.QueryCarLengthEvt;
import cn.com.xj.plat.evt.carType.QueryCarTypeEvt;
import cn.com.xj.plat.factory.BaseFactory;
import cn.com.xj.plat.service.CarLengthService;
import cn.com.xj.plat.service.CarTypeService;
import cn.com.xj.plat.service.CarVolumeService;
import cn.com.xj.plat.service.CarWeightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/carLength")
public class CarLengthController extends BaseController {

    @Autowired
    private CarLengthService carLengthService;
    @Autowired
    private CarTypeService carTypeService;
    @Autowired
    private CarWeightService carWeightService;
    @Autowired
    private CarVolumeService carVolumeService;

    @RequestMapping("/list")
    public String list() {
        return "/carLength/list";
    }

    @RequestMapping("/page")
    public String page(QueryCarLengthEvt evt, Model model) {
        try {
            evt.setLoadCarTypes(true);
            evt.setLoadCarVolumes(true);
            evt.setLoadCarWeights(true);
            BaseFactory.getInstance().page(carLengthService, evt, model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/carLength/page";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        ServiceResp<Object> types =
                carTypeService.query(BeanUtil.transBean2Map(new QueryCarTypeEvt()));
        model.addAttribute("types", types.getBody());
        ServiceResp<Object> weights =
                carWeightService.query(BeanUtil.transBean2Map(new QueryCarTypeEvt()));
        model.addAttribute("weights", weights.getBody());
        ServiceResp<Object> volumes =
                carVolumeService.query(BeanUtil.transBean2Map(new QueryCarTypeEvt()));
        model.addAttribute("volumes", volumes.getBody());
        return "/carLength/edit";
    }

    @RequestMapping(value = "doAdd", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddCarLengthEvt evt, HttpSession session) {
        try {
            return BaseFactory.getInstance().add(carLengthService, evt, session);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryCarLengthEvt evt,
                         Model model) {
        try {
            if (evt.getId() == null) {
                return "/common/error";
            }
            ServiceResp<Object> types =
                    carTypeService.query(BeanUtil.transBean2Map(new QueryCarTypeEvt()));
            model.addAttribute("types", types.getBody());
            ServiceResp<Object> weights =
                    carWeightService.query(BeanUtil.transBean2Map(new QueryCarTypeEvt()));
            model.addAttribute("weights", weights.getBody());
            ServiceResp<Object> volumes =
                    carVolumeService.query(BeanUtil.transBean2Map(new QueryCarTypeEvt()));
            model.addAttribute("volumes", volumes.getBody());
            evt.setLoadCarTypes(true);
            evt.setLoadCarVolumes(true);
            evt.setLoadCarWeights(true);
            BaseFactory.getInstance().toEdit(carLengthService, BeanUtil.transBean2Map(evt), model);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/carLength/edit";
    }

    @RequestMapping(value = "doEdit", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditCarLengthEvt evt) {
        try {
            return BaseFactory.getInstance().doEdit(carLengthService, evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }


}
