<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/userGroup/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }用户组</h3>
                <h5>用户组用于对某些有共同特征的用户进行归类管理。</h5>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/userGroup/${item.id==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="parentId" name="parentId" value="${item.parentId }">
        <input type="hidden" id="id" name="id" value="${item.id }">

        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>用户组类型</label>
                </dt>
                <dd class="opt">
                    <select class="audit-input" name="groupType" id="groupType">
                        <option value="">请选择用户组类型...</option>
                        <c:forEach items="${groupTypes }" var="r">
                            <option value="${r.dictValue }" ${item.groupType==r.dictValue?'selected':'' }>${r.dictDesc }</option>
                        </c:forEach>
                    </select>
                    <p class="notic">
                        用户组类型在字典表配置，配置表名为t_web_user_group，列名为：groupType
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>用户组名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.groupName }" class="input-txt"
                           datatype="*" maxlength="64" nullmsg="用户组名称不能为空"
                           id="groupName" name="groupName" placeholder="请输入用户组名称">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入用户组名称，长度最大64。
                    </p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>执照编码</label>
                </dt>
                <dd class="opt">
                    <input type="text" class="input-txt" value="${item.businessLic }"
                           id="businessLic" name="businessLic"
                            placeholder="请输入营业执照编码" maxlength="128">
                    <%--<p class="notic">--%>
                        <%--!!!!!!!!!!!!!!!!!!!!!!!!!!--%>
                    <%--</p>--%>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>是否可用</label>
                </dt>
                <dd class="opt">
                    <input type="radio" ${(item.isEnable=='Y'||item.isEnable==null)?'checked':''} class="ace" name="isEnable" value="Y"/>
                    <span class="lbl">&nbsp;可用</span>
                    &nbsp;&nbsp;
                    <input type="radio" ${item.isEnable=='N'?'checked':''} class="ace" name="isEnable" value="N"/>
                    <span class="lbl">&nbsp;不可用</span>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        不可用的用户组将无法绑定用户。
                    </p>
                </dd>
            </dl>

            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">

    $(".cb-enable").click(function () {
        $("#isLocal").val("1");
    });
    $(".cb-disable").click(function () {
        $("#isLocal").val("0");
    });

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/userGroup/list";
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });


    $('input[name="isMenu"]').click(function(){
        var val = $(this).val();
        if(1*1 == val){
            $('#row_isLocal').show();
            $('#row_openType').show();
            $('#row_userGroupImg').show();
        }else{
            $('#row_isLocal').hide();
            $('#row_openType').hide();
            $('#row_userGroupImg').hide();
        }
    });

</script>
</body>
</html>
