<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>菜单列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="true"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="true"/>
        <jsp:param name="ZUIplugin" value="true"/>
        <jsp:param name="cookieutil" value="true"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body class="jui-blue-theme">
<form method="post" id="queryForm" class="form-horizontal clearfix" role="form">
    <input type="hidden" value="${queryEvt.userId }" name="userId">
</form>
<table class="jui-theme-table table-hover margin-top" style="width: 600px;"
       id="myTable">
    <thead>
    <tr>
        <th width="120px">发票类型</th>
        <th width="120px">税号</th>
        <th width="120px">公司名称</th>
        <th width="120px">联系电话</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody id="paging_content">

    </tbody>
</table>
<div id="paging" style="margin:10px 0 0 0;">

</div>
<script type="application/javascript">

    /** 分页 */
    function loadPage(init) {
        var target = '#paging_content';
        var paging1 = PagingManage.create();
        paging1.pageUri = '${pageContext.request.contextPath}/plat/webUser/invoice/choosePage';
        paging1.paging(init, target);
        if (init * 1 == 1) {
            initial('#data_total', paging1, 'paging', target);
        }
    }

    choose = function (invoiceType,companyName,itin,tel,bankName,address,bankAccount) {
        $('#invoiceType', window.parent.document).val(invoiceType);
        $('#companyName', window.parent.document).val(companyName);
        $('#itin', window.parent.document).val(itin);
        $('#address', window.parent.document).val(address);
        $('#tel', window.parent.document).val(tel);
        $('#bankName', window.parent.document).val(bankName);
        $('#bankAccount', window.parent.document).val(bankAccount);
        parent.dochange(invoiceType);
        $('#chooseFrame', window.parent.document).attr("src", "");
        $('#chooseRow', window.parent.document).hide();
    }


    $(document).ready(function () {
        loadPage(1);
    });


    $('#carLength').change(function () {
        $('#carType').html("");
        if ($('#carLength').val() == null || $('#carLength').val() == '') {
            return;
        }
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath() %>/carType/queryWithCarLength",
            data: {"carLengthId": $('#carLength').val()},
            success: function (data) {
                $('#carType').append('<option value="">请选择...</option>');
                for (var i = 0; i < data.body.length; i++) {
                    if ('${item.carType}' == data.body[i].carTypeId) {
                        var opt = '<option selected value = "' + data.body[i].carTypeId + '">' + data.body[i].typeName + '</option>';
                    } else {
                        var opt = '<option value = "' + data.body[i].carTypeId + '">' + data.body[i].typeName + '</option>';
                    }
                    $('#carType').append(opt);
                }
            }
        });
    });

</script>

</body>
</html>
