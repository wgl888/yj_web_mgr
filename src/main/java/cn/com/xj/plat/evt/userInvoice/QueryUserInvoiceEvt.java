package cn.com.xj.plat.evt.userInvoice;

import cn.com.xj.common.param.in.QueryEvt;
import cn.com.xj.common.param.valid.Field;

import java.io.Serializable;

public class QueryUserInvoiceEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -8663077839313984304L;
    @Field(length = 11, nullable = false, value = "用户ID")
    private Long userId;
    @Field(length = 11, value = "ID", nullable = true)
    private Long id;
    @Field(value = "发票类型 PP - 普票 ZP - 专票", length = 12, nullable = true)
    private String invoiceType;

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "QueryUserInvoiceEvt{" +
                "userId=" + userId +
                ", id=" + id +
                ", invoiceType='" + invoiceType + '\'' +
                '}';
    }
}
