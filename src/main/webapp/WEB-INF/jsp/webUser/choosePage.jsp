<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${fn:length(list)>0 }">
		<c:forEach items="${list}" var="item" varStatus="status">
			<tr>
				<td id="${item.id }">${item.id }</td>
				<td>${item.userAlias }</td>
				<td>${item.loginName}</td>
				<td>${item.userPhone}</td>
				<td>
					<c:if test="${item.carownerStatus=='DSQ'}">
						<span style="color:orange;">待申请</span>
					</c:if>
					<c:if test="${item.carownerStatus=='DSH'}">
						<span style="color:green;">待审核</span>
					</c:if>
					<c:if test="${item.carownerStatus=='SHTG'}">
						<span style="color:green;">审核通过</span>
					</c:if>
					<c:if test="${item.carownerStatus=='SHBTG'}">
						<span style="color:red;">审核不通过</span>
					</c:if>
				</td>
				<td>
					<c:if test="${item.status=='E'}">
						<span style="color:green;">在用</span>
					</c:if>
					<c:if test="${item.status=='T'}">
						<span style="color:red;">已禁用</span>
					</c:if>
				</td>
				<td>
					<button type="button" class="btn btn-xs btn-info"
							onclick="choose(${item.id},'${item.userAlias}');">选择</button>
				</td>
			</tr>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="9">
				暂无符合条件的数据记录
			</td>
		</tr>
	</c:otherwise>
</c:choose>
<input type="hidden" id="data_total" value="${total} ">