<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="height: 800px;">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/plat/webUser/list" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>用户审核(认证|货主|车主)</h3>
                <h5>审核用户提交的身份或车辆信息，审核通过后的用户才能发货或接单。</h5>
            </div>
        </div>
    </div>

    <div class="explanation" id="explanation">
        <div class="title" id="checkZoom">
            <i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            <span id="explanationZoom" title="收起提示"></span>
        </div>
        <ul>
            <li>用户认证审核通过后将可以发货，并可以申请成为车主。</li>
        </ul>
    </div>

    <form action="${pageContext.request.contextPath}/plat/webUser/audit"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="webUserId" value="${item.id }"/>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><strong>用户基本信息</strong></label>
                </dt>
                <dd class="opt">
                    登录账号：<strong style="color:red;" id="code_value">${item.loginName }</strong> <br>
                    姓名性别：${item.userAlias}(${item.userSex=='NAN'?'男':item.userSex=='NV'?'女':'未知'}) <br>
                    手机号码：${item.userPhone} <br>
                    身份证号：${item.userCardNo}
                </dd>
            </dl>

            <c:if test="${item.authStatus == 'DSH'}">
                <dl class="row">
                    <dt class="tit">
                        <label><strong>用户认证审核</strong></label>
                    </dt>
                    <dd class="opt">
                        <c:if test="${item.cardImg1 != null && item.cardImg1 != ''}">
                            <a href="${item.cardImg1 }" target="_blank">
                                <img src="${item.cardImg1 }" height="100px"
                                     style="border:1px solid #d4d4d4;">
                            </a>
                        </c:if>
                        <c:if test="${item.cardImg2 != null && item.cardImg2 != ''}">
                            <a href="${item.cardImg2 }" target="_blank">
                                <img src="${item.cardImg2 }" height="100px"
                                     style="border:1px solid #d4d4d4;">
                            </a>
                        </c:if>
                        <c:if test="${item.cardImg3 != null && item.cardImg3 != ''}">
                            <a href="${item.cardImg3 }" target="_blank">
                                <img src="${item.cardImg3 }" height="100px"
                                     style="border:1px solid #d4d4d4;"></a>
                        </c:if>
                        <p class="notic">
                            上方的图片为用户上传的身份证正反面照片和手持身份证照片，点击可以查看大图。
                        </p>
                    </dd>
                </dl>
            </c:if>

            <dl class="row" style="height: 50px;">
                <dt class="tit">
                    <label><em>*</em>是否通过</label>
                </dt>
                <dd class="opt">
                    <input type="radio" checked class="ace" name="auditStatus" value="SHTG"/>
                    <span class="lbl">&nbsp;通过</span>
                    &nbsp;&nbsp;
                    <input type="radio" class="ace" name="auditStatus" value="SHBTG"/>
                    <span class="lbl">&nbsp;不通过</span>
                </dd>
            </dl>

            <dl class="row" id="dl_auditDesc" style="display: none;">
                <dt class="tit">
                    <label>审核备注</label>
                </dt>
                <dd class="opt">
                    <select class="audit-input" name="auditRemark" id="auditRemark">
                        <option value="">请选择审核不通过原因...</option>
                        <c:forEach items="${auditRemarks }" var="r">
                            <option value="${r.dictDesc}">${r.dictDesc }</option>
                        </c:forEach>
                    </select>
                </dd>
            </dl>

            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
</body>

<script type="text/javascript">
    /*限制文本框只能输入数字*/
    $("#contractTel").keyup(function () {
        $(this).val($(this).val().replace(/[^\d]/g, ""));
    });

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    parent.layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        location.href = "<%=request.getContextPath() %>/plat/webUser/list";
                    });
                } else {
                    parent.layer.msg(data.head.respMsg, {icon: 2, time: 2000});
                }
            }
        });

    });

    $("[name='isSuper']").click(function () {
        var v = $(this).val();
        if ('Y' == v) {
            $('#dl_choose_role').hide();
        } else {
            $('#dl_choose_role').show();
        }
    });


    $('input[name="auditStatus"]').click(function(){
        var val = $(this).val();
        if('SHTG' == val){
            $('#dl_auditDesc').hide();
        }else{
            $('#dl_auditDesc').show();
        }
    });

    $(document).ready(function () {
        if ('${item.id}' != null) {
            var roles = $('#editRoleIds').val();
            var array = roles.split('#');
            if (array.length > 0) {
                for (var i = 0; i < array.length; i++) {
                    $('#role_' + array[i]).attr("checked", "checked");
                }
            }
        }
    });

</script>

</html>
