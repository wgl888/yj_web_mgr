package cn.com.xj.plat.evt.insurerRate;


import cn.com.xj.common.param.in.BaseEvt;
import cn.com.xj.common.param.valid.Field;

public class EditInsurerRateEvt extends BaseEvt {

    @Field(length = 11,value = "ID",nullable = false)
    private Long id;
    @Field(value = "费率",length = 11,nullable = false)
    private Float rate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
}
