<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <%--引入公共js、css--%>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="javascript:close_layer();" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>查看用户详情</h3>
                <h5>查看用户基本信息，余额信息，认证信息等资料信息。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/plat/webUser/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }"/>
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label>用户基本信息</label>
                </dt>
                <dd class="opt">
                    登录账号：<strong style="color:red;" id="code_value">${item.loginName }</strong> <br>
                    用户编号：${item.userNo} <br>
                    姓名性别：${item.userAlias}(${item.userSex=='NAN'?'男':item.userSex=='NV'?'女':'未知'}) <br>
                    手机号码：${item.userPhone} <br>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>用户身份信息</label>
                </dt>
                <dd class="opt">
                    真实姓名：${item.userAlias}<br>
                    身份证号：${item.userCardNo!=null?item.userCardNo:"-"}<br>
                    　有效期：${item.userCardPeriod}<br>
                    　归属地：${item.userCardAddress }
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>用户认证信息</label>
                </dt>
                <dd class="opt">
                    认证状态：
                    <c:if test="${item.authStatus=='DSQ'}">
                        <strong style="color:orange;font-size: 16px;">待申请</strong>
                    </c:if>
                    <c:if test="${item.authStatus=='DSH'}">
                        <strong style="color:green;font-size: 16px;">待审核</strong>
                    </c:if>
                    <c:if test="${item.authStatus=='SHTG'}">
                        <strong style="color:green;font-size: 16px;">审核通过</strong>
                    </c:if>
                    <c:if test="${item.authStatus=='SHBTG'}">
                        <strong style="color:red;font-size: 16px;">审核不通过</strong>
                    </c:if>
                    <br>
                    <c:if test="${item.cardImg1 != null && item.cardImg1 != ''}">
                        <a href="${item.cardImg1 }" target="_blank">
                            <img src="${item.cardImg1 }" height="60px"
                                 style="border:1px solid #d4d4d4;">
                        </a>
                    </c:if>
                    <c:if test="${item.cardImg2 != null && item.cardImg2 != ''}">
                        <a href="${item.cardImg2 }" target="_blank">
                            <img src="${item.cardImg2 }" height="60px"
                                 style="border:1px solid #d4d4d4;">
                        </a>
                    </c:if>
                    <c:if test="${item.cardImg3 != null && item.cardImg3 != ''}">
                        <a href="${item.cardImg3 }" target="_blank">
                            <img src="${item.cardImg3 }" height="60px"
                                 style="border:1px solid #d4d4d4;"></a>
                    </c:if>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        用户申请认证的时候上传的身份证正反面以及手持身份证照片，点击查看大图。 </p>
                </dd>
            </dl>
            <c:if test="${item.carownerStatus != 'DSQ'}">
                <dl class="row">
                    <dt class="tit">
                        <label>车主认证信息</label>
                    </dt>
                    <dd class="opt">
                        审核状态：
                        <c:if test="${item.webCarBean.auditStatus=='DSH'}">
                            <strong style="color:green;font-size: 16px;">待审核</strong>
                        </c:if>
                        <c:if test="${item.webCarBean.auditStatus=='SHTG'}">
                            <strong style="color:green;font-size: 16px;">审核通过</strong>
                        </c:if>
                        <c:if test="${item.webCarBean.auditStatus=='SHBTG'}">
                            <strong style="color:red;font-size: 16px;">审核不通过</strong>
                        </c:if><br>
                        所属用户：${item.webCarBean.userAlias } <br>
                        车牌号码：${item.webCarBean.plateFullNo } <br>
                        车辆类型：${item.webCarBean.carTypeName } <br>
                        车辆长度：${item.webCarBean.carLengthValue }米 <br>
                        车牌颜色：${item.webCarBean.plateColorRemark } <br>
                        车险到期日：${item.webCarBean.formatAutoInsuranceDate } <br>
                        年检到期日：${item.webCarBean.formatAnunalSurveyDate }<br>
                        <c:if test="${item.webCarBean.driverLicenseImg != null
                                            && item.webCarBean.driverLicenseImg != ''}">
                            <a target="_blank" href="${item.webCarBean.driverLicenseImg }">
                                <img height="60px" id="driverLicenseImg_img" style="border:1px solid #d4d4d4;"
                                     src='${item.webCarBean.driverLicenseImg }'>
                            </a>
                        </c:if>
                        <c:if test="${item.webCarBean.drivingLicenseImg != null
                                            && item.webCarBean.drivingLicenseImg != ''}">
                            <a target="_blank" href="${item.webCarBean.drivingLicenseImg }">
                                <img height="60px" id="drivingLicenseImg_img" style="border:1px solid #d4d4d4;"
                                     src="${item.webCarBean.drivingLicenseImg }">
                            </a>
                        </c:if>
                        <c:if test="${item.webCarBean.autoInsuranceImg != null
                                                    && item.webCarBean.autoInsuranceImg != ''}">
                            <a target="_blank" href="${item.webCarBean.autoInsuranceImg }">
                                <img height="60px" id="autoInsuranceImg_img" style="border:1px solid #d4d4d4;"
                                     src="${item.webCarBean.autoInsuranceImg }">
                            </a>
                        </c:if>
                        <br>
                        <c:if test="${item.webCarBean.anunalSurveyImg != null
                                                    && item.webCarBean.anunalSurveyImg != ''}">
                            <a target="_blank" href="${item.webCarBean.anunalSurveyImg }">
                                <img height="60px" id="anunalSurveyImg_img" style="border:1px solid #d4d4d4;margin-top: 8px;"
                                     src="${item.webCarBean.anunalSurveyImg }">
                            </a>
                        </c:if>

                        <c:if test="${item.webCarBean.plateImg != null
                                                    && item.webCarBean.plateImg != ''}">
                            <a target="_blank" href="${item.webCarBean.plateImg }">
                                <img height="60px" id="plateImg_img" style="border:1px solid #d4d4d4;margin-top: 8px;"
                                     src="${item.webCarBean.plateImg }">
                            </a>
                        </c:if>

                        <c:if test="${item.webCarBean.carImg != null
                                                    && item.webCarBean.carImg != ''}">
                            <a target="_blank" href="${item.webCarBean.carImg }">
                                <img height="60px" id="carImg_img" style="border:1px solid #d4d4d4;margin-top: 8px;"
                                     src="${item.webCarBean.carImg }">
                            </a>
                        </c:if>
                        <p class="notic">
                            点击查看大图。 </p>
                    </dd>
                </dl>

            </c:if>

            <c:choose>
                <c:when test="${fn:length(item.webUserBanks)>0 }">
                    <dl class="row">
                        <dt class="tit">
                            <label>银行卡信息</label>
                        </dt>
                        <dd class="opt">
                            <c:forEach items="${item.webUserBanks }" var="item" varStatus="status">
                                [银行名称：<strong>${item.bankName }</strong>]/
                                [卡片类型：<strong>${item.bankType=='XYK'?'信用卡':'储蓄卡'}</strong>]/
                                [银行卡号：<strong>${item.bankCardNo}</strong>]/
                                [持卡人：<strong>${item.userAlias}</strong>]<br>
                            </c:forEach>
                        </dd>
                    </dl>
                </c:when>
            </c:choose>

            <c:choose>
                <c:when test="${fn:length(item.webUserInvoice)>0 }">
                    <dl class="row">
                        <dt class="tit">
                            <label>常用发票信息</label>
                        </dt>
                        <dd class="opt">
                            <c:forEach items="${item.webUserInvoice }" var="item" varStatus="status">
                                发票类型：<strong>${item.invoiceType == 'PP' ? '普票' : '专票'}</strong><br>
                                公司名称：<strong>${item.companyName }</strong><br>
                                税　　号：<strong>${item.itin }</strong><br>
                                <c:if test="${item.invoiceType == 'ZP'}">
                                    公司地址：<strong>${item.address }</strong><br>
                                    公司电话：<strong>${item.tel }</strong><br>
                                    开户银行：<strong>${item.bankName }</strong><br>
                                    银行账号：<strong>${item.bankAccount }</strong><br>
                                </c:if>
                                <br>
                            </c:forEach>
                        </dd>
                    </dl>
                </c:when>
            </c:choose>
        </div>
    </form>
</div>
</body>
<script type="application/javascript">

</script>
</html>
